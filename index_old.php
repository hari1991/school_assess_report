<?php
ob_start();
ini_set( "display_errors", 0);
include('../db.php');
session_start();
$act=$_GET['act'];
$adminid=$_SESSION['aid'];

if($act=='logout')
{
	echo "<script>window.location='index.php'</script>";
}

?>

<!DOCTYPE html>
<html lang="en">
    <head>
		<meta charset="utf-8">
		<title>SkillAngels -SuperBrains,Online Brain Games ,Edsix Brain Lab,Cognitive Skills Development, Edutainment ,Competition for Students</title>
		<meta name="keywords" content="SkillAngels, SuperBrains,Online Brain Games ,Edsix Brain Lab,Cognitive Skills Development, Educational Digital Games,Life Skill games,Brain Games for Kids,Brain skill Gaming Competition, Brain Games Free Download" />
		<meta name="description" content="SkillAngels, An IIT Madras RTBI incubated company, Skillangels was conceptualized with the vision of improving cognitive skills and performance for students and adults through next generation brain enhancement programs.">
		<meta name="author" content="kraftpanda.com">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Libs CSS -->
		<link rel="stylesheet" href="newcss/bootstrap.css">
		<link rel="stylesheet" href="newcss/fonts/font-awesome/css/font-awesome.css">
		<link rel="stylesheet" href="vendor/flexslider/flexslider.css" media="screen" />
		<link rel="stylesheet" href="vendor/fancybox/jquery.fancybox.css" media="screen" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="newcss/theme.css">
		<link rel="stylesheet" href="newcss/theme-elements.css">

		
		<!-- Current Page Styles -->
		<link rel="stylesheet" href="vendor/revolution-slider/css/settings.css" media="screen" />
		<link rel="stylesheet" href="vendor/revolution-slider/css/captions.css" media="screen" />
		<link rel="stylesheet" href="vendor/circle-flip-slideshow/css/component.css" media="screen" />
		
		<!-- Custom CSS -->
		<link rel="stylesheet" href="newcss/custom.css">

		<!-- Skin -->
		<link rel="stylesheet" href="newcss/skins/blue.css">

		<!-- Responsive CSS -->
		<link rel="stylesheet" href="newcss/bootstrap-responsive.css" />
		<link rel="stylesheet" href="newcss/theme-responsive.css" />

		<!-- Favicons -->
		<link rel="icon" type="image/png"  href="img/favicon.png">

		<!-- Head Libs -->
		<script src="vendor/modernizr.js"></script>
		
		<script type="text/javascript" src="promo/popup.js"></script>
		<link rel="stylesheet" type="text/css" href="promo/style.css" />
		<script type="text/javascript" src="newjs/jquery-1.9.0.min.js"></script>
		<script type="text/javascript" src="newjs/jquery.fancybox.js"></script>
		<link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css" media="screen" />


		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="vendor/jquery.js"><\/script>')</script>
		<script src="vendor/jquery.easing.js"></script>
		<script src="vendor/jquery.cookie.js"></script>
		<!--[if IE]>
			<link rel="stylesheet" href="css/ie.css">
		<![endif]-->

		<!--[if lte IE 8]>
			<script src="vendor/respond.js"></script>
		<![endif]-->
	 	<script src="newjs/jquery.min.js"></script>
		<style type="text/css">
			.cnt_min { min-height: 480px; }
			.loginright { float: right; }
			.fancy-intro-head { color: #0E91A1;font-family: 'Gochi Hand',cursive; font-size: 28px;text-align: left;
				}
				.menu {
					font-family: 'Gochi Hand',cursive;
					text-align: center;
					display: block;
					line-height: 30px;
					padding: 5px 0px;
					color: white;
					}
				.menu  a:hover,.menu  a:focus { background-color:  }
				.menu a{color: white;
		    font-family: 'Gochi Hand',cursive;
		    font-size: 16px;
		    font-style: italic;
		    line-height: 24px;
		    margin: 0 2px 0;
		    padding-top: 1px 8px;}
		    .current { background-color: rgba(255, 255, 255, 0.3); }
		    
		    div.login { background-color: rgba(255, 255, 255, 0.3); color: white;
		    font-family: 'Gochi Hand',cursive;
		    font-size: 23px;
		    font-style: italic;
		    line-height: 24px;
		    margin: 0 2px 0;
		    padding-top: 1px 8px;}
		    div.login a{color: white; }
		    nav li.menu a { background-color: rgba(255, 255, 255, 0.3); color: white;
		    font-family: 'Gochi Hand',cursive;
		    font-size: 16px;
		    font-style: italic;
		    line-height: 24px;
		    margin: 0 2px 0;
		    padding-top: 1px 8px;}
 
		    .sub_login {padding: 3px;
			margin: 17px 0 0 0;
			background-color: #008FFE;
			color: white;
			font-size: 18px; font-family: 'Gochi Hand',cursive; padding: 5px 12px;
			border-radius: 7px; border: 0px; }
			/**/
			.fancy-intro { border: 1px solid #008FAF; padding: 18px; }
			.mandatory { color: red; }
			h2.font_bold { font-weight: bold !important; font-size: 16px !important; }
			.intro { text-align: justify; line-height: 19px;}
			h4.sub_head { color: #888; }
			
		</style>
    </head>
<body>
		<div class="body">
		<?php include('header.php');?>
		<?php include('content.php');?>
		</div>
		<div>
		<?php include('footer.php');?>
				<!-- Libs -->
		
		<!-- <script src="master/style-switcher/style.switcher.js"></script> -->
		<script src="vendor/bootstrap.js"></script>
		<script src="vendor/selectnav.js"></script>
		<script src="vendor/twitterjs/twitter.js"></script>
		<script src="vendor/revolution-slider/js/jquery.themepunch.plugins.js"></script>
		<script src="vendor/revolution-slider/js/jquery.themepunch.revolution.js"></script>
		<script src="vendor/flexslider/jquery.flexslider.js"></script>
		<script src="vendor/circle-flip-slideshow/js/jquery.flipshow.js"></script>
		<script src="vendor/fancybox/jquery.fancybox.js"></script>
		<script src="vendor/jquery.validate.js"></script>

		<script src="newjs/plugins.js"></script>

		<!-- Current Page Scripts -->
		<script src="newjs/views/view.home.js"></script>

		<!-- Theme Initializer -->
		<script src="newjs/theme.js"></script>

		<!-- Custom JS -->
		<script src="newjs/custom.js"></script>
		
		
		</div>
	</body>
