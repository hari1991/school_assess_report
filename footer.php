 <!-- Footer starts here-->

	 <div class="footer pagefooter">
    	<div class="row">
        	<div class="col-lg-6">
    			Copyright © <?php echo date('Y'); ?> Skill Angels. All rights reserved.
            </div>
            <div class="col-lg-6 footerLinks">
    			<ul>
                	<li><a href="index.php?act=term_and_service">Terms Of Services</a><span>|</span></li>
                    <li><a href="index.php?act=policies">Policies</a><span>|</span></li>
					<li><a href="index.php?act=faq">FAQ</a></li>
                </ul>
            </div>
        </div>
    </div>
	<!-- footer ends here --> 
