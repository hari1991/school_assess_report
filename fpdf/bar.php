<?php
require_once( "fpdf.php" );
class PDF extends FPDF
{
	function Cell($w, $h=0, $txt='', $border=0, $ln=0, $align='', $fill=false, $link='')
{
    $k=$this->k;
    if($this->y+$h>$this->PageBreakTrigger && !$this->InHeader && !$this->InFooter && $this->AcceptPageBreak())
    {
        $x=$this->x;
        $ws=$this->ws;
        if($ws>0)
        {
            $this->ws=0;
            $this->_out('0 Tw');
        }
        $this->AddPage($this->CurOrientation);
        $this->x=$x;
        if($ws>0)
        {
            $this->ws=$ws;
            $this->_out(sprintf('%.3F Tw',$ws*$k));
        }
    }
    if($w==0)
        $w=$this->w-$this->rMargin-$this->x;
    $s='';
    if($fill || $border==1)
    {
        if($fill)
            $op=($border==1) ? 'B' : 'f';
        else
            $op='S';
        $s=sprintf('%.2F %.2F %.2F %.2F re %s ',$this->x*$k,($this->h-$this->y)*$k,$w*$k,-$h*$k,$op);
    }
    if(is_string($border))
    {
        $x=$this->x;
        $y=$this->y;
        if(is_int(strpos($border,'L')))
            $s.=sprintf('%.2F %.2F m %.2F %.2F l S ',$x*$k,($this->h-$y)*$k,$x*$k,($this->h-($y+$h))*$k);
        if(is_int(strpos($border,'T')))
            $s.=sprintf('%.2F %.2F m %.2F %.2F l S ',$x*$k,($this->h-$y)*$k,($x+$w)*$k,($this->h-$y)*$k);
        if(is_int(strpos($border,'R')))
            $s.=sprintf('%.2F %.2F m %.2F %.2F l S ',($x+$w)*$k,($this->h-$y)*$k,($x+$w)*$k,($this->h-($y+$h))*$k);
        if(is_int(strpos($border,'B')))
            $s.=sprintf('%.2F %.2F m %.2F %.2F l S ',$x*$k,($this->h-($y+$h))*$k,($x+$w)*$k,($this->h-($y+$h))*$k);
    }
    if($txt!='')
    {
        if($align=='R')
            $dx=$w-$this->cMargin-$this->GetStringWidth($txt);
        elseif($align=='C')
            $dx=($w-$this->GetStringWidth($txt))/2;
        elseif($align=='FJ')
        {
            //Set word spacing
            $wmax=($w-2*$this->cMargin);
            $this->ws=($wmax-$this->GetStringWidth($txt))/substr_count($txt,' ');
            $this->_out(sprintf('%.3F Tw',$this->ws*$this->k));
            $dx=$this->cMargin;
        }
        else
            $dx=$this->cMargin;
        $txt=str_replace(')','\\)',str_replace('(','\\(',str_replace('\\','\\\\',$txt)));
        if($this->ColorFlag)
            $s.='q '.$this->TextColor.' ';
        $s.=sprintf('BT %.2F %.2F Td (%s) Tj ET',($this->x+$dx)*$k,($this->h-($this->y+.5*$h+.3*$this->FontSize))*$k,$txt);
        if($this->underline)
            $s.=' '.$this->_dounderline($this->x+$dx,$this->y+.5*$h+.3*$this->FontSize,$txt);
        if($this->ColorFlag)
            $s.=' Q';
        if($link)
        {
            if($align=='FJ')
                $wlink=$wmax;
            else
                $wlink=$this->GetStringWidth($txt);
            $this->Link($this->x+$dx,$this->y+.5*$h-.5*$this->FontSize,$wlink,$this->FontSize,$link);
        }
    }
    if($s)
        $this->_out($s);
    if($align=='FJ')
    {
        //Remove word spacing
        $this->_out('0 Tw');
        $this->ws=0;
    }
    $this->lasth=$h;
    if($ln>0)
    {
        $this->y+=$h;
        if($ln==1)
            $this->x=$this->lMargin;
    }
    else
        $this->x+=$w;
}

    public function Header() {
		$this->SetDrawColor(0,0,0);
		$this->Image('logo.png',10,6,60);
		$this->SetFont( 'Times', 'B', 12 );
        $this->Cell(0, 10, "EdSix Brain Lab Pvt Ltd", 0, 0, 'R');
		$this->Ln( 5 );
		$this->SetFont( 'Times', '', 10);
        $this->Cell(0, 10, "Incubated by IIT Madras' RTBI", 0, 0, 'R');
		$this->Ln( 5 );
		$this->SetFont( 'Times', '', 10);
        $this->Cell(0, 10, "Supported by IIM Ahmedabad's CIIE & Village Capital, U.S.A", 0, 0, 'R');
		$this->Line(10, 30, 210-10, 30); // 20mm from each edge
		$this->Ln( 5);
    } 
	public function Footer() {
		$this->SetDrawColor(0,0,0);
		$this->Line(10, 275, 210-10, 275); // 20mm from each edge
		$this->SetFont( 'Times', '', 10 );
		$this->SetY(-20);
        $this->Cell(0, 2, "For More Details, Log on to", 0, 0, 'C');
		$this->Ln( 5 );
		$this->SetTextColor( 9,120,218 );
		$this->Cell(0, 2, "http://schools.skillangels.com/", 0, 0, 'C');
    }
}

$textColour = array( 0, 0, 0 );
$headerColour = array( 100, 100, 100 );
$reportName = "SKILLANGELS - PERFORMANCE REPORT";
$reportNameYPos = 160;


// End configuration


/**
  Create the title page
**/

$pdf = new PDF( 'P', 'mm', 'A4' );

$pdf->SetTextColor( $textColour[0], $textColour[1], $textColour[2] );
$pdf->AddPage();
 $pdf->SetDrawColor(195,195,195);
 $pdf->SetLineWidth(0.3);

// Logo
//$pdf->Image( $logoFile, $logoXPos, $logoYPos, $logoWidth );

// Report Name


/**
  Create the page header, main heading, and intro text
**/
$pdf->Ln(4);
 $pdf->SetLineWidth(0.4);
$pdf->SetTextColor( 0,112,192 );
$pdf->SetFont( 'Times', 'B', 10 );
$pdf->Cell( 0, 8, "Kalpana M", 0, 0, 'L' );
$pdf->Cell( 0, 8, "Grade I - Section A", 0, 0, 'R' );
$pdf->Ln( 5 );
$pdf->Cell( 0, 8, "CK School of Practical Knowledge", 0, 0, 'L' );
$pdf->Ln( 7 );
$pdf->SetTextColor( $headerColour[0], $headerColour[1], $headerColour[2] );
$pdf->SetFont( 'Times', 'B', 12 );
$pdf->SetFillColor( 204,204,204 );

$pdf->Cell( 0, 7, $reportName, 0, 0, 'C',TRUE );
$pdf->Ln( 5 );
$pdf->SetFont( 'Times', 'B', 10 );
$pdf->Cell( 0, 7, "Jun-Jul-Aug - 2016", 0, 0, 'C',TRUE );

$pdf->SetTextColor( $textColour[0], $textColour[1], $textColour[2] );
$pdf->Ln( 7);
$pdf->SetFont( 'Times', '', 11 );

$pdf->Cell( 0,7, "Dear Student,",0,0,'L' );
$pdf->Ln( 6 );
$pdf->Cell( 0,7, "Congratulations for your participation in the Cognitive Skill Assessment and Training Programme. This",0,0,'FJ' );
$pdf->Ln( 5 );
$pdf->Cell( 0,7, "program aims at training the core brain skills of the participant - Problem Solving, Memory, Focus and",0,0,'FJ' );
$pdf->Ln( 5 );
$pdf->Cell( 0,7, "Attention, Visual Processing and Linguistics. This report is designed to assist you to identify strengths and",0,0,'FJ' );
$pdf->Ln( 5 );
$pdf->Cell( 0,7, "weaknesses and focus on areas requiring improvement to achieve greater standards of performance.",0,0,'FJ' );

$pdf->Ln( 7 );
$pdf->SetFont( 'Times', 'B', 10 );
$pdf->Cell( 0,7, "BSPI Comparison",0,0,'L' );
$pdf->Cell( 0,7, "Overall Performance",0,0,'R' );
$pdf->Ln( 5 );



$textColour = array( 0, 0, 0 );
$headerColour = array( 100, 100, 100 );


$rowLabels = array( "Pre Assessment BSPI ", "Current Average BSPI" );
$chartXPos = 0;
$chartYPos = 130;
$chartWidth = 100;
$chartHeight = 35;
$chartXLabel = "";
$chartYLabel = "";
$chartYStep = 20;

$chartColours = array(
                  array( 0, 0, 0 ),
                  array( 0, 0, 0 )
                );

$data = array(
          array( 50 ),
          array( 60)
        );
$xScale = count($rowLabels) / ( $chartWidth - 40 );

$maxTotal = 100;



$yScale = $maxTotal / $chartHeight;

// Compute the bar width
$barWidth = 0.1;

// Add the axes:

$pdf->SetFont( 'Arial', '', 8 );

// X axis
$pdf->Line( $chartXPos + 30, $chartYPos, $chartXPos + $chartWidth, $chartYPos );

for ( $i=0; $i < count( $rowLabels ); $i++ ) {
  $pdf->SetXY( $chartXPos + 40 +  $i / $xScale, $chartYPos );
  $pdf->Cell( $barWidth, 10, $rowLabels[$i], 0, 0, 'C' );
}

// Y axis
//$pdf->Line( $chartXPos + 30, $chartYPos, $chartXPos + 30, $chartYPos - $chartHeight - 8 );

for ( $i=0; $i <= $maxTotal; $i += $chartYStep ) {
  $pdf->SetXY( $chartXPos + 7, $chartYPos - 5 - $i / $yScale );
  $pdf->Cell( 20, 10, '' . number_format( $i ), 0, 0, 'R' );
 // $pdf->Line( $chartXPos + 28, $chartYPos - $i / $yScale, $chartXPos + 30, $chartYPos - $i / $yScale );
}


// Add the axis labels
$pdf->SetFont( 'Arial', 'B', 8 );
$pdf->SetXY( $chartWidth / 2 + 20, $chartYPos + 8 );
$pdf->Cell( 30, 10, $chartXLabel, 0, 0, 'C' );
$pdf->SetXY( $chartXPos + 7, $chartYPos - $chartHeight - 12 );
$pdf->Cell( 20, 10, $chartYLabel, 0, 0, 'R' );

// Create the bars
$xPos = $chartXPos + 40;
$bar = 0;

foreach ( $data as $dataRow ) {

  // Total up the sales figures for this product
  $totalSales = 0;
  foreach ( $dataRow as $dataCell ) $totalSales += $dataCell;

  // Create the bar
  $colourIndex = $bar % count( $chartColours );
  $pdf->SetFillColor( $chartColours[$colourIndex][0], $chartColours[$colourIndex][1], $chartColours[$colourIndex][2] );
  $pdf->Rect( $xPos, $chartYPos - ( $totalSales / $yScale ), $barWidth, $totalSales / $yScale, 'F' );
  
   $pdf->SetXY( $chartXPos + 40 +  $bar / $xScale,  $chartYPos-10 - ( $totalSales / $yScale ) );
  $pdf->Cell( $barWidth, 10, $totalSales, 0, 0, 'C' );
  
  $xPos += ( 1 / $xScale );
  
  $bar++;
}
 $pdf->SetDrawColor(115,169,218);
 $pdf->SetLineWidth(0.8);
		$pdf->Line($chartXPos + 40 , $chartYPos - ( 50 / $yScale ), $chartXPos + 40 +  1/ $xScale ,$chartYPos- ( 60 / $yScale )); // 20mm from each edge
 $pdf->SetDrawColor(195,195,195);
 $pdf->SetLineWidth(0.3);

 

$rowLabels = array( "Jun-16", "Jul-16", "Aug-16" );
$chartXPos =80;
$chartYPos = 130;
$chartWidth = 125;
$chartHeight = 35;
$chartXLabel = "";
$chartYLabel = "BSPI";
$chartYStep = 20;

$chartColours = array(
                  array( 0, 112, 192 ),
                  array( 0, 112, 192 ),
                  array( 0, 112, 192 )
                );

$data = array(
          array( 20),
          array( 34 ),
          array( 48 )
        );
$xScale = count($rowLabels) / ( $chartWidth - 40 );


$maxTotal = 100;
$yScale = $maxTotal / $chartHeight;

// Compute the bar width
$barWidth = ( 1 / $xScale ) / 1.5;

// Add the axes:

$pdf->SetFont( 'Arial', '', 8 );

// X axis
$pdf->Line( $chartXPos + 30, $chartYPos, $chartXPos + $chartWidth, $chartYPos );

for ( $i=0; $i < count( $rowLabels ); $i++ ) {
  $pdf->SetXY( $chartXPos + 40 +  $i / $xScale, $chartYPos );
  $pdf->Cell( $barWidth, 10, $rowLabels[$i], 0, 0, 'C' );
}

// Y axis
//$pdf->Line( $chartXPos + 30, $chartYPos, $chartXPos + 30, $chartYPos - $chartHeight - 8 );

for ( $i=0; $i <= $maxTotal; $i += $chartYStep ) {
  $pdf->SetXY( $chartXPos + 7, $chartYPos - 5 - $i / $yScale );
  $pdf->Cell( 20, 10, '' . number_format( $i ), 0, 0, 'R' );
 // $pdf->Line( $chartXPos + 28, $chartYPos - $i / $yScale, $chartXPos + 30, $chartYPos - $i / $yScale );
}

// Add the axis labels
$pdf->SetFont( 'Arial', 'B', 8 );
$pdf->SetXY( $chartWidth / 2 + 20, $chartYPos + 8 );
$pdf->Cell( 30, 10, $chartXLabel, 0, 0, 'C' );
$pdf->SetXY( $chartXPos + 7, $chartYPos - $chartHeight - 12 );
$pdf->Cell( 20, 10, $chartYLabel, 0, 0, 'R' );

// Create the bars
$xPos = $chartXPos + 40;
$bar = 0;

foreach ( $data as $dataRow ) {

  // Total up the sales figures for this product
  $totalSales = 0;
  foreach ( $dataRow as $dataCell ) $totalSales += $dataCell;

  // Create the bar
  $colourIndex = $bar % count( $chartColours );
  $pdf->SetFillColor( $chartColours[$colourIndex][0], $chartColours[$colourIndex][1], $chartColours[$colourIndex][2] );
  $pdf->Rect( $xPos, $chartYPos - ( $totalSales / $yScale ), $barWidth, $totalSales / $yScale, 'F' );
  
   $pdf->SetXY( $chartXPos + 40 +  $bar / $xScale,  $chartYPos-10 - ( $totalSales / $yScale ) );
  $pdf->Cell( $barWidth, 10, $totalSales, 0, 0, 'C' );
  
  $xPos += ( 1 / $xScale );
  $bar++;
}
$pdf->Ln(8);
$pdf->SetFont( 'Times', 'B', 10 );
$pdf->Cell( 0,60, "Skill Performance",0,0,'L' );
$pdf->Ln( 5 );


$rowLabels = array( "Memory", "Visual Processing", "Focus & Attention", "Problem Solving", "Linguistics" );
$chartXPos =20;
$chartYPos = 190;
$chartWidth = 160;
$chartHeight = 35;
$chartXLabel = "";
$chartYLabel = "";
$chartYStep = 20;

$chartColours = array(
                  array( 255, 0, 0 ),
                  array( 192, 0, 0 ),
                  array( 255, 192, 0 ),
                  array( 0, 176, 80 ),
                  array( 91, 155, 213 )
                );

$data = array(
          array( 45 ),
          array( 40 ),
          array( 43 ),
          array( 50 ),
          array( 30 ) 
        );
$xScale = count($rowLabels) / ( $chartWidth - 40 );

$maxTotal = 100;



$yScale = $maxTotal / $chartHeight;

// Compute the bar width
$barWidth = ( 1 / $xScale ) / 3;

// Add the axes:

$pdf->SetFont( 'Arial', '', 8 );

// X axis
$pdf->Line( $chartXPos + 30, $chartYPos, $chartXPos + $chartWidth, $chartYPos );

for ( $i=0; $i < count( $rowLabels ); $i++ ) {
  $pdf->SetXY( $chartXPos + 40 +  $i / $xScale, $chartYPos );
  $pdf->Cell( $barWidth, 10, $rowLabels[$i], 0, 0, 'C' );
}

// Y axis
//$pdf->Line( $chartXPos + 30, $chartYPos, $chartXPos + 30, $chartYPos - $chartHeight - 8 );

for ( $i=0; $i <= $maxTotal; $i += $chartYStep ) {
  $pdf->SetXY( $chartXPos + 7, $chartYPos - 5 - $i / $yScale );
  $pdf->Cell( 20, 10, '' . number_format( $i ), 0, 0, 'R' );
  //$pdf->Line( $chartXPos + 28, $chartYPos - $i / $yScale, $chartXPos + 30, $chartYPos - $i / $yScale );
}

// Add the axis labels
$pdf->SetFont( 'Arial', 'B', 8 );
$pdf->SetXY( $chartWidth / 2 + 20, $chartYPos + 8 );
$pdf->Cell( 30, 10, $chartXLabel, 0, 0, 'C' );
$pdf->SetXY( $chartXPos + 7, $chartYPos - $chartHeight - 12 );
$pdf->Cell( 20, 10, $chartYLabel, 0, 0, 'R' );

// Create the bars
$xPos = $chartXPos + 40;
$bar = 0;

foreach ( $data as $dataRow ) {

  // Total up the sales figures for this product
  $totalSales = 0;
  foreach ( $dataRow as $dataCell ) $totalSales += $dataCell;

  // Create the bar
  $colourIndex = $bar % count( $chartColours );
  $pdf->SetFillColor( $chartColours[$colourIndex][0], $chartColours[$colourIndex][1], $chartColours[$colourIndex][2] );
  $pdf->Rect( $xPos, $chartYPos - ( $totalSales / $yScale ), $barWidth, $totalSales / $yScale, 'F' );
  
   $pdf->SetXY( $chartXPos + 40 +  $bar / $xScale,  $chartYPos-10 - ( $totalSales / $yScale ) );
  $pdf->Cell( $barWidth, 10, $totalSales, 0, 0, 'C' );
  
  $xPos += ( 1 / $xScale );
  $bar++;
}



$pdf->Ln(8);
$pdf->SetFont( 'Times', 'B', 10 );
$pdf->Cell( 0,45, "Skill Angels Utilization",0,0,'L' );
$pdf->Ln( 5 );


$textColour = array( 0, 0, 0 );
$headerColour = array( 100, 100, 100 );

$rowLabels = array( "Jun-16", "Jul-16", "Aug-16" );
$chartXPos =0;
$chartYPos = 253;
$chartWidth = 130;
$chartHeight = 40;
$chartXLabel = "";
$chartYLabel = "";
$chartYStep = 2;

$chartColours = array(
                  array( 91, 155, 213 ),
                  array( 91, 155, 213 ),
                  array( 91, 155, 213 )
                );
$chartColours1 = array(
                  array( 237, 125, 49 ),
                  array( 237, 125, 49 ),
                  array( 237, 125, 49 )
				  );
$data = array(
          array(array(5,2)),
          array(array(7,3)),
          array(array(6,4))
        );

// End configuration

$xScale = count($rowLabels) / ( $chartWidth - 40 );

// Compute the Y scale

$maxTotal = 12;



$yScale = $maxTotal / $chartHeight;

// Compute the bar width
$barWidth = ( 1 / $xScale ) / 3;

// Add the axes:

$pdf->SetFont( 'Arial', '', 8 );
 
// X axis
$pdf->Line( $chartXPos + 30, $chartYPos, $chartXPos + $chartWidth, $chartYPos );

for ( $i=0; $i < count( $rowLabels ); $i++ ) {
  $pdf->SetXY( $chartXPos + 40 +5+  $i / $xScale, $chartYPos );
  $pdf->Cell( $barWidth, 10, $rowLabels[$i], 0, 0, 'C' );
}

// Y axis
//$pdf->Line( $chartXPos + 30, $chartYPos, $chartXPos + 30, $chartYPos - $chartHeight - 8 );

for ( $i=0; $i <= $maxTotal; $i += $chartYStep ) {
  $pdf->SetXY( $chartXPos + 7, $chartYPos - 5 - $i / $yScale );
  $pdf->Cell( 20, 10, '' . number_format( $i ), 0, 0, 'R' );
  //$pdf->Line( $chartXPos + 28, $chartYPos - $i / $yScale, $chartXPos + 30, $chartYPos - $i / $yScale );
}

// Add the axis labels
$pdf->SetFont( 'Arial', 'B', 8 );
$pdf->SetXY( $chartWidth / 2 + 20, $chartYPos + 8 );
$pdf->Cell( 30, 10, $chartXLabel, 0, 0, 'C' );
$pdf->SetXY( $chartXPos + 7, $chartYPos - $chartHeight - 12 );
$pdf->Cell( 20, 10, $chartYLabel, 0, 0, 'R' );

// Create the bars
$chartXPos1=$chartXPos;
$xPos = $chartXPos + 40;
$xPos1 = $xPos + $barWidth;
$yScale1=$yScale;
$barWidth1=$barWidth;
$chartYPos1=$chartYPos;
$totalSales1=$totalSales;
$bar = 0;

		  

foreach ( $data as $dataRow ) {

  // Total up the sales figures for this product
  $totalSales = 0;
  $totalSales1 = 0;
  foreach ( $dataRow as $dataCell ) {$totalSales += $dataCell[0];$totalSales1 += $dataCell[1];}
  

  // Create the bar
  $colourIndex = $bar % count( $chartColours );
  $pdf->SetFillColor( $chartColours[$colourIndex][0], $chartColours[$colourIndex][1], $chartColours[$colourIndex][2] );
  $pdf->Rect( $xPos, $chartYPos - ( $totalSales / $yScale ), $barWidth, $totalSales / $yScale, 'F' );
  $colourIndex = $bar % count( $chartColours1 );
  $pdf->SetFillColor( $chartColours1[$colourIndex][0], $chartColours1[$colourIndex][1], $chartColours1[$colourIndex][2] );
  $pdf->Rect( $xPos1, $chartYPos1 - ( $totalSales1 / $yScale1 ), $barWidth1, $totalSales1 / $yScale1, 'F' );
  
   $pdf->SetXY( $chartXPos + 40 +  $bar / $xScale,  $chartYPos-10 - ( $totalSales / $yScale ) );
  $pdf->Cell( $barWidth, 10, $totalSales, 0, 0, 'C' );
  
   $pdf->SetXY( $chartXPos1+$barWidth + 40 +  $bar / $xScale,  $chartYPos1-10 - ( $totalSales1 / $yScale1 ) );
  $pdf->Cell( $barWidth1, 10, $totalSales1, 0, 0, 'C' );
  
  $xPos += ( 1 / $xScale );
  $xPos1 += ( 1 / $xScale );
  $bar++;
}

$pdf->Ln(5);
$pdf->SetFont( 'Times', '', 10 );
  $pdf->SetFillColor( 91, 155, 213);
  
  $pdf->Cell( 175, -25, $pdf->Rect( 135, 221, 2.5, 2.5, 'F' )."                                                                                                                                                   Training Sessions Attempted", 0, 0, 'L' );
  $pdf->Ln(6);
  $pdf->SetFillColor( 237, 125, 49);
  $pdf->Cell( 175, -25, $pdf->Rect( 135, 227, 2.5, 2.5, 'F' )."                                                                                                                                                   Completed Sessions", 0, 0, 'L' );

/*$pdf->SetFont( 'Times', '', 10 );
  $pdf->SetFillColor( 237, 125, 49);
  $pdf->Rect( 135, 230, 2.5, 2.5, 'F' );
  $pdf->Cell( 165,-12, "Completed Sessions", 0, 0 );
*/

/***
  Serve the PDF
***/

$pdf->AddPage();


$pdf->Ln(4);
 $pdf->SetLineWidth(0.4);
$pdf->SetTextColor( 0,112,192 );
$pdf->SetFont( 'Times', 'B', 10 );
$pdf->Cell( 0, 8, "Kalpana M", 0, 0, 'L' );
$pdf->Cell( 0, 8, "Grade I - Section A", 0, 0, 'R' );
$pdf->Ln( 5 );
$pdf->Cell( 0, 8, "CK School of Practical Knowledge", 0, 0, 'L' );
$pdf->Ln( 7 );
$pdf->SetTextColor( $headerColour[0], $headerColour[1], $headerColour[2] );
$pdf->SetFont( 'Times', 'B', 12 );
$pdf->SetFillColor( 204,204,204 );

$pdf->Cell( 0, 7, $reportName, 0, 0, 'C',TRUE );
$pdf->Ln( 5 );
$pdf->SetFont( 'Times', 'B', 10 );
$pdf->Cell( 0, 7, "Jun-Jul-Aug - 2016", 0, 0, 'C',TRUE );

$pdf->SetTextColor( $textColour[0], $textColour[1], $textColour[2] );
$pdf->Ln( 12);


$pdf->SetFont( 'Times', 'B', 10 );
$pdf->Cell( 140,7, "                                                          Memory",0,0  );
$pdf->Cell( 100,7, "Visual Processing",0,0 );
$pdf->Ln( 8 );

$textColour = array( 0, 0, 0 );
$headerColour = array( 100, 100, 100 );

$rowLabels = array( "Jun-16", "Jul-16", "Aug-16" );
$chartXPos =0;
$chartYPos = 110;
$chartWidth = 110;
$chartHeight = 35;
$chartXLabel = "";
$chartYLabel = "";
$chartYStep = 20;

$chartColours = array(
                  array( 255, 0, 0),
                  array( 255, 0, 0),
                  array( 255, 0, 0 )
                );

$data = array(
          array( 20),
          array( 34 ),
          array( 48 )
        );
$xScale = count($rowLabels) / ( $chartWidth - 40 );


$maxTotal = 100;
$yScale = $maxTotal / $chartHeight;

// Compute the bar width
$barWidth = ( 1 / $xScale ) / 1.5;

// Add the axes:

$pdf->SetFont( 'Arial', '', 8 );

// X axis
$pdf->Line( $chartXPos + 30, $chartYPos, $chartXPos + $chartWidth, $chartYPos );

for ( $i=0; $i < count( $rowLabels ); $i++ ) {
  $pdf->SetXY( $chartXPos + 40 +  $i / $xScale, $chartYPos );
  $pdf->Cell( $barWidth, 10, $rowLabels[$i], 0, 0, 'C' );
}

// Y axis
 $pdf->SetLineWidth(0.4);
//$pdf->Line( $chartXPos + 30, $chartYPos, $chartXPos + 30, $chartYPos - $chartHeight - 8 );

for ( $i=0; $i <= $maxTotal; $i += $chartYStep ) {
  $pdf->SetXY( $chartXPos + 7, $chartYPos - 5 - $i / $yScale );
  $pdf->Cell( 20, 10, '' . number_format( $i ), 0, 0, 'R' );
  //$pdf->Line( $chartXPos + 28, $chartYPos - $i / $yScale, $chartXPos + 30, $chartYPos - $i / $yScale );
}

// Add the axis labels
$pdf->SetFont( 'Arial', 'B', 8 );
$pdf->SetXY( $chartWidth / 2 + 20, $chartYPos + 8 );
$pdf->Cell( 30, 10, $chartXLabel, 0, 0, 'C' );
$pdf->SetXY( $chartXPos + 7, $chartYPos - $chartHeight - 12 );
$pdf->Cell( 20, 10, $chartYLabel, 0, 0, 'R' );

// Create the bars
$xPos = $chartXPos + 40;
$bar = 0;

foreach ( $data as $dataRow ) {

  // Total up the sales figures for this product
  $totalSales = 0;
  foreach ( $dataRow as $dataCell ) $totalSales += $dataCell;

  // Create the bar
  $colourIndex = $bar % count( $chartColours );
  $pdf->SetFillColor( $chartColours[$colourIndex][0], $chartColours[$colourIndex][1], $chartColours[$colourIndex][2] );
  $pdf->Rect( $xPos, $chartYPos - ( $totalSales / $yScale ), $barWidth, $totalSales / $yScale, 'F' );
  
   $pdf->SetXY( $chartXPos + 40 +  $bar / $xScale,  $chartYPos-10 - ( $totalSales / $yScale ) );
  $pdf->Cell( $barWidth, 10, $totalSales, 0, 0, 'C' );
  
  $xPos += ( 1 / $xScale );
  $bar++;
}


$rowLabels = array( "Jun-16", "Jul-16", "Aug-16" );
$chartXPos =90;
$chartYPos = 110;
$chartWidth = 110;
$chartHeight = 35;
$chartXLabel = "";
$chartYLabel = "";
$chartYStep = 20;

$chartColours = array(
                  array( 192, 0, 0 ),
                  array( 192, 0, 0),
                  array( 192, 0, 0 )
                );

$data = array(
          array( 20),
          array( 34 ),
          array( 48 )
        );
$xScale = count($rowLabels) / ( $chartWidth - 40 );


$maxTotal = 100;
$yScale = $maxTotal / $chartHeight;

// Compute the bar width
$barWidth = ( 1 / $xScale ) / 1.5;

// Add the axes:

$pdf->SetFont( 'Arial', '', 8 );
 $pdf->SetDrawColor(195,195,195);
 $pdf->SetLineWidth(0.3);
// X axis
$pdf->Line( $chartXPos + 30, $chartYPos, $chartXPos + $chartWidth, $chartYPos );
 
for ( $i=0; $i < count( $rowLabels ); $i++ ) {
  $pdf->SetXY( $chartXPos + 40 +  $i / $xScale, $chartYPos );
  $pdf->Cell( $barWidth, 10, $rowLabels[$i], 0, 0, 'C' );
}

// Y axis
//$pdf->Line( $chartXPos + 30, $chartYPos, $chartXPos + 30, $chartYPos - $chartHeight - 8 );

for ( $i=0; $i <= $maxTotal; $i += $chartYStep ) {
  $pdf->SetXY( $chartXPos + 7, $chartYPos - 5 - $i / $yScale );
  $pdf->Cell( 20, 10, '' . number_format( $i ), 0, 0, 'R' );
 // $pdf->Line( $chartXPos + 28, $chartYPos - $i / $yScale, $chartXPos + 30, $chartYPos - $i / $yScale );
}

// Add the axis labels
$pdf->SetFont( 'Arial', 'B', 8 );
$pdf->SetXY( $chartWidth / 2 + 20, $chartYPos + 8 );
$pdf->Cell( 30, 10, $chartXLabel, 0, 0, 'C' );
$pdf->SetXY( $chartXPos + 7, $chartYPos - $chartHeight - 12 );
$pdf->Cell( 20, 10, $chartYLabel, 0, 0, 'R' );

// Create the bars
$xPos = $chartXPos + 40;
$bar = 0;

foreach ( $data as $dataRow ) {

  // Total up the sales figures for this product
  $totalSales = 0;
  foreach ( $dataRow as $dataCell ) $totalSales += $dataCell;

  // Create the bar
  $colourIndex = $bar % count( $chartColours );
  $pdf->SetFillColor( $chartColours[$colourIndex][0], $chartColours[$colourIndex][1], $chartColours[$colourIndex][2] );
  $pdf->Rect( $xPos, $chartYPos - ( $totalSales / $yScale ), $barWidth, $totalSales / $yScale, 'F' );
  
   $pdf->SetXY( $chartXPos + 40 +  $bar / $xScale,  $chartYPos-10 - ( $totalSales / $yScale ) );
  $pdf->Cell( $barWidth, 10, $totalSales, 0, 0, 'C' );
  
  $xPos += ( 1 / $xScale );
  $bar++;
}



$pdf->Ln(6);



$pdf->SetFont( 'Times', 'B', 10 );
$pdf->Cell( 140,80, "                                                   Focus & Attention",0,0  );
$pdf->Cell( 100,80, "Problem Solving",0,0 );
$pdf->Ln( 8 );

$textColour = array( 0, 0, 0 );
$headerColour = array( 100, 100, 100 );


$rowLabels = array( "Jun-16", "Jul-16", "Aug-16" );
$chartXPos =0;
$chartYPos = 175;
$chartWidth = 110;
$chartHeight = 35;
$chartXLabel = "";
$chartYLabel = "";
$chartYStep = 20;


$chartColours = array(
                  array( 255, 192, 0 ),
                  array( 255, 192, 0 ),
                  array( 255, 192, 0 )
                );

$data = array(
          array( 20),
          array( 34 ),
          array( 48 )
        );
$xScale = count($rowLabels) / ( $chartWidth - 40 );


$maxTotal = 100;
$yScale = $maxTotal / $chartHeight;

// Compute the bar width
$barWidth = ( 1 / $xScale ) / 1.5;

// Add the axes:

$pdf->SetFont( 'Arial', '', 8 );

// X axis
$pdf->Line( $chartXPos + 30, $chartYPos, $chartXPos + $chartWidth, $chartYPos );

for ( $i=0; $i < count( $rowLabels ); $i++ ) {
  $pdf->SetXY( $chartXPos + 40 +  $i / $xScale, $chartYPos );
  $pdf->Cell( $barWidth, 10, $rowLabels[$i], 0, 0, 'C' );
}

// Y axis
$pdf->SetDrawColor(195,195,195);
$pdf->SetLineWidth(0.3);
//$pdf->Line( $chartXPos + 30, $chartYPos, $chartXPos + 30, $chartYPos - $chartHeight - 8 );

for ( $i=0; $i <= $maxTotal; $i += $chartYStep ) {
  $pdf->SetXY( $chartXPos + 7, $chartYPos - 5 - $i / $yScale );
  $pdf->Cell( 20, 10, '' . number_format( $i ), 0, 0, 'R' );
  //$pdf->Line( $chartXPos + 28, $chartYPos - $i / $yScale, $chartXPos + 30, $chartYPos - $i / $yScale );
}

// Add the axis labels
$pdf->SetFont( 'Arial', 'B', 8 );
$pdf->SetXY( $chartWidth / 2 + 20, $chartYPos + 8 );
$pdf->Cell( 30, 10, $chartXLabel, 0, 0, 'C' );
$pdf->SetXY( $chartXPos + 7, $chartYPos - $chartHeight - 12 );
$pdf->Cell( 20, 10, $chartYLabel, 0, 0, 'R' );

// Create the bars
$xPos = $chartXPos + 40;
$bar = 0;

foreach ( $data as $dataRow ) {

  // Total up the sales figures for this product
  $totalSales = 0;
  foreach ( $dataRow as $dataCell ) $totalSales += $dataCell;

  // Create the bar
  $colourIndex = $bar % count( $chartColours );
  $pdf->SetFillColor( $chartColours[$colourIndex][0], $chartColours[$colourIndex][1], $chartColours[$colourIndex][2] );
  $pdf->Rect( $xPos, $chartYPos - ( $totalSales / $yScale ), $barWidth, $totalSales / $yScale, 'F' );
  
   $pdf->SetXY( $chartXPos + 40 +  $bar / $xScale,  $chartYPos-10 - ( $totalSales / $yScale ) );
  $pdf->Cell( $barWidth, 10, $totalSales, 0, 0, 'C' );
  
  $xPos += ( 1 / $xScale );
  $bar++;
}
 

$rowLabels = array( "Jun-16", "Jul-16", "Aug-16" );
$chartXPos =90;
$chartYPos = 175;
$chartWidth = 110;
$chartHeight = 35;
$chartXLabel = "";
$chartYLabel = "";
$chartYStep = 20;

$chartColours = array(
                  array(  0, 176, 80 ),
                  array(  0, 176, 80 ),
                  array(  0, 176, 80 )
                );

$data = array(
          array( 20),
          array( 34 ),
          array( 48 )
        );
$xScale = count($rowLabels) / ( $chartWidth - 40 );


$maxTotal = 100;
$yScale = $maxTotal / $chartHeight;

// Compute the bar width
$barWidth = ( 1 / $xScale ) / 1.5;

// Add the axes:

$pdf->SetFont( 'Arial', '', 8 );
 $pdf->SetDrawColor(195,195,195);
 $pdf->SetLineWidth(0.3);
// X axis
$pdf->Line( $chartXPos + 30, $chartYPos, $chartXPos + $chartWidth, $chartYPos );
  
for ( $i=0; $i < count( $rowLabels ); $i++ ) {
  $pdf->SetXY( $chartXPos + 40 +  $i / $xScale, $chartYPos );
  $pdf->Cell( $barWidth, 10, $rowLabels[$i], 0, 0, 'C' );
}

// Y axis
//$pdf->Line( $chartXPos + 30, $chartYPos, $chartXPos + 30, $chartYPos - $chartHeight - 8 );

for ( $i=0; $i <= $maxTotal; $i += $chartYStep ) {
  $pdf->SetXY( $chartXPos + 7, $chartYPos - 5 - $i / $yScale );
  $pdf->Cell( 20, 10, '' . number_format( $i ), 0, 0, 'R' );
 // $pdf->Line( $chartXPos + 28, $chartYPos - $i / $yScale, $chartXPos + 30, $chartYPos - $i / $yScale );
}

// Add the axis labels
$pdf->SetFont( 'Arial', 'B', 8 );
$pdf->SetXY( $chartWidth / 2 + 20, $chartYPos + 8 );
$pdf->Cell( 30, 10, $chartXLabel, 0, 0, 'C' );
$pdf->SetXY( $chartXPos + 7, $chartYPos - $chartHeight - 12 );
$pdf->Cell( 20, 10, $chartYLabel, 0, 0, 'R' );

// Create the bars
$xPos = $chartXPos + 40;
$bar = 0;

foreach ( $data as $dataRow ) {

  // Total up the sales figures for this product
  $totalSales = 0;
  foreach ( $dataRow as $dataCell ) $totalSales += $dataCell;

  // Create the bar
  $colourIndex = $bar % count( $chartColours );
  $pdf->SetFillColor( $chartColours[$colourIndex][0], $chartColours[$colourIndex][1], $chartColours[$colourIndex][2] );
  $pdf->Rect( $xPos, $chartYPos - ( $totalSales / $yScale ), $barWidth, $totalSales / $yScale, 'F' );
  
   $pdf->SetXY( $chartXPos + 40 +  $bar / $xScale,  $chartYPos-10 - ( $totalSales / $yScale ) );
  $pdf->Cell( $barWidth, 10, $totalSales, 0, 0, 'C' );
  
  $xPos += ( 1 / $xScale );
  $bar++;
}
$pdf->Ln(6);



$pdf->SetFont( 'Times', 'B', 10 );
$pdf->Cell( 140,80, "                                                          Linguistics",0,0  );
$pdf->Ln( 8 );

$textColour = array( 0, 0, 0 );
$headerColour = array( 100, 100, 100 );


$rowLabels = array( "Jun-16", "Jul-16", "Aug-16" );
$chartXPos =0;
$chartYPos = 240;
$chartWidth = 110;
$chartHeight = 35;
$chartXLabel = "";
$chartYLabel = "";
$chartYStep = 20;
 
$chartColours = array(
                  array( 91, 155, 213 ),
                  array( 91, 155, 213 ),
                  array( 91, 155, 213 )
                );

$data = array(
          array( 20),
          array( 34 ),
          array( 48 )
        );
$xScale = count($rowLabels) / ( $chartWidth - 40 );


$maxTotal = 100;
$yScale = $maxTotal / $chartHeight;

// Compute the bar width
$barWidth = ( 1 / $xScale ) / 1.5;

// Add the axes:

$pdf->SetFont( 'Arial', '', 8 );

// X axis
$pdf->Line( $chartXPos + 30, $chartYPos, $chartXPos + $chartWidth, $chartYPos );

for ( $i=0; $i < count( $rowLabels ); $i++ ) {
  $pdf->SetXY( $chartXPos + 40 +  $i / $xScale, $chartYPos );
  $pdf->Cell( $barWidth, 10, $rowLabels[$i], 0, 0, 'C' );
}

// Y axis
 $pdf->SetLineWidth(0.4);
//$pdf->Line( $chartXPos + 30, $chartYPos, $chartXPos + 30, $chartYPos - $chartHeight - 8 );

for ( $i=0; $i <= $maxTotal; $i += $chartYStep ) {
  $pdf->SetXY( $chartXPos + 7, $chartYPos - 5 - $i / $yScale );
  $pdf->Cell( 20, 10, '' . number_format( $i ), 0, 0, 'R' );
  //$pdf->Line( $chartXPos + 28, $chartYPos - $i / $yScale, $chartXPos + 30, $chartYPos - $i / $yScale );
}

// Add the axis labels
$pdf->SetFont( 'Arial', 'B', 8 );
$pdf->SetXY( $chartWidth / 2 + 20, $chartYPos + 8 );
$pdf->Cell( 30, 10, $chartXLabel, 0, 0, 'C' );
$pdf->SetXY( $chartXPos + 7, $chartYPos - $chartHeight - 12 );
$pdf->Cell( 20, 10, $chartYLabel, 0, 0, 'R' );

// Create the bars
$xPos = $chartXPos + 40;
$bar = 0;

foreach ( $data as $dataRow ) {

  // Total up the sales figures for this product
  $totalSales = 0;
  foreach ( $dataRow as $dataCell ) $totalSales += $dataCell;

  // Create the bar
  $colourIndex = $bar % count( $chartColours );
  $pdf->SetFillColor( $chartColours[$colourIndex][0], $chartColours[$colourIndex][1], $chartColours[$colourIndex][2] );
  $pdf->Rect( $xPos, $chartYPos - ( $totalSales / $yScale ), $barWidth, $totalSales / $yScale, 'F' );
  
   $pdf->SetXY( $chartXPos + 40 +  $bar / $xScale,  $chartYPos-10 - ( $totalSales / $yScale ) );
  $pdf->Cell( $barWidth, 10, $totalSales, 0, 0, 'C' );
  
  $xPos += ( 1 / $xScale );
  $bar++;
}

$pdf->AddPage();
$pdf->Ln(6);
 
$pdf->SetFont( 'Times', 'B', 12 );
$pdf->SetFillColor( 204,204,204 );

$pdf->Cell( 0, 6, "SkillAngels - Skill Assessment & Training", 0, 0, 'C',TRUE );

$pdf->SetTextColor( $textColour[0], $textColour[1], $textColour[2] );
$pdf->Ln( 7);
$pdf->SetFont( 'Times', '', 11 );

$pdf->Cell( 0,7, "The assessment and training sessions strive to measure the core skills in each student that form the basis of",0,0,'FJ' );
$pdf->Ln( 5 );
$pdf->Cell( 0,7, "higher order learning, critical thinking and decision making abilities.",0,0,'L' );
$pdf->Ln( 8 );
$pdf->Cell( 0,7, "The core skills measured are as follows:",0,0,'L' );
$pdf->Ln( 8 );
$pdf->SetFont( 'Times', 'B', 11 );
$pdf->Cell( 0,7, "Memory:",0,0,'L' );
$pdf->SetFont( 'Times', '', 11 );
$pdf->Ln( 5 );
$pdf->Cell( 0,7, "The ability to store and recall information.",0,0,'L' );
$pdf->Ln( 8 );
$pdf->SetFont( 'Times', 'B', 11 );
$pdf->Cell( 0,7, "Visual Processing:",0,0,'L' );
$pdf->SetFont( 'Times', '', 11 );
$pdf->Ln( 5 );
$pdf->Cell( 0,7, "The ability to perceive, analyze, and think in visual images. The test evaluates the student's ability to create a",0,0,'FJ' );
$pdf->Ln( 5 );
$pdf->Cell( 0,7, "picture of word or concepts in mind and visualize abilities.",0,0,'L' );
$pdf->Ln( 8 );
$pdf->SetFont( 'Times', 'B', 11 );
$pdf->Cell( 0,7, "Focus & Attention:",0,0,'L' );
$pdf->SetFont( 'Times', '', 11 );
$pdf->Ln( 5 );
$pdf->Cell( 0,7, "The ability to sustain and attend to incoming information.",0,0,'L' );
$pdf->Ln( 8 );
$pdf->SetFont( 'Times', 'B', 11 );
$pdf->Cell( 0,7, "Problem Solving:",0,0,'L' );
$pdf->SetFont( 'Times', '', 11 );
$pdf->Ln( 5 );
$pdf->Cell( 0,7, "The ability to reason, form concepts, and solve problems using unfamiliar information or novel procedures.",0,0,'FJ' );
$pdf->Ln( 5 );
$pdf->Cell( 0,7, "This evaluates logical thinking and reasoning abilities.",0,0,'L' );
$pdf->Ln( 8 );
$pdf->SetFont( 'Times', 'B', 11 );
$pdf->Cell( 0,7, "Linguistics:",0,0,'L' );
$pdf->SetFont( 'Times', '', 11 );
$pdf->Ln( 5 );
$pdf->Cell( 0,7, "The test evaluates the ability to recollect and process words or concepts in correlation with a particular",0,0,'FJ' );
$pdf->Ln( 5 );
$pdf->Cell( 0,7, "language (English) learnt over a period of a time.",0,0,'L' );
$pdf->Ln( 8 );
$pdf->SetFont( 'Times', 'B', 12 );
$pdf->SetFillColor( 204,204,204 );

$pdf->Cell( 0, 6, "How it works?", 0, 0, 'C',TRUE );
$pdf->SetFont( 'Times', '', 11 );
$pdf->Ln( 8 );
$pdf->Cell( 0,7, "A unique game plan is set each day. The plan consists of 5 games, 1 per skill. Each game can be played to a",0,0,'FJ' );
$pdf->Ln( 5 );
$pdf->Cell( 0,7, "maximum of 5 times. In case of more than one time of play, average score of the total times played is taken as",0,0,'FJ' );
$pdf->Ln( 5 );
$pdf->Cell( 0,7, "game score.",0,0,'L' );

$pdf->Ln( 8 );
$pdf->Cell( 0,7, "The Training Modules are not a tool to diagnose or establish the learning capacity of an individual but an",0,0,'FJ' );
$pdf->Ln( 5 );
$pdf->Cell( 0,7, "indicator for students, to help them identify their potential strong and areas of improvement for cognitive",0,0,'FJ' );
$pdf->Ln( 5 );
$pdf->Cell( 0,7, "skill. And further use this highly innovative and affordable tool for improvement and excellence.",0,0,'L' );
$pdf->Ln( 8 );
      
$pdf->SetFont( 'Times', 'B', 11 );
$pdf->Cell(0,7,"Note:",0,0,'L');
$pdf->Ln( 5 );
$pdf->SetFont( 'Times', '', 11 );
$pdf->Cell(0,7,"The outcome of the tests can be affected by numerous circumstances, like if the student was not clear ",0,0,'FJ');
 

 
$pdf->Ln( 5 );
$pdf->Cell( 0,7, "with the instructions, was feeling stressed or facing issues with the computer hardware or internet",0,0,'FJ' );
$pdf->Ln( 5 );
$pdf->Cell( 0,7, "connectivity issues , it can affect the end result .",0,0,'L' );

$pdf->Ln( 8 );
$pdf->Cell(0, 7, chr(127)."     A total of five skill areas were assessed with one game for every skill.", 0, 0, 'L');
$pdf->Ln( 6 );
$pdf->Cell( 0,7, chr(127)."     A defined set of questions in the gamified environment were presented.",0,0,'L' );
$pdf->Ln( 6 );
$pdf->Cell( 0,7, chr(127)."     The scores were computed based on the following parameters",0,0,'L' );
$pdf->Ln( 6 );
$pdf->Cell( 0,7,  "                ".chr(127)."     Total questions attempted",0,0,'L' );
$pdf->Ln( 6 );
$pdf->Cell( 0,7,  "                ".chr(127)."     Total number of correct or incorrect responses",0,0,'L' );
$pdf->Ln( 6 );
$pdf->Cell( 0,7,  "                ".chr(127)."     Response time - Time taken for each response",0,0,'L' );
$pdf->Ln( 6 );
$pdf->Cell( 0,7,  "                ".chr(127)."     Total Time taken to complete each game",0,0,'L' );
$pdf->Ln( 6 );
$pdf->Cell( 0,7, chr(127)."    Based on these parameters, the Skill Performance Chart and the Brain Skill Power Index is generated for all",0,0,'L' );
$pdf->Ln( 6 );
$pdf->Cell( 0,7, "     the students",0,0,'L' );

















$pdf->Output( "../student_pdf_report/report.pdf", "F" );

?>
