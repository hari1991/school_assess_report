<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script type="text/javascript" src="js/charts.js"></script>
<script type="text/javascript" src="js/charts-more.js"></script>

<div id="container">
</div>

<div id="container_memory">
</div>
<script>
 $(function() {
    // Create the first chart
    $('#container').highcharts({
        chart: {
            type: 'column' // Chart type (i.e. 'bar', 'column', 'spline' etc)
        },
        title: {
            text: 'Chart A' // Title for the chart
        },
        xAxis: {
            categories: ['Jane', 'John', 'Joe', 'Jack', 'jim']
            // Categories for the charts
        },
        yAxis: {
            min: 0, // Lowest value to show on the yAxis
            title: {
                text: 'Apple Consumption' // Title for the yAxis
            }
        },
        legend: {
            enabled: false // Enable/Disable the legend
        },
        credits: {
            enabled: true, // Enable/Disable the credits
            text: 'This is a credit'
        },
        tooltip: {
            shared: true // If you have multiple series then all points in each category will show up on one tooltip
        },
        series: [{
            name: 'Apples', // Name of your series
            data: [5, 3, 8, 2, 4] // The data in your series
            
        }]
    });
    
    $('#container_memory').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Chart B'
        },
        xAxis: {
            categories: ['Jane', 'John', 'Joe', 'Jack', 'jim']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Miles during Run'
            }
        },
        legend: {
            enabled: false
        },
        credits: {
            enabled: true,
            text: 'You can have a link in here too',
            href: 'http://www.google.com'
        },
        tooltip: {
            shared: true
        },
        series: [{
            name: 'Miles',
            data: [2.4, 3.8, 6.1, 5.3, 4.1]
            
        }]
    });
});
</script>