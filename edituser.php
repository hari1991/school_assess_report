
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,700' rel='stylesheet' type='text/css' />
	<script type="text/javascript" src="js/modernizr.custom.79639.js"></script> 
	<link href='http://fonts.googleapis.com/css?family=Gochi+Hand' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>

<link rel="stylesheet" href="css/jquery-ui.css" />

<script type="text/javascript" language="javascript" src="js/jquery-1.8.2.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.validate.js"></script>
<script type="text/javascript" language="javascript" src="datatable/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="js/jquery.fancybox.js"></script>
<link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css" media="screen" />
<link rel="stylesheet" type="text/css" href="css/demo.css" media="screen" />
<link rel="stylesheet" type="text/css" href="datatable/media/css/demo_page.css" media="screen" />
<link rel="stylesheet" type="text/css" href="datatable/media/css/demo_table_jui.css" media="screen" />
<link rel="stylesheet" type="text/css" href="datatable/examples/examples_support/themes/smoothness/jquery-ui-1.8.4.custom.css" media="screen" />

<link rel="stylesheet" type="text/css" href="css/common.css" />
    <link rel="stylesheet" type="text/css" href="css/style5.css" />

<script type="text/javascript" language="javascript" src="datatable/media/js/jquery.dataTables.js"></script>



<?php
require_once ('../dbassess.php');
error_reporting(0);
$euid = $_REQUEST['id'];



$sql1 = "select a.*,b.grdname,c.name as planname from users a left join skl_grade b on a.grade_id = b.id left join g_plans c on a.gp_id = c.id  where a.status='NULL' and a.id = $euid order by a.id desc";
$objresult = mysql_fetch_object(mysql_query($sql1));


?>


<script>
	$(document).ready(function(){
		
		
		$.validator.addMethod("NumbersOnly", function(value, element) {
	        return this.optional(element) || /^[0-9\-\+]+$/i.test(value);
	    }, "<?php echo '<font color=red>&nbsp;&nbsp;Mobile should contain only Numbers.</font>'?>");
	    
		
			
			var oriemail =$("#reg_editemail").val();
				$("#edituser").validate({
					rules: {
						reg_editemail: {
							remote: {
								url: "../ajax_data.php",
								type: "post",
								data: {
									oriemail: function() {
									return oriemail;
									}
								}
							}
						},
						phone: {
		        			NumbersOnly: true,
		        			minlength : 10,
		        			maxlength : 15
		            		}
					},
					messages: {
						reg_editemail :{
							remote : 'Email already Exists'
						},
						phone :{
			          		NumbersOnly : 'Phone should contain only Numbers',
			          		minlength : 'Please enter atleast 10 numbers',
			          		maxlength : 'Please enter no more than 15 Numbers'
			              	}
					},
					submitHandler: function(form) {
	                    $.ajax({
	                    type: "POST",
	          	           url: '../ajax_data.php',
	          	           data: $("#edituser").serialize(), // serializes the form's elements.
	          	           success: function(data)
	          	           {
	          	        	   self.parent.location.href='index.php?act=userslist&msg=1';
	          	        	  }           
	                    });
	                    return false;
	                }
				});
		
		
	});
</script>
<!-- edit form -->
<body style="background: none; overflow : auto;">
<div id="editform" class="editform"">
<div class="edit-head">Edit Record</div>

<div class="editlist">
<form action="edituser.php" class="cmxform" method="POST" id="edituser" accept-charset="utf-8">
<input type="hidden" name="userid" value="<?php echo $euid; ?>">
<div class="label">
<div class="firstlabel"><label class="fields" for="fname">First Name </label><label class="mandatory">*</label></div>
<div class="secondinput"><input type="text" name="fname" value="<?php echo $objresult->fname; ?>" title="Please enter school name" class="required" id="fname">
<div class="error"></div>
</div>
</div>	
<div class="label">
<div class="firstlabel"><label class="fields" for="lname">Last Name</label><label class="mandatory">*</label></div>
<div class="secondinput"><input type="text" name="lname" value="<?php echo $objresult->lname; ?>" title="Please enter school address" class="required" id="lname"></div>	
</div>
<div class="label">
<div class="firstlabel"><label class="fields" for="phone">Phone Number </label><label class="mandatory">*</label></div>
<div class="secondinput"><input type="text" name="phone" value="<?php echo $objresult->mobile; ?>" title="Please enter phone number" class="required" id="phone"></div>
</div>
<div class="label">
<div class="firstlabel"><label class="fields" for="reg_editemail">Email id </label><label class="mandatory">*</label></div>
<div class="secondinput"><input type="text" name="reg_editemail" value="<?php echo $objresult->email; ?>" title="Please enter email id" class="required email" id="reg_editemail"></div>
</div>

<div class="label">
<div class="firstlabel"><label class="fields" for="username">Username</label><label class="mandatory"></label></div>
<div class="secondinput"><input type="text" name="username" value="<?php echo $objresult->username; ?>" title="Please enter email id" id="username"></div>
</div>

<div class="label">
<div class="firstlabel"><label class="fields" for="grdname">Class</label><label class="mandatory">*</label></div>
<div class="secondinput"><select name = "grade_users" id = "grade_users" class="required">
<?php 
$sklgrade = mysql_query("select * from class");
while ($skl_grade = mysql_fetch_array($sklgrade)){
if($skl_grade['id'] == $objresult->grade_id){
	$selected = 'selected';
}
else {
	$selected = '';
}
?>	

  <option <?php echo $selected; ?> value="<?php echo $skl_grade['id']; ?>"><?php echo $skl_grade['classname']; ?></option>

<?php 
}
?>
  
</select></div>
</div>




<div class="submitlabel"><input type="submit" class="submitbutton" id="usereditsubmit" name="usereditsubmit" value="Update"></div>
</form>
</div>
</div>
</body>