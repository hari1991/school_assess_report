<?php
ob_start();
ini_set( "display_errors", 0);
include('../dbassess.php');
session_start();
$act=$_GET['act'];
$adminid=$_SESSION['aid'];

if($act=='logout')
{
	echo "<script>window.location='index.php'</script>";
}

?>

<!DOCTYPE html>
<html lang="en">
    <head>
		<meta charset="utf-8">
		<title>SkillAngels -SuperBrains,Online Brain Games ,Edsix Brain Lab,Cognitive Skills Development, Edutainment ,Competition for Students</title>
		<meta name="keywords" content="SkillAngels, SuperBrains,Online Brain Games ,Edsix Brain Lab,Cognitive Skills Development, Educational Digital Games,Life Skill games,Brain Games for Kids,Brain skill Gaming Competition, Brain Games Free Download" />
		<meta name="description" content="SkillAngels, An IIT Madras RTBI incubated company, Skillangels was conceptualized with the vision of improving cognitive skills and performance for students and adults through next generation brain enhancement programs.">
		<meta name="author" content="kraftpanda.com">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
 <!-- Bootstrap Core CSS -->
     <link rel="stylesheet" href="css/bootstrap-3.1.1.min.css" type="text/css" />

<link rel="stylesheet" href="css/bootstrap-multiselect.css" type="text/css" />
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<!-- MetisMenu CSS -->
    <link href="css/metisMenu.min.css" rel="stylesheet">
	<!-- Custom CSS -->
    <link href="css/sb-admin-2.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
	<!-- Custom Fonts -->
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="css/jquery.dataTables.css" rel="stylesheet" type="text/css">
    <link href="css/dataTables.tableTools.css" rel="stylesheet" type="text/css">
		<!-- Web Fonts  -->
		<link href="fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<link rel="icon" type="image/png"  href="img/favicon.png">
		
		<script src="js/jquery_library.js" type="text/javascript"></script>
		<script src="js/jquery.dataTables.js" type="text/javascript"></script>
		<script src="js/dataTables.tableTools.js" type="text/javascript"></script>


		<!-- Head Libs -->
		

		
		
		<!--[if IE]>
			<link rel="stylesheet" href="css/ie.css">
		<![endif]-->

		<!--[if lte IE 8]>
			<script src="vendor/respond.js"></script>
		<![endif]-->
		
    </head>
<body>
		<div id="wrapper">
		<?php include('header.php');?>
		<div id="page-wrapper">
		<?php include('content.php');?>
		</div>
		<?php if(!isset($_SESSION['schoolid'])){ ?>
	<style>
	#page-wrapper{margin:0;}
</style>	
		<?php } ?>
		<?php include('footer.php');?>
				<!-- Libs -->
		
		<!-- <script src="master/style-switcher/style.switcher.js"></script> -->
		
			<script src="js/bootstrap.min.js" type="text/javascript"></script>
	
		
		
	</body>
