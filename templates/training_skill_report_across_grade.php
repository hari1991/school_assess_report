<?php 
if(empty($adminid)){
	echo "<script>window.location='index.php?act=login'</script>";
}
include_once("training_skill_report_across_grade_bl.php");

?>
	 <script type="text/javascript" src="js/bootstrap-multiselect.js"></script>
 <?php $AverageScore=$skillQuotient_value; ?>

<div class="pageHomePager Dashboardhide mygameshide myreporthide myprofilehide">
   <div class="row">
  <div class="col-lg-12">
                    <h1 class="page-header">Gradewise Average Skill Scores</h1>
                </div>
			</div>	
			<div class="row">
      			<div class="col-lg-12">
				<form name="frmStudentReport" id="frmStudentReport" method="post">
				<div class="panel panel-default">
            <div class="panel-body reportChartContainer">
			<div class="col-lg-4">
				<div class="row">
				<label class="col-lg-6">Grade</label>
        <select name="chkGrade" id="chkveg" multiple="multiple">
			<?php foreach($GradeList as $grades) {
				$gradinid='';
				$selected='';
				if($ddlGradeType==$grades['id']){$selected="selected='selected'";$gradinid=$grades['id'];}
				echo "<option id='".$grades['id']."' ".$selected." value='".$grades['id']."'>".trim(str_replace("Grade",'',$grades['classname']))."</option>";
			} ?>
			            </select>
			            <input type="hidden" value="" name="hdnGradeID" id="hdnGradeID" />
			            	<script type="text/javascript">
			            	    $(function() {
			            	        $('#chkveg').multiselect({
			            	            includeSelectAllOption: true,numberDisplayed: 1,nonSelectedText: 'Select',buttonClass: 'form-control col-lg-6',
										onChange: function(element, checked) {
        var brands = $('#chkveg option:selected');
        var selected = [];
        $(brands).each(function(index, brand){
            selected.push([$(this).val()]);
        });
		$("#hdnGradeID").val(selected);
		
										}
			            	        });
									
			            	        $('#btnget').click(function() {
			            	       // alert($('#chkveg').val());
			            	        })
			            	    });
			</script>
			<script type="text/javascript">
			    $(document).ready(function() {
			    $('#chkveg').multiselect();
				<?php if(isset($_POST['btnSearch'])) { ?>
				var data="<?php echo $hdnGradeID; ?>";
				var dataarray=data.split(",");
				$("#chkveg").val(dataarray);
				$("#hdnGradeID").val(dataarray);
				$("#chkveg").multiselect("refresh");
				<?php } ?>
			    });
</script>
				
			</div>
			
				</div>
		
			
		<!--	<div class="col-lg-4">
			<div class="row">
			<label class="col-lg-6">Month</label>
			 <select id="chkveg1" multiple="multiple">
			 <?php $ini=0; foreach($academicMonths as $am){
				$ini++;
				
				echo '<option value="'.$am['monthNumber'].'">'.$am['monthName'].'</option>';
			} ?>
			
			
			            </select>
			         <input type="hidden" value="" name="hdnMonthID" id="hdnMonthID" />    
			            	<script type="text/javascript">
			            	    $(function() {
			            	        $('#chkveg1').multiselect({
			            	            includeSelectAllOption: true,numberDisplayed: 1,nonSelectedText: 'Select',buttonClass: 'form-control col-lg-6',
										onChange: function(element, checked) {
        var brands = $('#chkveg1 option:selected');
        var selected = [];
        $(brands).each(function(index, brand){
            selected.push([$(this).val()]);
			//alert(selected);
        });
			$("#hdnMonthID").val(selected);
			}
			            	        });
			            	        $('#btnget').click(function() {
			            	        alert($('#chkveg1').val());
			            	        })
			            	    });
			</script>
			<script type="text/javascript">
			    $(document).ready(function() {
			    $('#chkveg1').multiselect();
				<?php if(isset($_POST['btnSearch'])) { ?>
				var data1="<?php echo $hdnMonthID; ?>";
				var dataarray1=data1.split(",");
				$("#chkveg1").val(dataarray1);
				$("#chkveg1").multiselect("refresh");
				<?php } ?>
				
			    });
</script>
				
			</div>
			</div>-->
			<div class="col-lg-1"></div>
			<div class="col-lg-1">
			<div class="row">
			<input  type="submit" name="btnSearch" class="btn btn-success" id="btnSubmit" value="Submit" />
			</div>
				</div>
				
			<div class="col-lg-1">
			<div class="row">
			<input  type="button" onclick="javascript:window.location.href=window.location.href;" name="btnSubmit" class="btn btn-warning" id="btnSubmit" value="Reset" />
			</div>
				</div>
				
				
				</div>
				</div>
				</form>
				
				</div>
				</div>
				
			
  
   
   <div id="StudentReportResult"  >
       <?php if(isset($_POST['btnSearch'])) { ?>      
			
			
			<div class="row">
      			<div class="col-lg-6">
                	<label>Memory</label>
                    <div class="panel panel-default">
                        <div class="panel-body reportChartContainer">
                            <div id="container_memory" ></div>
                        </div>
                    </div>
                </div>
				<div class="col-lg-6">
                	<label>Visual Processing <b></b></label>
                    <div class="panel panel-default">
                        <div class="panel-body reportChartContainer">
                            <div id="container_vp"></div>
                        </div>
                    </div>
                </div>
				
 			</div>
			<div class="row">
      			<div class="col-lg-6">
                	<label>Focus and Attention</label>
                    <div class="panel panel-default">
                        <div class="panel-body reportChartContainer">
                            <div id="container_fa"></div>
                        </div>
                    </div>
                </div>
				<div class="col-lg-6">
                	<label>Problem Solving <b></b></label>
                    <div class="panel panel-default">
                        <div class="panel-body reportChartContainer">
                            <div id="container_ps"></div>
                        </div>
                    </div>
                </div>
				
 			</div>
			<div class="row">
      			<div class="col-lg-6">
                	<label>Linguistics</label>
                    <div class="panel panel-default">
                        <div class="panel-body reportChartContainer">
                            <div id="container_L"></div>
                        </div>
                    </div>
                </div>
				<div class="col-lg-6">                   
                </div>
				
 			</div>
			<hr/>
			
	   <?php } ?>		
			
</div>
</div>

<script src="js/skillpiecharts.js" type="text/javascript"></script><script type="text/javascript" src="js/charts.widgets.js"></script>

 <script type="text/javascript" src="js/charts.js"></script>
<script type="text/javascript" src="js/charts-more.js"></script>
<script src="http://code.highcharts.com/modules/solid-gauge.src.js"></script>

 <script>
 <?php if(isset($_POST['btnSearch'])) { ?>


 
$(document).ready(function(){

	$('#container_memory').highcharts({
		chart: {
            type: 'column'
        },
		
        title: {
            text: '',
            x: -20 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
            categories: [<?php $ini=0; foreach($GradeList_name as $am){
				$ini++;
				if($ini>1){echo ",";}
				echo "'".$am['classname']."'";
			} ?>]
        },
        yAxis: {
            min: 0,tickInterval: 10,max:100,
            title: {
                text: 'Score'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#ff3300'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
		plotOptions: {
        column: {
            dataLabels: {
                enabled: true,
                crop: false,
                overflow: 'none'
            }
        }
    },
        credits: {
      enabled: false
  },
        series: [{showInLegend: false,   color: '#ff3300',
            name: 'Score',
            data: [<?php $ini=0; foreach($hdnGradeID_arr as $am){
				$ini++;
				if($ini>1){echo ",";}
				if(isset($month_array_chart['59'][$am])){
				echo "".round($month_array_chart['59'][$am],2)."";
				}
				else{echo "0";}
			} ?>]
        }]
    });
	
	$('#container_vp').highcharts({
		chart: {
            type: 'column'
        },
        title: {
            text: '',
            x: -20 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
            categories: [<?php $ini=0; foreach($GradeList_name as $am){
				$ini++;
				if($ini>1){echo ",";}
				echo "'".$am['classname']."'";
			} ?>]
        },
        yAxis: {
            min: 0,tickInterval: 10,max:100,
            title: {
                text: 'Score'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#9a1e00'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
		
		plotOptions: {
        column: {
            dataLabels: {
                enabled: true,
                crop: false,
                overflow: 'none'
            }
        }
    },
        credits: {
      enabled: false
  },
        series: [{showInLegend: false, color:"#9a1e00", 
            name: 'Score',
            data: [<?php $ini=0; foreach($hdnGradeID_arr as $am){
				$ini++;
				if($ini>1){echo ",";}
				if(isset($month_array_chart['60'][$am])){
				echo "".round($month_array_chart['60'][$am],2)."";
				}
				else{echo "0";}
			} ?>]
        }]
    });
	$('#container_fa').highcharts({
		chart: {
            type: 'column'
        },
        title: {
            text: '',
            x: -20 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
            categories: [<?php $ini=0; foreach($GradeList_name as $am){
				$ini++;
				if($ini>1){echo ",";}
				echo "'".$am['classname']."'";
			} ?>]
        },
        yAxis: {
            min: 0,tickInterval: 10,max:100,
            title: {
                text: 'Score'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#ff9900'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
		
		plotOptions: {
        column: {
            dataLabels: {
                enabled: true,
                crop: false,
                overflow: 'none'
            }
        }
    },
       credits: {
      enabled: false
  },
        series: [{showInLegend: false, color:"#ff9900" ,
            name: 'Score',
            data: [<?php $ini=0; foreach($hdnGradeID_arr as $am){
				$ini++;
				if($ini>1){echo ",";}
				if(isset($month_array_chart['61'][$am])){
				echo "".round($month_array_chart['61'][$am],2)."";
				}
				else{echo "0";}
			} ?>]
        }]
    });
	$('#container_ps').highcharts({
		chart: {
            type: 'column'
        },
        title: {
            text: '',
            x: -20 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
            categories: [<?php $ini=0; foreach($GradeList_name as $am){
				$ini++;
				if($ini>1){echo ",";}
				echo "'".$am['classname']."'";
			} ?>]
        },
        yAxis: {
            min: 0,tickInterval: 10,max:100,
            title: {
                text: 'Score'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#05b923'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
		
		plotOptions: {
        column: {
            dataLabels: {
                enabled: true,
                crop: false,
                overflow: 'none'
            }
        }
    },
       credits: {
      enabled: false
  },
        series: [{showInLegend: false, color:"#05b923",  
            name: 'Score',
            data: [<?php $ini=0; foreach($hdnGradeID_arr as $am){
				$ini++;
				if($ini>1){echo ",";}
				if(isset($month_array_chart['62'][$am])){
				echo "".round($month_array_chart['62'][$am],2)."";
				}
				else{echo "0";}
			} ?>]
        }]
    });
	$('#container_L').highcharts({
		chart: {
            type: 'column'
        },
        title: {
            text: '',
            x: -20 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
            categories: [<?php $ini=0; foreach($GradeList_name as $am){
				$ini++;
				if($ini>1){echo ",";}
				echo "'".$am['classname']."'";
			} ?>]
        },
        yAxis: {
            min: 0,tickInterval: 10,max:100,
            title: {
                text: 'Score'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#33ccff'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
		
		plotOptions: {
        column: {
            dataLabels: {
                enabled: true,
                crop: false,
                overflow: 'none'
            }
        }
    },
        credits: {
      enabled: false
  },
        series: [{showInLegend: false,  color:"#33ccff",
            name: 'Score',
            data: [<?php $ini=0; foreach($hdnGradeID_arr as $am){
				$ini++;
				if($ini>1){echo ",";}
				if(isset($month_array_chart['63'][$am])){
				echo "".round($month_array_chart['63'][$am],2)."";
				}
				else{echo "0";}
			} ?>]
        }]
    });
});
 <?php } ?>
 </script>

<script>

function ajaxsectionload(gradeID,selectID)
{
$.ajax({
		url: "templates/ajax_sectionbygrade.php", 
		data:{gradeid:gradeID,schoolid:'<?php echo $_SESSION['schoolid'];?>',defaultselect:selectID},
		success: function(result)
		{
			$("#ddlSection").html(result);
		}
	});
	}

$(document).ready(function() {
	
    $('#assementTable').DataTable( );
} );
</script>
 