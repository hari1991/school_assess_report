<?php
if(empty($adminid)){
	echo "<script>window.location='index.php?act=login'</script>";
}
?>
<html>
<head>
<link rel="stylesheet" href="css/jquery-ui.css" />

<style type="text/css" title="currentStyle">
@import "datatable/media/css/demo_page.css";
@import "datatable/media/css/demo_table_jui.css";
@import "datatable/examples/examples_support/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<script type="text/javascript" language="javascript" src="js/jquery-1.8.2.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.validate.js"></script>
<script type="text/javascript" language="javascript" src="datatable/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="js/jquery.fancybox.js"></script>
<link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css" media="screen" />
<script type="text/javascript" src="js/watermark/jquery.watermark.js"></script>
<style type="text/css">
#status
{
font-size:11px;
margin-left:10px;
}
#email { float: left; }

p { clear: both; }

em { font-weight: bold; padding-right: 1em; vertical-align: top; }
.label { padding: 5px 0 }
</style>

<script>
$(document).ready(function(){
	var container = $('div.error');
    $("#addplan").validate({
		 rules: {
			 nplan: {
				 remote: {
					 url: "../ajax_data.php",
					 type: "post"
						 
					}
				 }
		 },
		
		messages: {
			nplan :{
					remote : 'Plan name is already taken'
				}
			}
	});
    var oTable = $('#planlist').dataTable({
		"bJQueryUI": true,
		"sPaginationType": "full_numbers"
		});
	$(".fancybox").fancybox();
});
</script>
</head>
</html>
<?php

if(isset($_POST['planeditsubmit'])){
$id = $_POST['planid'];
	$gradeid = $_POST['ddlgrade'];
$planname =  $_POST['nplan'];
$validity = $_POST['validity'];
$price = $_POST['price'];
$ngames = $_POST['ngames'];
$nskills = $_POST['nskills'];
$bp_status = $_POST['bprof'];
$gwpt_status = $_POST['gwpt'];
$swpt_status = $_POST['swpt'];
$bmeter_status = $_POST['braino'];
$comp_game_status = $_POST['gwcpt'];
$bspi_status = $_POST['obspi'];
$os = $_POST['os'];
mysql_query("update g_plans set grade_id = '$gradeid',name='$planname',validity = '$validity',price = '$price',ngames = '$ngames',nskills = '$nskills',bp_status = '$bp_status',gwpt_status = '$gwpt_status',swpt_status = '$swpt_status',bmeter_status = '$bmeter_status',comp_game_status = '$comp_game_status',bspi_status = '$bspi_status',online_status = '$os' where id = $id");
$message = 'Plan Edited successfully';
}

if(isset($_POST['oktodel'])){
	$id = $_POST['planid'];
	mysql_query("DELETE FROM  g_plans WHERE id='$id'");
	$message = 'Plan deleted Successfully';
}

if(isset($_POST['plansubmit'])){
	$gradeid = $_POST['ddlgrade'];
	$planname =  $_POST['nplan'];
	$validity = $_POST['validity'];
	$price = $_POST['price'];
	$ngames = $_POST['ngames'];
	$nskills = $_POST['nskills'];
	$bp_status = $_POST['bprof'];
	$gwpt_status = $_POST['gwpt'];
	$swpt_status = $_POST['swpt'];
	$bmeter_status = $_POST['braino'];
	$comp_game_status = $_POST['gwcpt'];
	$comp_skill_status = $_POST['swcpt'];
	$bspi_status = $_POST['obspi'];
	$os = $_POST['os'];
	mysql_query("insert into g_plans (grade_id,name,validity,price,ngames,nskills,bp_status,gwpt_status,swpt_status,bmeter_status,comp_game_status,comp_skill_status,bspi_status,online_status,status) values ('$gradeid','$planname','$validity','$price','$ngames','$nskills','$bp_status','$gwpt_status','$swpt_status','$bmeter_status','$comp_game_status','$comp_skill_status','$bspi_status','$os','1')");
	$message = 'Plan added Successfully';
}
?>


<?php 
		$allplans = mysql_query("SELECT a.*,b.id as gradeid,b.grdname FROM g_plans a join skl_grade b on a.grade_id = b.id");
		
		while($allplan = mysql_fetch_array($allplans)){
		?>
		<script>
$(document).ready(function(){
	$(".fancybox<?php echo $allplan['id']; ?>").fancybox({
		 beforeShow : function() {
			 
			$("#editplan<?php echo $allplan['id']; ?>").validate();
		},
	});
	 
});
</script>
			<div id="editform<?php echo $allplan['id']; ?>" class="editform" style="width:450px; display: none;">
		
				<div class="edit-head">Edit Record</div>
				<div class="editlist editplan">
					<form action="" class="cmxform " method="POST" id="editplan<?php echo $allplan['id']; ?>" accept-charset="utf-8">
						<input type="hidden" name="planid" value="<?php echo $allplan['id']; ?>">
						<div class="reqindicates"><label class="top_mandatory">*</label>&nbsp; Indicates required fields</div>
						<div class="mainlist">
							<div class="label">
								<div class="firstlabel"><label class="fields" for="grade">Grade </label><label class="mandatory">*</label></div>
								<div class="secondinput">
									<select name="ddlgrade" title="Please select grade" class="required" id="ddlgrade">
										<option value="">Select Grade</option>
										<?php
										$getgrade = mysql_query("select id,grdname from skl_grade");
			              					while($getarraycode = mysql_fetch_array($getgrade)){
												if($getarraycode['id'] == $allplan['gradeid'])
													$selected = 'selected';
												else
													$selected = '';
												
			             					 	echo "<option $selected value=".$getarraycode['id'].">".$getarraycode['grdname']."</option>";
								         } 
								         ?>
									</select>
								</div>
							</div>
							<div class="label">
								<div class="firstlabel"><label class="fields" for="nplan">Plan Name </label><label class="mandatory">*</label></div>
								<div class="secondinput"><input type="text" name="nplan" title="Please enter plan name" value="<?php echo $allplan['name']; ?>" class="required" id="nplan"></div>
							</div>												
							<div class="label">
								<div class="firstlabel"><label class="fields" for="validity">Validity (days) </label><label class="mandatory">*</label></div>
								<div class="secondinput"><input type="text" name="validity" title="Please enter validity" value="<?php echo $allplan['validity']; ?>" class="required" id="validity"></div>						
							</div class="label">
							<div class="label">
								<div class="firstlabel"><label class="fields" for="price">Price </label><label class="mandatory">*</label></div>
								<div class="secondinput"><input type="text" name="price" title="Please enter price" value="<?php echo $allplan['price']; ?>" class="required" id="price"></div>
							</div>
							<div class="label">
								<div class="firstlabel"><label class="fields" for="ngmaes">No. of Games </label><label class="mandatory">*</label></div>
								<div class="secondinput"><input type="text" name="ngames" value="<?php echo $allplan['ngames']; ?>" title="Please enter no. of games" class="required number" id="ngames"></div>
							</div>
							<div class="label">
								<div class="firstlabel"><label class="fields" for="nskills">No. of Skills </label><label class="mandatory">*</label></div>
								<div class="secondinput"><input type="text" name="nskills" value="<?php echo $allplan['nskills']; ?>" title="Please enter no. of skills" class="required number" id="nskills"></div>
							</div>
							<div class="label">
								<div class="firstlabel"><label class="fields" for="nskills">Brain Profiling </label><label class="mandatory">*</label></div>
								<div class="secondinput">
									<select name = "bprof" id = "bprof" title = "please choose bprof">
										<option <?php if($allplan['bp_status'] == 1) { $selected = ' selected '; echo $selected; } else { $selected = ''; echo $selected; } ?>value = "1">Yes</option>
										<option <?php if($allplan['bp_status'] == 0) { $selected = ' selected '; echo $selected; } else { $selected = ''; echo $selected; }?>value = "0">No</option>
									</select>
								</div>
							</div>
							<div class="label">
								<div class="firstlabel"><label class="fields" for="gwpt">Game wise Performance Tracking </label><label class="mandatory">*</label></div>
								<div class="secondinput">
									<select name = "gwpt" id = "gwpt" title = "please choose bprof">
										<option <?php if($allplan['gwpt_status'] == 1) { $selected = ' selected '; echo $selected; } else { $selected = ''; echo $selected; } ?>value = "1">Yes</option>
										<option <?php if($allplan['gwpt_status'] == 0) { $selected = ' selected '; echo $selected; } else { $selected = ''; echo $selected; } ?>value = "0">No</option>
									</select>
								</div>
							</div>
							<div class="label">
								<div class="firstlabel"><label class="fields" for="swpt">Skill wise Performance Tracking </label><label class="mandatory">*</label></div>
								<div class="secondinput">
									<select name = "swpt" id = "swpt" title = "please choose bprof">
										<option <?php if($allplan['swpt_status'] == 1) { $selected = ' selected '; echo $selected; } else { $selected = ''; echo $selected; } ?>value = "1">Yes</option>
										<option <?php if($allplan['swpt_status'] == 0) { $selected = ' selected '; echo $selected; } else { $selected = ''; echo $selected; } ?>value = "0">No</option>
									</select>
								</div>
							</div>
							<div class="label">
								<div class="firstlabel"><label class="fields" for="braino">Brain-o-Meter </label><label class="mandatory">*</label></div>
								<div class="secondinput">
									<select name = "braino" id = "braino" title = "please choose bprof">
										<option  <?php if($allplan['bmeter_status'] == 1) { $selected = ' selected '; echo $selected; } else { $selected = ''; echo $selected; } ?>value = "1">Yes</option>
										<option  <?php if($allplan['bmeter_status'] == 0) { $selected = ' selected '; echo $selected; } else { $selected = ''; echo $selected; } ?>value = "0">No</option>
									</select>
								</div>
							</div>
							<div class="label">
								<div class="firstlabel"><label class="fields" for="gwcpt">General Game Wise Comparison with plan members </label><label class="mandatory">*</label></div>
								<div class="secondinput">
									<select name = "gwcpt" id = "gwcpt" title = "please choose bprof">
										<option  <?php if($allplan['comp_game_status'] == 1) { $selected = ' selected '; echo $selected; } else { $selected = ''; echo $selected; } ?>value = "1">Yes</option>
										<option  <?php if($allplan['comp_game_status'] == 0) { $selected = ' selected '; echo $selected; } else { $selected = ''; echo $selected; } ?>value = "0">No</option>
									</select>
								</div>
							</div>
							
							<div class="label">
								<div class="firstlabel"><label class="fields" for="obspi">Overall BSPI</label><label class="mandatory">*</label></div>
								<div class="secondinput">
									<select name = "obspi" id = "obspi" title = "please choose bprof">
										<option  <?php if($allplan['bspi_status'] == 1) { $selected = ' selected '; echo $selected; } else { $selected = ''; echo $selected; } ?>value = "1">Yes</option>
										<option  <?php if($allplan['bspi_status'] == 0) { $selected = ' selected '; echo $selected; } else { $selected = ''; echo $selected; } ?>value = "0">No</option>
									</select>
								</div>
							</div>
							<div class="label">
								<div class="firstlabel"><label class="fields" for="os">Online Support </label><label class="mandatory">*</label></div>
								<div class="secondinput">
									<select name = "os" id = "os" title = "please choose bprof">
										<option  <?php if($allplan['online_status'] == 1) { $selected = ' selected '; echo $selected; } else { $selected = ''; echo $selected; } ?>value = "1">Yes</option>
										<option  <?php if($allplan['online_status'] == 0) { $selected = ' selected '; echo $selected; } else { $selected = ''; echo $selected; } ?>value = "0">No</option>
									</select>
								</div>
							</div>
						</div>
						<div class="submitlabel" align='center'><input type="submit" class="submitbutton" id="planeditsubmit" name="planeditsubmit" value="Submit"></div>
					</form>
				</div>
			</div>
				
				<!-- delete form -->
		<div id="deleteform<?php echo $allplan['id']; ?>" class="deleteform" style="display: none;">
			<form action"" method="post">
				<input type="hidden" name="planid" value="<?php echo $allplan['id']; ?>">
				<div class="edit-head">Are you sure to delete ? </div>
				<div class="editlist">
					<input class="submitbutton" type="submit" name="oktodel" id="oktodel" value="Delete">
					<input class="submitbutton" type="submit" name="canceltodel" id="canceltodel" value="Cancel">
				</div>
			</form>
		</div>
		<?php 
		}
		?>

<header>
	<div class="intro-head">Add New Plan</div>
	<div class="intro cnt_min">
		<div class="support-note">
			<div id="main">
				<div class="success"><?php echo $message; ?></div>
				<div class="reqindicates"><label class="top_mandatory">*</label>&nbsp; Indicates required fields</div>
				<form action="" class="cmxform newplan" method="POST" id="addplan" accept-charset="utf-8">
					<div class="mainlist">
						<div class="label">
							<div class="firstlabel"><label class="fields" for="grade">Grade </label><label class="mandatory">*</label></div>
							<div class="secondinput">
								<select name="ddlgrade" title="Please select grade" class="required" id="ddlgrade">
									<option value="">Select Grade</option>
									<?php
									$getgrade = mysql_query("select id,grdname from skl_grade");
		             				while($getarraycode = mysql_fetch_array($getgrade)){
		              					echo "<option value=".$getarraycode['id'].">".$getarraycode['grdname']."</option>";
		             				}
		              				?>
								</select>
							</div>						
						</div> 
						<div class="label">
							<div class="firstlabel"><label class="fields" for="nplan">Plan Name </label><label class="mandatory">*</label></div>
							<div class="secondinput"><input type="text" name="nplan" title="Please enter plan name" value="" class="required" id="nplan"></div>
						</div>												
						<div class="label">
							<div class="firstlabel"><label class="fields" for="validity">Validity (days) </label><label class="mandatory">*</label></div>
							<div class="secondinput"><input type="text" name="validity" title="Please enter validity" value="" class="required" id="validity"></div>						
						</div>
						<div class="label">
							<div class="firstlabel"><label class="fields" for="price">Price </label><label class="mandatory">*</label></div>
							<div class="secondinput"><input type="text" name="price" title="Please enter price" value="" class="required" id="price"></div>
						</div>
						<div class="label">
							<div class="firstlabel"><label class="fields" for="ngmaes">No. of Games </label><label class="mandatory">*</label></div>
							<div class="secondinput"><input type="text" name="ngames" value="" title="Please enter no. of games" class="required number" id="ngames"></div>
						</div>
						<div class="label">
							<div class="firstlabel"><label class="fields" for="nskills">No. of Skills </label><label class="mandatory">*</label></div>
							<div class="secondinput"><input type="text" name="nskills" value="" title="Please enter no. of skills" class="required number" id="nskills"></div>
						</div>					
				
						<div class="label">
							<div class="firstlabel"><label class="fields" for="nskills">Brain Profiling </label></div>
							<div class="secondinput">
								<select name = "bprof" id = "bprof" title = "please choose bprof">
									<option value = "1">Yes</option>
									<option value = "0">No</option>
								</select>
							</div>
						</div>
						<div class="label">
							<div class="firstlabel"><label class="fields" for="gwpt">Game wise Performance Tracking </label></div>
							<div class="secondinput">
								<select name = "gwpt" id = "gwpt" title = "please choose bprof">
									<option value = "1">Yes</option>
									<option value = "0">No</option>
								</select>
							</div>
						</div>
						<div class="label">
							<div class="firstlabel"><label class="fields" for="swpt">Skill wise Performance Tracking </label></div>
							<div class="secondinput">
								<select name = "swpt" id = "swpt" title = "please choose bprof">
									<option value = "1">Yes</option>
									<option value = "0">No</option>
								</select>
							</div>
						</div>
						<div class="label">
							<div class="firstlabel"><label class="fields" for="braino">Brain-o-Meter </label></div>
							<div class="secondinput">
								<select name = "braino" id = "braino" title = "please choose bprof">
									<option value = "1">Yes</option>
									<option value = "0">No</option>
								</select>
							</div>
						</div>
						<div class="label">
							<div class="firstlabel"><label class="fields" for="gwcpt">General Game Wise Comparison with plan members </label></div>
							<div class="secondinput">
								<select name = "gwcpt" id = "gwcpt" title = "please choose bprof">
									<option value = "1">Yes</option>
									<option value = "0">No</option>
								</select>
							</div>
						</div>
						<div class="label">
							<div class="firstlabel"><label class="fields" for="swcpt">General Skill Wise Comparison with plan members </label></div>
							<div class="secondinput">
								<select name = "swcpt" id = "swcpt" title = "please choose bprof">
									<option value = "1">Yes</option>
									<option value = "0">No</option>
								</select>
							</div>
						</div>
						<div class="label">
							<div class="firstlabel"><label class="fields" for="obspi">Overall BSPI (Brain Skill Power Index) Comparison with plan members</label></div>
							<div class="secondinput">
								<select name = "obspi" id = "obspi" title = "please choose bprof">
									<option value = "1">Yes</option>
									<option value = "0">No</option>
								</select>
							</div>
						</div>
						<div class="label">
							<div class="firstlabel"><label class="fields" for="os">Online Support </label></div>
							<div class="secondinput">
								<select name = "os" id = "os" title = "please choose bprof">
									<option value = "1">Yes</option>
									<option value = "0">No</option>
								</select>
							</div>
						</div>
					</div>
					<div class="submitlabel" align='center'><input type="submit" class="submitbutton" id="plansubmit" name="plansubmit" value="Submit"></div>
				</form>
  			</div>
  			
  			<br/>
  			
  			<div id="main">
  		<table cellpadding="0" cellspacing="0" border="0" class="display" id="planlist" >
		<thead>
			<tr>
				<th rowspan="2">Grade Name</th>
				<th rowspan="2">Plan Name</th>
				<th rowspan="2">Validity</th>
				<th rowspan="2">Price</th>
				<th rowspan="2">No. of Games</th>
				<th rowspan="2">No. of Skills</th>
				<th rowspan="2">Brain Profiling</th>
				<th rowspan="2">Game Wise</th>
				<th rowspan="2">Skill Wise</th>
				<th rowspan="2">Brain-o-Meter</th>
				<th rowspan="2">General Game Wise</th>
				<th rowspan="2">Overall BSPI</th>
				<th rowspan="2">Online Support</th>
				<th align="center" colspan="2">Action</th>
			</tr>
			<tr>
				<th>Edit</th>
				<th>Delete</th>
			</tr>
		</thead>
		<tbody>
		<?php 
		$allplans = mysql_query("SELECT a.*,b.id as gradeid,b.grdname FROM g_plans a join skl_grade b on a.grade_id = b.id");
		
		while($allplan = mysql_fetch_array($allplans)){
		?>
  			
		
		<?php
			echo "<tr>"; 
			
			echo "<td>$allplan[grdname]</td>";
			echo "<td>$allplan[name]</td>";
			echo "<td>$allplan[validity]</td>";
			echo "<td>$allplan[price]</td>";
			echo "<td>$allplan[ngames]</td>";
			echo "<td>$allplan[nskills]</td>";
			
			
			if($allplan[bp_status] == 1)
				echo "<td>Yes</td>";
			else
				echo "<td>No</td>";
			
			if($allplan[gwpt_status] == 1)
				echo "<td>Yes</td>";
			else
				echo "<td>No</td>";
			
			if($allplan[swpt_status] == 1)
				echo "<td>Yes</td>";
			else
				echo "<td>No</td>";
			
			if($allplan[bmeter_status] == 1)
				echo "<td>Yes</td>";
			else
				echo "<td>No</td>";
			
			if($allplan[comp_game_status] == 1)
				echo "<td>Yes</td>";
			else
				echo "<td>No</td>";
			
			
			if($allplan[bspi_status] == 1)
				echo "<td>Yes</td>";
			else
				echo "<td>No</td>";
			
			if($allplan[online_status] == 1)
				echo "<td>Yes</td>";
			else
				echo "<td>No</td>";
			
			echo "<td align='center'><a class='fancybox$allplan[id]' href='#editform$allplan[id]'><img src='images/edit.png' title='Edit Record' width='20' alt='Edit Record'></a></td>";
			echo "<td align='center'><a class='fancybox$allplan[id]' href='#deleteform$allplan[id]'><img src='images/delete.png' title='Delete Record' width='17' alt='Delete Record'></a></td>";
			echo "</tr>";
		}
		?>
		</tbody>
		</table>
				
  			</div>
  			
		</div>
	</div>
</header>