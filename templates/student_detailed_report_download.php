
<style>
.navbar-static-top{display:none;}
#page-wrapper{border:none;margin: 0;}
.pagefooter{display:none;}
</style>
	 <script type="text/javascript" src="js/bootstrap-multiselect.js"></script>
<?php 
if(empty($adminid)){
	echo "<script>window.location='index.php?act=login'</script>";
}
include_once("student_detailed_report_bl.php");

?>
 <?php $AverageScore=$skillQuotient_value; ?>

<div class="pageHomePager Dashboardhide mygameshide myreporthide myprofilehide">
 <?php if(isset($_POST['studentSubmit']))
	 { ?>
<div class="col-lg-12">
<img style="float: left;" src="images/logo.png" width="100px" height="100px" alt="logo">
                    <h1 style="border:none;float: left;padding-left: 50px;" class="page-header">Student Report - User name : <?php echo $params['txtStudentName']; ?> 
					
					</h1>
              
			</div>	
	 <?php } ?>
 <?php if(!isset($_POST['studentSubmit']))
	 { ?>
   <div class="row">
  <div class="col-lg-12">
                    <h1 class="page-header">Student Report II</h1>
                </div>
			</div>	
			
			<div class="row">
      			<div class="col-lg-12">
				<form name="frmStudentReport" id="frmStudentReport" method="post">
				<div class="panel panel-default">
            <div class="panel-body reportChartContainer">
			
			<div class="col-lg-4">
				<div class="row">
				<label class="col-lg-6">Enter Username</label>
				<input type="text" class="form-control col-lg-6" style="width:50%" value="<?php if(isset($params['txtStudentName']) && $params['txtStudentName']!=''){echo $params['txtStudentName'];} ?>" name="txtStudentName" id="txtStudentName" />
			</div>
			</div>
			
			<div class="col-lg-4">
			<div class="row">
			<label class="col-lg-6">Month</label>
			 <select id="chkveg1" multiple="multiple">
			 <?php $ini=0; foreach($academicMonths as $am){
				$ini++;
				
				echo '<option value="'.$am['monthNumber'].'">'.$am['monthName'].'</option>';
			} ?>
			
			
			            </select>
			         <input type="hidden" value="" name="hdnMonthID" id="hdnMonthID" />    
			            	<script type="text/javascript">
			            	    $(function() {
			            	        $('#chkveg1').multiselect({
			            	            includeSelectAllOption: true,numberDisplayed: 1,nonSelectedText: 'Select',buttonClass: 'form-control col-lg-6',
										onChange: function(element, checked) {
        var brands = $('#chkveg1 option:selected');
        var selected = [];
        $(brands).each(function(index, brand){
            selected.push([$(this).val()]);
			//alert(selected);
        });
			$("#hdnMonthID").val(selected);
			}
			            	        });
			            	        $('#btnget').click(function() {
			            	        alert($('#chkveg1').val());
			            	        })
			            	    });
			</script>
			<script type="text/javascript">
			    $(document).ready(function() {
			    $('#chkveg1').multiselect();
				<?php if(isset($_POST['studentSubmit'])) { ?>
				var data1="<?php echo $hdnMonthID; ?>";
				var dataarray1=data1.split(",");
				$("#chkveg1").val(dataarray1);
				$("#chkveg1").multiselect("refresh");
				<?php } ?>
				$("#chkveg1").multiselect("refresh");
			    });
</script>
				
			</div>
			</div>
			<div class="col-lg-1"></div>
			<div class="col-lg-1">
			<div class="row">
			<input  type="submit" name="studentSubmit" class="btn btn-success" id="btnSubmit" value="Submit" />
			</div>
				</div>
				
			<div class="col-lg-1">
			<div class="row">
			<input  type="button" onclick="javascript:window.location.href=window.location.href;" name="btnSubmit" class="btn btn-warning" id="btnSubmit" value="Reset" />
			</div>
				</div>
				
				
				</div>
				</div>
				</form>
				
				</div>
				</div>
				
			
   <hr/>
 <?php } ?>
   <div class="col-lg-12" <?php if(isset($params['txtStudentName']) && $uid==''){?> <?php } else{?> style="display:none;" <?php } ?>>
				<div class="row">
				<div class="alert alert-danger">Sorry, no user found for given username</div>
				</div>
				</div>
   <div id="StudentReportResult" <?php if(($uid=='')){ ?>  style="display:none;" <?php } ?> >
              <div class="row">
      			<div class="col-lg-12">
                	<label>Skill Quotient: &nbsp; <b><?php echo $AverageScore ?> %</b></label>
                    <div class="panel panel-default">
                        <div class="panel-body reportChartContainer">
                            <div id="chart-container"></div>
                        </div>
                    </div>
                </div>
				
				
 			</div>
			<hr/>
			<div class="row">
				 <div class="alert alert-warning" role="alert"> <strong>Assessment Scores </strong> </div>
				 </div>
			 <div class="row">
      			<div class="col-lg-4">
                	<label>Pre Assessment &nbsp; </label>
                    <div class="panel panel-default">
                        <div class="panel-body reportChartContainer">
                           <div class="gamePercentageMemory">
                           
                	<span class="memory<?php echo getAssmentchart('pretest',$assessment); ?>"><?php echo getAssmentchart('pretest',$assessment); ?>%</span>
                </div>
                        </div>
                    </div>
                </div>
				<div class="col-lg-4">
                	<label>Post Assessment - I&nbsp; </label>
                    <div class="panel panel-default">
                        <div class="panel-body reportChartContainer">
                            <div class="gamePercentageMemory">
                           
                	<span class="linguistics<?php echo getAssmentchart('posttest1',$assessment); ?>"><?php echo getAssmentchart('posttest1',$assessment); ?>%</span>
                </div>
                        </div>
                    </div>
                </div>
				<div class="col-lg-4">
                	<label>Post Assessment - II &nbsp; </label>
                    <div class="panel panel-default">
                        <div class="panel-body reportChartContainer">
                            <div class="gamePercentageMemory">
                           
                	<span class="FA<?php echo getAssmentchart('posttest2',$assessment); ?>"><?php echo getAssmentchart('posttest2',$assessment); ?>%</span>
                </div>
                        </div>
                    </div>
                </div>
 			</div>
			<hr/>
			
			<div class="row">
      			<div class="col-lg-6">
                	<label>Overall Performance</label>
                    <div class="panel panel-default">
                        <div class="panel-body reportChartContainer">
                            <div id="chartOverallPerformance" ></div>
                        </div>
                    </div>
                </div>
				<div class="col-lg-6">
                	<label>Training Days <b></b></label>
                    <div class="panel panel-default">
                        <div class="panel-body reportChartContainer">
                            <div id="container_2"></div>
                        </div>
                    </div>
                </div>
				
 			</div>
			<hr/>
			<div class="row">
				 <div class="alert alert-warning" role="alert"> <strong>Skill Performance ( SkillScore vs # Days Played)</strong> </div>
				 </div>
			<div class="row">
      			<div class="col-lg-6">
                	<label>Memory</label>
                    <div class="panel panel-default">
                        <div class="panel-body reportChartContainer">
                            <div id="container_memory" ></div>
                        </div>
                    </div>
                </div>
				<div class="col-lg-6">
                	<label>Visual Processing <b></b></label>
                    <div class="panel panel-default">
                        <div class="panel-body reportChartContainer">
                            <div id="container_vp"></div>
                        </div>
                    </div>
                </div>
				
 			</div>
			<div class="row">
      			<div class="col-lg-6">
                	<label>Focus and Attention</label>
                    <div class="panel panel-default">
                        <div class="panel-body reportChartContainer">
                            <div id="container_fa"></div>
                        </div>
                    </div>
                </div>
				<div class="col-lg-6">
                	<label>Problem Solving <b></b></label>
                    <div class="panel panel-default">
                        <div class="panel-body reportChartContainer">
                            <div id="container_ps"></div>
                        </div>
                    </div>
                </div>
				
 			</div>
			<div class="row">
      			<div class="col-lg-6">
                	<label>Linguistics</label>
                    <div class="panel panel-default">
                        <div class="panel-body reportChartContainer">
                            <div id="container_L"></div>
                        </div>
                    </div>
                </div>
				<div class="col-lg-6">                   
                </div>
				
 			</div>
			<hr/>
			<div class="row">
				 <div class="alert alert-warning" role="alert"> <strong>Game Play Status</strong> </div>
				 </div>
			<div class="row">
      			<div class="col-lg-12 landingContainer">
<?php 	if(isset($_POST['studentSubmit'])) { ?>
        			<table id="assementTable" class="table table-striped table-bordered table-hover table-condensed dt-responsive">
    <thead>
      <tr>
        <th>S.No.</th>
        <th>Section</th>
        <th>Total</th>
        <th>Completed</th>
        <th>Incompleted</th>
        </tr>
    </thead>
    <tbody>
	<?php 
	$ini=0; 
	
	foreach($arrStudentplay[$uid] as $res){
		$in_completed=0;$ini++;$monthname='';
		foreach($res as $result)
		{
	
	$inj=0;
	$in_completed+=$result['Completed'];
	$monthname=$result['monthlist'];
	?>	
      
	<?php 
		}
		
		?>
		<tr>
        <td><?php echo $ini; ?></td>
        <td><?php echo date('M', mktime(0, 0, 0, $monthname, 10)); ?></td>
        <td><?php echo count($res); ?></td>
        <td><?php echo $in_completed ; ?></td>
        <td><?php echo  count($res)-$in_completed ; ?></td>
      </tr>
		
		<?php
	} ?>
      
	  
    </tbody>
  </table>
<?php } ?>        
      			</div>
				
				
 			</div>	
			
</div>
</div>

<script src="js/skillpiecharts.js" type="text/javascript"></script><script type="text/javascript" src="js/charts.widgets.js"></script>
<?php if(isset($_POST['studentSubmit'])) { ?>
<script type="text/javascript">
FusionCharts.ready(function () {
	
    var csatGauge = new FusionCharts({
        "type": "angulargauge",
        "renderAt": "chart-container",
        "width": "400",
        "height": "250",
        "dataFormat": "json",
            "dataSource": {
    "chart": {
        "caption": "",
        "lowerlimit": "0",
        "upperlimit": "100",
        "lowerlimitdisplay": "0%",
        "upperlimitdisplay": "100%",
        "palette": "1",
        "numbersuffix": "%",
        "tickvaluedistance": "10",
        "showvalue": "0",
        "gaugeinnerradius": "0",
        "bgcolor": "FFFFFF",
        "pivotfillcolor": "333333",
        "pivotradius": "8",
        "pivotfillmix": "333333, 333333",
        "pivotfilltype": "radial",
        "pivotfillratio": "0,100",
        "showtickvalues": "1",
        "showborder": "0"
    },
    "colorrange": {
        "color": [
            {
                "minvalue": "0",
                "maxvalue": "<?php echo $AverageScore+.5 ?>",
                "code": "e44a00"
            },
            {
                "minvalue": "<?php echo $AverageScore+1 ?>",
                "maxvalue": "100",
                "code": "#B0B0B0"
            }
        ]
    },
	
	
	
    "dials": {
        "dial": [
            {
                "value": "<?php echo $AverageScore ?>",
                "rearextension": "15",
                "radius": "100",
                "bgcolor": "333333",
                "bordercolor": "333333",
                "basewidth": "8"
            }
        ]
    }
}
      });

    csatGauge.render();
	
	
});
//alert(csatGauge.dataSource.dials);
</script>
<?php } ?>
 <script type="text/javascript" src="js/charts.js"></script>
<script type="text/javascript" src="js/charts-more.js"></script>
<script src="http://code.highcharts.com/modules/solid-gauge.src.js"></script>

 <script>
 <?php if(isset($_POST['studentSubmit'])) { ?>

 $(function () {

    $('#container_quage').highcharts({

        chart: {
            type: 'solidgauge',
            backgroundColor: 'transparent'
        },

        title: null,

        pane: {
            center: ['50%', '50%'],
            size: '200px',
            startAngle: -160,
            endAngle: 160,
            background: {
                backgroundColor: '#ccc',
                innerRadius: '58%',
                outerRadius: '102%',
                shape: 'arc',
                borderColor: 'transparent'
            }
        },

        tooltip: {
            enabled: false
        },

        // the value axis
        yAxis: {
            min: 0,
            max: <?php echo  $totalpalyingdays; ?>,
            stops: [
                
                [0.9, '#f1c40f'] // green
                ],
            minorTickInterval: null,
            tickPixelInterval: 10,
            tickWidth: 0,
            gridLineWidth: 0,
            gridLineColor: 'transparent',
            labels: {
                enabled: false
            },
            title: {
                enabled: false
            }
        },

        credits: {
            enabled: false
        },

        plotOptions: {
            solidgauge: {
                dataLabels: {
                    y: -45,
                    borderWidth: 0,
                    useHTML: true
                }
            }
        },

        series: [{
            data: [<?php echo ($playeddayCount); ?>],
            dataLabels: {
                format: '<p style="text-align:center;">{y} Days<br>Total <?php echo  $totalpalyingdays; ?> days</p>'
            }
        }]
    });

});
$(document).ready(function(){
	
    chart = new Highcharts.Chart({
        chart: {
			renderTo: 'chartOverallPerformance',
            type: 'column'
        },
        title: {
            text: ''
        },
        
        xAxis: {
            categories: [
			<?php $ini=0; foreach($req_academicMonths as $am){
				$ini++;
				if($ini>1){echo ",";}
				echo "'".$am['monthName']."'";
			} ?>
                
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,tickInterval: 10,max:100,
            title: {
                text: 'Score'
            }
        },
       
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
		credits: {
      enabled: false
  },
        series: [{showInLegend: true,  color:"#9a1e00",
            name: 'Score',dataLabels: {enabled: true},
            data: [
			<?php $ini=0; foreach($req_academicMonths as $am){
				$ini++;
				if($ini>1){echo ",";}
				echo "".getResultByMonth($am['monthNumber'],$OverallPerformance_value)."";
			} ?>
			
			]

        }]
    });
	 chart2 = new Highcharts.Chart({
        chart: {
			renderTo: 'container_2',
            type: 'column'
        },
        title: {
            text: ''
        },
        
        xAxis: {
            categories: [
                <?php $ini=0; foreach($req_academicMonths as $am){
				$ini++;
				if($ini>1){echo ",";}
				echo "'".$am['monthName']."'";
			} ?>
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,tickInterval: 10,max:<?php echo  $totalpalyingdays; ?>,
            title: {
                text: 'Day'
            }
        },
       
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
		credits: {
      enabled: false
  },
        series: [{showInLegend: true,  color:"#ff9900",
            name: 'Days',dataLabels: {enabled: true},
            data: [<?php $ini=0; foreach($req_academicMonths as $am){
				$ini++;
				if($ini>1){echo ",";}
				echo "".getDaysByMonth($am['monthNumber'],$arrPlayedDays)."";
			} ?>]

        }]
    });
	$('#container_memory').highcharts({
		chart: {
            type: 'column'
        },
		
        title: {
            text: '',
            x: -20 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
            categories: [<?php $ini=0; foreach($req_academicMonths as $am){
				$ini++;
				if($ini>1){echo ",";}
				echo "'".$am['monthName']."'";
			} ?>]
        },
        yAxis: {
            min: 0,tickInterval: 10,max:100,
            title: {
                text: 'Score'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#ff3300'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        credits: {
      enabled: false
  },
        series: [{showInLegend: true,   color: '#ff3300',
            name: 'Memory',
			dataLabels: {enabled: true},
            data: [<?php $ini=0; foreach($req_academicMonths as $am){
				$ini++;
				if($ini>1){echo ",";}
				echo "".getResultByMonth_Skill($am['monthNumber'],$month_score,59)."";
			} ?>]
        },{showInLegend: true,   
            name: 'Days',dataLabels: {enabled: true},
            data: [<?php $ini=0; foreach($req_academicMonths as $am){
				$ini++;
				if($ini>1){echo ",";}
				if(isset($arrPlayedDaysinner_skillwise[$am['monthNumber']]['59'])){
				echo "".($arrPlayedDaysinner_skillwise[$am['monthNumber']]['59'])."";
				}
				else{echo "0";}
			} ?>]
        }]
    });
	
	$('#container_vp').highcharts({
		chart: {
            type: 'column'
        },
        title: {
            text: '',
            x: -20 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
            categories: [<?php $ini=0; foreach($req_academicMonths as $am){
				$ini++;
				if($ini>1){echo ",";}
				echo "'".$am['monthName']."'";
			} ?>]
        },
        yAxis: {
            min: 0,tickInterval: 10,max:100,
            title: {
                text: 'Score'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#9a1e00'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        credits: {
      enabled: false
  },
        series: [{showInLegend: true, color:"#9a1e00", 
            name: 'Visual Processing',dataLabels: {enabled: true},
            data: [<?php $ini=0; foreach($req_academicMonths as $am){
				$ini++;
				if($ini>1){echo ",";}
				echo "".getResultByMonth_Skill($am['monthNumber'],$month_score,60)."";
			} ?>]
        },{showInLegend: true,   
            name: 'Days',dataLabels: {enabled: true},
            data: [<?php $ini=0; foreach($req_academicMonths as $am){
				$ini++;
				if($ini>1){echo ",";}
				if(isset($arrPlayedDaysinner_skillwise[$am['monthNumber']]['60'])){
				echo "".($arrPlayedDaysinner_skillwise[$am['monthNumber']]['60'])."";
				}
				else{echo "0";}
			} ?>]
        }]
    });
	$('#container_fa').highcharts({
		chart: {
            type: 'column'
        },
        title: {
            text: '',
            x: -20 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
            categories: [<?php $ini=0; foreach($req_academicMonths as $am){
				$ini++;
				if($ini>1){echo ",";}
				echo "'".$am['monthName']."'";
			} ?>]
        },
        yAxis: {
            min: 0,tickInterval: 10,max:100,
            title: {
                text: 'Score'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#ff9900'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
       credits: {
      enabled: false
  },
        series: [{showInLegend: true, color:"#ff9900" ,
            name: 'Focus and attention',dataLabels: {enabled: true},
            data: [<?php $ini=0; foreach($req_academicMonths as $am){
				$ini++;
				if($ini>1){echo ",";}
				echo "".getResultByMonth_Skill($am['monthNumber'],$month_score,61)."";
			} ?>]
        },{showInLegend: true,   
            name: 'Days',dataLabels: {enabled: true},
            data: [<?php $ini=0; foreach($req_academicMonths as $am){
				$ini++;
				if($ini>1){echo ",";}
				if(isset($arrPlayedDaysinner_skillwise[$am['monthNumber']]['61'])){
				echo "".($arrPlayedDaysinner_skillwise[$am['monthNumber']]['61'])."";
				}
				else{echo "0";}
			} ?>]
        }]
    });
	$('#container_ps').highcharts({
		chart: {
            type: 'column'
        },
        title: {
            text: '',
            x: -20 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
            categories: [<?php $ini=0; foreach($req_academicMonths as $am){
				$ini++;
				if($ini>1){echo ",";}
				echo "'".$am['monthName']."'";
			} ?>]
        },
        yAxis: {
            min: 0,tickInterval: 10,max:100,
            title: {
                text: 'Score'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#05b923'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
       credits: {
      enabled: false
  },
        series: [{showInLegend: true, color:"#05b923",  
            name: 'Problem Solving',dataLabels: {enabled: true},
            data: [<?php $ini=0; foreach($req_academicMonths as $am){
				$ini++;
				if($ini>1){echo ",";}
				echo "".getResultByMonth_Skill($am['monthNumber'],$month_score,62)."";
			} ?>]
        },{showInLegend: true,   
            name: 'Days',dataLabels: {enabled: true},
            data: [<?php $ini=0; foreach($req_academicMonths as $am){
				$ini++;
				if($ini>1){echo ",";}
				if(isset($arrPlayedDaysinner_skillwise[$am['monthNumber']]['62'])){
				echo "".($arrPlayedDaysinner_skillwise[$am['monthNumber']]['62'])."";
				}
				else{echo "0";}
			} ?>]
        }]
    });
	$('#container_L').highcharts({
		chart: {
            type: 'column'
        },
        title: {
            text: '',
            x: -20 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
            categories: [<?php $ini=0; foreach($req_academicMonths as $am){
				$ini++;
				if($ini>1){echo ",";}
				echo "'".$am['monthName']."'";
			} ?>]
        },
        yAxis: {
            min: 0,tickInterval: 10,max:100,
            title: {
                text: 'Score'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#33ccff'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        credits: {
      enabled: false
  },
        series: [{showInLegend: true,  color:"#33ccff",
            name: 'Linguistics',dataLabels: {enabled: true},
            data: [<?php $ini=0; foreach($req_academicMonths as $am){
				$ini++;
				if($ini>1){echo ",";}
				echo "".getResultByMonth_Skill($am['monthNumber'],$month_score,63)."";
			} ?>]
        },{showInLegend: true,   
            name: 'Days',dataLabels: {enabled: true},
            data: [<?php $ini=0; foreach($req_academicMonths as $am){
				$ini++;
				if($ini>1){echo ",";}
				if(isset($arrPlayedDaysinner_skillwise[$am['monthNumber']]['63'])){
				echo "".($arrPlayedDaysinner_skillwise[$am['monthNumber']]['63'])."";
				}
				else{echo "0";}
			} ?>]
        }]
    });
});
 <?php } ?>
 </script>

<script>

$(document).ready(function() {
   //$('#assementTable').DataTable();
	
} );
</script>
