<style>
label{background-color:white !important;}
	
 
 	
.label, .badge {
display: inline-block;
padding: 1% 25%
font-size: 11.844px;
font-weight: bold;
line-height: 14px;
color: #ffffff;
vertical-align: baseline;
white-space: none;
text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.0);
border:1px solid #7F7F7F;
background-color: #FFFFFF !important;
}
.intro-head{
height: 44px;
color: cadetblue;
font-size: 23px;
padding: 22px 5px 42px 5px;
}
.label{
padding: 2% 6% 1% 6%;
margin: 0% 16%;
	}

</style>
<link rel="stylesheet" href="css/jquery-ui.css" />
<style type="text/css" title="currentStyle">
			@import "datatable/media/css/demo_page.css";
			@import "datatable/media/css/demo_table_jui.css";
			@import "datatable/examples/examples_support/themes/smoothness/jquery-ui-1.8.4.custom.css";
		</style>
<style type="text/css">
#status
{
font-size:11px;
margin-left:10px;
}
#email { float: left; }
label.error { float: none; color: red; padding-left:20px; vertical-align: top; }
p { clear: both; }
.success { font-size: 15px; font-weight: bold; color: green;text-align: center;}
em { font-weight: bold; padding-right: 1em; vertical-align: top; }

.dte_header{
 	border-top-left-radius: 5px;
    border-top-right-radius: 5px;
    background-color: #F3F3F3;
    border-bottom: 1px solid #DDDDDD;
    height: 50px;
    left: 0;
   	top: 0;
    width: 100%;
}
</style>


<?php
if(empty($adminid)){
	echo "<script>window.location='index.php?act=login'</script>";
}
$schoolid=$_SESSION['schoolid'];
$get_school = mysql_query("select school_name from schools where id=".$schoolid."");
while($get_school_name = mysql_fetch_array($get_school)){
		$s_name = $get_school_name['school_name'];
	};

?>
 
<?php

function listAllYearMonth($startDate,$endDate){
    $startDate = strtotime(str_replace("-", "/", $startDate));
    $endDate = strtotime(str_replace("-", "/", $endDate));
    $currentDate = $endDate;
    $result = array();
    while ($currentDate >= $startDate) {
        $result[] = date('Y/m',$currentDate);
        $currentDate = strtotime( date('Y/m/01/',$currentDate).' -1 month');
    }
    return $result;
}

function getAcademicYear(){
    $accYear = mysql_query("SELECT startdate,enddate FROM academic_year limit 0,1");
    while($acc = mysql_fetch_array($accYear)){
        $return = array("startDate" => $acc['startdate'],
                        "endDate" => $acc['enddate']);
    }
    return $return;
}

function getAcademicData($aMonth){
    $accDates = getAcademicYear();
    $acdMonths = listAllYearMonth($accDates['startDate'],$accDates['endDate']);
    $return = array();
    foreach ($acdMonths as $acdMonths) {
        list($year,$month) = explode("/", $acdMonths);
        if(in_array($month, $aMonth)){
            $date = "BETWEEN '". $year."-". $month . "-1' AND '". $year."-". $month . "-31'";
            $return[] = $date;
        }
    }
    return $return;
}

    $months = array(
       '6' => 'Jun',
       '7' => 'Jul',
       '8' => 'Aug',
       '9' => 'Sep',
       '10' => 'Oct',
       '11' => 'Nov',
       '12' => 'Dec',
       '1' => 'Jan',
       '2' => 'Feb',
       '3' => 'Mar',
       '4' => 'Apr',
       '5' => 'May'
	   
   );

    ?>
	 <div role="main" class="main">
			<div class="container" style="border:1px solid #7F7F7F;min-height:60%;">
    <div class="intro-head" style="height:10px;"><b style="font-size:1.4em;color:crimson">Skill Toppers</b><div style="float:right;">&nbsp;<span style="text-decoration:underline;font-size:1.4em;"><?php echo $s_name; ?></span></div></div>
    <div class="intro">
        <div class="support-note">
            <div id="main">             
                <form action="" class="cmxform" method="POST" id="skilltoppers" accept-charset="utf-8">               
                    <div class="mainlist">
                        <div class="label">
                            <table align="center" style="background:#FFFFFF !important;color:#3F3C3C;width:55% !important;">
                                <tr>
                                    <td><div class="grade"><label class="fields" for="lname">Grades </label></div></td>
                                    <td>
                                        <div class="grade_details">
                                            <?php
											$school_id = $_SESSION['schoolid'];
                                            $overallskills = mysql_query("SELECT c.id,classname FROM class c inner join skl_class_plan s on c.id = s.class_id and s.school_id='$school_id'");
                                            while($skills = mysql_fetch_array($overallskills)){
                                                $checked = (in_array($skills['id'], $_POST['grade'])) ? "checked" : "";
                                            ?>
                                                <div><input type="checkbox" name="grade[]" <?php echo $checked;?> value="<?php echo $skills['id']; ?>">&nbsp;<?php echo $skills['classname']; ?></div>
                                            <?php } ?>
                                        </div>
                                    </td>
                                    <td>&nbsp;</td>
                                    <td><div class="months"><label class="fields" for="lname">Months </label></div></td>
                                    <td>
                                        <div class="month_details">
                                            <?php
                                                foreach ($months as $key => $value) {
                                                    $checked = (in_array($key, $_POST['months'])) ? "checked" : "";
                                                    echo "<div><input type='checkbox' id='months' name='months[]' ".$checked." value='".$key."'>&nbsp;".$value."</div>";
                                                }
                                            ?>
                                        </div>
                                    </td>
                                    <td>&nbsp;</td>
                                    <td><div class="skills"><label class="fields" for="lname">Skills </label></div></td>
                                    <td>
                                        <div class="skills_details">
                                            <?php
                                            $overallskills = mysql_query("SELECT id,name FROM category_skills where category_id = 1");
                                            while($skills = mysql_fetch_array($overallskills)){
                                                $checked = (in_array($skills['id'], $_POST['skills'])) ? "checked" : "";
                                            ?>
                                                <div><input type="checkbox" name="skills[]" <?php echo $checked;?> value="<?php echo $skills['id']; ?>">&nbsp;<?php echo $skills['name']; ?></div>
                                            <?php } ?>
                                        </div>

                                    </td>
                                    <td>&nbsp;</td>
                                    <td><div class="toprec"><label class="fields" for="toprec">Top </label></div></td>
                                    <td>
                                        <div class="toprec_details">
                                            <select name="toprec" style="width: auto;">
                                            <?php for ($i=1; $i <= 10; $i++) {  
                                                $selected = ($_POST['toprec'] == $i) ? "selected" : "";
                                            ?>
                                                <option <?php echo $selected;?> value="<?php echo $i; ?>" ><?php echo $i; ?></option>                                                
                                            <?php }?>
                                            </select>
                                        </div>
                                    </td>
                                    <td>&nbsp;</td>
                                    <td><div class="acrosssections"><label class="fields" for="acrosssections">Across Sections </label></div></td>
                                    <td>
                                        <div class="across_sections">
                                            <?php  
                                                $selecteda = ($_POST['acrosssections'] == "Yes") ? "selected" : "";
                                                $selectedb = ($_POST['acrosssections'] == "No") ? "selected" : "";												
                                            ?>
                                            <select name="acrosssections" style="width: auto;">
                                                <option <?php echo $selecteda;?> value="Yes" >Yes</option>
                                                <option <?php echo $selectedb;?> value="No" >No</option>												
                                            </select>
                                        </div>
                                    </td>									
                                </tr>
                                <tr><td>&nbsp;</td></tr>
                                <tr>
									<td></td>
									<td></td>
									<td></td>
                                    <td colspan="8" align="center"><div class="searchq"><input type="submit" class="submitbutton" id="search" name="search" value="Display"><div id="err_show"></div></div></td>
                                </tr>
                                <tr><td>&nbsp;</td></tr>
                            </table>
                        </div>
                     </div>
                </form>
            </div>
                <?php if($_POST['search']) {
                	$school_id = $_SESSION['schoolid'];
					function sort_max($a,$subkey) {
						foreach($a as $k=>$v) {
							$b[$k] = strtolower($v[$subkey]);
						}
						arsort($b);
						foreach($b as $key=>$val) {
							$c[] = $a[$key];
						}
						return $c;
					}

					$skills = $_POST['skills'];
					$skills = implode(',', $skills);
	
					$grade = $_POST['grade'];
					//$grade = implode(',', $grade);
	
					$aMonths = $_POST['months'];
					$monthsQuery = getAcademicData($aMonths);
					$vDateRange = "";
	
					if(is_array($monthsQuery)){
						$vDateRange = " AND (gr.lastupdate ".implode(" OR gr.lastupdate ", $monthsQuery).")";
					}

					$acrosssections = $_POST['acrosssections'];
					unset($sections);
					if($acrosssections == "Yes"){
						$sections[] = "All";
					}

					foreach($grade as $class){
					//echo "class: $class";
						if($acrosssections == "No"){
							//echo "inside No";
							unset($sections);							
							$overallsections = mysql_query("SELECT distinct(section) FROM skl_class_section where school_id='$school_id' and class_id = '$class'");
							while($sectionrec = mysql_fetch_array($overallsections)){
								$sections[] = $sectionrec['section'];
							}
						}

					foreach($sections as $section){
					//echo "section: $section";
					if($acrosssections == "Yes")
					{
					//echo "inside if $class";
					$query = "select grade_name, skill_name, name, score, sum(gscore) gscore, id, row, count(*) as daycnt, gu_id, grade_id, section, YearMonth, daycnt as daycntnew, score2, gs_id
							  from
							  (	
								select grade_name, skill_name, concat(fname,' ',lname) as name, score, sum(score) as score2, sum(score)/b.daycnt gscore, id, count(id) as row, b.daycnt, a.gu_id, grade_id, '' as section, a.YearMonth, a.gs_id
								from 
								(
									SELECT grade_name, skill_name, fname, lname, sum(score) as score, id, gu_id, gs_id, grade_id, DATE_FORMAT(lastupdate, '%Y-%m') as YearMonth, g_id
									From
									(
										SELECT cls.classname as grade_name, cs.name as skill_name, fname, lname, avg(gr.game_score) as score, gr.id as id, gr.gu_id, gs_id, grade_id, lastupdate, g_id
										FROM
										game_reports gr
										inner join users usr ON usr.id = gr.gu_id
										inner join class cls ON cls.id = usr.grade_id
										inner join category_skills cs ON cs.id = gr.gs_id
										WHERE 
											cls.id IN (".$class.")
											AND cs.id IN (".$skills.")
											AND usr.sid = " . $school_id . $vDateRange. "
										   group by grade_id, gu_id, g_id, DATE_FORMAT(lastupdate, '%Y-%m')
									) as c group by grade_id, gu_id, g_id, YearMonth order by g_id
								) as a 
								JOIN 
								(
									select count(daycnt) as daycnt, gu_id, gs_id, YearMonth
									From
									(
										select count(distinct(lastupdate)) as daycnt, gu_id, gs_id, DATE_FORMAT(lastupdate, '%Y-%m') as YearMonth
										from game_reports as gr 
										inner join category_skills cs ON cs.id = gr.gs_id
										where cs.id IN (".$skills.") " . $vDateRange."
										group by gu_id, g_id, DATE_FORMAT(lastupdate, '%Y-%m')
									)as d group by gu_id, gs_id, YearMonth
								)as b on a.gu_id = b.gu_id and a.gs_id = b.gs_id and a.YearMonth = b.YearMonth
								
								group by grade_id, skill_name, gu_id, a.gs_id, a.YearMonth
								order by a.gs_id
							  ) as e group by grade_id, skill_name, gu_id, gs_id
								order by gs_id";
								
								//select count(distinct(lastupdate)) as daycnt from game_reports as gr 
								//INNER JOIN users u on u.id = gr.gu_id where u.sid = ".$school_id. $vDateRange."								
					}
					else
					{
					$query = "
							  select grade_name, skill_name, name, score, sum(gscore) gscore, id, row, count(*) as daycnt, gu_id, grade_id, section, YearMonth, daycnt as daycntnew, score2, gs_id
							  from
							  (
									select grade_name, skill_name, concat(fname,' ',lname) as name, score, sum(score) as score2, sum(score)/b.daycnt gscore, id, count(id) as row, b.daycnt, a.gu_id, grade_id, section, a.YearMonth, a.gs_id
									from 
									(							  								
										SELECT grade_name, skill_name, fname, lname, sum(score) as score, id, gu_id, gs_id, grade_id, DATE_FORMAT(lastupdate, '%Y-%m') as YearMonth, g_id, section
										From
										(
											SELECT cls.classname as grade_name, cs.name as skill_name, fname, lname, avg(gr.game_score) as score, gr.id as id, gr.gu_id, gs_id, grade_id, section, lastupdate, g_id
											FROM game_reports gr
											inner join users usr ON usr.id = gr.gu_id
											inner join class cls ON cls.id = usr.grade_id
											inner join category_skills cs ON cs.id = gr.gs_id
											WHERE 
												cls.id IN (".$class.")
												AND cs.id IN (".$skills.")
												AND usr.sid = " . $school_id . $vDateRange.
											"   AND usr.section = '" . $section . "'" .
											" group by grade_id, section, gu_id, g_id, DATE_FORMAT(lastupdate, '%Y-%m')
										) as c group by grade_id, section, YearMonth, gu_id, g_id
									) as a 
									JOIN 
									(
										select count(daycnt) as daycnt, gu_id, gs_id, YearMonth
										From
										(
											select count(distinct(lastupdate)) as daycnt, gu_id, gs_id, DATE_FORMAT(lastupdate, '%Y-%m') as YearMonth
											from game_reports as gr 
											inner join category_skills cs ON cs.id = gr.gs_id
											where cs.id IN (".$skills.") " . $vDateRange."
											group by gu_id, g_id, DATE_FORMAT(lastupdate, '%Y-%m')
										)as d group by gu_id, gs_id, YearMonth
									)as b on a.gu_id = b.gu_id and a.gs_id = b.gs_id and a.YearMonth = b.YearMonth
								
									group by grade_id, skill_name, gu_id, a.gs_id, a.YearMonth
									order by a.gs_id
							  ) as e group by grade_id, skill_name, gu_id, gs_id
								order by gs_id";
								//select count(distinct(lastupdate)) as daycnt from game_reports as gr 
								//INNER JOIN users u on u.id = gr.gu_id where u.sid = ".$school_id. $vDateRange."								
					}
					//unset($overallskillTop);
					$overallskillTop = mysql_query($query);
					$aTop = array();
					//echo "$aTop";

					while($skillTop = mysql_fetch_array($overallskillTop)){
						$aTop[$skillTop['skill_name']][] = array(
													'grade' => $skillTop['grade_name'],
													'section' => $skillTop['section'],													
													'name' => $skillTop['name'],
													'score' => round($skillTop['gscore']/$skillTop['daycnt'], 2),
													'id' => $skillTop['id'],
													);
					}	
					//echo "aTop: ".count($aTop);		
					if((!is_array($aTop)) || (count($aTop) == 0)){
					?>
						<!-- <div class="support-note">
							<div id="skilltoppers_report">
								<table cellpadding="0" cellspacing="0" border="0" class="display" id="skilltoppers">
								<thead>
								<tr>
									<th rowspan="2">Grade</th>
									<th rowspan="2">Skills</th>
									<th rowspan="2">Student name</th>
									<th rowspan="2">Score</th>
								</tr>
								</thead>
								<tbody>
										<td colspan="4" align="center"><b>No Records found</b></td>
									</tbody>
									</table>
								</div>
							</div> -->
						<?php
						//exit();
					}
						//This loop is to initialize grid contents
						$GradeName = "";
						$Skill1 = "";
						$Skill2 = "";
						$Skill3 = "";
						$Skill4 = "";
						$Skill5 = "";
						$SkillUser1 = "";
						$SkillUser2 = "";
						$SkillUser3 = "";
						$SkillUser4 = "";
						$SkillUser5 = "";						
					foreach (array_keys($aTop) as $key) {
						$res = array();
						$res = sort_max($aTop[$key],"score");
						$top = $_POST['toprec'];
						$aTopRec = array();
						$curscore = 0;
						//echo "curscore: ".$key;
						for ($needle=0; $needle < $top; $needle++) { 
							//echo "<pre>";print_r($res[$needle]);echo "</pre>";
							
							if(is_array($res[$needle])){
								$aTopRec[] = $res[$needle];
								$curscore = $res[$needle]['score'];
							}
	
							if(is_array($res[$needle+1])){
								if($curscore == $res[$needle+1]['score']){
									//$aTopRec[] = $res[$needle+1];
									$top++;
								}
							}
							
						}
						$needle = 0;
						//}//by tamil
							?>
					<?php
													
						foreach ($aTopRec as $value) 
						{
							$GradeName = $value[grade];
							
							if($Skill1 == "" || $Skill1 == $key)
							{
								$Skill1 = $key;
								if($SkillUser1 != "")
									$SkillUser1 = $SkillUser1."<br>".$value[name]." - [".$value[score]."]";//.$value[score];
								else
									$SkillUser1 = $value[name]." - [".$value[score]."]";//.$value[score];
							}
							elseif($Skill2 == "" || $Skill2 == $key)
							{
								$Skill2 = $key;
								if($SkillUser2 != "")
									$SkillUser2 = $SkillUser2."<br>".$value[name]." - [".$value[score]."]";//.$value[score];
								else
									$SkillUser2 = $value[name]." - [".$value[score]."]";//.$value[score];
							}
							elseif($Skill3 == "" || $Skill3 == $key)
							{
								$Skill3 = $key;
								if($SkillUser3 != "")
									$SkillUser3 = $SkillUser3."<br>".$value[name]." - [".$value[score]."]";//.$value[score];
								else
									$SkillUser3 = $value[name]." - [".$value[score]."]";//.$value[score];
							 }
							elseif($Skill4 == "" || $Skill4 == $key)
							{
								$Skill4 = $key;
								if($SkillUser4 != "")
									$SkillUser4 = $SkillUser4."<br>".$value[name]." - [".$value[score]."]";//.$value[score];
								else
									$SkillUser4 = $value[name]." - [".$value[score]."]";//.$value[score];
							}
							elseif($Skill5 == "" || $Skill5 == $key)
							{
								$Skill5 = $key;
								if($SkillUser5 != "")
									$SkillUser5 = $SkillUser5."<br>".$value[name]." - [".$value[score]."]";//.$value[score];
								else
									$SkillUser5 = $value[name]." - [".$value[score]."]";//.$value[score];
							}	
							
						}

					}//by tamil1
					if((count($aTop) > 0)){
						if($acrosssections == "Yes"){
							echo "<div align='center'><b>Skill Toppers for $value[grade]</b></div></br>";//$skillTop";
						}
						else{
							echo "<div align='center'><b>Skill Toppers for $value[grade] - $value[section] Section</b></div></br>";//$skillTop";
						}
					}
					// echo "$SkillUser1<br>";
					// echo "$SkillUser2<br>";
					// echo "$SkillUser3<br>";
					// echo "$SkillUser4<br>";
					// echo "$SkillUser5<br>";
					// echo "$Skill1<br>"; 
					// echo "$Skill2<br>"; 
					// echo "$Skill3<br>";
						// echo "$Skill4<br>";
				// echo "$Skill5<br>";						
						?>
						<div class="support-note">
							<div id="skilltoppers_report">
								<table cellpadding="0" cellspacing="0" border="0" class="display" id="skilltoppers">
								<thead>
								<tr>
									<?php if($Skill1 != ""){
									echo "<th rowspan='2'> $Skill1</th>";
									} 
									 if($Skill2 != ""){
									echo "<th rowspan='2'> $Skill2</th>";
									} 
									 if($Skill3 != ""){
									echo "<th rowspan='2'> $Skill3</th>";
									} 
									 if($Skill4 != ""){
									echo "<th rowspan='2'> $Skill4</th>";
									} 
									 if($Skill5 != ""){
									echo "<th rowspan='2'> $Skill5</th>";
									} ?>
								</tr>
								</thead>
								<tbody>
										<?php	
										echo "<tr>"; 
										if($SkillUser1 !="")
										echo "<td>$SkillUser1</td>";
										if($SkillUser2 !="")
										echo "<td>$SkillUser2</td>";
										if($SkillUser3 !="")
										echo "<td>$SkillUser3</td>";
										if($SkillUser4 !="")
										echo "<td>$SkillUser4</td>";
										if($SkillUser5 !="")
										echo "<td>$SkillUser5</td>";										
										echo "</tr>";
								?>
								</tbody>
								</table>
							</div>
						</div>
						<br>
					<?php //} ?>					
            	<?php } ?>
			<?php } ?>        
			<?php } ?>        			    
	    </div>
	    </div>
	    </div>


<script type="text/javascript">
	$(document).ready(function(){
		$("#skilltoppers").submit(function(e) {	
		//var myLength = $("#grade").val().length;
	if((!$('input[name^=grade]:checked').length)) {
        $('#err_show').html("<p style='color:red;'>Please Select Grade</p>");
        //stop the form from submitting
        return false;
    } 
	if((!$('input[name^=months]:checked').length)) {
        $('#err_show').html("<p style='color:red;'>Please Select Month</p>");
        //stop the form from submitting
        return false;
    }	 
	if((!$('input[name^=skills]:checked').length)) {
        $('#err_show').html("<p style='color:red;'>Please Select Skills</p>");
        //stop the form from submitting
        return false;
    }
    
	 
    return true;
});
	});
	
	
</script>
