<?php 
if(empty($adminid)){
	echo "<script>window.location='index.php?act=login'</script>";
}

include_once("skillanalysis_bl.php");
//echo 'hellossss'; exit;
?>


<div class="pageHomePager Dashboardhide mygameshide myreporthide myprofilehide">
   <div class="row">
  <div class="col-lg-12">
                    <h1 class="page-header">Skill Analysis</h1>
                </div>
			</div>	
			<br/>
		
              <div class="row">
      			<div class="col-lg-12 landingContainer">
				
        			<table id="assementTable" class="table table-striped table-bordered table-hover table-condensed">
    <thead>
      <tr>
        <th>S.No.</th>
        <th>Skill</th>
        <th>< 20</th>
		<th>20 - 40</th>
        <th>40 - 60</th>
        <th>60 - 80</th>
        <th> > 80</th>
      </tr>
    </thead>
	
	
	
    <tbody>
	<?php 

	$ini=0; 
	$ini++;
	
	?>	
      <tr>
        <td>1</td>
        <td>Memory</td>
		<td><?php echo $memcount[0]['<=20']; ?></td>
        <td><?php echo $memcount[0]['20-40']; ?></td>
		<td><?php echo $memcount[0]['40-60']; ?></td>
		<td><?php echo $memcount[0]['60-80']; ?></td>
        <td><?php echo $memcount[0]['>80']; ?></td>
      </tr>
	  
	  <tr>
        <td>2</td>
        <td>Visual Processing</td>
		<td><?php echo $vpcount[0]['<=20']; ?></td>
        <td><?php echo $vpcount[0]['20-40']; ?></td>
		<td><?php echo $vpcount[0]['40-60']; ?></td>
		<td><?php echo $vpcount[0]['60-80']; ?></td>
        <td><?php echo $vpcount[0]['>80']; ?></td>
      </tr>
	  
	  <tr>
        <td>3</td>
        <td>Focus & Attention</td>
		<td><?php echo $facount[0]['<=20']; ?></td>
        <td><?php echo $facount[0]['20-40']; ?></td>
		<td><?php echo $facount[0]['40-60']; ?></td>
		<td><?php echo $facount[0]['60-80']; ?></td>
        <td><?php echo $facount[0]['>80']; ?></td>
      </tr>
	  
	  <tr>
        <td>4</td>
        <td>Problem Solving</td>
		<td><?php echo $pscount[0]['<=20']; ?></td>
        <td><?php echo $pscount[0]['20-40']; ?></td>
		<td><?php echo $pscount[0]['40-60']; ?></td>
		<td><?php echo $pscount[0]['60-80']; ?></td>
        <td><?php echo $pscount[0]['>80']; ?></td>
      </tr>
	  
	  <tr>
        <td>5</td>
        <td>Linguistics</td>
		<td><?php echo $licount[0]['<=20']; ?></td>
        <td><?php echo $licount[0]['20-40']; ?></td>
		<td><?php echo $licount[0]['40-60']; ?></td>
		<td><?php echo $licount[0]['60-80']; ?></td>
        <td><?php echo $licount[0]['>80']; ?></td>
      </tr>
      
	  
    </tbody>
  </table>
                    
      			</div>
				
				
 			</div>
			
</div>
<script>
	/* $('.dataTable').DataTable({
		"lengthMenu": [[10,  -1], [10,  "All"]]
		//"scrollX": true
	}); */
	$('#assementTable').DataTable();
	</script>
 