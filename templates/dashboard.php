	
<?php 
if(empty($adminid)){
	echo "<script>window.location='index.php?act=login'</script>";
}
include_once("dashboard_bl.php");


?>
 

<div class="pageHomePager Dashboardhide mygameshide myreporthide myprofilehide">
   <div class="row">
  <div class="col-lg-12">
                    <h1 class="page-header">Dashboard</h1>
                </div>
			</div>	
			
			<div class="row">
      			<div class="col-lg-12">
				<form name="frmStudentReport" id="frmStudentReport" method="post">
				<div class="panel panel-default">
            <div class="panel-body reportChartContainer">
			<p style="text-align:center; font-size: 30px; color: green; font-weight: 500;">The overall Average BSPI for the school is <?php echo round($overallbspi[0]['avgscore'], 2);  ?></p>
			</div>
			</div>
			</form>
			</div>
			</div>
			
			<h1 class="page-header">BSPI Analysis</h1>
			<div class="row">
      			<div class="col-lg-12 landingContainer">
				<div class="row">
				
				 </div>
        			<table id="assementTable" class="table table-striped table-bordered table-hover table-condensed">
    <thead>
      <tr>
        <th>S.No.</th>
		<th>Grade</th>
        <th>Section</th>
		<th>< 20</th>
		<th>20 to 40</th>
		<th>40 to 60</th>
		<th>60 to 80</th>
		<th>> 80</th>
		<th>Total</th>
		
		
        
      </tr>
    </thead>
	<tfoot align="right">
		<tr><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th></tr>
	</tfoot>
    <tbody>
	<?php 
	
	$ini=0; 
	foreach($bspi as $key1=>$val1){
	
	foreach($val1 as $key2=>$val2){
	$total=0;	$ini++;
	
	?>	
      <tr>
        <td><?php echo $ini; ?></td>
        <td><?php echo $key1; ?></td>
		<td><?php echo $key2; ?></td>
		<td><?php if (isset($val2['NI'])) { echo $val2['NI'];$total+=$val2['NI']; } else { echo 0; } ?></td>
		<td><?php if (isset($val2['AB'])) { echo $val2['AB'];$total+=$val2['AB']; } else { echo 0; } ?></td>
		<td><?php if (isset($val2['A'])) { echo $val2['A'];$total+=$val2['A']; } else { echo 0; } ?></td>
		<td><?php if (isset($val2['G'])) { echo $val2['G']; $total+=$val2['G'];} else { echo 0; } ?></td>
		<td><?php if (isset($val2['E'])) { echo $val2['E'];$total+=$val2['E']; } else { echo 0; } ?></td>
		<td><?php echo $total; ?></td>
		 
		
      </tr>
	<?php }
	
	} 
	 	?>
      
	  
    </tbody>
  </table>
                    
      			</div>
				
				
 			</div>
			
			<h1 class="page-header">Classwise Prominent Skill</h1>
			<div class="row">
      			<div class="col-lg-12 landingContainer">
				<div class="row">
				
				 </div>
        			<table id="assementTable1" class="table table-striped table-bordered table-hover table-condensed">
    <thead>
      <tr>
        <th>S.No.</th>
		<th>Grade</th>
        <th>Section</th>
		<th>Prominent Skill</th>        
      </tr>
    </thead>
    <tbody>
	<?php 
	//echo '<pre>'; print_r($results);
	$ini=0; 
	foreach($results as $key1=>$val1){
		
	foreach($val1 as $key2=>$val2){
	
	$ini++;
	
	?>	
      <tr>
        <td><?php echo $ini; ?></td>
        <td><?php echo $key1; ?></td>
		<td><?php echo $key2; ?></td>
		<td><?php echo $val2['value']; ?></td>
		 
		
      </tr>
	<?php 
	
	} 
	}
	 	?>
      
	  
    </tbody>
  </table>
                    
      			</div>
				
				
 			</div>
  
              <!--<div class="row">
      			<div class="col-lg-12 landingContainer">
        			<table class="table table-striped table-bordered table-hover table-condensed">
    <thead>
      <tr class="table-info">
        <th>Period/Day</th>
        <th>Mon</th>
        <th>Tue</th>
        <th>Wed</th>
        <th>Thur</th>
        <th>Fri</th>
        <th>Sat</th>
      </tr>
    </thead>
    <tbody>
	<?php 
	for ($ini=1;$ini<=9;$ini++){ ?>
      <tr>
        <td>Period <?php echo $ini; ?></td>
        <td><?php echo $PeriodList[$ini]['monday_grade']."  ".$PeriodList[$ini]['monday_section']; ?></td>
        <td><?php echo $PeriodList[$ini]['tuesday_grade']."  ".$PeriodList[$ini]['tuesday_section']; ?></td>
        <td><?php echo $PeriodList[$ini]['wednesday_grade']."  ".$PeriodList[$ini]['wednesday_section']; ?></td>
        <td><?php echo $PeriodList[$ini]['thursday_grade']."  ".$PeriodList[$ini]['thursday_section']; ?></td>
        <td><?php echo $PeriodList[$ini]['friday_grade']."  ".$PeriodList[$ini]['friday_section']; ?></td>
        <td><?php echo $PeriodList[$ini]['saturday_grade']."  ".$PeriodList[$ini]['saturday_section']; ?></td>
      </tr>
	<?php } ?>
      
    </tbody>
  </table>
                    
      			</div>
				
				
 			</div>
			<hr/>
			<div class="row">
			<div class="form-group">
			<label class="col-lg-3" style="font-size:20px;">Assessments</label>
			<select class="form-control col-lg-3" style="width:25%" name="ddlAssessmentType" id="ddlAssessmentType">
			<option value="">Select</option>
			<option value="pretest">Pre Assessment</option>
			<option value="posttest1">Post Assessment - I</option>
			<option value="posttest2">Post Assessment - II</option>
			</select>
			</div>
			</div>
			<br/>
			<div class="row">
			<div class="form-group">
			<label class="col-lg-6">Average Score</label>
			<label class="col-lg-4">Classwise Average Score</label>
			<select class="form-control col-lg-2" style="width:16%" name="ddlClassType" id="ddlClassType">
			<option value="">Select</option>
			<?php foreach($GradeList as $grades) {
				echo "<option id='".$grades['id']."' value='".trim(str_replace("Grade",'',$grades['classname']))."'>".trim(str_replace("Grade",'',$grades['classname']))."</option>";
			} ?>
			</select></div>
			</div>
			<br/>
			<div class="row">
			<div class="col-lg-6">
			
			<div class="panel panel-default">
            <div class="panel-body reportChartContainer">
			<div id="container"></div>
			</div>
			</div>
			</div>
			<div class="col-lg-6">
		
			<div class="panel panel-default">
            <div class="panel-body reportChartContainer">
			<div id="container_2"></div>
			</div>
			</div>
			</div>
			
			</div>-->

</div>
<script type="text/javascript" src="js/charts.js"></script>
<script type="text/javascript" src="js/charts-more.js"></script>
 <script>
 function averagescoreChart(assessmentType)
 {
	 $.ajax({
		url: "templates/ajax_dashboard_averagescore.php", 
		dataType:"json",
		data:{assessmentType:assessmentType,schoolid:'<?php echo $_SESSION['schoolid'];?>', Type:"1"},
		success: function(result)
		{		
	 chart = new Highcharts.Chart({
        chart: {
			renderTo: 'container',
            type: 'column'
        },
        title: {
            text: ''
        },
        
        xAxis: {
            categories: result.GradeScoreList_std,
            crosshair: true
        },
        yAxis: {
            min: 0,tickInterval: 10,max:100,
            title: {
                text: 'Average Score'
            }
        },
       credits: {
      enabled: false
  },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{showInLegend: false,  
            name: 'Average Score',
            data: result.GradeScoreList_score

        }]
    });
	}
	}); 
 }
 function classwiesAveragescoreChart(assessmentType,grade)
 {
	 //alert(assessmentType+","+grade);
	  $.ajax({
		url: "templates/ajax_dashboard_averagescore.php", 
		dataType:"json",
		data:{assessmentType:assessmentType,grade:grade,schoolid:'<?php echo $_SESSION['schoolid'];?>', Type:"2"},
		success: function(result)
		{
	 chart2 = new Highcharts.Chart({
        chart: {
			renderTo: 'container_2',
            type: 'column'
        },
        title: {
            text: ''
        },
        
        xAxis: {
            categories: result.GradeScoreList_section,
			crosshair: true
        },
        yAxis: {
            min: 0,tickInterval: 10,max:100,
            title: {
                text: 'Classwise Average Score'
            }
        },
       credits: {
      enabled: false
  },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{showInLegend: false,  
            name: 'Classwise Average Score',
            data: result.GradeScoreList_score

        }]
    });
		}
		});
 }
 $("#ddlAssessmentType").change(function(){
	averagescoreChart($(this).val());
	classwiesAveragescoreChart($(this).val(),$( "#ddlClassType option:selected" ).val()); 
 });
 $("#ddlClassType").change(function(){
	classwiesAveragescoreChart($( "#ddlAssessmentType option:selected" ).val(),$(this).val()); 
 });
 
$(document).ready(function(){
    averagescoreChart('');
	classwiesAveragescoreChart('','');
	
	$('#assementTable1').DataTable( );
	//$('#assementTable').DataTable( );
});


$('#assementTable').DataTable( {
	
	"lengthMenu": [[10,  -1], [10,  "All"]],
	
	"footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // converting to interger to find total
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // computing column Total of the complete result 
            var userTotal = api
                .column( 1 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
				
	    var lesstwentyTotal = api
                .column( 2 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
				
            var fortyTotal = api
                .column( 3 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
				
	     var sixtyTotal = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
				
	     var eightyTotal = api
                .column( 5 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
				
				var moreeightyTotal = api
                .column( 6 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
				
				var sevenTotal = api
                .column( 7 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
				
				var eightTotal = api
                .column( 8 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
			
				
            // Update footer by showing the total with the reference of the column index 
	    $( api.column( 0 ).footer() ).html('Total');
            $( api.column( 1 ).footer() ).html(userTotal);
            $( api.column( 2 ).footer() ).html(lesstwentyTotal);
            $( api.column( 3 ).footer() ).html(fortyTotal);
            $( api.column( 4 ).footer() ).html(sixtyTotal);
            $( api.column( 5 ).footer() ).html(eightyTotal);
			$( api.column( 6 ).footer() ).html(moreeightyTotal);
			$( api.column( 7 ).footer() ).html(sevenTotal);
			$( api.column( 8 ).footer() ).html(eightTotal);
        },
	
	
	
	
});
 </script>