<?php
if(empty($adminid)){
	echo "<script>window.location='index.php?act=login'</script>";
}
?>

<link rel="stylesheet" href="css/jquery-ui.css" />

<style type="text/css" title="currentStyle">
			@import "datatable/media/css/demo_page.css";
			@import "datatable/media/css/demo_table_jui.css";
			@import "datatable/examples/examples_support/themes/smoothness/jquery-ui-1.8.4.custom.css";
		</style>
				<script type="text/javascript" language="javascript" src="js/jquery-1.8.2.js"></script>
		<script type="text/javascript" language="javascript" src="js/jquery.validate.js"></script>
		<script type="text/javascript" language="javascript" src="datatable/media/js/jquery.dataTables.js"></script>
		<script type="text/javascript" src="js/jquery.fancybox.js"></script>
		<link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css" media="screen" />

		<style type="text/css">
#status
{
font-size:11px;
margin-left:10px;
}


#email { float: left; }



label.error { float: none; color: red; padding-left:20px; vertical-align: top; }
p { clear: both; }
.success { font-size: 15px; font-weight: bold; color: green;text-align: center;}
em { font-weight: bold; padding-right: 1em; vertical-align: top; }

.dte_header{
 	border-top-left-radius: 5px;
    border-top-right-radius: 5px;
    background-color: #F3F3F3;
    border-bottom: 1px solid #DDDDDD;
    height: 50px;
    left: 0;
   	top: 0;
    width: 100%;
}
</style>

<script>
$(document).ready(function(){
	
		
	var oTable = $('#schoollist').dataTable({
		
		"bJQueryUI": true,
		"sPaginationType": "full_numbers"
	});

	
	$("#addschool").validate({
		 rules: {
			 sname: {
				 remote: {
					 url: "../ajax_data.php",
					 type: "post"
					
					}
				 }
		 },
		
		messages: {
			sname :{
					remote : 'School name is already taken'
				}
			}
	});
	
    
    
    
});
</script>

<?php

if(isset($_POST['schooleditsubmit'])){

$id = $_POST['schoolid'];
$schoolname =  $_POST['sname'];
$saddress = $_POST['saddress'];
$phone = $_POST['phone'];
$emailid = $_POST['emailid'];
$scode = $_POST['scode'];

mysql_query("update schools set school_name = '$schoolname',school_address='$saddress',phone='$phone',email='$emailid',school_code='$scode' where id = '$id'");
$message = 'School Edited Successfully';
}

if(isset($_POST['schoolsubmit'])){
	
	$schoolname =  $_POST['sname'];
	$saddress = $_POST['saddress'];
	$phone = $_POST['phone'];
	$emailid = $_POST['emailid'];
	$scode = $_POST['scode'];
	
	mysql_query("insert into schools (school_name,school_address,phone,email,school_code) values ('$schoolname','$saddress','$phone','$emailid','$scode')");
	$message = 'School added successfully';
}
if(isset($_POST['oktodel'])){

	$id = $_POST['schoolid'];
	mysql_query("DELETE FROM schools WHERE id='$id'");
	$message = 'School deleted successfully';
}
?>
<?php 
$allschools = mysql_query("select * from schools");
while($allschool = mysql_fetch_array($allschools)){

?>
<script>
$(document).ready(function(){
	$(".fancybox<?php echo $allschool['id']; ?>").fancybox({
		 beforeShow : function() {
			 var schoolname =$("#sname<?php echo $allschool['id']; ?>").val();
			$("#editschool<?php echo $allschool['id']; ?>").validate({
				 rules: {
					 sname: {
						 remote: {
							 url: "../ajax_data.php",
							 type: "post",
							 data: {
								 schoolname: function() {
								 return schoolname;
								 }
							}
						 }
				 	 }
				},
				messages: {
					sname :{
							remote : 'School name is already taken'
						}
					}
			});
		},
	});
	 
});
</script>
					<!-- edit form -->
					
					<div id="editform<?php echo $allschool['id']; ?>" class="editform" style="display: none;">
						<div class="edit-head">Edit Record</div>
						<div class="editlist">
							<form action="" class="cmxform" method="POST" id="editschool<?php echo $allschool['id']; ?>" accept-charset="utf-8">
								<input type="hidden" name="schoolid" value="<?php echo $allschool['id']; ?>">
								<div class="label">
									<div class="firstlabel"><label class="fields" for="sname">School Name </label><label class="mandatory">*</label></div>
									<div class="secondinput"><input type="text" name="sname" value="<?php echo $allschool['school_name']; ?>" title="Please enter school name" class="required" id="sname<?php echo $allschool['id']; ?>">
										<div class="error"></div>
									</div>
								</div>												
								<div class="label">
									<div class="firstlabel"><label class="fields" for="saddress">School Address </label><label class="mandatory">*</label></div>
									<div class="secondinput"><input type="text" name="saddress" value="<?php echo $allschool['school_address']; ?>" title="Please enter school address" class="required" id="saddress"></div>						
								</div>
								<div class="label">
									<div class="firstlabel"><label class="fields" for="phone">Phone Number </label><label class="mandatory">*</label></div>
									<div class="secondinput"><input type="text" name="phone" value="<?php echo $allschool['phone']; ?>" title="Please enter phone number" class="required" id="phone"></div>
								</div>
								<div class="label">
									<div class="firstlabel"><label class="fields" for="emailid">Email id </label><label class="mandatory">*</label></div>
									<div class="secondinput"><input type="text" name="emailid" value="<?php echo $allschool['email']; ?>" title="Please enter email id" class="required email" id="emailid"></div>
								</div>
								<div class="label">
									<div class="firstlabel"><label class="fields" for="scode">School Code </label><label class="mandatory">*</label></div>
									<div class="secondinput"><input type="text" name="scode" value="<?php echo $allschool['school_code']; ?>" title="Please enter school code" class="required" id="scode"></div>
								</div>
								<div class="submitlabel"><input type="submit" class="submitbutton" id="schooleditsubmit" name="schooleditsubmit" value="Update"></div>
							</form>
						</div>
					</div>
					
					<!-- delete form -->
					<div id="deleteform<?php echo $allschool['id']; ?>" class="deleteform" style="display: none;">
						<form action"" method="post">
							<input type="hidden" name="schoolid" value="<?php echo $allschool['id']; ?>">
							<div class="edit-head">Are you sure to delete ? </div>
							<div class="editlist">
								<input class="submitbutton" type="submit" name="oktodel" id="oktodel" value="Delete">
								<input class="submitbutton" type="submit" name="canceltodel" id="canceltodel" value="Cancel">
							</div>
						</form>
					</div>
					<?php 
					}
					?>
					
<header>
	<div class="intro-head">Add School</div>
	<div class="intro cnt_min">
		<div class="support-note">
			<div id="main">				
				<div class="success"><?php echo $message; ?></div>
				<form action="" class="cmxform" method="POST" id="addschool" accept-charset="utf-8">				
						<div class="reqindicates"><label class="top_mandatory">*</label>&nbsp; Indicates required fields</div>
						<div class="label">
							<div class="firstlabel"><label class="fields" for="sname">School Name </label><label class="mandatory">*</label></div>
							<div class="secondinput"><input type="text" name="sname" title="Please enter school name" value="" class="required" id="sname"></div>
							<div id="status1"></div>
						</div>												
						<div class="label">
							<div class="firstlabel"><label class="fields" for="saddress">School Address </label><label class="mandatory">*</label></div>
							<div class="secondinput"><input type="text" name="saddress" title="Please enter school address" value="" class="required" id="saddress"></div>						
						</div>
						<div class="label">
							<div class="firstlabel"><label class="fields" for="phone">Phone Number </label><label class="mandatory">*</label></div>
							<div class="secondinput"><input type="text" name="phone" title="Please enter phone number" value="" class="required" id="phone"></div>
						</div>
						<div class="label">
							<div class="firstlabel"><label class="fields" for="emailid">Email id </label><label class="mandatory">*</label></div>
							<div class="secondinput"><input type="text" name="emailid" value="" title="Please enter email id" class="required email" id="emailid"></div>
						</div>
						<div class="label">
							<div class="firstlabel"><label class="fields" for="scode">School Code</label><label class="mandatory">*</label></div>
							<div class="secondinput"><input type="text" name="scode" value="" title="Please enter school code" class="required" id="scode"></div>
						</div>
						<div class="submitlabel"><input type="submit" class="submitbutton" id="schoolsubmit" name="schoolsubmit" value="Add New School"></div>
					
				</form>
  			</div>
		</div>
	
	<!-- View School List -->		

		<div class="support-note">
			<div id="main">
		<table cellpadding="0" cellspacing="0" border="0" class="display" id="schoollist" >
			
			<thead>
				<tr>
					<th rowspan="2">Name</th>
					<th rowspan="2">Address</th>
					<th rowspan="2">Phone</th>
					<th rowspan="2">Email</th>
					<th rowspan="2">School Code</th>
					<th align="center" colspan="2">Action</th>
				</tr>
				<tr>
					<th align="center">Edit</th>
					<th align="center">Delete</th>
				</tr>
			</thead>
			<tbody>
			
			
				<?php 
				$allschools = mysql_query("select * from schools");
				while($allschool = mysql_fetch_array($allschools)){
				?>
					
					<?php
					echo "<tr>"; 
					echo "<td>$allschool[school_name]</td>";
					echo "<td>$allschool[school_address]</td>";
					echo "<td>$allschool[phone]</td>";
					echo "<td>$allschool[email]</td>";
					echo "<td>$allschool[school_code]</td>";
					echo "<td align='center'><a class='fancybox$allschool[id]' href='#editform$allschool[id]'><img src='images/edit.png' title='Edit Record' width='20' alt='Edit Record'></a></td>";
					echo "<td align='center'><a class='fancybox$allschool[id]' href='#deleteform$allschool[id]'><img src='images/delete.png' title='Delete Record' width='17' alt='Delete Record'></a></td>";
					echo "</tr>";
				}
					?>
			
			</tbody>
			</table>
  			</div>
		</div>

</header>