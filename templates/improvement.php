<style>
label{background-color:white !important;}
thead th{font-weight:normal !important;height:25px;font-family:'open sans';}	
 
 	
.label, .badge {
display: inline-block;
padding: 2px 4px;
font-size: 11.844px;
font-weight: bold;
line-height: 14px;
color: #321F1F;
vertical-align: baseline;
white-space: none;
text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.0);
background-color: #FFFFFF !important;
}
 .intro-head{height: 44px;

color: cadetblue;
font-weight: bold;
font-size: 23px;
padding: 22px 5px 42px 5px;}
</style>

<style type="text/css" title="currentStyle">
@import "datatable/media/css/demo_page.css";
@import "datatable/media/css/demo_table_jui.css";
@import "css/morris.css";
@import "datatable/examples/examples_support/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

	<link class="include" rel="stylesheet" type="text/css" href="../css/jquery.jqplot.min.css" />
    <link type="text/css" rel="stylesheet" href="../syntaxhighlighter/styles/shCoreDefault.min.css" />
    <link type="text/css" rel="stylesheet" href="../syntaxhighlighter/styles/shThemejqPlot.min.css" />
<?php 
if(empty($adminid)){
	echo "<script>window.location='index.php?act=login'</script>";
}

$schoolid=$_SESSION['schoolid'];
$get_school = mysql_query("select school_name from schools where id=".$schoolid."");
while($get_school_name = mysql_fetch_array($get_school)){
		$s_name = $get_school_name['school_name'];
	};
?>
 


 <div role="main" class="main">
			<div class="container" style="border:1px solid #7F7F7F;min-height:60%;">
			<div class="intro-head"><b style="font-size:1.4em;color:crimson">Improvement Decline</b><div style="float:right;">&nbsp;<span style="text-decoration:underline;font-size:1.4em;"><?php echo $s_name; ?></span></div></div>
<div class="intro">
<div class="support-note"><!-- let's check browser support with modernizr -->
<!--span class="no-cssanimations">CSS animations are not supported in your browser</span-->
<!--  ><span class="no-csstransforms">CSS transforms are not supported in your browser</span>
<!--span class="no-csstransforms3d">CSS 3D transforms are not supported in your browser</span-->
<!--  <span class="no-csstransitions">CSS transitions are not supported in your browser</span>
<span class="note-ie">Sorry, only modern browsers.</span>-->
<div id="main">


	<script class="include" type="text/javascript" src="../js/jquery-1.9.0.min.js"></script>
	
	    
<!-- Don't touch this! -->
    <script class="include" type="text/javascript" src="../js/jquery.jqplot.report.js"></script>
    <script type="text/javascript" src="../syntaxhighlighter/scripts/shCore.min.js"></script>
    <script type="text/javascript" src="../syntaxhighlighter/scripts/shBrushJScript.min.js"></script>
    <script type="text/javascript" src="../syntaxhighlighter/scripts/shBrushXml.min.js"></script>
<!-- Additional plugins go here -->
    
   
    <script type="text/javascript" src="../plugins/jqplot.dateAxisRenderer.min.js"></script>
<script type="text/javascript" src="../plugins/jqplot.canvasTextRenderer.min.js"></script>
<script type="text/javascript" src="../plugins/jqplot.canvasAxisTickRenderer.min.js"></script>

  <script class="include" type="text/javascript" src="../plugins/jqplot.barRenderer.min.js"></script>
  <script class="include" type="text/javascript" src="../plugins/jqplot.categoryAxisRenderer.min.js"></script>
  <script class="include" type="text/javascript" src="../plugins/jqplot.pointLabels.min.js"></script>
  <script class="include" type="text/javascript" src="../plugins/jqplot.highlighter.min.js"></script>
  <script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>
<!-- End additional plugins -->
        
<style type="text/css">       
   #chart1 .jqplot-point-label {
  /*border: 1.5px solid #aaaaaa;
  padding: 1px 3px;
  background-color: #eeccdd;*/
  color: #333333;
  font-size:12px;
  
</style>
	
<style type="text/css">
	table tbody td{
		
		
		padding: 5px;
		margin: 4px;
	}
	.monthscroll, .skillscroll {
		height: 150px;
		width: 250px;
		overflow-y: scroll;
	}
	.submitlabel {
		text-align: center;
		margin-top: 5px;
	}
</style>
<script type="text/javascript" language="javascript" src="datatable/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" language="javascript" src="js/raphael-min.js"></script>
<script type="text/javascript" language="javascript" src="js/morris.js"></script>

<script type="text/javascript">
	/*$(document).ready(function() {
		oTable = $('.example ').dataTable({
			"bJQueryUI": true,
			"sPaginationType": "full_numbers"
		});		
	});*/
</script>
<?php 

function listAllYearMonth($startDate,$endDate){
    $startDate = strtotime(str_replace("-", "/", $startDate));
    $endDate = strtotime(str_replace("-", "/", $endDate));
    $currentDate = $endDate;
    $result = array();
    while ($currentDate >= $startDate) {
        $result[] = date('Y/m',$currentDate);
        $currentDate = strtotime( date('Y/m/01/',$currentDate).' -1 month');
    }
    return $result;
}

function getAcademicYear(){
    $accYear = mysql_query("SELECT startdate,enddate FROM academic_year limit 0,1");
    while($acc = mysql_fetch_array($accYear)){
        $return = array("startDate" => $acc['startdate'],
                        "endDate" => $acc['enddate']);
    }
    return $return;
}

function getAcademicData($aMonth){
    $accDates = getAcademicYear();
    $acdMonths = listAllYearMonth($accDates['startDate'],$accDates['endDate']);
    $return = array();
    foreach ($acdMonths as $acdMonths) {
        list($year,$month) = explode("/", $acdMonths);
        if(in_array($month, $aMonth)){
            $date = "BETWEEN '". $year."-". $month . "-1' AND '". $year."-". $month . "-31'";
            $return[] = $date;
        }
    }
    return $return;
}


function getAcademicYear2($vMonth){
    $accDates = getAcademicYear();
    $acdMonths = listAllYearMonth($accDates['startDate'],$accDates['endDate']);
    foreach ($acdMonths as $acdMonths) {
        list($year,$month) = explode("/", $acdMonths);
        if($month == $vMonth){
            return $year;
        }
    }
}

	$school_id = $_SESSION['schoolid'];
	$skillsQuery = "select name,id from category_skills where category_id = 1";
	$objSkills = mysql_query($skillsQuery,$con);
	$months = array(
        '06' => 'Jun',
        '07' => 'Jul',
        '08' => 'Aug',
        '09' => 'Sep',
        '10' => 'Oct',
        '11' => 'Nov',
        '12' => 'Dec',
        '01' => 'Jan',
        '02' => 'Feb',
        '03' => 'Mar',
        '04' => 'Apr',
        '05' => 'May'		
    );
    if(isset($_POST['studentSubmit'])) {
    	$success = true;
    	$params = $_POST;
		//echo $params['month'][0];
		//echo $params['month'][1];
    	if(!empty($params['username'])) {
    		$uid = toGetUserId($params['username']);
    		if(empty($uid)) {
    			$success = false;
    		}
    	}
    	else{
    		$success = false;
    	}
    	$skillType = !empty( $params['skill']) ? join(',', $params['skill']) : '';
    	$skillsTypeQuery = "select name,id from category_skills where id in(".$skillType.") order by id";
		$objSkillsList= mysql_query($skillsTypeQuery);
    }
    function toGetUserId($username) {
    	$uid = '';
		$school_id = $_SESSION['schoolid'];
    	$usersQuery = "select id from users where username='".trim($username)."' and sid = '".$school_id."'";
    	$objusersQuery= mysql_query($usersQuery);
    	while($objresult = mysql_fetch_assoc($objusersQuery)){	
    		$uid =  $objresult['id'];
		}
		return $uid;
    }

    function calculateScore($uid, $month, $year, $skills, $skillArray) {
		$school_id = $_SESSION['schoolid'];
    	$total = 0;
    	$attempt = 0;
    	$studentReport = array();
    	//$skills = join(',', $skills);
    	$date = date('Y-m', strtotime($year.'-'.$month));
    	//$Query = 'SELECT sum(gr.game_score) as score,count(gr.id) as count,gr.gs_id as skid,sk.name as skill FROM game_reports gr join category_skills sk join users u WHERE gr.gu_id = u.id and u.sid = '.$school_id.' and gr.gu_id='.$uid.' and sk.id = gr.gs_id and gr.gs_id in ('.$skills.') and DATE_FORMAT(lastupdate, "%Y-%m")=\''.$date.'\' group by gr.gs_id order by gr.gs_id';
		//$skills_arr[] = explode(',', $skills);
		//print_r(explode(',', $skills));
		
		$monthsQuery = getAcademicData($month);
		//echo "month: $month";
		//echo "monthsQuery: ".$monthsQuery[1];
		$vDateRange = "";

		if(is_array($monthsQuery)){
			$vDateRange = " AND (gr.lastupdate ".implode(" OR gr.lastupdate ", $monthsQuery).")";
		}		
		//echo "vDateRange: $vDateRange";
		foreach ($skills as $value) {
			$Query = 'SELECT sum(game_score) as score, a.gs_id as skid, skill, daycnt 
					From
					(
						SELECT avg(c.game_score) as game_score, c.gs_id, c.skill, c.g_id 
						From
						(
							SELECT avg(gr.game_score) as game_score, gr.gs_id, sk.name as skill, lastupdate, g_id
							FROM game_reports gr
							INNER JOIN category_skills sk ON gr.gs_id = sk.id
							INNER JOIN users u ON gr.gu_id = u.id
							WHERE gr.gu_id = u.id and u.sid = '.$school_id.' and gr.gu_id='.$uid.' and sk.id = gr.gs_id and gr.gs_id in ('.$value.') and DATE_FORMAT(lastupdate, "%Y-%m")=\''.$date.'\' 
							group by lastupdate, gr.g_id order by gr.g_id
						) as c group by g_id order by g_id
					) as a
					JOIN 
					(
						select count(lastupdate) as daycnt, gs_id 
						From
						(
							SELECT COUNT( lastupdate ) AS lastupdate, gr.gs_id, gr.g_id
							From game_reports gr 
							where gu_id='.$uid.' and gr.gs_id in ('.$value.') and DATE_FORMAT(lastupdate, "%Y-%m")=\''.$date.'\'
							group by gr.g_id 
							order by gr.g_id
						) as d group by gs_id
					)as b on a.gs_id = b.gs_id
					group by skid order by skid';		
					//						INNER JOIN users u on u.id = gr.gu_id where u.sid = '.$school_id.' AND DATE_FORMAT(lastupdate, "%Y-%m")=\''.$date.'\'
				     //"select g_id,AVG(game_score) as gscore from game_reports where gu_id='$uid'  and gp_id = '$pid'  and g_id='$sk6_gids'  and DATE_FORMAT(lastupdate, '%Y-%m')='$today' group by lastupdate order by id desc"
			//echo "$Query";
			$query = mysql_query($Query);
			$rowcnt = mysql_num_rows($query);
			$gscore = 0;
			$skillid = 0;
			$skillname = "";
			$daycount = 0;
			while($game_data = mysql_fetch_array($query)) {
			 	//echo "score: ".$game_data['score'];
				$gscore = $gscore + $game_data['score'];
				$skillid = $game_data['skid'];
				$sname = $game_data['skill'];
				$daycount = $game_data['daycnt'];
			}
			if($skillid <> 0)
			{
				$studentReport[$sname]['skid'] = $skillid;
				$studentReport[$sname]['skill'] = $sname;
				$studentReport[$sname]['score'] = number_format((float)$gscore/$daycount, 2, '.', '');
				$studentReport[$sname]['count'] = $daycount;
			}
			//echo "skillid: ".$studentReport[$sname]['skid'];
			//echo "sname: ".$studentReport[$sname]['skill'];
			//echo "score: ".$studentReport[$sname]['score'];			
		}
    	//while($row = mysql_fetch_assoc($query)){
    	//	$studentReport[$row['skill']]= $row;
		//}
		if(!empty($skillArray)) {
			foreach ($skillArray as $key => $value) {
				if(!array_key_exists($value, $studentReport)) {					
					$studentReport[$value]['skid'] = $key;
					$studentReport[$value]['skill'] = $value;
					$studentReport[$value]['score'] = 0;
				}
			}
		}
		
              usort($studentReport, function($a, $b) {
                 return $a['skid'] - $b ['skid'];
               });
		foreach ($studentReport as $key => $value) {
			$total = $total + $value['score'];
			$attempt = $attempt + $value['count'];
		}
		$attempt = count($skills);
		//echo "attempt: $attempt";
		//$studentReport['total'] = $total;
		$studentReport['total'] = $total/$attempt;
		//$studentReport['attempt'] = $attempt;
		$studentReport['attempt'] = $attempt;
		
		return $studentReport;	  	
    }
?>
<form action="" class="cmxform" method="POST" id="stud_performance" accept-charset="utf-8">
	<table align="center"    style="background:#FFFFFF !important;color:#3F3C3C;width:60% !important;border:1px solid #7F7F7F;">
		<tr>
			<td>
				<div class="label">
					<div class="firstlabel"><label class="fields" for="username">Enter Username:</label></div>
					<div class="secondinput">
						<input type="text" value="<?php echo $_POST['username'];?>" name="username" id="username" />
					</div>
				</div>
			</td>
			<td>&nbsp;</td>
			<td><div class="months"><label class="fields" for="lname">Months </label></div></td>
            <td>
                <div class="month_details">
					<?php 
						foreach ($months as $key => $month) { 
						$checked = (in_array($key, $_POST['month'])) ? "checked" : "";
					?>
							<div><input type="checkbox" name="month[]"   <?php echo $checked;?> value="<?php echo $key;?>" class="month" /><?php echo $month;?></div>
				    <?php } ?>
                </div>
            </td>
            <td>&nbsp;</td>
            <td><div class="skills"><label class="fields" for="lname">Skills </label></div></td>
            <td>
                <div class="skills_details">
                    <?php while($objresult = mysql_fetch_object($objSkills)){ 
                    	$checked = (in_array($objresult->id, $_POST['skill'])) ? "checked" : "";
                    ?>	
							<div><input type="checkbox" name="skill[]"   <?php echo $checked;?> value="<?php echo $objresult->id;?>" class="skill" /><?php echo $objresult->name;?></div>
					<?php } ?>
                </div>

            </td>

			
		</tr>
	</table>	
	<div class="submitlabel"><input type="submit" class="submitbutton" id="editsubmit" name="studentSubmit" value="Display"><div id="err_show"></div></div></div>
</form>
<?php if(isset($_POST['studentSubmit'])) {
$skillSetsLabel='';
		if($success) {
		 ?>
		
		<table class="display example" id="example" style="font-family: 'open sans';">
			<thead>
				<tr>
					<th>Month</th>
					<?php 
						$skillArray = array();
						while($objresult = mysql_fetch_object($objSkillsList)){
							$skillArray[$objresult->id] =  $objresult->name;
						}
						$skill_arr=array();
						$skillColor_arr=array();
						if(!empty($skillArray)) {
							foreach ($skillArray as $key => $skill) {
								$skill_arr[]=$skill;
								switch ($skill) {
								  case "Memory":
									$skillColor_arr[]='#de2300';
									break;
								  case "Visual Processing":
									$skillColor_arr[]='#983818';
									break;
								  case "Problem Solving":
									$skillColor_arr[]='#178a1b';
									break;
								  case "Focus And Attention":
									$skillColor_arr[]='#ffac0f';
									break;
								  case "Linguistics":
									$skillColor_arr[]='#008c92';
									break;
								}
							}
						}	
						$skillSets = implode('","',$skill_arr);
						$skillSetsLabel='';
						for($inc=0,$cnt=count($skillColor_arr);$inc<$cnt;$inc++) {
						 $skillSetsLabel=$skillSetsLabel."'".$skill_arr[$inc]."',";
						}
						if($skillSetsLabel!='')
						{
						 $skillSetsLabel=substr($skillSetsLabel,0,strlen($skillSetsLabel)-1);
						}
						
						if($skillSets!=''){ $skillSets='"'.$skillSets.'"'; }
						$skillColors = implode('","',$skillColor_arr);
						if($skillColors!=''){ $skillColors='"'.$skillColors.'"'; }
					?>
					<?php if(!empty($skillArray)) {
							foreach ($skillArray as $key => $skill) { ?>
								<th><?php echo $skill;?></th>
						<?php }						
					} ?>
					
					<th>Total</th>
					<th>BSPI</th>
				</tr>
			</thead>
			<tbody>
				<?php 
					$grapKey = '';
					$jsonArray = array();
					$tot_resVal=array();
					foreach ($skillArray as $key => $value) {
						$grapKey .= "'".$value."',";
					}
					$monthNames='';
					if(!empty($params['month'])) { 
						foreach ($params['month'] as $key => $month) {
							//echo "$month";
                            $year = getAcademicYear2($month);
							$data = calculateScore($uid,$month,$year,$params['skill'],$skillArray);
							$total = $data['total'];
							$attempt = $data['attempt'];
							unset($data['total']);
							unset($data['attempt']);
							 ?>
							<tr>
								
								<?php 
									 if(!empty($data)) {
										$monthNames=$monthNames."'".$months[$month]."',"; ?>
										<td><?php echo $months[$month] ."-".$year;?></td>
										<?php
										$i =1;
										$looptotal = 0;
										foreach ($data as $key => $value) {
												$jsonArray[$months[$month]][$key] = number_format((float)$value['score'], 2, '.', '');
												$looptotal = $looptotal + number_format((float)$value['score'], 2, '.', '');
												$tot_resVal[$i][]=number_format((float)$value['score'], 2, '.', '');
											?>
											<td>
												<?php echo number_format((float)$value['score'], 2, '.', '');?>
											</td>	
										<?php $i++;} ?>
									<td>
										<?php echo number_format((float)$looptotal, 2, '.', '');?>
									</td>
									<td>
									<?php 
										$attempt = ($attempt == 0) ? 1 : $attempt;
										echo number_format((float)$looptotal/$attempt, 2, '.', '');
										$bspi_graph[] = number_format((float)$looptotal/$attempt, 2, '.', '')
									?>
									</td>
								<?php } ?>
							</tr>

						<?php } ?>



				<?php 
				} ?>
				<?php
				 $monthNames = rtrim($monthNames,","); 
				 $bsp = "";
				 foreach($bspi_graph as $bspi_val){
					$bsp .="".$bspi_val.",";
					 }
				$bspi_values = rtrim($bsp,",");
				 
				 ?>
		<script type="text/javascript">
$(document).ready(function(){  
	var tot_resVal = new Array();
	<?php
	$tot_resValReset=array();
	$i=0;
	foreach ($tot_resVal as $key => $value) {
		for($j=0;$j<count($value);$j++){
			$tot_resValReset[$j][]=$value[$j];
		}
		$i++;	
	}	
	
	$i=0;
	foreach ($tot_resValReset as $key => $value) {
	$resval=implode(",",$value);
	?>
	tot_resVal[<?php echo $i; ?>]=[<?php echo $resval;?>];
	<?php
	$i++;	
	}
	?>
	
    $('#barchart').highcharts({
        chart: {
            type: 'line'
        },
        title: {
            text: 'Improvement Decline'
        },
         
        xAxis: {
			gridLineColor: '#7F7F7F',
            categories: [<?php echo $monthNames; ?>]
        },
       
        yAxis: {
				min:0,
				max:100,
			  minorTickInterval: 'auto',
			  lineColor: '#000',
			  lineWidth: 1,
			  tickWidth: 1,
			  tickColor: '#000',
			  labels: {
				 style: {
					color: '#000',
					font: '11px Trebuchet MS, Verdana, sans-serif'
				 }
      },
      title: {
		  text: 'BSPI Report (%)' ,
         style: {
            color: '#333',
            fontWeight: 'bold',
            fontSize: '12px',
            fontFamily: 'Trebuchet MS, Verdana, sans-serif'
         }
      }
   },
        tooltip: {
            valueSuffix: '%'
        },
        legend: {
      itemStyle: {
         font: '9pt Trebuchet MS, Verdana, sans-serif',
         color: 'black'

      },
      itemHoverStyle: {
         color: '#039'
      },
      itemHiddenStyle: {
         color: 'gray'
      }
   },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: [{
            name: 'Months',
            data: [<?php echo $bspi_values; ?>]
        }]
    });
    
     
    
});
		</script>
		
			</tbody>
		</table>
<table><tr><td><div style="width:956px;margin-top:5%;float:left;margin-left:14%;" id='barchart'></div></td></tr>		
		</table>
		
	<?php } else{ ?>
		<p style="margin-left:42%;"><b>Student could not be found</b></p>
	<?php } } ?>	
	
</div>
</div>
</div>		 
					 
			</div>
	</div>
<script type="text/javascript">
	$(document).ready(function(){
		$("#stud_performance").submit(function(e) {	
		//var myLength = $("#grade").val().length;
	 if($('#username').val() == ""){
		 $('#err_show').html("<p style='color:red;'>Please Enter Username</p>");
		  return false;
		 }
	if((!$('input[name^=month]:checked').length)) {
        $('#err_show').html("<p style='color:red;'>Please Select Month</p>");
        //stop the form from submitting
        return false;
    }	 
	if((!$('input[name^=skill]:checked').length)) {
        $('#err_show').html("<p style='color:red;'>Please Select Skills</p>");
        //stop the form from submitting
        return false;
    }
    
	 
    return true;
});
	});
	
	
</script>
