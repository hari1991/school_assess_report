<?php
if(empty($adminid)){
	echo "<script>window.location='index.php?act=login'</script>";
}
?>

<link rel="stylesheet" href="../../school_admin/templates/css/jquery-ui.css" />

<style type="text/css" title="currentStyle">
			@import "../../school_admin/templates/datatable/media/css/demo_page.css";
			@import "../../school_admin/templates/datatable/media/css/demo_table_jui.css";
			@import "../../school_admin/templates/datatable/examples/examples_support/themes/smoothness/jquery-ui-1.8.4.custom.css";
		</style>
				<script type="text/javascript" language="javascript" src="../../school_admin/templates/js/jquery-1.8.2.js"></script>
		<script type="text/javascript" language="javascript" src="../../school_admin/templates/js/jquery.validate.js"></script>
		<script type="text/javascript" language="javascript" src="../../school_admin/templates/datatable/media/js/jquery.dataTables.js"></script>
		<script type="text/javascript" src="../../school_admin/templates/js/jquery.fancybox.js"></script>
		<link rel="stylesheet" type="text/css" href="../../school_admin/templates/css/jquery.fancybox.css" media="screen" />

		<style type="text/css">
#status
{
font-size:11px;
margin-left:10px;
}


#email { float: left; }



label.error { float: none; color: red; padding-left:20px; vertical-align: top; }
p { clear: both; }
.success { font-size: 15px; font-weight: bold; color: green;text-align: center;}
em { font-weight: bold; padding-right: 1em; vertical-align: top; }

.dte_header{
 	border-top-left-radius: 5px;
    border-top-right-radius: 5px;
    background-color: #F3F3F3;
    border-bottom: 1px solid #DDDDDD;
    height: 50px;
    left: 0;
   	top: 0;
    width: 100%;
}
</style>
<script type="text/javascript">
$(document).ready(function(){

//select category
	
	
	$("#classid").change(function()
	{
		var classid=$(this).val();
		alert(classid);
		var dataString = 'classid='+ classid;		
		$.ajax
		({
			type: "POST",
			url: "../ajax_data.php",
			data: dataString,
			cache: false,
			success: function(html)
			{
				$("#section").html(html);
			} 
		});
	});

	//select no.of times
	$("#section").change(function(){
		var section=$(this).val();
		var classid=$('#classid').val();
		
		var dataString = 'section_id='+ section+'&class_id='+classid;		
		$.ajax
		({
			type: "POST",
			url: "../ajax_data.php",
			data: dataString,
			cache: false,
			success: function(html)
			{
				$("#limit").val(html);
			} 
		});
	});
});	
	
	</script>
<?php

if(isset($_POST['limitsubmit'])){
	
	$classid = $_POST['classid'];
	 $limit =  $_POST['limit'];
	$sectionid = $_POST['section'];	
	// echo "$classid";
	// echo "$limit";
	// echo "$sectionid";
	// $sql = "insert into user_section_limit (section,userlimit) values ('$section','$limit')";
	//echo $sql ."<br>";
	
	// mysql_query($sql);
	// $message = 'Section user limit added successfully';
	
	
	
	//foreach($_POST['limit'] as $limit)
		 //{
		$sql = "select * from user_section_limit where class_id='$classid' and section='$sectionid' ";
		
	    $result=mysql_query($sql);
	    $rowCount = mysql_num_rows($result);
	
		if($rowCount<=0)
		{ 
		    $sql2="insert into user_section_limit(class_id,section,userlimit)values('$classid','$sectionid','$limit')";		
		
			$resultinsert=mysql_query($sql2);
			$message = 'Section user limit Added Successfully';
			
		}
		else
		{
			$sql3="update user_section_limit set userlimit = '$limit' where class_id = '$classid' and section = '$sectionid'";			
			$resultinsert=mysql_query($sql3);
			
			$message = 'Section user limit Updated Successfully';
		}
		//}
	//$message = 'Section user limit added successfully';
}

?>

<header>
	<div class="intro-head">Add User Limit</div>
	<div class="intro cnt_min">
		<div class="support-note">
			<div id="main">				
				<div class="success"><?php echo $message; ?></div>
				<form action="" class="cmxform" method="POST" id="add" accept-charset="utf-8">				
						<div class="reqindicates"><label class="top_mandatory">*</label>&nbsp; Indicates required fields</div>
						
						 <div class="label">
							<div class="firstlabel"><label class="fields" for="class">Class </label><label class="mandatory">*</label></div>
							<div class="secondinput"><select name="classid" id="classid" class="classid required" >
									<option value="">--Select Class--</option>
									<?php 
									$class = mysql_query("select id,classname from class ");
									while ($classinarr =mysql_fetch_array($class)) {
										
									?>
										<option value="<?php echo $classinarr['id']; ?>"><?php echo $classinarr['classname'];?></option>
									<?php }?>
								</select></div>
							<div id="status1"></div>
						</div> 
						
						<div class="label">
							<div class="firstlabel"><label class="fields" for="academic">Section </label><label class="mandatory">*</label></div>
							<div class="secondinput"><select name="section" id="section" class="required" >
									<option value="">--Select Section--</option>
									<?php
									$section = mysql_query("select distinct(section) from skl_class_section where school_id=".$_SESSION['schoolid']);
									while ($sectioninarr = mysql_fetch_array($section)) {										
									?>
										<option value="<?php echo $sectioninarr['section']; ?>"><?php echo $sectioninarr['section'];?></option>
									<?php }?>
								</select></div>
							<div id="status1"></div>
						</div>
                        <div class="label">
							<div class="firstlabel"><label class="fields" for="limit">User Limit </label><label class="mandatory">*</label></div>
							<div class="secondinput"><input type="text" name="limit" title="Please enter user limit" value="" class="required" id="limit"></div>
							<div id="status1"></div>
						</div>                                               
						<div class="submitlabel"><input type="submit" class="submitbutton" id="limitsubmit" name="limitsubmit" value="Add User Limit"></div>
					
				</form>
  			</div>
		</div>

</header>