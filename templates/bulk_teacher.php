<?php 
if(empty($adminid)){
	echo "<script>window.location='index.php?act=login'</script>";
}
require_once 'excelreader/reader.php';
date_default_timezone_set('Asia/Calcutta');
require_once 'phpexcel/Classes/PHPExcel.php';
require_once 'phpexcel/Classes/PHPExcel/IOFactory.php';
require_once 'phpexcel/Classes/PHPExcel/Writer/Excel5.php';
//error_reporting(E_ALL);
?>

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.validate.js"></script>
<style type="text/css">
#status
{
font-size:11px;
margin-left:10px;
}
p { clear: both; }
.success { font-size: 15px; font-weight: bold; color: green;text-align: center;}
em { font-weight: bold; padding-right: 1em; vertical-align: top; }
.label { padding: 0 0 15px }
</style>

<script>
$(document).ready(function(){
	
	
	 var container = $('div.error');
    $("#addschoolclasses").validate();
    $("#editschoolclasses").validate();
	 
	//select category
	$("#grades").change(function(){
		var grade_id=$(this).val();
		var dataString = 'grade_id='+ grade_id;		
		$.ajax
		({
			type: "POST",
			url: "../ajax_data.php",
			data: dataString,
			cache: false,
			success: function(html)
			{
				$(".grades").html(html);
			} 
		});
	});
	$("#schools").change(function(){
		var schoolid=$(this).val();
		var dataString = 'schoolid='+ schoolid;		
		$.ajax
		({
			type: "POST",
			url: "../ajax_data.php",
			data: dataString,
			cache: false,
			success: function(html)
			{
				$(".smailid").val(html);
			} 
		});
	});
});
</script>

<?php 

if(isset($_POST['teachersubmit']))
{
	$data = new Spreadsheet_Excel_Reader();
	$data->setOutputEncoding('CP1251');
	$path = '';
	$path	= $_FILES['uploadeteacher']['tmp_name'];
	$data->read($path);
	for ($i=2; $i<=$data->sheets[0]['numRows']; $i++)
	{
		$id = $data->sheets[0]['cells'][$i][1];
		$teacher = $data->sheets[0]['cells'][$i][2];
	
		if($teacher != ''){
			$teacherssql = "UPDATE teachers SET classname='$class' WHERE id=$id";
			mysql_query($teacherssql);
			$message = 'Teachers Successfully edited';
		}
	}
}
?>
<header>
	<div class="intro-head">Bulk Upload Teacher</div>
	<div class="intro cnt_min">
		<div class="support-note">
			<div id="main">
				<div class="success"><?php if(isset($_GET['msg']) == 1){ echo "Successfully uploaded"; }?></div>
				<form enctype="multipart/form-data" action="templates/phpexcel/Tests/excelteacher.php" class="cmxform" method="POST" id="addclasses" accept-charset="utf-8">
					<table width="520px" class="bulk_classes" border="0" cellspacing="4" cellpadding="6">	
                    <tr>
							<td width="181px" valign="top">School</td>
							<td width="250px" valign="top">
								<select name="schools" id="schools" class="required" >
									<option value="">--Select School--</option>
									<?php 
									$school = mysql_query("select id,school_name from schools");
									while ($schoolinarr =mysql_fetch_array($school)) {
										
									?>
										<option value="<?php echo $schoolinarr['id']; ?>"><?php echo $schoolinarr['school_name'];?></option>
									<?php }?>
								</select>
							</td>							
						</tr>					
						<tr class="uploadclasses">
							<td valign="top" >Upload Teacher</td>
							<td colspan="3" valign="top">
								<div style="width: 150px;"><input type="file" name="uploadteacher" size="10" id="uploadteacher" value="" class="required"></div>
							</td>
						</tr>
						
						<tr>
							<td></td>
							<td colspan="3" align="left"><input class="submitbutton" type="submit" name="teachersubmit" id="teachersubmit" value="Bulk Upload Teacher"></td>
						</tr>
					</table>
					<div class="temp_down"><a href="school_teachers.xls">Click here to download template</a></div>
				</form>
			</div>
  		</div>
	</div>
</header>
<header>
	<div class="intro-head">Bulk Edit Teacher</div>
	<div class="intro cnt_min">
		<div class="support-note">
			<div id="main">
				<div class="success"><?php echo $message; ?></div>			
				<form enctype="multipart/form-data" action="" class="cmxform" method="POST" id="editschoolclasses" accept-charset="utf-8">
					<div class="reqindicates"><label class="top_mandatory">*</label>&nbsp; Indicates required fields</div>
					<table class="bulk_classes" border="0" cellspacing="4" cellpadding="4">                    
						<tr>
							<td valign="top">Upload Teacher</td>
							<td>
								<div style="width: 150px;"><input type="file" size="10" name="uploadeteacher" id="uploadeteacher" value="" class="required"></div>
							</td>
						</tr>
						<tr class="uploadclasses">
							<td></td>
							<td align="center"><input  class="submitbutton" type="submit" name="schoolesubmit" id="schoolesubmit" value="Bulk Edit Teacher"></td>
						</tr>
					</table>
					<div class="temp_down"><a href="edit_teachers.xls">Click here to download template</a></div>
				</form>
				
  			</div>
  		</div>
	</div>
</header>