<?php 
if(empty($adminid)){
	echo "<script>window.location='index.php?act=login'</script>";
}
require_once 'excelreader/reader.php';
date_default_timezone_set('Asia/Calcutta');
require_once 'phpexcel/Classes/PHPExcel.php';
require_once 'phpexcel/Classes/PHPExcel/IOFactory.php';
require_once 'phpexcel/Classes/PHPExcel/Writer/Excel5.php';
//error_reporting(E_ALL);
?>

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.validate.js"></script>
<style type="text/css">
#status
{
font-size:11px;
margin-left:10px;
}
p { clear: both; }
.success { font-size: 15px; font-weight: bold; color: green;text-align: center;}
em { font-weight: bold; padding-right: 1em; vertical-align: top; }
.label { padding: 0 0 15px }
</style>

<script>
$(document).ready(function(){
	
	
	 var container = $('div.error');
    $("#addschoolusers").validate();
    $("#editschoolusers").validate();
	 
	//select category
	$("#grades").change(function(){
		var grade_id=$(this).val();
		var dataString = 'grade_id='+ grade_id;		
		$.ajax
		({
			type: "POST",
			url: "../ajax_data.php",
			data: dataString,
			cache: false,
			success: function(html)
			{
				$(".grades").html(html);
			} 
		});
	});
	$("#schools").change(function(){
		var schoolid=$(this).val();
		var dataString = 'schoolid='+ schoolid;		
		$.ajax
		({
			type: "POST",
			url: "../ajax_data.php",
			data: dataString,
			cache: false,
			success: function(html)
			{
				$(".smailid").val(html);
			} 
		});
	});
});
</script>

<?php 
$grade = mysql_query("select id,classname from class");
$school = mysql_query("select id,school_name from schools");

if(isset($_POST['schoolesubmit']))
{
	$data = new Spreadsheet_Excel_Reader();
	$data->setOutputEncoding('CP1251');
	$path = '';
	$path	= $_FILES['uploadeusers']['tmp_name'];
	$data->read($path);
	for ($i=2; $i<=$data->sheets[0]['numRows']; $i++)
	{
	$username = $data->sheets[0]['cells'][$i][1];
	$password = $data->sheets[0]['cells'][$i][2];
	$passhash = md5($password);
	if($password != '')
		$pass = "password = '$passhash',";
	$fname = $data->sheets[0]['cells'][$i][3];
	if($fname != '')
		$firstname = "fname = '$fname',";
	$initial = $data->sheets[0]['cells'][$i][4];
	if($initial != '')
		$ini = "initial = '$initial',";
	$lname = $data->sheets[0]['cells'][$i][5];
	if($lname != '')
		$lastname = "lname = '$lname',";
	$address = $data->sheets[0]['cells'][$i][6];
	if($address != '')
		$add = "address = '$address',";
	$mobile = $data->sheets[0]['cells'][$i][7];
	if($mobile != '')
		$mob = "mobile = '$mobile',";
	$email = $data->sheets[0]['cells'][$i][8];
	if($email != '')
		$emails = "email= '$email',";
	if($username != ''){
	$string = $pass.$firstname.$ini.$lastname.$add.$mob.$emails;
	$string = substr($string,0,-1);
	$euserssql = "UPDATE users
	SET $string
	WHERE username='$username'";
	mysql_query($euserssql);
	$message = 'Users Successfully edited';
	}
	}
	
	
}
?>
<header>
	<div class="intro-head">Bulk Upload Users</div>
	<div class="intro cnt_min">
		<div class="support-note">
			<div id="main">
				<div class="success"><?php if(isset($_GET['msg']) == 1){ echo "Successfully uploaded"; }?></div>
				<form enctype="multipart/form-data" action="templates/phpexcel/Tests/excelexport.php" class="cmxform" method="POST" id="addschoolusers" accept-charset="utf-8">
					<div class="reqindicates"><label class="top_mandatory">*</label>&nbsp; Indicates required fields</div>
					<table width="520px" class="bulk_users" border="0" cellspacing="4" cellpadding="6">
						<tr>
							<td width="181px" valign="top">School</td>
							<td width="250px" valign="top">
								<select name="schools" id="schools" class="required" >
									<option value="">--Select School--</option>
									<?php 
									
									while ($schoolinarr =mysql_fetch_array($school)) {
										
									?>
										<option value="<?php echo $schoolinarr['id']; ?>"><?php echo $schoolinarr['school_name'];?></option>
									<?php }?>
								</select>
							</td>
							<td width="100px" valign="top">Email</td>
							<td width="250px" valign="top"><input type="text" id="smailid" name="smailid" class="smailid"/></td>
						</tr>
						<tr>
							<td valign="top">Class</td>
							<td valign="top">
								<select name="grades" id="grades" class="required">
									<option value="">--Select Class--</option>
									<?php 
									while ($gradeinarr=mysql_fetch_array($grade)) {
									?>
									<option value="<?php echo $gradeinarr['id'];?>"><?php echo $gradeinarr['classname'];?></option>
									<?php }?>
								</select>
							</td>
							<td valign="top">Plan</td>
							<td valign="top">
								<select name="plans" id="plans" class="grades required">
									<option value="" selected="selected">--Select Plan--</option>
								</select>
							</td>
						</tr>
						<tr class="uploadusers">
							<td valign="top" >Upload Users</td>
							<td colspan="3" valign="top">
								<div style="width: 150px;"><input type="file" name="uploadusers" size="10" id="uploadusers" value="" class="required"></div>
							</td>
						</tr>
						
						<tr>
							<td></td>
							<td colspan="3" align="left"><input class="submitbutton" type="submit" name="schoolsubmit" id="schoolsubmit" value="Bulk Upload Users"></td>
						</tr>
					</table>
					<div class="temp_down"><a href="school_users.xls">Click here to download template</a></div>
				</form>
			</div>
  		</div>
	</div>
</header>
<header>
	<div class="intro-head">Bulk Edit Users</div>
	<div class="intro cnt_min">
		<div class="support-note">
			<div id="main">
				<div class="success"><?php echo $message; ?></div>			
				<form enctype="multipart/form-data" action="" class="cmxform" method="POST" id="editschoolusers" accept-charset="utf-8">
					<div class="reqindicates"><label class="top_mandatory">*</label>&nbsp; Indicates required fields</div>
					<table class="bulk_users" border="0" cellspacing="4" cellpadding="4">
						<tr>
							<td valign="top">Upload Users</td>
							<td>
								<div style="width: 150px;"><input type="file" size="10" name="uploadeusers" id="uploadeusers" value="" class="required"></div>
							</td>
						</tr>
						<tr class="uploadusers">
							<td></td>
							<td align="center"><input  class="submitbutton" type="submit" name="schoolesubmit" id="schoolesubmit" value="Bulk Edit Users"></td>
						</tr>
					</table>
					<div class="temp_down"><a href="edit_users.xls">Click here to download template</a></div>
				</form>
				
  			</div>
  		</div>
	</div>
</header>