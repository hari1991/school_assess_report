	
<?php 
if(empty($adminid)){
	echo "<script>window.location='index.php?act=login'</script>";
}
include_once("student_performance_report_bl.php");

?>
 <?php $AverageScore=$skillQuotient_value; ?>

<div class="pageHomePager Dashboardhide mygameshide myreporthide myprofilehide">
   <div class="row">
  <div class="col-lg-12">
                    <h1 class="page-header">Student Report</h1>
                </div>
			</div>	
			<div class="row">
      			<div class="col-lg-12">
				<form name="frmStudentReport" id="frmStudentReport" method="post">
				<div class="panel panel-default">
            <div class="panel-body reportChartContainer">
			
			<div class="col-lg-4">
				<div class="row">
				<label class="col-lg-6">Enter Username</label>
				<input type="text" class="form-control col-lg-6" style="width:50%" value="<?php if(isset($params['txtStudentName']) && $params['txtStudentName']!=''){echo $params['txtStudentName'];} ?>" name="txtStudentName" id="txtStudentName" />
			</div>
			</div>
			<div class="col-lg-1"></div>
			<div class="col-lg-1">
			<div class="row">
			<input  type="submit" name="studentSubmit" class="btn btn-success" id="btnSubmit" value="Submit" />
			</div>
				</div>
				
			<div class="col-lg-1">
			<div class="row">
			<input  type="button" onclick="javascript:window.location.href=window.location.href;" name="btnSubmit" class="btn btn-warning" id="btnSubmit" value="Reset" />
			</div>
				</div>
				
				
				</div>
				</div>
				</form>
				
				</div>
				</div>
				
			
   <hr/>
   <div class="col-lg-12" <?php if(isset($params['txtStudentName']) && $uid==''){?> <?php } else{?> style="display:none;" <?php } ?>>
				<div class="row">
				<div class="alert alert-danger">Sorry, no user found for given username</div>
				</div>
				</div>
   <div id="StudentReportResult" <?php if(($uid=='')){ ?>  style="display:none;" <?php } ?> >
              <div class="row">
      			<div class="col-lg-6">
                	<label>Skill Quotient: &nbsp; <b><?php echo $AverageScore ?> %</b></label>
                    <div class="panel panel-default">
                        <div class="panel-body reportChartContainer">
                            <div id="chart-container"></div>
                        </div>
                    </div>
                </div>
				<div class="col-lg-6">
                	<label>Total Training Days <b></b></label>
                    <div class="panel panel-default">
                        <div class="panel-body reportChartContainer">
                            <div style="height:254px;" id="container_quage"></div>
                        </div>
                    </div>
                </div>
				
 			</div>
			<hr/>
			<div class="row">
				 <div class="alert alert-warning" role="alert"> <strong>Assessment Scores </strong> </div>
				 </div>
			 <div class="row">
      			<div class="col-lg-4">
                	<label>Pre Assessment &nbsp; </label>
                    <div class="panel panel-default">
                        <div class="panel-body reportChartContainer">
                           <div class="gamePercentageMemory">
                           
                	<span class="memory<?php echo getAssmentchart('pretest',$assessment); ?>"><?php echo getAssmentchart('pretest',$assessment); ?>%</span>
                </div>
                        </div>
                    </div>
                </div>
				<div class="col-lg-4">
                	<label>Post Assessment - I&nbsp; </label>
                    <div class="panel panel-default">
                        <div class="panel-body reportChartContainer">
                            <div class="gamePercentageMemory">
                           
                	<span class="linguistics<?php echo getAssmentchart('posttest1',$assessment); ?>"><?php echo getAssmentchart('posttest1',$assessment); ?>%</span>
                </div>
                        </div>
                    </div>
                </div>
				<div class="col-lg-4">
                	<label>Post Assessment - II &nbsp; </label>
                    <div class="panel panel-default">
                        <div class="panel-body reportChartContainer">
                            <div class="gamePercentageMemory">
                           
                	<span class="FA<?php echo getAssmentchart('posttest2',$assessment); ?>"><?php echo getAssmentchart('posttest2',$assessment); ?>%</span>
                </div>
                        </div>
                    </div>
                </div>
 			</div>
			<hr/>
			
			<div class="row">
      			<div class="col-lg-6">
                	<label>Overall Performance</label>
                    <div class="panel panel-default">
                        <div class="panel-body reportChartContainer">
                            <div id="chartOverallPerformance" ></div>
                        </div>
                    </div>
                </div>
				<div class="col-lg-6">
                	<label>Training Days <b></b></label>
                    <div class="panel panel-default">
                        <div class="panel-body reportChartContainer">
                            <div id="container_2"></div>
                        </div>
                    </div>
                </div>
				
 			</div>
			<hr/>
			<div class="row">
				 <div class="alert alert-warning" role="alert"> <strong>Skill Performance</strong> </div>
				 </div>
			<div class="row">
      			<div class="col-lg-6">
                	<label>Memory</label>
                    <div class="panel panel-default">
                        <div class="panel-body reportChartContainer">
                            <div id="container_memory" ></div>
                        </div>
                    </div>
                </div>
				<div class="col-lg-6">
                	<label>Visual Processing <b></b></label>
                    <div class="panel panel-default">
                        <div class="panel-body reportChartContainer">
                            <div id="container_vp"></div>
                        </div>
                    </div>
                </div>
				
 			</div>
			<div class="row">
      			<div class="col-lg-6">
                	<label>Focus and Attention</label>
                    <div class="panel panel-default">
                        <div class="panel-body reportChartContainer">
                            <div id="container_fa"></div>
                        </div>
                    </div>
                </div>
				<div class="col-lg-6">
                	<label>Problem Solving <b></b></label>
                    <div class="panel panel-default">
                        <div class="panel-body reportChartContainer">
                            <div id="container_ps"></div>
                        </div>
                    </div>
                </div>
				
 			</div>
			<div class="row">
      			<div class="col-lg-6">
                	<label>Linguistics</label>
                    <div class="panel panel-default">
                        <div class="panel-body reportChartContainer">
                            <div id="container_L"></div>
                        </div>
                    </div>
                </div>
				<div class="col-lg-6">                   
                </div>
				
 			</div>
			
</div>
</div>

<script src="js/skillpiecharts.js" type="text/javascript"></script><script type="text/javascript" src="js/charts.widgets.js"></script>
<script type="text/javascript">
FusionCharts.ready(function () {
	
    var csatGauge = new FusionCharts({
        "type": "angulargauge",
        "renderAt": "chart-container",
        "width": "400",
        "height": "250",
        "dataFormat": "json",
            "dataSource": {
    "chart": {
        "caption": "",
        "lowerlimit": "0",
        "upperlimit": "100",
        "lowerlimitdisplay": "0%",
        "upperlimitdisplay": "100%",
        "palette": "1",
        "numbersuffix": "%",
        "tickvaluedistance": "10",
        "showvalue": "0",
        "gaugeinnerradius": "0",
        "bgcolor": "FFFFFF",
        "pivotfillcolor": "333333",
        "pivotradius": "8",
        "pivotfillmix": "333333, 333333",
        "pivotfilltype": "radial",
        "pivotfillratio": "0,100",
        "showtickvalues": "1",
        "showborder": "0"
    },
    "colorrange": {
        "color": [
            {
                "minvalue": "0",
                "maxvalue": "<?php echo $AverageScore+.5 ?>",
                "code": "e44a00"
            },
            {
                "minvalue": "<?php echo $AverageScore+1 ?>",
                "maxvalue": "100",
                "code": "#B0B0B0"
            }
        ]
    },
	
	
	
    "dials": {
        "dial": [
            {
                "value": "<?php echo $AverageScore ?>",
                "rearextension": "15",
                "radius": "100",
                "bgcolor": "333333",
                "bordercolor": "333333",
                "basewidth": "8"
            }
        ]
    }
}
      });

    csatGauge.render();
	
	
});
//alert(csatGauge.dataSource.dials);
</script>
<script src="js/highcharts.js"></script>
<script type="text/javascript" src="js/charts-more.js"></script>
<script src="js/solid-gauge.js"></script>

 <script>
 $(function () {

    $('#container_quage').highcharts({

        chart: {
            type: 'solidgauge',
            backgroundColor: 'transparent'
        },

        title: null,

        pane: {
            center: ['50%', '50%'],
            size: '200px',
            startAngle: -160,
            endAngle: 160,
            background: {
                backgroundColor: '#ccc',
                innerRadius: '58%',
                outerRadius: '102%',
                shape: 'arc',
                borderColor: 'transparent'
            }
        },

        tooltip: {
            enabled: false
        },

        // the value axis
        yAxis: {
            min: 0,
            max: <?php echo  $totalpalyingdays; ?>,
            stops: [
                
                [0.9, '#f1c40f'] // green
                ],
            minorTickInterval: null,
            tickPixelInterval: 10,
            tickWidth: 0,
            gridLineWidth: 0,
            gridLineColor: 'transparent',
            labels: {
                enabled: false
            },
            title: {
                enabled: false
            }
        },

        credits: {
            enabled: false
        },

        plotOptions: {
            solidgauge: {
                dataLabels: {
                    y: -45,
                    borderWidth: 0,
                    useHTML: true
                }
            }
        },

        series: [{
            data: [<?php echo ($playeddayCount); ?>],
            dataLabels: {
                format: '<p style="text-align:center;">{y} Days<br>Total <?php echo  $totalpalyingdays; ?> days</p>'
            }
        }]
    });

});
$(document).ready(function(){
	
    chart = new Highcharts.Chart({
        chart: {
			renderTo: 'chartOverallPerformance',
            type: 'column'
        },
        title: {
            text: ''
        },
        
        xAxis: {
            categories: [
			<?php $ini=0; foreach($academicMonths as $am){
				$ini++;
				if($ini>1){echo ",";}
				echo "'".$am['monthName']."'";
			} ?>
                
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,tickInterval: 10,max:100,
            title: {
                text: 'Score'
            }
        },
       
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
		credits: {
      enabled: false
  },
        series: [{showInLegend: false,  color:"#9a1e00",
            name: 'Score',
            data: [
			<?php $ini=0; foreach($academicMonths as $am){
				$ini++;
				if($ini>1){echo ",";}
				echo "".getResultByMonth($am['monthNumber'],$OverallPerformance_value)."";
			} ?>
			
			]

        }]
    });
	 chart2 = new Highcharts.Chart({
        chart: {
			renderTo: 'container_2',
            type: 'column'
        },
        title: {
            text: ''
        },
        
        xAxis: {
            categories: [
                <?php $ini=0; foreach($academicMonths as $am){
				$ini++;
				if($ini>1){echo ",";}
				echo "'".$am['monthName']."'";
			} ?>
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,tickInterval: 10,max:<?php echo  $totalpalyingdays; ?>,
            title: {
                text: 'Day'
            }
        },
       
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
		credits: {
      enabled: false
  },
        series: [{showInLegend: false,  color:"#ff9900",
            name: 'Day',
            data: [<?php $ini=0; foreach($academicMonths as $am){
				$ini++;
				if($ini>1){echo ",";}
				echo "".getDaysByMonth($am['monthNumber'],$arrPlayedDays)."";
			} ?>]

        }]
    });
	$('#container_memory').highcharts({
        title: {
            text: '',
            x: -20 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
            categories: [<?php $ini=0; foreach($academicMonths as $am){
				$ini++;
				if($ini>1){echo ",";}
				echo "'".$am['monthName']."'";
			} ?>]
        },
        yAxis: {
            min: 0,tickInterval: 10,max:100,
            title: {
                text: 'Score'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#ff3300'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        credits: {
      enabled: false
  },
        series: [{showInLegend: false,   color: '#ff3300',
            name: 'Memory',
            data: [<?php $ini=0; foreach($academicMonths as $am){
				$ini++;
				if($ini>1){echo ",";}
				echo "".getResultByMonth_Skill($am['monthNumber'],$month_score,59)."";
			} ?>]
        }]
    });
	
	$('#container_vp').highcharts({
        title: {
            text: '',
            x: -20 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
            categories: [<?php $ini=0; foreach($academicMonths as $am){
				$ini++;
				if($ini>1){echo ",";}
				echo "'".$am['monthName']."'";
			} ?>]
        },
        yAxis: {
            min: 0,tickInterval: 10,max:100,
            title: {
                text: 'Score'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#9a1e00'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        credits: {
      enabled: false
  },
        series: [{showInLegend: false, color:"#9a1e00", 
            name: 'Visual Processing',
            data: [<?php $ini=0; foreach($academicMonths as $am){
				$ini++;
				if($ini>1){echo ",";}
				echo "".getResultByMonth_Skill($am['monthNumber'],$month_score,60)."";
			} ?>]
        }]
    });
	$('#container_fa').highcharts({
        title: {
            text: '',
            x: -20 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
            categories: [<?php $ini=0; foreach($academicMonths as $am){
				$ini++;
				if($ini>1){echo ",";}
				echo "'".$am['monthName']."'";
			} ?>]
        },
        yAxis: {
            min: 0,tickInterval: 10,max:100,
            title: {
                text: 'Score'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#ff9900'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
       credits: {
      enabled: false
  },
        series: [{showInLegend: false, color:"#ff9900" ,
            name: 'Focus and attention',
            data: [<?php $ini=0; foreach($academicMonths as $am){
				$ini++;
				if($ini>1){echo ",";}
				echo "".getResultByMonth_Skill($am['monthNumber'],$month_score,61)."";
			} ?>]
        }]
    });
	$('#container_ps').highcharts({
        title: {
            text: '',
            x: -20 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
            categories: [<?php $ini=0; foreach($academicMonths as $am){
				$ini++;
				if($ini>1){echo ",";}
				echo "'".$am['monthName']."'";
			} ?>]
        },
        yAxis: {
            min: 0,tickInterval: 10,max:100,
            title: {
                text: 'Score'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#05b923'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
       credits: {
      enabled: false
  },
        series: [{showInLegend: false, color:"#05b923",  
            name: 'Problem Solving',
            data: [<?php $ini=0; foreach($academicMonths as $am){
				$ini++;
				if($ini>1){echo ",";}
				echo "".getResultByMonth_Skill($am['monthNumber'],$month_score,62)."";
			} ?>]
        }]
    });
	$('#container_L').highcharts({
        title: {
            text: '',
            x: -20 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
            categories: [<?php $ini=0; foreach($academicMonths as $am){
				$ini++;
				if($ini>1){echo ",";}
				echo "'".$am['monthName']."'";
			} ?>]
        },
        yAxis: {
            min: 0,tickInterval: 10,max:100,
            title: {
                text: 'Score'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#33ccff'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        credits: {
      enabled: false
  },
        series: [{showInLegend: false,  color:"#33ccff",
            name: 'Linguistics',
            data: [<?php $ini=0; foreach($academicMonths as $am){
				$ini++;
				if($ini>1){echo ",";}
				echo "".getResultByMonth_Skill($am['monthNumber'],$month_score,63)."";
			} ?>]
        }]
    });
});
 </script>

<script>

$(document).ready(function() {
    $('#assementTable').DataTable();
} );
</script>
 