	
<?php 
if(empty($adminid)){
	echo "<script>window.location='index.php?act=login'</script>";
}


?>
 

<div class="pageHomePager Dashboardhide mygameshide myreporthide myprofilehide">
   <div class="row">
  <div class="col-lg-12">
                    <h1 class="page-header">Assessment Report</h1>
                </div>
			</div>	
   <div class="row">
      			<div class="col-lg-12">
				<div class="form-group">
				<label class="col-lg-2">Grade</label>
				<select class="form-control col-lg-2" style="width:16%" name="ddlClassType" id="ddlClassType">
			<option value="">Select</option>
			<option value="1">I</option>
			<option value="2">II</option>
			<option value="3">III</option>
			<option value="4">IV</option>
			<option value="5">V</option>
			<option value="6">VI</option>
			<option value="7">VII</option>
			<option value="8">VIII</option>
			<option value="9">IX</option>
			<option value="10">X</option>
			<option value="11">XI</option>
			<option value="12">XII</option>
			</select>
			<label class="col-lg-2">Section</label>
				<select class="form-control col-lg-2" style="width:16%" name="ddlClassType" id="ddlClassType">
			<option value="">Select</option>
			<option value="1">A</option>
			<option value="2">B</option>
			<option value="3">C</option>
			</select>
			<label class="col-lg-2">Assessment</label>
				<select class="form-control col-lg-2" style="width:16%" name="ddlClassType" id="ddlClassType">
			<option value="">Select</option>
			<option value="1">Pre Assessment</option>
			<option value="2">Post Assessment - I</option>
			<option value="3">Post Assessment - II</option>
			</select>
				</div>
				</div>
				</div>
              <div class="row">
      			<div class="col-lg-12 landingContainer">
				
        			<table id="assementTable" class="table table-striped table-bordered table-hover table-condensed">
    <thead>
      <tr>
        <th>S.No.</th>
        <th>Name</th>
        <th>Class</th>
        <th>M</th>
        <th>VP</th>
        <th>FA</th>
        <th>PS</th>
        <th>L</th>
        <th>BSPI</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>1</td>
        <td>Kalpana</td>
        <td>I-A</td>
        <td>80</td>
        <td>92</td>
        <td>88</td>
        <td>91</td>
		<td>89</td>
		<td>88</td>
      </tr>
      <tr>
        <td>2</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
		<td></td>
		<td></td>
      </tr>
      <tr>
        <td>3</td>
		<td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
		<td></td>
		<td></td>
      </tr>
	  <tr>
        <td>4</td>
		<td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
		<td></td>
		<td></td>
      </tr>
	  
    </tbody>
  </table>
                    
      			</div>
				
				
 			</div>
			
<div class="row">
<input type="button" class="btn btn-success" id="btnExcelExport" name="btnExcelExport" value="Export to excel">
</div>
</div>
<script>
$(document).ready(function() {
    $('#assementTable').DataTable();
} );
</script>
 