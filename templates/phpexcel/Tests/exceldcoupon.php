<?php
date_default_timezone_set('Asia/Calcutta'); 
require_once '../../../../class.phpmailer.php';
require_once '../../../../dbassess.php';
require_once '../../excelreader/reader.php';
require_once '../Classes/PHPExcel/IOFactory.php';
error_reporting(0);


function rand_str($length = 32, $chars = '123456789')
{
	// Length of character list
	$chars_length = (strlen($chars) - 1);

	// Start our string
	$string = $chars{rand(0, $chars_length)};
	 
	// Generate random string
	for ($i = 1; $i < $length; $i = strlen($string))
	{
		// Grab a random character from our list
		$r = $chars{rand(0, $chars_length)};
		 
		// Make sure the same two characters don't appear next to each other
		if ($r != $string{$i - 1}) $string .=  $r;
	}
	 
	// Return the string
	return $string;
}

function newfilename($vFilename,$gradeid){
	
		$files = glob('/var/www/vhosts/skillangels.com/school/admin/templates/phpexcel/Tests/'."*.xls");
	//$files = glob('/opt/lampp/htdocs/subrahmaniam/edsix_school/trunk/admin/templates/phpexcel/Tests/'."*.xls");
	
	foreach($files as $file)
		{
			$file_li = basename($file, ".xls");
			$efiles[] = $file_li;
		}
		$explodedfname = explode("_", $vFilename);
		
			
		
			if(in_array($vFilename,$efiles)){
				$int = $explodedfname[2] + 1;
				$vFilename = $explodedfname[0].'_'.$gradeid.'_'.$int;
				$vFilename = newfilename($vFilename,$gradeid);
				
				return $vFilename;
			}
			else{
				return $vFilename;
			}
	
}


//getting filenames for xls
$files = glob('/var/www/vhosts/skillangels.com/school/admin/templates/phpexcel/Tests/'."*.xls");
//$files = glob('/opt/lampp/htdocs/subrahmaniam/edsix_school/trunk/admin/templates/phpexcel/Tests/'."*.xls");

foreach($files as $file)
{
	$file_li = basename($file, ".xls");
	$efiles[] = $file_li;
}


//getting all usernames to check with the uploaded names

$alldcntcodes = mysql_query("select dcnt_code from skl_dcnt_coupon");
while($all_dcnt_codes = mysql_fetch_array($alldcntcodes)){
	$all_dcntcodes[] = $all_dcnt_codes['dcnt_code'];
}
//submit
if(isset($_POST['gdcouponsubmit']))
{
	
	$curtime = time();
	$percentage = $_POST['percentage'];
	$discountname = $_POST['discountname'];
	$ncoupon = $_POST['ncoupon'];
	$emailid = $_POST['email'];
	
	$sql= "INSERT INTO  skl_dcnt_coupon(dcnt_name,dcnt_code,percentage,timecreated,startdate,enddate,status) VALUES ";
	
	$dataa = array();
	$vFirstLevel = 0;
	$baseRow = 3;
	
	for($i=1;$i<=$ncoupon;$i++) {
		$dcntcode = rand_str(7);
		if(in_array($dcntcode,$all_dcntcodes)){
			$dcntcode = rand_str(8);
		}
		$dataa[$vFirstLevel]['coupons'] = $dcntcode;
		$sql .= "('$discountname', '$dcntcode','$percentage','$curtime','$curtime','1394346549','1'), ";
		$vFirstLevel++;
	}
	
	$sql = substr($sql,0,-2);
	
	mysql_query($sql);
	
	$objReader = PHPExcel_IOFactory::createReader('Excel5');
	$objPHPExcel = $objReader->load("dcoupontemp.xls");
	$time = time();
	$vFileName = $ncoupon.'coupons'.'_'.$gradeid.'_1.xls';
	$vFileNameoutxls = basename($vFileName,".xls");
	
	if(in_array($vFileNameoutxls,$efiles)){
		$vFileName = newfilename($vFileNameoutxls,$gradeid);
		$vFileName = $vFileName.'.xls';
		
	}
	$a2 = "Grade : $gradename[grdname]";
	$d2 = "Plan : $planname[name]";
	$sheet = $objPHPExcel->getActiveSheet();
	
	
	foreach($dataa as $r => $dataRow) {
		$row = $baseRow + $r;
		$sheet->setCellValue('c'.$row, $dataRow['coupons']);
	}
	$styleArray = array(
			'borders' => array(
					'allborders' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN,
							'color' => array('rgb' => '000000'),
					),
			),
	);
	
	
	$objPHPExcel->getActiveSheet()->getStyle('C3:C'.$row)->applyFromArray($styleArray);
	
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	
	$objWriter->save($vFileName);
	
	//send mail
	$subject = "Discount Coupon List";
	
	$mail             = new PHPMailer();
	
	$body             = "
        <p>Dear Admin,</p><br>
        <p>Please find the attached file containing discount coupon list.</p>
      	
      	
		<p>Regards,<br>Skill angels Team</p>";
	
	
	
	$mail->SetFrom('angelsadmin@skillangels.com', 'SkillAngels');
	
	$mail->AddReplyTo("angelsadmin@skillangels.com","SkillAngels");
	$mail->AddAttachment("/var/www/vhosts/skillangels.com/school/admin/templates/phpexcel/Tests/$vFileName", "$vFileName");
	//$mail->AddAttachment("/opt/lampp/htdocs/subrahmaniam/edsix_school/trunk/admin/templates/phpexcel/Tests/$vFileName", "$vFileName");
	
	
	$mail->AddAddress($emailid,"");
	
	$mail->Subject    = $subject;
	
	$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
	
	$mail->MsgHTML($body);
	
	if(!$mail->Send()) {
		echo "Mailer Error: " . $mail->ErrorInfo;
	} else {
		header('Location: http://school.skillangels.com/admin/index.php?act=dcoupon&msg=1');
	}
}
?>
