<?php
date_default_timezone_set('Asia/Calcutta'); 
require_once '../../../../class.phpmailer.php';
require_once '../../../../dbassess.php';
require_once '../../../../function.php';
require_once '../../excelreader/reader.php';
require_once '../Classes/PHPExcel/IOFactory.php';
error_reporting(0);


function newusername($username,$fname,$lname){
	
	$allusernames = mysql_query("select username from users");
	while ($all_usernames = mysql_fetch_array($allusernames)){
		$usernames[] = $all_usernames['username'];
	}
	$int = filter_var($username, FILTER_SANITIZE_NUMBER_INT);
	$usernames = array_filter($usernames);
	if(in_array($username,$usernames)){
		$int = $int+1;
		$username = $fname.$int.'.'.$lname;
		$username = newusername($username,$fname,$lname);
		return $username;
	}
	else{
		return $username;
	}
}


function newfilename($vFilename,$gradeid){
	
		$files = glob('/var/www/vhosts/skillangels.com/school/admin/templates/phpexcel/Tests/'."*.xls");
		foreach($files as $file)
		{
			$file_li = basename($file, ".xls");
			$efiles[] = $file_li;
		}
		$explodedfname = explode("_", $vFilename);
		
			
		
			if(in_array($vFilename,$efiles)){
				$int = $explodedfname[2] + 1;
				$vFilename = $explodedfname[0].'_'.$gradeid.'_'.$int;
				$vFilename = newfilename($vFilename,$gradeid);
				
				return $vFilename;
			}
			else{
				return $vFilename;
			}
	
}


//getting filenames for xls
$files = glob('/var/www/vhosts/skillangels.com/school/admin/templates/phpexcel/Tests/'."*.xls");
foreach($files as $file)
{
	$file_li = basename($file, ".xls");
	$efiles[] = $file_li;
}

//getting all usernames to check with the uploaded names
$allusernames = mysql_query("select username from users");
while ($all_usernames = mysql_fetch_array($allusernames)){
	$usernames[] = $all_usernames['username'];
}

//submit
if(isset($_POST['schoolsubmit']))
{
	
	$curtime = time();
	$todaysdate = date('Y-m-d',time());
	$planid = $_POST['plans'];
	$smailid = $_POST['smailid'];
	$pieces = explode("@", $smailid);
	$planvalidity = mysql_fetch_array(mysql_query("select validity,name from g_plans where id = '$planid'"));
	$pvalidity = $planvalidity['validity'];
	$exptime = strtotime("+$pvalidity days");
	$gradeid = $_POST['grades'];
	$gname = mysql_fetch_array(mysql_query("select grdname from skl_grade where id = '$gradeid'"));
	$gradename = $gname['grdname'];
	$sid = $_POST['schools'];
	$sname = mysql_fetch_array(mysql_query("select school_name from schools where id = '$sid'"));
	$schoolname = $sname['school_name'];
	//$sql= "INSERT INTO users(email,password,fname,lname,mobile,status,gp_id,glevel,creation_date,grade_id,sname,address,username,initial,sid) VALUES ";
	$data = new Spreadsheet_Excel_Reader();
	$data->setOutputEncoding('CP1251');
	$path = '';
	$path	= $_FILES['uploadusers']['tmp_name'];
	$data->read($path);
	for ($i=2; $i<=$data->sheets[0]['numRows']; $i++)
	{
		$fname = $data->sheets[0]['cells'][$i][1];
		$initial = $data->sheets[0]['cells'][$i][2];
		$lname = $data->sheets[0]['cells'][$i][3];
		$address = $data->sheets[0]['cells'][$i][4];
		$mobile = $data->sheets[0]['cells'][$i][5];
		$email = $data->sheets[0]['cells'][$i][6];
		
		$username = $fname.'.'.$initial;
		
		if(in_array($username,$usernames))
			$username = $fname.'.'.$lname;
			
		if(in_array($username,$usernames))
			$username = $fname.'1.'.$lname;
		
		if(in_array($username,$usernames))
		{
			$username = newusername($username,$fname,$lname);
		}
			
		$usernames[] = $username;
			
		if($email == '')
			$email = $username.'@skillangels.com';
		
		$arrusername[] = $username;
		
		
		$passhash = md5($username);
		
		if(checkDuplicateUser($fname,$lname,$initial,$gradeid,$schoolname) == 1)
		{
			
			$sql= "INSERT INTO users(email,password,fname,lname,mobile,status,gp_id,glevel,creation_date,grade_id,sname,address,username,initial,sid) VALUES
			('$email', '$passhash','$fname','$lname','$mobile','NULL','$planid','1','$todaysdate','$gradeid','$schoolname','$address','$username','$initial','$sid')";
		
			mysql_query($sql);
			$lastinsertedid = mysql_insert_id();
			mysql_query("insert into user_games_plans (gu_id,gp_id,startdate,expirytime) values ('$lastinsertedid','$planid','$curtime','$exptime')");
		}
	}
	//$sql = substr($sql,0,-2);
	
	$objReader = PHPExcel_IOFactory::createReader('Excel5');
	$objPHPExcel = $objReader->load("templates/template.xls");
	$time = time();
	
	$vFileName = $schoolname.'_'.$gradeid.'_1.xls';
	$vFileNameoutxls = basename($vFileName,".xls");
	
	if(in_array($vFileNameoutxls,$efiles)){
		$vFileName = newfilename($vFileNameoutxls,$gradeid);
		$vFileName = $vFileName.'.xls';
		
	}
	
	$dataa = array();
	$vFirstLevel = 0;
	$baseRow = 5;
	
	foreach($arrusername as $arrusernam) {
		$dataa[$vFirstLevel]['username'] = $arrusernam;
		$dataa[$vFirstLevel]['password'] = $arrusernam;
		$vFirstLevel++;
	}
	
	$sheet = $objPHPExcel->getActiveSheet();
	$sheet->setCellValue("B1", $schoolname);
	$sheet->setCellValue("B2", $gradename);
	$sheet->setCellValue("B3", $planvalidity['name']);
	
	foreach($dataa as $r => $dataRow) {
		$row = $baseRow + $r;
		$sheet->setCellValue('A'.$row, $dataRow['username'])
		->setCellValue('B'.$row, $dataRow['password']);
	}
	$styleArray = array(
			'borders' => array(
					'allborders' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN,
							'color' => array('rgb' => '000000'),
					),
			),
	);
	
	
	$objPHPExcel->getActiveSheet()->getStyle('A5:B'.$row)->applyFromArray($styleArray);
	
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save($vFileName);
	
	//send mail
	$subject = "Users List";
	$mail             = new PHPMailer();
	
	$body             = "
        <p>Dear ".$pieces[0].",</p>
        <p>Please find the attached file containing users list.</p>
      	
      	
		<p>Regards,<br>Skill angels Team</p>";
	
	
	
	$mail->SetFrom('angelsadmin@skillangels.com', 'SkillAngels');
	
	$mail->AddReplyTo("angelsadmin@skillangels.com","SkillAngels");
	$mail->AddAttachment("/var/www/vhosts/skillangels.com/school/admin/templates/phpexcel/Tests/$vFileName", "$vFileName");

	$address = $smailid;
	$mail->AddAddress($address,"");
	
	$mail->Subject    = $subject;
	
	$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
	
	$mail->MsgHTML($body);
	
	if(!$mail->Send()) {
		echo "Mailer Error: " . $mail->ErrorInfo;
	} else {
		header('Location: http://school.skillangels.com/admin/index.php?act=addsusers&msg=1');
	}
	
	
	
}
	
	
?>
<!--  header('Content-Type: application/vnd.ms-excel');
	//header('Content-Disposition: attachment;filename="'.$vFileName.'"');
	//header('Cache-Control: max-age=0');
	//$objWriter->save('php://output');
	-->