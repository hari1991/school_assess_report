<?php
date_default_timezone_set('Asia/Calcutta'); 
require_once '../../../../class.phpmailer.php';
require_once '../../../../dbassess.php';
require_once '../../excelreader/reader.php';
require_once '../Classes/PHPExcel/IOFactory.php';
error_reporting(0);


function rand_str($length = 32, $chars = '123456789')
{
	// Length of character list
	$chars_length = (strlen($chars) - 1);

	// Start our string
	$string = $chars{rand(0, $chars_length)};
	 
	// Generate random string
	for ($i = 1; $i < $length; $i = strlen($string))
	{
		// Grab a random character from our list
		$r = $chars{rand(0, $chars_length)};
		 
		// Make sure the same two characters don't appear next to each other
		if ($r != $string{$i - 1}) $string .=  $r;
	}
	 
	// Return the string
	return $string;
}

function newfilename($vFilename,$gradeid){
	
		$files = glob('/var/www/vhosts/skillangels.com/school/admin/templates/phpexcel/Tests/'."*.xls");
	//$files = glob('/opt/lampp/htdocs/subrahmaniam/edsix_school/trunk/admin/templates/phpexcel/Tests/'."*.xls");
	
	foreach($files as $file)
		{
			$file_li = basename($file, ".xls");
			$efiles[] = $file_li;
		}
		$explodedfname = explode("_", $vFilename);
		
			
		
			if(in_array($vFilename,$efiles)){
				$int = $explodedfname[2] + 1;
				$vFilename = $explodedfname[0].'_'.$gradeid.'_'.$int;
				$vFilename = newfilename($vFilename,$gradeid);
				
				return $vFilename;
			}
			else{
				return $vFilename;
			}
	
}


//getting filenames for xls
$files = glob('/var/www/vhosts/skillangels.com/school/admin/templates/phpexcel/Tests/'."*.xls");
//$files = glob('/opt/lampp/htdocs/subrahmaniam/edsix_school/trunk/admin/templates/phpexcel/Tests/'."*.xls");

foreach($files as $file)
{
	$file_li = basename($file, ".xls");
	$efiles[] = $file_li;
}


//getting all usernames to check with the uploaded names

$allprecodes = mysql_query("select prepaid_code from skl_prepaid_coupon");
while($all_pre_codes = mysql_fetch_array($allprecodes)){
	$all_precodes[] = $all_pre_codes['prepaid_code'];
}
//submit
if(isset($_POST['gpcouponsubmit']))
{
	
	$curtime = time();
	$couponname = $_POST['couponname'];
	$gradeid = $_POST['grades'];
	$planid = $_POST['plans'];
	$expirydate = $_POST['datepicker'];
	$expirydate = strtotime($expirydate);
	$ncoupon = $_POST['ncoupon'];
	$emailid = $_POST['email'];
	
	$gradename = mysql_fetch_array(mysql_query("select grdname from  skl_grade where id = $gradeid"));
	$planname = mysql_fetch_array(mysql_query("select name from  g_plans where id = $planid"));
	
	$sql= "INSERT INTO skl_prepaid_coupon(prepaid_name,prepaid_code,plan_id,timecreated,expirytime) VALUES ";
	
	$dataa = array();
	$vFirstLevel = 0;
	$baseRow = 4;
	
	for($i=1;$i<=$ncoupon;$i++) {
		$precode = rand_str(7);
		$precode = $precode.$gradeid.$planid;
		if(in_array($precode,$all_precodes)){
			$precode = rand_str(8);
			$precode = $precode.$gradeid.$planid;
		}
		$dataa[$vFirstLevel]['coupons'] = $precode;
		$sql .= "('$couponname', '$precode','$planid','$curtime','$expirydate'), ";
		$vFirstLevel++;
	}
	
	$sql = substr($sql,0,-2);
	
	mysql_query($sql);
	
	$objReader = PHPExcel_IOFactory::createReader('Excel5');
	$objPHPExcel = $objReader->load("coupontemplate.xls");
	$time = time();
	$vFileName = $ncoupon.'coupons'.'_'.$gradeid.'_1.xls';
	$vFileNameoutxls = basename($vFileName,".xls");
	
	if(in_array($vFileNameoutxls,$efiles)){
		$vFileName = newfilename($vFileNameoutxls,$gradeid);
		$vFileName = $vFileName.'.xls';
		
	}
	$a2 = "Grade : $gradename[grdname]";
	$d2 = "Plan : $planname[name]";
	$sheet = $objPHPExcel->getActiveSheet();
	$sheet->setCellValue("A2", "$a2");
	$sheet->setCellValue("D2", "$d2");
	
	foreach($dataa as $r => $dataRow) {
		$row = $baseRow + $r;
		$sheet->setCellValue('c'.$row, $dataRow['coupons']);
	}
	$styleArray = array(
			'borders' => array(
					'allborders' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN,
							'color' => array('rgb' => '000000'),
					),
			),
	);
	
	
	$objPHPExcel->getActiveSheet()->getStyle('C4:C'.$row)->applyFromArray($styleArray);
	
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	
	$objWriter->save($vFileName);
	
	//send mail
	$subject = "Prepaid Coupon List";
	$smailid = 'ssgodfather@gmail.com';
	$mail             = new PHPMailer();
	
	$body             = "
        <p>Dear Admin,</p>
        <p>Please find the attached file containing coupon list for ".$gradename['grdname']." - ".$planname['name']." plan.</p>
      	
      	
		<p>Regards,<br>Skill angels Team</p>";
	
	
	
	$mail->SetFrom('angelsadmin@skillangels.com', 'SkillAngels');
	
	$mail->AddReplyTo("angelsadmin@skillangels.com","SkillAngels");
	$mail->AddAttachment("/var/www/vhosts/skillangels.com/school/admin/templates/phpexcel/Tests/$vFileName", "$vFileName");
	//$mail->AddAttachment("/opt/lampp/htdocs/subrahmaniam/edsix_school/trunk/admin/templates/phpexcel/Tests/$vFileName", "$vFileName");
	
	
	$mail->AddAddress($emailid,"");
	
	$mail->Subject    = $subject;
	
	$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
	
	$mail->MsgHTML($body);
	
	if(!$mail->Send()) {
		echo "Mailer Error: " . $mail->ErrorInfo;
	} else {
		header('Location: http://school.skillangels.com/admin/index.php?act=pcoupon&msg=3');
	}
	
	
	
}
	
	
?>
<!--  header('Content-Type: application/vnd.ms-excel');
	//header('Content-Disposition: attachment;filename="'.$vFileName.'"');
	//header('Cache-Control: max-age=0');
	//$objWriter->save('php://output');
	-->
