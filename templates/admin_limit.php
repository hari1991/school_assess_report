<?php
//ini_set( "display_errors", 0);
//include('db.php');
//session_start();
//$userid=$_SESSION['aid'];

//$userid='2';
if(empty($adminid)){
	//header("Location:index.php?act=login");
	echo "<script>window.location='index.php?act=login'</script>";
}
$plan=mysql_query("select * from g_plans where status='1'");
$category=mysql_query("select * from g_category where status='1'");

?>
<script src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	 $("#limits").validate();
		 
	//select category
	$("#gc_id").change(function(){
		var gc_id=$(this).val();
		var dataString = 'category_id='+ gc_id;		
		$.ajax
		({
			type: "POST",
			url: "../ajax_data.php",
			data: dataString,
			cache: false,
			success: function(html)
			{
				$(".gs_id").html(html);
			} 
		});
	});
	//select skills
	$("#gs_id").change(function(){
		var gc_id=$('.gc_id').val();
		var gs_id=$(this).val();
		var dataString = 'gs_id='+ gs_id +'&gc_id='+ gc_id;		
		$.ajax
		({
			type: "POST",
			url: "../ajax_data.php",
			data: dataString,
			cache: false,
			success: function(html)
			{
				$(".game_id").html(html);
			} 
		});
	});
	//select no.of times
	$("#game_id").change(function(){
		var g_id=$(this).val();
		var gp_id=$('#gp_id').val();//plan
		var dataString = 'gm_id='+ g_id+'&gp_id='+gp_id;		
		$.ajax
		({
			type: "POST",
			url: "../ajax_data.php",
			data: dataString,
			cache: false,
			success: function(html)
			{
				$("#ntimes").val(html);
			} 
		});
	});
	
	
	//admin limits
	
	//var progressbox     = $('#progressbox');
   // var progressbar     = $('#progressbar');
    var statustxt       = $('#statustxt');
    var submitbutton    = $("#admin_limits");
    var myform          = $("#limits");
    var output          = $("#output");
    var completed       = '0%';
	 $(myform).ajaxForm({
         //alert("asdad");
         beforeSend: function() { //before sending form
             submitbutton.attr('disabled', ''); // disable upload button
             statustxt.empty();
            // progressbox.slideDown(); //show progressbar
             //progressbar.width(completed); //initial value 0% of progressbar
             //statustxt.html(completed); //set status text
             //statustxt.css('color','#000'); //initial color of status text
         },
        /* uploadProgress: function(event, position, total, percentComplete) { //on progress
             progressbar.width(percentComplete + '%') //update progressbar percent complete
             statustxt.html(percentComplete + '%'); //update status text   percentComplete + '%'
             if(percentComplete>50)
                 {
                     statustxt.css('color','#fff'); //change status text to white after 50%
                 }
             },
             */
         complete: function(response) { // on complete
             output.html(response.responseText); //update element with received data
             myform.resetForm();  // reset form
             submitbutton.removeAttr('disabled'); //enable submit button
             //progressbox.slideUp(); // hide progressbar
         }
 });
	
});
</script>
<header>
	<div class="intro-head">Set Game Limits</div>
	<div class="intro">
		<div class="support-note"><!-- let's check browser support with modernizr -->
			<!--span class="no-cssanimations">CSS animations are not supported in your browser</span
			<!--  <span class="no-csstransforms">CSS transforms are not supported in your browser</span>-->
			<!--span class="no-csstransforms3d">CSS 3D transforms are not supported in your browser</span-->
			<!--  <span class="no-csstransitions">CSS transitions are not supported in your browser</span>
			<span class="note-ie">Sorry, only modern browsers.</span>-->
			<div id="main">
				<div id="statustxt"></div ><div id="output"></div>
				<form name="limits" id="limits" method="post" action="../ajax_data.php">
					<table border="0" cellspacing="4" cellpadding="4">
						<tr>
							<td valign="top"><label class="fields" for="validity">Select Price Plan</label><label class="mandatory">*</label></td>
							<td>
								<select name="gplan_id" id="gp_id" class="required">
									<option value="">--Game Plan--</option>
									<?php 
									while ($plist=mysql_fetch_array($plan)) {
									?>
									<option value="<?php echo $plist['id'];?>"><?php echo $plist['name'];?></option>
									<?php }?>
								</select>
							</td>
						</tr>
						<tr>
							<td valign="top"><label class="fields" for="validity">Game Category</label><label class="mandatory">*</label></td>
							<td>
								<select name="gcat_id" id="gc_id" class="gc_id required" >
									<option value="">--Select Category--</option>
									<?php 
									
									while ($gc=mysql_fetch_array($category)) {
										$id=$gc['id'];
										$gname=$gc['name'];
									?>
										<option value="<?php echo $id; ?>"><?php echo $gname;?></option>
									<?php }?>
								</select>
							</td>
						</tr>
						<tr>
							<td valign="top"><label class="fields" for="validity">Game Skills</label><label class="mandatory">*</label></td>
							<td>
								<select name="gskills_id" id="gs_id" class="gs_id required">
									<option value="" selected="selected">--Select Skills--</option>
								</select>
							</td>
						</tr>
						<tr>
							<td valign="top"><label class="fields" for="validity">Games</label><label class="mandatory">*</label></td>
							<td>
								<select name="game_id" id="game_id" class="game_id required">
									<option value="" selected="selected">--Select Games--</option>
								</select>
							</td>
						</tr>
						<tr>
							<td valign="top"><label class="fields" for="validity">No.of Times</label><label class="mandatory">*</label></td>
							<td>
								<input type="text" name="ntimes" id="ntimes" value="" class="required">
							</td>
						</tr>
						<tr>
							<td></td>
							<td align="left"><input class="submitbutton" type="submit" name="admin_limits" id="admin_limits" value="Submit"></td>
						</tr>
					</table>
				</form>
			</div>
		</div>
	</div>
</header>