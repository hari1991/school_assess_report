<?php
//ini_set( "display_errors", 0);
//include('db.php');
//session_start();
//$adminid=$_SESSION['aid'];

//$userid='2';
if(empty($adminid)){
	//header("Location:index.php?act=login");
	echo "<script>window.location='index.php?act=login'</script>";
}
?>
<script src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<script type="text/javascript" src="js/additional-methods.js"></script>
<link href="css/style2.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">


$(document).ready(function(){
	
	$("#gname").change(function()
	{
		var gname=$(this).val();
		var dataString = 'gamename='+ gname;		
		$.ajax
		({
			type: "POST",
			url: "../ajax_data.php",
			data: dataString,
			cache: false,
			success: function(html)
			{				
				if(html==1)
				{
					$(".gname").html("<img align='center' width='16' src='images/error.png' /> Game name already taken.");	
				} 
				else
				{
					$(".gname").html("<img align='center' width='16' src='images/available.png' /> Game name available.");
				}
			}
		});
	});

	$(".gc_id").change(function()
	{
		var gc_id=$(this).val();
		var dataString = 'category_id='+ gc_id;		
		$.ajax
		({
			type: "POST",
			url: "../ajax_data.php",
			data: dataString,
			cache: false,
			success: function(html)
			{
				$(".gs_id").html(html);
			} 
		});
	});

	 var progressbox     = $('#progressbox');
     var progressbar     = $('#progressbar');
     var statustxt       = $('#statustxt');
     var submitbutton    = $("#up_game");
     var myform          = $("#upload");
     var output          = $("#output");
     var completed       = '0%';
    /* jQuery.validator.setDefaults({
   	  debug: true,
   	  success: "valid"
   	});*/
     $("#upload").validate({
    		 rules: {
    			 img_path: {
    			      required: true,
    			      extension: "jpeg|jpg|png|gif"
    			    },
    			    path: {
      			      required: true,
      			      extension: "swf|mp4"
      			    }
    			  }
     });
   //alert("asdas");
             $(myform).ajaxForm({
                 //alert("asdad");
                 beforeSend: function() { //before sending form
                     submitbutton.attr('disabled', ''); // disable upload button
                     statustxt.empty();
                     progressbox.slideDown(); //show progressbar
                     progressbar.width(completed); //initial value 0% of progressbar
                     statustxt.html(completed); //set status text
                     statustxt.css('color','#000'); //initial color of status text
                 },
                 uploadProgress: function(event, position, total, percentComplete) { //on progress
                     progressbar.width(percentComplete + '%') //update progressbar percent complete
                     statustxt.html(percentComplete + '%'); //update status text   percentComplete + '%'
                     if(percentComplete>50)
                         {
                             statustxt.css('color','#fff'); //change status text to white after 50%
                         }
                     },
                 complete: function(response) { // on complete
                     output.html(response.responseText); //update element with received data
                     myform.resetForm();  // reset form
                     submitbutton.removeAttr('disabled'); //enable submit button
                     progressbox.slideUp(); // hide progressbar
                 }
         });
   
    
});
</script>
<?php $grade = mysql_query("select id,grdname from skl_grade"); ?>
<header>
	<div class="intro-head">Upload Games</div>
	<div class="intro">
		<div class="support-note"><!-- let's check browser support with modernizr -->
			<!--span class="no-cssanimations">CSS animations are not supported in your browser</span-->
			<!--  <span class="no-csstransforms">CSS transforms are not supported in your browser</span>
			<!--span class="no-csstransforms3d">CSS 3D transforms are not supported in your browser</span-->
			<!--  <span class="no-csstransitions">CSS transitions are not supported in your browser</span>
			<span class="note-ie">Sorry, only modern browsers.</span>-->
			<div id="main">

				<form name="upload" id="upload" action="../check_ajax.php" method="POST" enctype="multipart/form-data">
					<div class="reqindicates"><label class="top_mandatory">*</label>&nbsp; Indicates required fields</div>
					<table border="0" cellpadding="4" cellspacing="0">
					<tr>
							<td valign="top"><label class="fields" for="validity">Grade</label><label class="mandatory">*</label></td>
							<td>
								<select name="grades" id="grades" class="required">
									<option value="">--Select Grades--</option>
									<?php 
									while ($gradeinarr=mysql_fetch_array($grade)) {
									?>
									<option value="<?php echo $gradeinarr['id'];?>"><?php echo $gradeinarr['grdname'];?></option>
									<?php }?>
								</select>
							</td>
						</tr>
					<tr>
						<td valign="top"><label class="fields" for="validity">Game Category</label><label class="mandatory">*</label></td>
						<td>
							<select name="gc_id" id="gc_id" class="gc_id required" >
								<option value="">--Select Category--</option>
								<?php 
								$sql="select * from g_category where status='1'";
								$result=mysql_query($sql);
								while ($gc=mysql_fetch_array($result))
								{
									$id=$gc['id'];
									$gname=$gc['name'];
								?>
									<option value="<?php echo $id; ?>"><?php echo $gname;?></option>
								<?php }?>
							</select>
						</td>
					</tr>
					<tr>
						<td valign="top"><label class="fields" for="validity">Game Skills</label><label class="mandatory">*</label></td>
						<td>
							<select name="gs_id" id="gs_id" class="gs_id required">
								<option value="" selected="selected">--Select Skills--</option>
							</select>
						</td>
					</tr>
					<tr>
						<td valign="top"><label class="fields" for="validity">Game Name</label><label class="mandatory">*</label></td>
						<td><input type="text" name="gname" id="gname" class="required" value=""><span class="gname"></span></td>
					</tr>
					 <tr>
						<td valign="top"><label class="fields" for="validity">Game level</label><label class="mandatory">*</label></td>
						<td><select name="glevel" id="glevel" class="required" >
							<option value="">--Select Level--</option>
							<?php 
							$sql="select * from game_levels where status='1'";
							$result=mysql_query($sql);
							while ($gc=mysql_fetch_array($result))
							{
							$id=$gc['id'];
							$gname=$gc['level'];
							?>
							<option value="<?php echo $id; ?>"><?php echo $gname;?></option>
							<?php }?>
							</select></td>
					</tr>
					<tr>
						<td valign="top"><label class="fields" for="validity">Game Image</label><label class="mandatory">*</label></td>
						<td><input type="file" size="5" name="img_path" id="img_path"  class="required" ></td>
					</tr>
					<tr>
						<td valign="top"><label class="fields" for="validity">Flash Upload</label><label class="mandatory">*</label></td>
						<td><input type="file" size="5" name="path" id="path"  class="required" ></td>
					</tr>
					<tr>
						<td valign="top"><label class="fields" for="validity">Description</label><label class="mandatory">*</label></td>
						<td><textarea rows="5" cols="15" name="desc" class="required"></textarea></td>
					</tr> 
					<tr>
						<td></td>
						<td align="left"><input class="submitbutton" type="submit" name="up_game" id="up_game" value="Submit"></td>
					</tr>
					</table>
				</form>
				<div id="progressbox"><div id="progressbar"></div ><div id="statustxt">0%</div ></div>
				<div id="output"></div>
			</div>
		</div>
	</div>
</header>
