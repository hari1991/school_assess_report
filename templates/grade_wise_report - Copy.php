<style>
label{background-color:white !important;} 	
.label, .badge {
display: inline-block;
padding: 2px 4px;
font-size: 11.844px;
font-weight: bold;
line-height: 14px;
color: #ffffff;
vertical-align: baseline;
white-space: none;
text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.0);
background-color: #FFFFFF !important;
}
 .intro-head{height: 44px;

color: cadetblue;

font-size: 23px;
padding: 22px 5px 42px 5px;}
</style>

<style type="text/css" title="currentStyle">
@import "datatable/media/css/demo_page.css";
@import "datatable/media/css/demo_table_jui.css";
@import "datatable/examples/examples_support/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<style type="text/css">
	table tbody td{
		padding: 5px;
		margin: 4px;
	}
	.monthscroll, .skillscroll {
		height: 150px;
		width: 250px;
		overflow-y: scroll;
	}
	.submitlabel {
		text-align: center;
		margin-top: 5px;
	}
</style>
<?php 
if(empty($adminid)){
	echo "<script>window.location='index.php?act=login'</script>";
}

$schoolid=$_SESSION['schoolid'];
$get_school = mysql_query("select school_name from schools where id=".$schoolid."");
while($get_school_name = mysql_fetch_array($get_school)){
		$s_name = $get_school_name['school_name'];
	};
?>
<div role="main" class="main">

				<div class="container" style="border:1px solid #7F7F7F;min-height:60%;">
<div class="intro-head" style="height:10px;"><b style="font-size:1.4em;color:crimson">Gradewise Report</b><div style="float:right;">&nbsp;<span style="text-decoration:underline;font-size:1.4em;"><?php echo $s_name; ?></span></div></div>
<div class="intro">
<div class="support-note"><!-- let's check browser support with modernizr -->
<!--span class="no-cssanimations">CSS animations are not supported in your browser</span-->
<!--  ><span class="no-csstransforms">CSS transforms are not supported in your browser</span>
<!--span class="no-csstransforms3d">CSS 3D transforms are not supported in your browser</span-->
<!--  <span class="no-csstransitions">CSS transitions are not supported in your browser</span>
<span class="note-ie">Sorry, only modern browsers.</span>-->
<div id="main">


<script type="text/javascript" language="javascript" src="datatable/media/js/jquery.dataTables.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		oTable = $('.example ').dataTable({
			"bJQueryUI": true,
			"sPaginationType": "full_numbers"
		});
	});
</script>
<?php 

function listAllYearMonth($startDate,$endDate){
    $startDate = strtotime(str_replace("-", "/", $startDate));
    $endDate = strtotime(str_replace("-", "/", $endDate));
    $currentDate = $endDate;
    $result = array();
    while ($currentDate >= $startDate) {
        $result[] = date('Y/m',$currentDate);
        $currentDate = strtotime( date('Y/m/01/',$currentDate).' -1 month');
    }
    return $result;
}

function getAcademicYear(){
    $accYear = mysql_query("SELECT startdate,enddate FROM academic_year limit 0,1");
    while($acc = mysql_fetch_array($accYear)){
        $return = array("startDate" => $acc['startdate'],
                        "endDate" => $acc['enddate']);
    }
    return $return;
}

function getAcademicData($vMonth){
    $accDates = getAcademicYear();
    $acdMonths = listAllYearMonth($accDates['startDate'],$accDates['endDate']);
    foreach ($acdMonths as $acdMonths) {
        list($year,$month) = explode("/", $acdMonths);
        if($month == $vMonth){
            return $year;
        }
    }
}

function getAcademicData2($aMonth){
    $accDates = getAcademicYear();
    $acdMonths = listAllYearMonth($accDates['startDate'],$accDates['endDate']);
    $return = array();
    foreach ($acdMonths as $acdMonths) {
        list($year,$month) = explode("/", $acdMonths);
        if(in_array($month, $aMonth)){
            $date = "BETWEEN '". $year."-". $month . "-1' AND '". $year."-". $month . "-31'";
            $return[] = $date;
        }
    }
    return $return;
}

	$months = array(
        '06' => 'Jun',
        '07' => 'Jul',
        '08' => 'Aug',
        '09' => 'Sep',
        '10' => 'Oct',
        '11' => 'Nov',
        '12' => 'Dec',
        '01' => 'Jan',
        '02' => 'Feb',
        '03' => 'Mar',
        '04' => 'Apr',
        '05' => 'May'		
    );
	global $total, $attempt, $count;
	$total = 0;
	$count = 0;
	$school_id = $_SESSION['schoolid'];
    $sql1  = "SELECT classname, c.id FROM class c inner join skl_class_plan s on c.id = s.class_id and s.school_id='$school_id'";
	$skillsQuery = "select name,id from category_skills where category_id = 1";
	$objClass = mysql_query($sql1,$con);
	$objSkills = mysql_query($skillsQuery,$con);
	if(isset($_POST['gradeSubmit'])) {
		$sids = join(',', $_POST['skill']);
		$gid = $_POST['grade'];
		$selected_month = $_POST['month'];
		$year = !empty($_POST['year']) ? $_POST['year'] : 2009;
		$skillsGetQuery = 'select id,name from category_skills where id in('.$sids.')';
		
		//$gradeStudentQuery = 'select id,fname,lname from users where sid ='.$school_id.' and grade_id ='.$gid;
		//echo "$gradeStudentQuery";
		//$objGradeStudentQuery = mysql_query($gradeStudentQuery,$con);
	}


	function getScores($skillid, $months, $userid) {		
		$tot = 0;
		$school_id = $_SESSION['schoolid'];
		if(!empty($months)){
			$datein = "";
			foreach ($months as $key => $month) {
				$year = getAcademicData($month);
				$date = date('Y-m', strtotime($year.'-'.$month));
				if($datein <> "")
					$datein = $datein." OR ";
				$datein = $datein." DATE_FORMAT(lastupdate, '%Y-%m')='".$date."' ";
			}
			$moncount = count($months);

			$monthsQuery = getAcademicData2($months);
			$vDateRange = "";

			if(is_array($monthsQuery)){
				$vDateRange = " AND (gr.lastupdate ".implode(" OR gr.lastupdate ", $monthsQuery).")";
			}
			//$query = "SELECT count(*) as total, sum(attempt_question) as attempt, sum(game_score) as score, DATE_FORMAT(lastupdate, '%b') as month FROM game_reports where gs_id =".$skillid." and (".$datein.") and gu_id ='".$userid."' order by gs_id";
			$query = "select grade_name, avg(score) as score, gu_id
					  From
					  (
						select grade_name, name, (sum(score) / (daycnt)) as score, gu_id, YearMonth from
						(			
							select a.grade_name, a.name, sum(a.score) as score, gu_id, b.daycnt, grade_id, a.gs_id, a.YearMonth 
							from
							(
								SELECT c.grade_name, c.grade_id, c.name, avg(c.score) as score, c.gu_id, c.gs_id, c.g_id, DATE_FORMAT(lastupdate, '%Y-%m') as YearMonth
								From
								(
									SELECT cls.classname as grade_name, grade_id, concat(usr.fname,' ',lname) as name, avg(gr.game_score) as score, gr.gu_id, gr.gs_id, gr.g_id, lastupdate
									FROM game_reports gr 
									inner join users usr ON usr.id = gr.gu_id
									inner join class cls ON cls.id = usr.grade_id
									WHERE gs_id =".$skillid." and (".$datein.") and gu_id ='".$userid."' 
									group by lastupdate, gr.g_id order by gr.g_id
								) as c group by g_id, DATE_FORMAT(lastupdate, '%Y-%m') order by g_id
							) AS a 
							JOIN 
							(
								select count(daycnt) as daycnt, gs_id, YearMonth
								From
								(
									SELECT COUNT(lastupdate) AS daycnt, gr.gs_id, gr.g_id, DATE_FORMAT(lastupdate, '%Y-%m') as YearMonth
									From game_reports gr 
									where gu_id=".$userid." and gr.gs_id in (".$skillid.") " . $vDateRange."
									group by gr.g_id, DATE_FORMAT(lastupdate, '%Y-%m')
									order by gr.g_id
								) as d group by gs_id, YearMonth							
							)as b on a.gs_id = b.gs_id and a.YearMonth = b.YearMonth
							group by grade_id, gu_id, YearMonth
						) as e group by gu_id, YearMonth
					   )as f group by gu_id";
			
			//select count(distinct(lastupdate)) as daycnt from game_reports as gr where gs_id =".$skillid." and gu_id ='".$userid."'" . $vDateRange."
			//select count(distinct(lastupdate)) as daycnt from game_reports as gr 
			//INNER JOIN users u on u.id = gr.gu_id where u.sid = ".$school_id. $vDateRange."
			//INNER JOIN users u on u.id = gr.gu_id where u.sid = ".$school_id. $vDateRange."
			//echo "$userid";
			//echo "$query";
			$objQuery = mysql_query($query);
			$cnt = 0;
			while($row = mysql_fetch_assoc($objQuery)){
				$tot = $tot + $row['score'];
			}
			//while($row = mysql_fetch_assoc($objQuery)){
			//	$cnt = $cnt + $row['total'];	
			//	$tot = $tot + $row['score'];
			//}
		}
		//return round($tot/$cnt);
		return number_format((float)$tot, 2, '.', '');
	}
	
	function getBSPI($skillArr, $months, $userid) {		
		$tot = 0;
		if(!empty($months)){
			$datein = "";
			foreach ($months as $key => $month) {
				$year = getAcademicData($month);
				$date = date('Y-m', strtotime($year.'-'.$month));
				if($datein <> "")
					$datein = $datein." OR ";
				$datein = $datein." DATE_FORMAT(lastupdate, '%Y-%m')='".$date."' ";
			}
			$query = "SELECT 1 as total, avg(game_score) as score FROM game_reports where gs_id in (".implode(",", $skillArr).") and (".$datein.") and gu_id ='".$userid."' group by gs_id order by gs_id";
			//echo "$query";
			$objQuery = mysql_query($query);
			$cnt = 0;
			while($row = mysql_fetch_assoc($objQuery)){
				$cnt = $cnt + $row['total'];	
				$tot = $tot + $row['score'];
			}
		}
		return round($tot/$cnt);
	}

?>
<form action="" class="cmxform" method="POST" id="gradewise_rep" accept-charset="utf-8">
	<table  align="center" style="background:#FFFFFF !important;color:#3F3C3C;width:92% !important;border:1px solid #7F7F7F;">
		<tr>
			<td>
				<div class="firstlabel"><label class="fields" for="grade">Grade </label></div>
			</td>
			<td><div class="secondinput">
						<select name="grade" id="grade_sel" onchange="grade_change(this.value);">
							<option value="-1">Select Grade</option>
							<?php 
							$overallskills = mysql_query("SELECT class.id as id, class.classname as classname FROM class 
                                            LEFT JOIN skl_class_section ON skl_class_section.class_id = class.id
                                            WHERE skl_class_section.school_id =".$schoolid." group by class.classname");
							while($objresult = mysql_fetch_object($overallskills)){ 
								$selected = ($_POST['grade'] == $objresult->id) ? "selected" : "";
							?>					
				            	<option value="<?php echo $objresult->id;?>" <?php echo $selected;?>><?php echo $objresult->classname;?></option>
				            <?php } ?>
						</select>
					</div>
				</div>
			</td>
			<td>&nbsp;</td>
			<td><div class="months"><label class="fields" for="lname">Months </label></div></td>
            <td>
                <div class="month_details">
					<?php 
						foreach ($months as $key => $month) { 
							$checked = (in_array($key, $_POST['month'])) ? "checked" : "";
					?>
							<div><input type="checkbox" name="month[]" id="mth" <?php echo $checked;?> value="<?php echo $key;?>" class="month" /><?php echo $month;?></div>
				    <?php } ?>
                </div>
            </td>
            <td>&nbsp;</td>
			<td><div class="skills"><label class="fields" for="lname">Skills </label></div></td>
            <td>
                <div class="skills_details">
                    <?php while($objresult = mysql_fetch_object($objSkills)){ 
                    	$checked = (in_array($objresult->id, $_POST['skill'])) ? "checked" : "";
                    ?>	
							<div><input type="checkbox" name="skill[]" <?php echo $checked;?> value="<?php echo $objresult->id;?>" class="skill" /><?php echo $objresult->name;?></div>
					<?php } ?>
                </div>
            </td>
			<td>&nbsp;</td>
			
			<?php /*<td>
				<div class="across_sections">
					<?php  
						$selecteda = ($_POST['acrosssections'] == "Yes") ? "selected" : "";
						$selectedb = ($_POST['acrosssections'] == "No") ? "selected" : "";												
					?>
					<select name="acrosssections" style="width: auto;">
						<option <?php echo $selecteda;?> value="Yes" >Yes</option>
						<option <?php echo $selectedb;?> value="No" >No</option>												
					</select>
				</div>
			</td>	*/?>
			<td><div class="grade"><label class="fields" for="lname">Section </label></div></td>
                                    <td>
										<?php
										 
										 $sql_sec = mysql_query("select section,id from skl_class_section where school_id = ".$schoolid." and class_id=".$_POST['grade']."");						
										  
										?>
										<?php /*if($_POST['section'] != ""){?>
										<div>
											<select name="section" id="sect">
											<?php while($dd = mysql_fetch_array($sql_sec)){
												
												 ?>
													
													<option <?php if($_POST['section'] == $dd['section']){?> selected="selected"<?php }?> value="<?php echo $dd['section'];?>"><?php echo $dd['section'];?></option>
													<?php
													
												}?>
											</select>
										</div>
										<?php }else{?>
                                        <div class="section">	
											<select><option value="0">Select</option></select>
                                        </div>
                                        <?php } */?>
                                        <div class="section">	
											<select name="section">
											<?php while($dd = mysql_fetch_array($sql_sec)){
												
												 ?>
													
													<option <?php if($_POST['section'] == $dd['section']){?> selected="selected"<?php }?> value="<?php echo $dd['section'];?>"><?php echo $dd['section'];?></option>
													<?php
													
												}?>
											</select>
                                        </div> 
                                        <td style="position: relative;min-width:30px;">
										<div id="wait" style="display:none;height:55px;"><img src="images/ajax-loader2.gif"  /><br></div> 
                                    </td>
                                    </td>
                                    
                                    		
		</tr>
	</table>	
	<div class="submitlabel"><input type="submit" class="submitbutton" id="editsubmit" name="gradeSubmit" value="Display"><div id="err_show"></div></div></div>
</form>


	<?php if(isset($_POST['gradeSubmit'])) { 
		/*$gid = $_POST['grade'];
		if($_POST['acrosssections'] == "Yes"){
			$sql1="select 'All' as section, classname from class as b where b.id = '$gid'";
			$result1=mysql_query($sql1);						
		}
		else{*/
			$sql1="select section, classname from skl_class_section as a inner join class as b on a.class_id = b.id where a.class_id = '$gid' and a.section='".$_POST['section']."' group  by section";
			
			$result1=mysql_query($sql1);
			mysql_num_rows($result1);
			//echo "$sql1";
		//}	
		
		while($objgrade = mysql_fetch_object($result1)){ 
			echo "<br>";
			/*if($_POST['acrosssections'] == "Yes"){
				echo "<div align='center'><b>Performance Report - All Sections</b></div></br>";//$skillTop";
				$gradeStudentQuery = 'select id,fname,lname from users where sid ='.$school_id.' and grade_id ='.$gid.' order by fname, lname';
			}
			else{*/
				$classname = $objgrade->classname;
				$section = $objgrade->section;
				echo "<div align='center'><b>Gradewise Report - ".$classname." - ".$section."</b></div></br>";//$skillTop";
				$gradeStudentQuery = 'select id,fname,lname from users where sid ='.$school_id.' and grade_id ='.$gid.' and section = "'.$_POST['section'].'" order by fname, lname';
				//echo "$gradeStudentQuery";
			//}
			$objSkillGetQuery = mysql_query($skillsGetQuery,$con);
			$objGradeStudentQuery = mysql_query($gradeStudentQuery,$con);		
		?>
			<table class="display example" id="example">
				<thead>
					<tr>
						<th>Student Name</th>
						<?php 
							$skillArray = array();
							while($objresult = mysql_fetch_object($objSkillGetQuery)){ 
								array_push($skillArray, $objresult->id)
							?>
							<th><?php echo $objresult->name;?></th>
						<?php } ?>
						<th>Total</th>
						<th>BSPI</th>
					</tr>
				</thead>
				<tbody>
					<?php while($objresult = mysql_fetch_object($objGradeStudentQuery)){ ?>
						<tr>
							<td>
								<?php echo $objresult->fname.' '.$objresult->lname; ?>
							</td>
							<?php if(!empty($skillArray)){
									$loopcount = 0;
									$nonzeroskill = 0;
									foreach ($skillArray as $key => $value) { ?>
									<td>
										<?php 
										 $score = getScores($value, $selected_month,$objresult->id);
										$loopcount = $loopcount  + $score;
										$nonzeroskill = $nonzeroskill + 1;
										echo $score;?>
									</td>
										
								<?php } ?>
							<?php } ?>
							<td>
								<?php echo $loopcount; ?>
							</td>
							<td>
								<?php 
									//$count = !empty($count) ? $count : 1;
									//$bspi = $total/$count;
									//echo !empty($bspi) ? round($bspi,2) : 0; 
									if(!empty($skillArray)){
										//$bspi = getBSPI($skillArray, $selected_month,$objresult->id);
										//echo !empty($bspi) ? round($bspi) : 0; 
										$bspi = number_format((float)$loopcount / $nonzeroskill, 2, '.', '');
										echo !empty($bspi) ? number_format((float)$bspi, 2, '.', '') : number_format((float)0, 2, '.', '');
									}
								 ?>
							</td>
						</tr>
					<?php $total = 0;$count=0;  } ?>
				</tbody>
	
			</table>
	<?php } ?>				
<?php } ?>	
</div>
</div>
</div>
</div>
</div>
 
<script type="text/javascript">
function grade_change(val){
	
	 
	 
		 
		var school = '<?php echo $schoolid;?>';

		var dataString = 'grade_section_sadmin='+val+'&schl_adm='+school;
		 
			//var dataString = 'classone='+ classone+'&schoolone='+schoolone;	
     
     $.ajax({
		 type:"POST",
		 url: "../ajax_data.php",
		 data: dataString,
		 cache: false,
		 success:function(result)
		 {
			 
			$(".section").html(result);
		 }
		});


  
	
	
	}
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#gradewise_rep").submit(function(e) {	
		//var myLength = $("#grade").val().length;
	if($("#grade_sel").val() == "-1") {
		
			$('#err_show').html("<p style='color:red;'>Please Select Grade</p>");
			return false;
		}
		if((!$('input[name^=month]:checked').length)) {
		$('#err_show').html("<p style='color:red;'>Please Select Month</p>");
		return false;
	}			
	if((!$('input[name^=skill]:checked').length)) {
        $('#err_show').html("<p style='color:red;'>Please Select Skills</p>");
        //stop the form from submitting
        return false;
    }
    
	if($("#sect").val() == "-1") {
		$('#err_show').html("<p style='color:red;'>Please Select Section</p>");
		return false;
	}

    return true;
});
	});
	 $(document).ajaxStart(function(){
    $("#wait").css("display","inline");
  });
  $(document).ajaxComplete(function(){
    $("#wait").css("display","none");
  });
	
</script>
