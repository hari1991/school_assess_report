<?php 
if(empty($adminid)){
	echo "<script>window.location='index.php?act=login'</script>";
}
include_once("class_avg_skillscore_bl.php");

?>
	 <script type="text/javascript" src="js/bootstrap-multiselect.js"></script>
 <?php $AverageScore=$skillQuotient_value; ?>

<div class="pageHomePager Dashboardhide mygameshide myreporthide myprofilehide">
   <div class="row">
  <div class="col-lg-12">
                    <h1 class="page-header">Classwise – Average Skill Scores</h1>
                </div>
			</div>	
			<div class="row">
      			<div class="col-lg-12">
				<form name="frmStudentReport" id="frmStudentReport" method="post">
				<div class="panel panel-default">
            <div class="panel-body reportChartContainer">
			<div class="col-lg-4">
				<div class="row">
				<label class="col-lg-6">Grade</label>
				<select name="chkGrade" id="chkveg1" multiple="multiple">
			<?php foreach($GradeList as $grades) {
				$gradinid='';
				$selected='';
				if($ddlGradeType==$grades['id']){$selected="selected='selected'";$gradinid=$grades['id'];}
				echo "<option id='".$grades['id']."' ".$selected." value='".$grades['id']."'>".trim(str_replace("Grade",'',$grades['classname']))."</option>";
			} ?>
			            </select>
						<input type="hidden" value="" name="hdnGradeID" id="hdnGradeID" />
			            	<script type="text/javascript">
			            	    $(function() {
			            	        $('#chkveg1').multiselect({
			            	            includeSelectAllOption: true,numberDisplayed: 1,nonSelectedText: 'Select',buttonClass: 'form-control col-lg-6',
										onChange: function(element, checked) {
        var brands = $('#chkveg1 option:selected');
        var selected = [];
        $(brands).each(function(index, brand){
            selected.push([$(this).val()]);
        });
		$("#hdnGradeID").val(selected);
		
										}
			            	        });
									
			            	        $('#btnget').click(function() {
			            	       // alert($('#chkveg').val());
			            	        })
			            	    });
			</script>
			<script type="text/javascript">
			    $(document).ready(function() {
			    $('#chkveg1').multiselect();
				<?php if(isset($_POST['btnSearch'])) { ?>
				var data="<?php echo $hdnGradeID; ?>";
				var dataarray=data.split(",");
				$("#chkveg1").val(dataarray);
				$("#hdnGradeID").val(dataarray);
				$("#chkveg1").multiselect("refresh");
				<?php } ?>
			    });
</script>
			</div>
			
				</div>
		
			
			<div class="col-lg-4">
			<div class="row">
				<label class="col-lg-6">Skill</label>
        <select name="chkSkill" id="chkveg" multiple="multiple">
			<option value="59">Memory</option>
			<option value="60">Visual Processing</option>
			<option value="61">Focus and Attention</option>
			<option value="62">Problem Solving</option>
			<option value="63">Linguistics</option>
			            </select>
			            <input type="hidden" value="" name="hdnSkillID" id="hdnSkillID" />
			            	<script type="text/javascript">
			            	    $(function() {
			            	        $('#chkveg').multiselect({
			            	            includeSelectAllOption: true,numberDisplayed: 1,nonSelectedText: 'Select',buttonClass: 'form-control col-lg-6',
										onChange: function(element, checked) {
        var brands = $('#chkveg option:selected');
        var selected = [];
        $(brands).each(function(index, brand){
            selected.push([$(this).val()]);
        });
		$("#hdnSkillID").val(selected);
		
										}
			            	        });
									
			            	        $('#btnget').click(function() {
			            	       // alert($('#chkveg').val());
			            	        })
			            	    });
			</script>
			<script type="text/javascript">
			    $(document).ready(function() {
			    $('#chkveg').multiselect();
				<?php if(isset($_POST['btnSearch'])) { ?>
				var data="<?php echo $hdnSkillID; ?>";
				var dataarray=data.split(",");
				$("#chkveg").val(dataarray);
				$("#chkveg").multiselect("refresh");
				<?php } ?>
			    });
</script>
				
			</div>
		
			</div>
			<div class="col-lg-1"></div>
			<div class="col-lg-1">
			<div class="row">
			<input  type="submit" name="btnSearch" class="btn btn-success" id="btnSubmit" value="Submit" />
			</div>
				</div>
				
			<div class="col-lg-1">
			<div class="row">
			<input  type="button" onclick="javascript:window.location.href=window.location.href;" name="btnSubmit" class="btn btn-warning" id="btnSubmit" value="Reset" />
			</div>
				</div>
				
				
				</div>
				</div>
				</form>
				
				</div>
				</div>
				

   
   <div id="StudentReportResult"  >
             
		 <?php if(isset($_POST['btnSearch'])) { ?>  	
			
			<div class="row">
			
				
				
 			</div>
			
			
			
		 <?php } ?>
			
</div>
</div>


 <div class="row" <?php if(isset($_POST['btnSearch'])) {  } else {?> style="display:none;" <?php } ?>>
      			<div class="col-lg-12 landingContainer">
				<div class="row">
				
				 </div>
        			<table id="assementTable" class="table table-striped table-bordered table-hover table-condensed">
    <thead>
      <tr>
        <th>S.No.</th>
        <th>Grade</th>
        <th>Section</th>
		<th>Skill</th>
		<th>Average Skill Score</th>
        
      </tr>
    </thead>
    <tbody>
	<?php 
	
	$ini=0; 
	foreach($reportdatas as $r1){
	$ini++;
	
	?>	
      <tr>
        <td><?php echo $ini; ?></td>
        <td><?php echo $r1['gradename']; ?></td>
        <td><?php echo $r1['section']; ?></td>
		<td><?php echo $r1['skillname']; ?></td>
		<td><?php echo round($r1['avgscore'], 2); ?></td>
		
      </tr>
	<?php } ?>
      
	  
    </tbody>
  </table>
                    
      			</div>
				
				
 			</div>
			
			<?php $path='reports/'.$_SESSION['schoolid'].'/Classwise_Avg_Skillscore_'.date('Ymdhis').'.xlsx'; ?>
			<div class="row" <?php if(isset($_POST['btnSearch'])) {  } else {?> style="display:none;" <?php } ?>>
<input type="button" class="btn btn-success" id="btnExcelExport" name="btnExcelExport" value="Export to excel">
</div>

<script>
$("#btnExcelExport").click(function(){
	//alert('hello');
	//var ddlGradeType = $('#ddlGradeType').val();
	
		$.ajax({
		type: "POST",
		url: "templates/ajax_classwise_avgskillscore_excelexport.php", 
		data:{hdnGradeID:'<?php echo $hdnGradeID; ?>',hdnSkillID:'<?php echo $hdnSkillID; ?>',path:'<?php echo $path; ?>',schoolid:'<?php echo $_SESSION['schoolid'];?>'},
		success: function(result)
		{
			//alert(result);
			//console.log(result);
			if('<?php echo "templates/".$path;?>'==$.trim(result))
			window.location=result; 
		}
	});
		});




function ajaxsectionload(gradeID,selectID)
{
$.ajax({
		url: "templates/ajax_sectionbygrade.php", 
		data:{gradeid:gradeID,schoolid:'<?php echo $_SESSION['schoolid'];?>',defaultselect:selectID},
		success: function(result)
		{
			$("#ddlSection").html(result);
		}
	});
	}

$(document).ready(function() {
	
    $('#assementTable').DataTable( );
} );
</script>
 