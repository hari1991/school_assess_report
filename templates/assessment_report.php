
<?php 
if(empty($adminid)){
	echo "<script>window.location='index.php?act=login'</script>";
}
include_once("assessment_report_bl.php");

?>
 

<div class="pageHomePager Dashboardhide mygameshide myreporthide myprofilehide">
   <div class="row">
  <div class="col-lg-12">
                    <h1 class="page-header">Assessment Report</h1>
                </div>
			</div>	
			<br/>
   <div class="row">
      			<div class="col-lg-12">
				<form name="frmAssessmentReport" id="frmAssessmentReport" method="post">
				<div class="panel panel-default">
            <div class="panel-body reportChartContainer">
			
			<div class="col-lg-4">
				<div class="row">
				<label class="col-lg-6">Grade </label>
				<select class="form-control col-lg-6" required="true" style="width:50%" name="ddlGradeType" id="ddlGradeType">
			<option value="">Select</option>
			<?php foreach($GradeList as $grades) {
				$gradinid='';
				$selected='';
				if($ddlGradeType==trim(str_replace("Grade",'',$grades['classname']))){$selected="selected='selected'";$gradinid=$grades['id'];}
				echo "<option id='".$grades['id']."' ".$selected." value='".trim(str_replace("Grade",'',$grades['classname']))."'>".trim(str_replace("Grade",'',$grades['classname']))."</option>";
			} ?>
			</select>
			</div>
			<br/>
			<div class="row">
			<label class="col-lg-6">Section</label>
				<select class="form-control col-lg-6" required="true"  style="width:50%" name="ddlSection" id="ddlSection">
			<option value="">Select</option>
			</select>
			</div>
				</div>
				<div class="col-lg-4">
				<label class="col-lg-6">Assessment</label>
				<select class="form-control col-lg-6" required="true"  style="width:50%" name="ddlAssessmentType" id="ddlAssessmentType">
			<option value="">Select</option>
			<option <?php if($ddlAssessmentType=="pretest"){ echo "selected='selected'"; } ?> value="pretest">Pre Assessment</option>
			<option <?php if($ddlAssessmentType=="posttest1"){ echo "selected='selected'"; } ?> value="posttest1">Post Assessment - I</option>
			<option <?php if($ddlAssessmentType=="posttest2"){ echo "selected='selected'"; } ?> value="posttest2">Post Assessment - II</option>
			</select>
				</div>
				<div class="col-lg-4">
				<div class="row">
				<span class="col-lg-4"></span>
				<input type="submit" value="Search" name="btnSearch" id="btnSearch" class="btn btn-success col-lg-4" />
				<span class="col-lg-2"></span>
			</div>
			<br/>
			<div class="row">
				<span class="col-lg-4"></span>
				<input type="button" value="Reset" onclick="javascript:window.location.href=window.location.href;" name="btnReset" id="btnReset" class="btn btn-warning col-lg-4" />
				<span class="col-lg-2"></span>
			</div>
				</div>
				
				</div>
				</div>
				</form>
				</div>
				</div>
				 
				 
              <div class="row" <?php if(isset($_POST['btnSearch'])) {  } else {?> style="display:none;" <?php } ?>>
      			<div class="col-lg-12 landingContainer">
				<div class="row">
				 <div class="alert alert-warning" role="alert"> <strong><?php echo $ddlGradeType.' - '.$ddlSection.' - '. $AssessmentText; ?>  </strong> </div>
				 </div>
        			<table id="assementTable" class="table table-striped table-bordered table-hover table-condensed">
    <thead>
      <tr>
        <th>S.No.</th>
        <th>Name</th>
        <th>Class</th>
        <th>M</th>
        <th>VP</th>
        <th>FA</th>
        <th>PS</th>
        <th>L</th>
        <th>BSPI</th>
      </tr>
    </thead>
    <tbody>
	<?php 
	$exporttext='';
	$exporttext.='S.No.,Name,Class,Memory,Visual Processing,Focus and Attention,Problem Solving, Linguistics,BSPI'.""."PHP_EOL";
	$ini=0; 
	foreach($AssessmentReport as $assessment) { 
	$ini++;
	$exporttext.=$ini.','.$assessment['studentname'].','.$ddlGradeType.'-'.$ddlSection.','.$assessment['memory'].','.$assessment['visualprocessing'].','.$assessment['focusattention'].','.$assessment['problemsolving'].','.$assessment['linguistics'].','.round((($assessment['memory']+$assessment['visualprocessing']+$assessment['problemsolving']+$assessment['focusattention']+$assessment['linguistics'])/5),2).""."PHP_EOL";
	
	?>
      <tr>
        <td><?php echo $ini; ?></td>
        <td><?php echo $assessment['studentname']; ?></td>
        <td><?php echo $ddlGradeType.'-'.$ddlSection; ?></td>
        <td><?php echo $assessment['memory']; ?></td>
        <td><?php echo $assessment['visualprocessing']; ?></td>
        <td><?php echo $assessment['focusattention']; ?></td>
        <td><?php echo $assessment['problemsolving']; ?></td>
		<td><?php echo $assessment['linguistics']; ?></td>
		<td><?php echo round((($assessment['memory']+$assessment['visualprocessing']+$assessment['problemsolving']+$assessment['focusattention']+$assessment['linguistics'])/5),2); ?></td>
      </tr>
	<?php } ?>
      
	  
    </tbody>
  </table>
                   
      			</div>
		<script>
		
</script>		
				
 			</div>
		<?php $path='reports/'.$_SESSION['schoolid'].'/'.$ddlGradeType.'-'.$ddlSection.'-'.str_replace(" ",'',$AssessmentText).'_AssessmentReport_'.date('Ymdhis').'.xlsx'; ?>	
<div class="row" <?php if(isset($_POST['btnSearch'])) {  } else {?> style="display:none;" <?php } ?>>
<input type="button" class="btn btn-success"  id="btnExcelExport" name="btnExcelExport" value="Export to excel">
</div>
</div>

<script>
$("#btnExcelExport").click(function(){
		$.ajax({
		url: "templates/ajax_assessment_excelexport.php", 
		data:{ddlAssessmentType:'<?php echo $ddlAssessmentType; ?>',ddlSection:'<?php echo $ddlSection; ?>',ddlGradeType:'<?php echo $ddlGradeType; ?>',content:'<?php echo $exporttext; ?>',path:'<?php echo $path; ?>',schoolid:'<?php echo $_SESSION['schoolid'];?>'},
		success: function(result)
		{
			//alert(result);
			if('<?php echo "templates/".$path;?>'==$.trim(result))
			window.location=result;
		}
	});
		});
function ajaxsectionload(gradeID,selectID)
{
$.ajax({
		url: "templates/ajax_sectionbygrade.php", 
		data:{gradeid:gradeID,schoolid:'<?php echo $_SESSION['schoolid'];?>',defaultselect:selectID},
		success: function(result)
		{
			$("#ddlSection").html(result);
		}
	});
	}
$("#ddlGradeType").change(function(){
ajaxsectionload($('option:selected', this).attr('id'),'')	
});
$(document).ready(function() {
	<?php if(isset($_POST['btnSearch'])) { ?>
		ajaxsectionload($('option:selected', $("#ddlGradeType")).attr('id'),'<?php echo $ddlSection; ?>')	;
		
	<?php } ?>
    $('#assementTable').DataTable( );
} );
</script>
 