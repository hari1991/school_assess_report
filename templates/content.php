<?php
switch ($act)
{
	case 'dashboard':
		include_once 'templates/dashboard.php';
		break;
	case 'assessment_report':
		include_once 'templates/assessment_report.php';
		break;
	case 'login':
		include_once 'templates/login.php';
		break;
	case 'logout':
		include_once 'templates/login.php';
		break;
	case 'addteacher':
		include_once 'templates/addteacher.php';
		break;		
	case 'addsusers':
		include_once 'templates/addsusers.php';
		break;
	case 'bulk_teacher':
		include_once 'templates/bulk_teacher.php';
		break;
	case 'bulk_section':
		include_once 'templates/bulk_section.php';
		break;
	case 'assign_class':
		include_once 'templates/assign_class.php';
		break;
	case 'user_limit':
		include_once 'templates/user_limit.php';
		break;		
	
	case 'addstudent':
		include_once 'templates/addstudent.php';
		break;		
	case 'sectioncreation': 
		include_once 'templates/sectioncreation.php';
		break;	
	case 'editprofilestudent': 
		include_once 'templates/editprofilestudent.php';
		break;			
	case 'grade_report':
		include_once 'templates/grade_wise_report.php';
		break;
	case 'grade_report1':
		include_once 'templates/grade_wise_report1.php';
		break;
	case 'skill_analysis':
		include_once 'templates/skillanalysis.php';
		break;
	case 'skill_improvement_need':
		include_once 'templates/skill_improvement_need.php';
		break;
	case 'student_performance_report':
		include_once 'templates/student_performance_report.php';
		break;
	case 'student_detailed_report':
		include_once 'templates/student_detailed_report.php';
		break;
	case 'student_detailed_report_pdf':
		include_once 'templates/student_detailed_report_pdf.php';
		break;
	case 'student_detailed_report_pdf_all':
		include_once 'templates/student_detailed_report_pdf_all.php';
		break;
	case 'student_detailed_report_download':
		include_once 'templates/student_detailed_report_download.php';
		break;	
	case 'bspi_report1':
		include_once 'templates/bspi_report1.php';
		break;	
	case 'class_summary_report':
		include_once 'templates/class_summary_report.php';
		break;
	case 'usage_summary_report':
		include_once 'templates/usage_summary_report.php';
		break;
	case 'bspi_report2':
		include_once 'templates/bspi_report2.php';
		break;
	case 'bspi_report3':
		include_once 'templates/bspi_report3.php';
		break;
	case 'bspi_report_across_grade':
		include_once 'templates/bspi_report_across_grade.php';
		break;
	case 'training_skill_report1':
		include_once 'templates/training_skill_report1.php';
		break;
	case 'training_skill_report2':
		include_once 'templates/training_skill_report2.php';
		break;
	case 'training_skill_report_across_grade':
		include_once 'templates/training_skill_report_across_grade.php';
		break;
	case 'student_game_status':
		include_once 'templates/student_game_status.php';
		break;	
	case 'all_students_game_status':
		include_once 'templates/all_students_game_status.php';
		break;	
	case 'all_students_game_play_status':
		include_once 'templates/all_students_game_play_status.php';
		break;
		case 'all_students_game_play_status_detailed':
		include_once 'templates/all_students_game_play_status_detailed.php';
		break;	
	case 'classwise_average_bspi':
		include_once 'templates/classwise_average_bspi.php';
		break;
	case 'gradewise_average_bspi':
		include_once 'templates/gradewise_average_bspi.php';
		break;
	case 'gradewise_average_skillscore':
		include_once 'templates/gradewise_average_skillscore.php';
		break;
	case 'gradewise_skill_analysis':
		include_once 'templates/gradewise_skill_analysis.php';
		break;
	case 'skilltoppers':
		include_once 'templates/skilltoppers.php';
		break;	
	case 'gsskilltoppers':
		include_once 'templates/gsskilltoppers.php';
		break;
	case 'overalltoppers':
		include_once 'templates/overalltoppers.php';
		break;						
	case 'datewise':
		include_once 'templates/datewisereport.php';
		break;		
	case 'datewise_student_report':
		include_once 'templates/datewise_student_report.php';
		break;	
	case 'gradewise_game_play_status':
		include_once 'templates/gradewise_game_play_status.php';
		break;	
	case 'improvement':
		include_once 'templates/improvement.php';
		break;
	case 'bspi_range':
		include_once 'templates/bspi_range.php';
		break;				
	case 'monthly_performance':
		include_once 'templates/monthly_performance_tracking.php';
		break;				
	case 'overall_performance':
		include_once 'templates/overall_performance_tracking.php';
		break;				
	case 'term_and_service':
			include_once 'terms-of-service.html';
	break;
	case 'policies':
			include_once 'policies.html';
	break;
	case 'faq':
			include_once 'faq.html';
	break;		
	
	case 'class_avg_skillscore':
		include_once 'templates/class_avg_skillscore.php';
		break;
		
		case 'skills_overalltoppers':
		include_once 'templates/skills_overalltopper.php';
		break;
		
		
	
	default:
		include_once 'templates/dashboard.php';
		break;
}

?>
