<?php 
if(empty($adminid)){
	echo "<script>window.location='index.php?act=login'</script>";
}

include_once("grade_wise_report_bl1.php");

//echo 'hellossss'; exit;
?>


 <script type="text/javascript" src="js/bootstrap-multiselect.js"></script>

<div class="pageHomePager Dashboardhide mygameshide myreporthide myprofilehide">
   <div class="row">
		<div class="col-lg-12">
						<h1 class="page-header">Individual Students Results</h1>
		</div>
	</div>	
	<div class="row">
		<div class="col-lg-12">
			<form name="frmdownloaddoc" id="frmdownloaddoc" method="POST" action="templates/grade_wise_report_bl1_doc.php" >
				<input type="submit" name="btndoc" id="btndoc" value="Download Docment" />
			</form>
		</div>
	</div>	
			<br/>
		
              <div class="row">
      			<div class="col-lg-12 landingContainer">
				
    <table id="assementTable" class="table table-striped table-bordered table-hover table-condensed">
    <thead>
      <tr>
        <th>S.No.</th>
        <th>Name</th>
        <th>Class</th>
		<th>Section</th>
        <th>Memory</th>
        <th>Visual Processing</th>
        <th>Focus and Attention</th>
        <th>Problem Solving</th>
        <th>Linguistics</th>
        <th>BSPI</th>
		<th>Need Improvement</th>
      </tr>
    </thead>
	
	<tfoot>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
				
				<th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </tfoot>
	
    <tbody>
	<?php 

	$ini=0; 
	foreach($GradeWiseReport as $gradewise){
	$ini++;
	
	?>	
      <tr>
        <td><?php echo $ini; ?></td>
        <td><?php echo $gradewise['fname']; ?></td>
		<td><?php echo $gradewise['gradename']; ?></td>
        <td><?php echo $gradewise['section']; ?></td>
		<td><?php if($gradewise['skillscorem']=='') { echo '-'; } else { echo round($gradewise['skillscorem'], 2); } ?></td>
       <td><?php if($gradewise['skillscorev']=='') { echo '-'; } else { echo round($gradewise['skillscorev'], 2); } ?></td>
        <td><?php if($gradewise['skillscoref']=='') { echo '-'; } else { echo round($gradewise['skillscoref'], 2); } ?></td>
       <td><?php if($gradewise['skillscorep']=='') { echo '-'; } else {  echo round($gradewise['skillscorep'], 2); } ?></td>
       <td><?php if($gradewise['skillscorel']=='') { echo '-'; } else {  echo round($gradewise['skillscorel'], 2); } ?></td>
		<td><?php if($gradewise['avgbspiset1']=='') { echo '-'; } else { echo round($gradewise['avgbspiset1'], 2); } ?></td>
		<td><?php if($gradewise['needimprovement']=='') { echo '-'; } else { echo $gradewise['needimprovement']; } ?></td>
      </tr>
	<?php } ?>
      
	  
    </tbody>
  </table>
                    
      			</div>
				
				
 			</div>
			<?php $path='reports/'.$_SESSION['schoolid'].'/'.$frontGradeName.'-'.$ddlSection.'_GradeWiseReport_'.date('Ymdhis').'.xlsx'; ?>
<div class="row" <?php if(isset($_POST['btnSearch'])) {  } else {?> style="display:none;" <?php } ?>>
<input type="button" class="btn btn-success" id="btnExcelExport" name="btnExcelExport" value="Export to excel">
</div>
</div>


<script>
$("#btnExcelExport").click(function(){
		$.ajax({
		url: "templates/ajax_gradewise_excelexport.php", 
		data:{ddlAssessmentType:'<?php echo $ddlAssessmentType; ?>',ddlSection:'<?php echo $ddlSection; ?>',ddlGradeType:'<?php echo $ddlGradeType; ?>',hdnSkillID:'<?php echo $hdnSkillID; ?>',hdnMonthID:'<?php echo $hdnMonthID; ?>',content:'<?php echo $exporttext; ?>',path:'<?php echo $path; ?>',schoolid:'<?php echo $_SESSION['schoolid'];?>'},
		success: function(result)
		{
			if('<?php echo "templates/".$path;?>'==$.trim(result))
			window.location=result;
		}
	});
		});
function ajaxsectionload(gradeID,selectID)
{
$.ajax({
		url: "templates/ajax_sectionbygrade.php", 
		data:{gradeid:gradeID,schoolid:'<?php echo $_SESSION['schoolid'];?>',defaultselect:selectID},
		success: function(result)
		{
			$("#ddlSection").html(result);
		}
	});
	}
$("#ddlGradeType").change(function(){
ajaxsectionload($('option:selected', this).attr('id'),'')	
});
$(document).ready(function() {
	<?php if(isset($_POST['btnSearch'])) { ?>
		ajaxsectionload($('option:selected', $("#ddlGradeType")).attr('id'),'<?php echo $ddlSection; ?>')	;
		
	<?php } ?>
    
} );
//$('#assementTable').DataTable( );
</script>

<script>
	/* $('.dataTable').DataTable({
		"lengthMenu": [[10,  -1], [10,  "All"]]
		//"scrollX": true
	}); */
	$(document).ready(function() {
		$('#assementTable').DataTable( {
			initComplete: function () {
				this.api().columns().every( function () {
					var column = this; //console.log(column);
					var select = $('<select><option value=""></option></select>')
						.appendTo( $(column.footer()).empty() )
						.on( 'change', function () {
							var val = $.fn.dataTable.util.escapeRegex(
								$(this).val()
							);
	 
							column
								.search( val ? '^'+val+'$' : '', true, false )
								.draw();
						} );
	 
					column.data().unique().sort().each( function ( d, j ) {
						select.append( '<option value="'+d+'">'+d+'</option>' )
					} );
				});
			}
		} );
	});
	</script>
 <style>
.dataTables_wrapper{overflow: auto;}
#assementTable tfoot{display: table-header-group;}
</style>
 