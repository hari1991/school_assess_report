<?php
ob_start();
ini_set( "display_errors", 0);
include('../../dbassess.php');
session_start();
$act=$_GET['act'];
$adminid=$_SESSION['aid'];
	
if(isset($_POST['btndoc']))
{ 
	$qryGradeWiseReport= mysql_query("select id, sid, (select school_name from schools where id = ".$_SESSION['schoolid'].") as schoolname, fname, section, grade_id,(select classname from class where id=grade_id) as gradename, s1.skillscore_M as skillscorem, skillscore_V as skillscorev,skillscore_F as skillscoref,skillscore_P as skillscorep,skillscore_L as skillscorel,a3.finalscore as avgbspiset1,CASE
WHEN LEAST(s1.skillscore_M, skillscore_V, skillscore_F, skillscore_P, skillscore_L) = s1.skillscore_M THEN 'Memory'
WHEN LEAST(s1.skillscore_M, skillscore_V, skillscore_F, skillscore_P, skillscore_L) = skillscore_V THEN 'Visual Processing'
WHEN LEAST(s1.skillscore_M, skillscore_V, skillscore_F, skillscore_P, skillscore_L) = skillscore_F THEN 'Focus & Attention'
WHEN LEAST(s1.skillscore_M, skillscore_V, skillscore_F, skillscore_P, skillscore_L) = skillscore_P THEN 'Problem Solving'
WHEN LEAST(s1.skillscore_M, skillscore_V, skillscore_F, skillscore_P, skillscore_L) = skillscore_L THEN 'Linguistics'

END as needimprovement from users mu

left join 
 (SELECT SUM(score)/5 as finalscore,MIN(score) as impr, gu_id, (SELECT sid from users where id=gu_id) as schoolid from (select (AVG(score)) as score, gu_id, gs_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports` WHERE gs_id in (59,60,61,62,63) and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id ) a2 group by gu_id) a3   on a3.gu_id=mu.id 
 
 left join
(select (AVG(score)) as skillscore_M, gu_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports` WHERE gs_id =59 and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id) s1 on s1.gu_id=mu.id 

left join
(select (AVG(score)) as skillscore_V, gu_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports` WHERE gs_id =60 and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id) s2 on s2.gu_id=mu.id 

left join
(select (AVG(score)) as skillscore_F, gu_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports` WHERE gs_id =61 and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id) s3 on s3.gu_id=mu.id 

left join
(select (AVG(score)) as skillscore_P, gu_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports` WHERE gs_id =62 and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id) s4 on s4.gu_id=mu.id 

left join
(select (AVG(score)) as skillscore_L, gu_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports` WHERE gs_id =63 and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id) s5 on s5.gu_id=mu.id where sid=".$_SESSION['schoolid']." and status=1 ORDER BY gradename,section,fname ");
		while($objGradeWiseReport = mysql_fetch_array($qryGradeWiseReport))
		{
			
			$GradeWiseReport[]= $objGradeWiseReport;
			
		}
	//echo "<pre>";print_r($GradeWiseReport);exit;	
  header("Content-Type:application/msword");
  header("Expires: 0");
  header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
  header("content-disposition: attachment;filename=Students_list.doc");
	
	
	echo "<div style='font-style:Calibri;font-size:16px;font-weight:bold;text-align:center;color:#0070c0'>Annexure</div><br/>";
	echo "<div style='font-style:Calibri;font-size:16px;font-weight:bold;text-align:center;color:#0070c0'>Individual Students Results - Assessment 2018-19</div><br/>";
	echo "<div style='font-style:Calibri;font-size:16px;font-weight:bold;text-align:center;color:#e36c09'>".$GradeWiseReport[0]['schoolname']."</div><br/>";
	
	echo '<table id="assementTable" border=1 cellspacing=0 cellpadding=0 
       style="border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt;width:100%;border:1px solid #000;margin: 0 auto;padding: 0;float:left;font-size:15px;table-layout: fixed; width: 100%">
    <thead>
      <tr style="font-style:Calibri;font-size:11px;font-weight:bold;">
        <th>S.No.</th>
        <th>Name</th>
        <th>Class</th>
		<th>Section</th>
        <th>Memory</th>
        <th>Visual Processing</th>
        <th>Focus and Attention</th>
        <th>Problem Solving</th>
        <th>Linguistics</th>
        <th>BSPI</th>
		<th>Need Improvement</th>
      </tr>
    </thead>
    <tbody  style="font-style:Calibri;font-size:11px;">';
	$ini=0; 
	foreach($GradeWiseReport as $gradewise)
	{
		$ini++;
		if($gradewise["skillscorem"]=='') { $skillscorem= '-'; } else { $skillscorem=round($gradewise["skillscorem"], 2); } 
		if($gradewise["skillscorev"]=='') { $skillscorev= '-'; } else { $skillscorev=round($gradewise["skillscorev"], 2); } 
		if($gradewise["skillscoref"]=='') { $skillscoref= '-'; } else { $skillscoref=round($gradewise["skillscoref"], 2); } 
		if($gradewise["skillscorep"]=='') { $skillscorep= '-'; } else { $skillscorep=round($gradewise["skillscorep"], 2); } 
		if($gradewise["skillscorel"]=='') { $skillscorel= '-'; } else { $skillscorel=round($gradewise["skillscorel"], 2); }
		
		if($gradewise["avgbspiset1"]=='') { $avgbspiset1= '-'; } else { $avgbspiset1=round($gradewise["avgbspiset1"], 2); }
		if($gradewise['needimprovement']=='') { $needimprovement='-'; } else { $needimprovement=$gradewise['needimprovement']; }
		
      echo '<tr>
        <td>'.$ini.'</td>
        <td>'.$gradewise["fname"].'</td>
		<td>'.$gradewise["gradename"].'</td>
        <td>'.$gradewise["section"].'</td>
		<td>'.$skillscorem.'</td>
       <td>'.$skillscorev.'</td>
        <td>'.$skillscoref.'</td>
       <td>'.$skillscorep.'</td>
       <td>'.$skillscorel.'</td>
		<td>'.$avgbspiset1.'</td>
		<td>'.$needimprovement.'</td>
      </tr>';
	} 
      
	  
   echo ' </tbody>
  </table>';

 }
 
else if(isset($_POST['btndoc1']))
{ 
	$main = mysql_query("select max(avgscore) as promi,gradeid,section,gradename from

 (select ROUND(avg(avgscore), 2) as avgscore,gradeid,section,gs_id, (select name from category_skills where id in (gs_id)) as skillname, (select classname from class where id in (gradeid)) as gradename from 
 (select avg(avgscore) as avgscore,gradeid,section,gs_id from(select avg(avgscore) as avgscore,gradeid, section,gs_id,lastupdate,gu_id from (SELECT (AVG(`game_score`)) as avgscore,gs_id , lastupdate,gu_id,(select section from users where users.id=gu_id) as section, (select grade_id from users where users.id=gu_id) as gradeid FROM `game_reports` WHERE gu_id in (select users.id as user_id from users where sid=".$_SESSION['schoolid']." and status=1) and gs_id in (59,60,61,62,63) and lastupdate between (select startdate from academic_year order by id desc limit 1) and (select enddate from academic_year order by id desc limit 1) group by gs_id, gu_id , lastupdate)as a1 group by gs_id ,gu_id,gradeid,section) as a2 group by gs_id,gradeid,section) as a3 
 group by gs_id,gradeid,section order by gs_id, avgscore DESC ) z1 group by gradeid, section ");
		
		while($skill_res = mysql_fetch_array($main)){
	
	
	$query2 = mysql_query("select group_concat(skillname) as skillname from
(select ROUND(avg(avgscore), 2) as avgscore,gradeid,section,gs_id, (select name from category_skills where id in (gs_id)) as skillname, (select classname from class where id in (gradeid)) as gradename from 
 
 (select avg(avgscore) as avgscore,gradeid,section,gs_id from(select avg(avgscore) as avgscore,gradeid, section,gs_id,lastupdate,gu_id from (SELECT (AVG(`game_score`)) as avgscore,gs_id , lastupdate,gu_id,(select section from users where users.id=gu_id) as section, (select grade_id from users where users.id=gu_id) as gradeid FROM `game_reports` WHERE gu_id in (select users.id as user_id from users where sid=".$_SESSION['schoolid']." and status=1) and gs_id in (59,60,61,62,63) and lastupdate between (select startdate from academic_year order by id desc limit 1) and (select enddate from academic_year order by id desc limit 1) group by gs_id, gu_id , lastupdate)as a1 group by gs_id ,gu_id,gradeid,section) as a2 group by gs_id,gradeid,section) as a3 
 group by gs_id,gradeid,section order by gs_id, avgscore DESC  ) a4  where gradeid=".$skill_res['gradeid']." and section='".$skill_res['section']."' and avgscore=".$skill_res['promi']."");
		
		//$promiskills[] = $skill_res;
	while($skillname =	mysql_fetch_array($query2))
	{
		$results[$skill_res['gradename']][$skill_res['section']]['value']= $skillname['skillname'];
	}
		
		
	
	}
	//echo "<pre>";print_r($GradeWiseReport);exit;	
   header("Content-Type:application/msword");
  header("Expires: 0");
  header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
  header("content-disposition: attachment;filename=Skill_Strength.doc");   
  
   
  echo '<div style="font-size:16px;font-family:Calibri;color:#498df1;font-weight:700;">1. Skill Strength</div><br/>';
  echo '<div style="font-size:16px;font-family:Calibri;color:#498df1;font-weight:700;text-decoration: underline;">1.1 Class wise Prominent Skill</div><br/>';
 // echo '<br/><br/><br/><h1>1.1 Class wise Prominent Skill</h1><br/><br/><br/>';
  
//	echo '<br/><br/><br/><h1>1.1 Class wise Prominent Skill</h1><br/><br/><br/>';
	echo '<table id="assementTable" border=1 cellspacing=0 cellpadding=0 
       style="border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt;width:100%;border:1px solid #000;margin: 0 auto;padding: 0;float:left;font-size:15px;table-layout: fixed; width: 100%">
   <thead>
      <tr>
        <th>S.No.</th>
		<th>Grade</th>
        <th>Section</th>
		<th>Prominent Skill</th>        
      </tr>
    </thead>
    <tbody>';
	$ini=0; 
	foreach($results as $key1=>$val1){
		
	foreach($val1 as $key2=>$val2){
	
		$ini++;
		
      echo '<tr>
        <td>'.$ini.'</td>
        <td>'.$key1.'</td>
		<td>'.$key2.'</td>
        <td>'.$val2['value'].'</td>
      </tr>';
	} 
      
	  
   

 }
echo ' </tbody>
  </table>';
  
  
  
  
  $main1 = mysql_query("select MIN(avgscore) as promi,gradeid,section,gradename from

 (select ROUND(avg(avgscore), 2) as avgscore,gradeid,section,gs_id, (select name from category_skills where id in (gs_id)) as skillname, (select classname from class where id in (gradeid)) as gradename from 
 (select avg(avgscore) as avgscore,gradeid,section,gs_id from(select avg(avgscore) as avgscore,gradeid, section,gs_id,lastupdate,gu_id from (SELECT (AVG(`game_score`)) as avgscore,gs_id , lastupdate,gu_id,(select section from users where users.id=gu_id) as section, (select grade_id from users where users.id=gu_id) as gradeid FROM `game_reports` WHERE gu_id in (select users.id as user_id from users where sid=".$_SESSION['schoolid'].") and gs_id in (59,60,61,62,63) and lastupdate between (select startdate from academic_year order by id desc limit 1) and (select enddate from academic_year order by id desc limit 1) group by gs_id, gu_id , lastupdate)as a1 group by gs_id ,gu_id,gradeid,section) as a2 group by gs_id,gradeid,section) as a3 
 group by gs_id,gradeid,section order by gs_id, avgscore DESC ) z1 group by gradeid, section ");
		
		while($skill_res = mysql_fetch_array($main1)){
	
	$query_2 = mysql_query("select (skillname) as skillname from
(select ROUND(avg(avgscore), 2) as avgscore,gradeid,section,gs_id, (select name from category_skills where id in (gs_id)) as skillname, (select classname from class where id in (gradeid)) as gradename from 
 
 (select avg(avgscore) as avgscore,gradeid,section,gs_id from(select avg(avgscore) as avgscore,gradeid, section,gs_id,lastupdate,gu_id from (SELECT (AVG(`game_score`)) as avgscore,gs_id , lastupdate,gu_id,(select section from users where users.id=gu_id) as section, (select grade_id from users where users.id=gu_id) as gradeid FROM `game_reports` WHERE gu_id in (select users.id as user_id from users where sid=".$_SESSION['schoolid'].") and gs_id in (59,60,61,62,63) and lastupdate between (select startdate from academic_year order by id desc limit 1) and (select enddate from academic_year order by id desc limit 1) group by gs_id, gu_id , lastupdate)as a1 group by gs_id ,gu_id,gradeid,section) as a2 group by gs_id,gradeid,section) as a3 
 group by gs_id,gradeid,section order by gs_id, avgscore DESC  ) a4  where gradeid=".$skill_res['gradeid']." and section='".$skill_res['section']."' ORDER BY avgscore ASC limit 2");
		
		//$promiskills[] = $skill_res;
	while($skill_name =	mysql_fetch_array($query_2))
	{
		$results1[$skill_res['gradename']][$skill_res['section']]['value'][]= $skill_name['skillname'];
	}
	
	}
	
 	 echo '<div>1.2 Class wise Two Skills that need Improvement</div>';
	 
	echo '<table id="assementTable" border=1 cellspacing=0 cellpadding=0 
       style="border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt;width:100%;border:1px solid #000;margin: 0 auto;padding: 0;float:left;font-size:15px;table-layout: fixed; width: 100%">
   <thead>
      <tr>
       <th>S.No.</th>
		<th>Grade</th>
        <th>Section</th>
		<th>Improvement needed 1</th>        
		<th>Improvement needed 2</th>        
      </tr>
    </thead>
    <tbody>';
	$ini=0; 
	foreach($results1 as $key1=>$val1){
		
	foreach($val1 as $key2=>$val2){
	
		$ini++;
		
      echo '<tr>
        <td>'.$ini.'</td>
        <td>'.$key1.'</td>
		<td>'.$key2.'</td>
        <td>'.$val2['value'][0].'</td>
		<td>'.$val2['value'][1].'</td>
      </tr>';
	} 
      
	  
   

 }
echo ' </tbody>
  </table>';
  
  
  
  /***OVERALL ***/
  
 echo '<div>2. Toppers</div>';
 echo '<div>2.1 Class wise Overall Toppers</div>';

 	
	echo '<table id="assementTable" border=1 cellspacing=0 cellpadding=0 
       style="border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt;width:100%;border:1px solid #000;margin: 0 auto;padding: 0;float:left;font-size:15px;table-layout: fixed; width: 100%">
   <thead>
      <tr>
		<th>Grade</th>
        <th>Student Name</th>
		<th>BSPI</th>        
      </tr>
    </thead>
    <tbody>';

$school_id = $_SESSION['schoolid'];
                function sort_max($a,$subkey) {
                    foreach($a as $k=>$v) {
                        $b[$k] = strtolower($v[$subkey]);
                    }
                    arsort($b);
                    foreach($b as $key=>$val) {
                        $c[] = $a[$key];
                    }
                    return $c;
                }
                
               
              
				
				 $grade =  array("3,4,5,6,7,8,9,10,11,12,13,14,15");
                //$grade = implode(',', $grade);

				$acrosssections = 'No';					
				if($acrosssections == "Yes"){
					$sections[] = "All";
				}
					
				foreach($grade as $class)
				{
					if($acrosssections == "No"){
						//echo "inside No";
						unset($sections);
						$overallsections = mysql_query("SELECT distinct(section) FROM skl_class_section where school_id='$school_id' and class_id = '$class'");
						while($sectionrec = mysql_fetch_array($overallsections)){
							$sections[] = $sectionrec['section'];
						}
					}
					
					foreach($sections as $section){ 
						$aTop="";
						
					
							$query = "select grade_name, name, sum(prescore)/5 as score, gu_id, section  
									from 
									(
										select grade_name, skill_name, name, score, sum(gscore) gscore, id, row, count(*) as daycnt, (sum(gscore)/count(*)) as prescore, gu_id, grade_id, section, YearMonth, daycnt as daycntnew, score2, gs_id
										  from
										  (
												select grade_name, skill_name, concat(fname,' ',lname) as name, score, sum(score) as score2, sum(score)/b.daycnt gscore, id, count(id) as row, b.daycnt, a.gu_id, grade_id, section, a.YearMonth, a.gs_id
												from 
												(							  								
													SELECT grade_name, skill_name, fname, lname, sum(score) as score, id, gu_id, gs_id, grade_id, DATE_FORMAT(lastupdate, '%Y-%m') as YearMonth, g_id, section
													From
													(
														SELECT cls.classname as grade_name, cs.name as skill_name, fname, lname, avg(gr.game_score) as score, gr.id as id, gr.gu_id, gs_id, grade_id, section, lastupdate, g_id
														FROM game_reports gr
														inner join users usr ON usr.id = gr.gu_id
														inner join class cls ON cls.id = usr.grade_id
														inner join category_skills cs ON cs.id = gr.gs_id
														WHERE 
															cls.id IN (".$class.")
															AND cs.id IN (59, 60, 61, 62, 63)
															AND usr.sid = " . $school_id .
														"   AND usr.section = '" . $section . "'" .
														" group by grade_id, section, gu_id, g_id, DATE_FORMAT(lastupdate, '%Y-%m')
													) as c group by grade_id, section, YearMonth, gu_id, g_id
												) as a 
												JOIN 
												(
													select count(daycnt) as daycnt, gu_id, gs_id, YearMonth
													From
													(
														select count(distinct(lastupdate)) as daycnt, gu_id, gs_id, DATE_FORMAT(lastupdate, '%Y-%m') as YearMonth
														from game_reports as gr 
														inner join category_skills cs ON cs.id = gr.gs_id
														where cs.id IN (59, 60, 61, 62, 63) and date(lastupdate) between (select startdate from academic_year ) and (select enddate from academic_year )
														group by gu_id, g_id, DATE_FORMAT(lastupdate, '%Y-%m')
													)as d group by gu_id, gs_id, YearMonth
												)as b on a.gu_id = b.gu_id and a.gs_id = b.gs_id and a.YearMonth = b.YearMonth
											
												group by grade_id, section, skill_name, gu_id, a.gs_id, a.YearMonth
												order by a.gs_id
										  ) as e group by grade_id, section, skill_name, gu_id, gs_id
											order by gs_id
									) as f group by gu_id";
							//select count(distinct(lastupdate)) as daycnt from game_reports as gr 
							//INNER JOIN users u on u.id = gr.gu_id where u.sid = ".$school_id. $vDateRange."						
						
					//echo "$query"."<br/><br/><br/>";
					
					$overallskillTop = mysql_query($query);
					
					while($skillTop = mysql_fetch_array($overallskillTop)){
					
					//echo "inside loop";
						$aTop[$skillTop['grade_name']][] = array(
													'name' => $skillTop['name'],
													'grade_name' => $skillTop['grade_name'],
													'section' => $skillTop['section'],
													'score' => $skillTop['score']
													);
					}
					
					foreach (array_keys($aTop) as $key) {
						$res = sort_max($aTop[$key],"score");
						$top = 1;
						$aTopRec = array();
						$curscore = 0;						
						for ($needle=0; $needle < $top; $needle++) { 
							//$aTopRec[] = $res[$needle];
							
							if(is_array($res[$needle])){
								$aTopRec[] = $res[$needle];
								$curscore = $res[$needle]['score'];
							}
	
							if(is_array($res[$needle+1])){
								if($curscore == $res[$needle+1]['score']){
									$top++;
								}
							}
						}
						$needle = 0;
						?>
						
						<?php
						
						foreach ($aTopRec as $value) {
							//echo "$query"."<br/><br/><br/>";
							echo "<tr>";
							echo "<td>$value[grade_name] - $value[section]</td>";
							echo "<td>$value[name]</td>";
							$rscore = round($value[score], 2);
							echo "<td>$rscore</td>";
							echo "</tr>";
						}
						?>
						
					<?php } ?>
				<?php } ?>
			<?php } 
			echo ' </tbody>
  </table>';
			?>	
  
  
<?php

 echo '<div>2.2. Grade wise Skill Toppers</div>';


 
$qryacademicYear=mysql_query("select startdate,enddate from academic_year order by id desc limit 1");
		while($objacademicYear = mysql_fetch_array($qryacademicYear))
		{
		$academicYear[]=$objacademicYear;
		}
		
		$school_id = $_SESSION['schoolid'];
		 $overallsections = mysql_query("SELECT class_id,section,classname FROM skl_class_section join class as c on c.id=class_id where school_id='$school_id'  order by class_id asc");
		 while($sectionrec = mysql_fetch_array($overallsections))
		 {
			$sectionsr[] = $sectionrec;
		 }
		 
		$query = "SELECT grade_name, skill_name, MAX(score) as score,gs_id, grade_id,section From 
				(SELECT cls.classname as grade_name, cs.name as skill_name, fname, lname, avg(gr.game_score) as score, gr.id as id, gr.gu_id, gs_id, grade_id, section, lastupdate, g_id FROM game_reports gr 

				inner join users usr ON usr.id = gr.gu_id 

				inner join class cls ON cls.id = usr.grade_id 

				inner join category_skills cs ON cs.id = gr.gs_id 

				WHERE   cs.id IN (59,60,61,62,63) AND usr.sid = '".$school_id."' AND (gr.lastupdate between '".$academicYear[0]['startdate']."' and '".$academicYear[0]['enddate']."')   group by grade_id, section, gu_id, g_id, DATE_FORMAT(lastupdate, '%Y-%m') ) as c 

				group by grade_id, section,gs_id order by score dESC ";
		
		
		$overallskillTop = mysql_query($query);
		$aTop = array();
		//echo "$aTop";
		
		while($skillTop = mysql_fetch_array($overallskillTop))
		{
			$query1 = "select group_concat(fname) as fname,score,gs_id from (SELECT cls.classname as grade_name, cs.name as skill_name, fname, lname, avg(gr.game_score) as score, gr.id as id, gr.gu_id, gs_id, grade_id, section, lastupdate, g_id FROM game_reports gr 
			inner join users usr ON usr.id = gr.gu_id 

			inner join class cls ON cls.id = usr.grade_id 

			inner join category_skills cs ON cs.id = gr.gs_id 

			WHERE   cs.id IN (59,60,61,62,63) AND usr.sid = '".$school_id."' AND (gr.lastupdate between '".$academicYear[0]['startdate']."' and '".$academicYear[0]['enddate']."')  group by grade_id, section, gu_id, g_id, DATE_FORMAT(lastupdate, '%Y-%m')) as a1 where score IN('".$skillTop['score']."') and grade_id=".$skillTop['grade_id']." and section='".$skillTop['section']."' and gs_id='".$skillTop['gs_id']."' ";
			
			$topuserlist = mysql_query($query1);
				while($topuser = mysql_fetch_array($topuserlist))
				{
					
					$aTop[$skillTop['grade_id']."-".$skillTop['section']."-".$topuser['gs_id']][] = array(
										'name' => $topuser['fname'],
										'score' => round($topuser['score'], 2)
										);
				}
		}
		
		echo '<table id="assementTable" border=1 cellspacing=0 cellpadding=0 
       style="border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt;width:100%;border:1px solid #000;margin: 0 auto;padding: 0;float:left;font-size:15px;table-layout: fixed; width: 100%">
    <thead>
      <tr>
        <th>Grade</th>
				<th>Section</th>
				<th>Memory</th>
				<th>Visual Processing</th>
				<th>Focus And Attention</th>
				<th>Problem Solving</th>
				<th>Linguistics</th>
      </tr>
    </thead>
    <tbody>';
	
	
			foreach($sectionsr as $section1)
			{ ?>
				<tr>
				<td><?php echo $section1['classname']; ?></td>
				<td><?php echo $section1['section']; ?></td>
				
				<td><?php echo $aTop[$section1['class_id']."-".$section1['section']."-59"][0]['name']." - ".$aTop[$section1['class_id']."-".$section1['section']."-59"][0]['score']; ?></td>
				<td><?php echo $aTop[$section1['class_id']."-".$section1['section']."-60"][0]['name']." - ".$aTop[$section1['class_id']."-".$section1['section']."-60"][0]['score']; ?></td>
				<td><?php echo $aTop[$section1['class_id']."-".$section1['section']."-61"][0]['name']." - ".$aTop[$section1['class_id']."-".$section1['section']."-61"][0]['score']; ?></td>
				<td><?php echo $aTop[$section1['class_id']."-".$section1['section']."-62"][0]['name']." - ".$aTop[$section1['class_id']."-".$section1['section']."-62"][0]['score']; ?></td>
				<td><?php echo $aTop[$section1['class_id']."-".$section1['section']."-63"][0]['name']." - ".$aTop[$section1['class_id']."-".$section1['section']."-63"][0]['score']; ?></td>
				
				</tr>
					
			

			<?php }
echo ' </tbody>
  </table>';

 }
 
 else if(isset($_POST['btndoc2']))
{ 

	$bspidata = mysql_query("select bspirange, count(bspirange) as val, gradeid,section, gradename from ( select CASE  WHEN  sum(avgscore)/5>80 THEN 'E' WHEN  sum(avgscore)/5 >60 AND sum(avgscore)/5  <=80 THEN 'G' WHEN  sum(avgscore)/5 > 40 AND sum(avgscore)/5  <=60 THEN 'A' WHEN  sum(avgscore)/5 > 20 AND sum(avgscore)/5  <=40 THEN 'AB' WHEN  sum(avgscore)/5<=20 THEN 'NI' END as bspirange, sum(avgscore)/5 as avgscore, gs_id,gu_id,gradeid,section,gradename from (select avg(avgscore) as avgscore, gs_id,lastupdate,gu_id,gradeid,section, (select classname from class where id in (gradeid)) as gradename from (SELECT (AVG(`game_score`)) as avgscore,gs_id , lastupdate,gu_id, (select section from users where users.id=gu_id) as section, (select grade_id from users where users.id=gu_id) as gradeid FROM `game_reports` WHERE gu_id in (select users.id as user_id from users where sid=".$_SESSION['schoolid']." and status=1) and gs_id in (59,60,61,62,63) and lastupdate between (select startdate from academic_year order by id desc limit 1) and (select enddate from academic_year order by id desc limit 1) group by gs_id, gu_id , lastupdate) as a1 group by gu_id ,gs_id) as a2 group by gu_id,gradeid,section) y1 group by gradeid,section, bspirange");
		
		while($bspires = mysql_fetch_array($bspidata)){
	
	//	$bspi[] = $bspires;
		
		$bspi[$bspires['gradename']][$bspires['section']][$bspires['bspirange']]= $bspires['val'];
	
	}
	
	header("Content-Type:application/msword");
  header("Expires: 0");
  header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
  header("content-disposition: attachment;filename=Skill_Strength.doc");
	
	 echo 'The following table shows the detailed class wise BSPI score analysis';
	 
	 echo '<table id="bspianalysis" border=1 cellspacing=0 cellpadding=0 
       style="border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt;width:100%;border:1px solid #000;margin: 0 auto;padding: 0;float:left;font-size:15px;table-layout: fixed; width: 100%">
   <thead>
      <tr>
         <th>S.No.</th>
		<th>Grade</th>
        <th>Section</th>
		<th>< 20</th>
		<th>20 to 40</th>
		<th>40 to 60</th>
		<th>60 to 80</th>
		<th>> 80</th>
     </tr>
    </thead>
	
    <tbody>';
	
	$ini=0; 	$ini++; $totallesst=0; $totaltwetofor=0; $totalfortosix=0; $totalsixtoeig=0; $totalmoreeighty=0;
	foreach($bspi as $key1=>$val1){
	
	foreach($val1 as $key2=>$val2){
	
	
	if (isset($val2['NI'])) { $lesst =  $val2['NI'];$total+=$val2['NI']; } else { $lesst = 0; }
	if (isset($val2['AB'])) { $twetofor =  $val2['AB'];$total+=$val2['AB']; } else { $twetofor =  0; }
	if (isset($val2['A'])) { $fortosix =  $val2['A'];$total+=$val2['A']; } else { $fortosix = 0; }
	if (isset($val2['G'])) { $sixtoeig =  $val2['G']; $total+=$val2['G'];} else { $sixtoeig = 0; }
	if (isset($val2['E'])) { $moreeighty =  $val2['E'];$total+=$val2['E']; } else { $moreeighty = 0; }
	
	
	
	$totallesst+=$lesst;
	$totaltwetofor+=$twetofor;
	$totalfortosix+=$fortosix;
	$totalsixtoeig+=$sixtoeig;
	$totalmoreeighty+=$moreeighty;
	
	
	
	 echo '<tr>
        <td>'.$ini.'</td>
        <td>'.$key1.'</td>
		<td>'.$key2.'</td>
        <td>'.$lesst.'</td> 
		<td>'.$twetofor.'</td> 
		<td>'.$fortosix.'</td> 
		<td>'.$sixtoeig.'</td> 
		<td>'.$moreeighty.'</td>
      </tr>';
	  
	}
	
	}
	echo '</tbody>
	<tfoot align="left">
		<tr>
		<td>Total</td>
        <td></td>
		<td></td>
        <td>'.$totallesst.'</td>
        <td>'.$totaltwetofor.'</td>
		<td>'.$totalfortosix.'</td>
        <td>'.$totalsixtoeig.'</td> 
		<td>'.$totalmoreeighty.'</td> 
      </tr></tfoot>';
	echo ' 
  </table>';
	
	
}

?>

