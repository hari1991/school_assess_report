<?php
ob_start();
ini_set( "display_errors", 0);
include('../../dbassess.php');
session_start();
$act=$_GET['act'];
$adminid=$_SESSION['aid'];
	
if(isset($_POST['btndoc']))
{ 
	$qryGradeWiseReport= mysql_query("select id, sid, (select school_name from schools where id = ".$_SESSION['schoolid'].") as schoolname, fname, section, grade_id,(select classname from class where id=grade_id) as gradename, s1.skillscore_M as skillscorem, skillscore_V as skillscorev,skillscore_F as skillscoref,skillscore_P as skillscorep,skillscore_L as skillscorel,a3.finalscore as avgbspiset1,CASE
WHEN LEAST(s1.skillscore_M, skillscore_V, skillscore_F, skillscore_P, skillscore_L) = s1.skillscore_M THEN 'Memory'
WHEN LEAST(s1.skillscore_M, skillscore_V, skillscore_F, skillscore_P, skillscore_L) = skillscore_V THEN 'Visual Processing'
WHEN LEAST(s1.skillscore_M, skillscore_V, skillscore_F, skillscore_P, skillscore_L) = skillscore_F THEN 'Focus & Attention'
WHEN LEAST(s1.skillscore_M, skillscore_V, skillscore_F, skillscore_P, skillscore_L) = skillscore_P THEN 'Problem Solving'
WHEN LEAST(s1.skillscore_M, skillscore_V, skillscore_F, skillscore_P, skillscore_L) = skillscore_L THEN 'Linguistics'

END as needimprovement from users mu

left join 
 (SELECT SUM(score)/5 as finalscore,MIN(score) as impr, gu_id, (SELECT sid from users where id=gu_id) as schoolid from (select (AVG(score)) as score, gu_id, gs_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports` WHERE gs_id in (59,60,61,62,63) and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id ) a2 group by gu_id) a3   on a3.gu_id=mu.id 
 
 left join
(select (AVG(score)) as skillscore_M, gu_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports` WHERE gs_id =59 and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id) s1 on s1.gu_id=mu.id 

left join
(select (AVG(score)) as skillscore_V, gu_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports` WHERE gs_id =60 and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id) s2 on s2.gu_id=mu.id 

left join
(select (AVG(score)) as skillscore_F, gu_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports` WHERE gs_id =61 and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id) s3 on s3.gu_id=mu.id 

left join
(select (AVG(score)) as skillscore_P, gu_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports` WHERE gs_id =62 and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id) s4 on s4.gu_id=mu.id 

left join
(select (AVG(score)) as skillscore_L, gu_id from (SELECT (AVG(`game_score`)) as score , gs_id , gu_id, lastupdate FROM `game_reports` WHERE gs_id =63 and gu_id in (select id from users) group by gs_id , gu_id, lastupdate) a1 group by gs_id, gu_id) s5 on s5.gu_id=mu.id where sid=".$_SESSION['schoolid']." and status=1 ORDER BY avgbspiset1 DESC");
		while($objGradeWiseReport = mysql_fetch_array($qryGradeWiseReport))
		{
			
			$GradeWiseReport[]= $objGradeWiseReport;
			
		}
	//echo "<pre>";print_r($GradeWiseReport);exit;	
  header("Content-Type:application/msword");
  header("Expires: 0");
  header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
  header("content-disposition: attachment;filename=test.doc");
	
	echo '<table id="assementTable" border=1 cellspacing=0 cellpadding=0 
       style="border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt;width:100%;border:1px solid #000;margin: 0 auto;padding: 0;float:left;font-size:15px;table-layout: fixed; width: 100%">
    <thead>
      <tr>
        <th>S.No.</th>
        <th>Name</th>
        <th>Class</th>
		<th>Section</th>
        <th>Memory</th>
        <th>Visual Processing</th>
        <th>Focus and Attention</th>
        <th>Problem Solving</th>
        <th>Linguistics</th>
        <th>BSPI</th>
		<th>Need Improvement</th>
      </tr>
    </thead>
    <tbody>';
	$ini=0; 
	foreach($GradeWiseReport as $gradewise)
	{
		$ini++;
		if($gradewise["skillscorem"]=='') { $skillscorem= '-'; } else { $skillscorem=round($gradewise["skillscorem"], 2); } 
		if($gradewise["skillscorev"]=='') { $skillscorev= '-'; } else { $skillscorev=round($gradewise["skillscorev"], 2); } 
		if($gradewise["skillscoref"]=='') { $skillscoref= '-'; } else { $skillscoref=round($gradewise["skillscoref"], 2); } 
		if($gradewise["skillscorep"]=='') { $skillscorep= '-'; } else { $skillscorep=round($gradewise["skillscorep"], 2); } 
		if($gradewise["skillscorel"]=='') { $skillscorel= '-'; } else { $skillscorel=round($gradewise["skillscorel"], 2); }
		
		if($gradewise["avgbspiset1"]=='') { $avgbspiset1= '-'; } else { $avgbspiset1=round($gradewise["avgbspiset1"], 2); }
		if($gradewise['needimprovement']=='') { $needimprovement='-'; } else { $needimprovement=$gradewise['needimprovement']; }
		
      echo '<tr>
        <td>'.$ini.'</td>
        <td>'.$gradewise["fname"].'</td>
		<td>'.$gradewise["gradename"].'</td>
        <td>'.$gradewise["section"].'</td>
		<td>'.$skillscorem.'</td>
       <td>'.$skillscorev.'</td>
        <td>'.$skillscoref.'</td>
       <td>'.$skillscorep.'</td>
       <td>'.$skillscorel.'</td>
		<td>'.$avgbspiset1.'</td>
		<td>'.$needimprovement.'</td>
      </tr>';
	} 
      
	  
   echo ' </tbody>
  </table>';

 }

?>