<?php
if(empty($adminid)){
	echo "<script>window.location='index.php?act=login'</script>";
}

?>
<html>
<head>

 <link rel="stylesheet" href="css/jquery-ui.css" />

<script type="text/javascript" src="js/jquery.min.js"></script>
<script src="js/jquery-1.9.1.js"></script>
<script src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery.validate.js"></script>



<style type="text/css">
#status
{
font-size:11px;
margin-left:10px;
}
.green
{
background-color:#CEFFCE;
}
.red
{
color:#f00;
}
input.error { color: red; border: 1px solid #f00; }
#status1 {  display: block !important; font-size: 13px; color: green; width: 250px;
padding: 0 0 2px 0px; text-align: left;font-weight: normal !important; }
#email { float: left; }
.mandatory { display: block !important;  color: #f00; }
.top_mandatory { display: block !important;  color: #f00; float: left; }
.fields {float: left; }
label.error { float: none; color: red; padding-left:20px; vertical-align: top; }
p { clear: both; }
.success { font-size: 15px; font-weight: bold; color: green;text-align: center;}
em { font-weight: bold; padding-right: 1em; vertical-align: top; }
.label { padding: 0 0 15px }
</style>

<script>
$(document).ready(function(){
	
	$.fn.filterByText = function(textbox, selectSingleMatch) {
		
        return this.each(function() {
            var select = this;
            
            var options = [];
            $(select).find('option').each(function() {
                options.push({value: $(this).val(), text: $(this).text()});
            });
            $(select).data('options', options);
            $(textbox).bind('change keyup', function() {
                var options = $(select).empty().data('options');
                var search = $.trim($(this).val());
                var regex = new RegExp(search,"gi");
              
                $.each(options, function(i) {
                    var option = options[i];
                    if(option.text.match(regex) !== null) {
                        $(select).append(
                           $('<option>').text(option.text).val(option.value)
                        );
                    }
                });
                if (selectSingleMatch === true && $(select).children().length === 1) {
                    $(select).children().get(0).selected = true;
                }
            });            
        });
    };
	$("#assgames").validate();

    //select category
	$("#assgrades").change(function(){
		
		$('#search').val("");
		$('#search1').val("");
		var grade_id=$(this).val();
		
		var dataString = 'grade_id='+ grade_id;		
		$.ajax
		({
			type: "POST",
			url: "../ajax_data.php",
			data: dataString,
			cache: false,
			success: function(html)
			{
				$(".grades").html(html);
			} 
		});
	});

	  //select category
	$("#plans").change(function(){

		$('#search').val("");
		$('#search1').val("");
		var grade_id = $("#assgrades").val();
		var planid=$(this).val();
		
		var dataString = 'assplan='+ planid +'&grade='+ grade_id;		
		$.ajax
		({
			type: "POST",
			url: "../ajax_data.php",
			data: dataString,
			cache: false,
			success: function(html)
			{
				var res = new Array();
				res = html.split("unpubtopub");
				$(".unpubgames").html(res[1]);
				$(".pubgames").html(res[0]);
				$('#unpubgames').filterByText($('#search'), true);
				$('#pubgames').filterByText($('#search1'), true);
			}
		});
	});


	$('#add').click(function() { 			
		 return !$('#unpubgames option:selected').remove().appendTo('#pubgames');
	});
	
	$('#remove').click(function() {
		return !$('#pubgames option:selected').remove().appendTo('#unpubgames');		 
	});

	 $('#assgames').submit(function() {	 
	 	$('#pubgames option').each(function(i) {
	  		$(this).attr("selected", "selected");
	 	});

	 	$('#unpubgames option').each(function(i) {
	  		$(this).attr("selected", "selected");
	 	});
	 });
	 	
});
</script>
</head>
</html>
<?php 
$grade = mysql_query("select id,grdname from skl_grade");
$school = mysql_query("select id,school_name from schools");

if(isset($_POST['asssubmit'])) {
		
	
		$planid = $_POST['plans'];
		$gradeid = $_POST['assgrades'];
		$playedgames = mysql_query("SELECT a.gid,a.gname FROM games a join skl_grade_game_map b on a.gid = b.game_id where b.grade_id = $gradeid and FIND_IN_SET( $planid, gplans )");
		
		
		while($played_games = mysql_fetch_array($playedgames)){
					$assignedgames[] = $played_games['gid'];
		}
		
		if(!empty($_POST['pubgames']))
			$arrdiffsdel = array_diff($assignedgames, $_POST['pubgames']);
		else 
			$arrdiffsdel = $assignedgames;
			
			
		if(!empty($arrdiffsdel)){
			foreach($arrdiffsdel as $arrdiffdel) {
				$gplans = mysql_fetch_array(mysql_query("select gplans from games where gid = $arrdiffdel"));
				$planarray = Array($planid);
				$planexplode = explode(',', $gplans['gplans']);
				$plancut = array_diff($planexplode, $planarray);
				$finalplinsert = implode(',', $plancut);
				mysql_query("update games set gplans='$finalplinsert' where gid = $arrdiffdel");
			}
		}
		
		foreach($_POST['pubgames'] as $pubgamesadd){
			if(!in_array($pubgamesadd,$assignedgames)){
				$gplans = mysql_fetch_array(mysql_query("select gplans from games where gid = $pubgamesadd"));
				
				$planexplode = explode(',', $gplans['gplans']);
				array_push($planexplode,$planid);
				$planimplode = implode(",", $planexplode);
				mysql_query("update games set gplans='$planimplode' where gid = $pubgamesadd");
				
			}
		}
		$message = "Games Updated";
}
?>
<header>
	<div class="intro-head">Assign Games</div>
	<div class="intro cnt_min">
		<div class="support-note">
			<div class="success"><?php echo $message; ?></div>
			<form action="" class="cmxform" method="POST" id="assgames" name="assgames" accept-charset="utf-8">
				<div class="reqindicates"><label class="top_mandatory">*</label>&nbsp; Indicates required fields</div>
				<table border="0" cellspacing="4" cellpadding="4">	
					<tr>
						<td colspan="5">
							<div class="label">
								<div class="firstlabel"><label class="fields" for="validity">Grade<label style="color:red";>*</label></div>
								<div class="secondinput">
									<select name="assgrades" id="assgrades" class="required">
										<option value="">--Select Grades--</option>
										<?php 
										while ($gradeinarr=mysql_fetch_array($grade)) {
										?>
										<option value="<?php echo $gradeinarr['id'];?>"><?php echo $gradeinarr['grdname'];?></option>
										<?php }?>
									</select>
								</div>
							</div>
							<div class="label">
								<div class="firstlabel"><label class="fields" for="validity">Plan<label style="color:red";>*</label></div>
								<div class="secondinput">
									<select name="plans" id="plans" class="grades required">
											<option value="" selected="selected">--Select Plan--</option>
										</select>
								</div>
							</div>
						</td>
					</tr>
					<tr height="30">
						<th align="center" colspan="2">Unpublished Games</th>
						<th width="100px"></th>
						<th align="center" colspan="2">Published Games</th>
					</tr>
					<tr>
						<td width="84px"><label class="fields" for="validity">Search</label></td>
						<td><input type="text" id="search" autocomplete="off" name="search"></td>
						<td></td>
						<td width="84px"><label class="fields" for="validity">Search</label></td>
						<td><input id="search1" type="text" autocomplete="off" name="search1"></td>
					</tr>
					<tr>
						<td colspan="2">
							<select name='unpubgames[]' id='unpubgames' class="unpubgames"
								style='width: 250px; height: 300px;' multiple>
							</select>
						</td>
						<td align="center">
							<a href="#" id="add"><input class="submitbutton" type="submit" value="Add" id="savechanges" name="Add"> </a><br> <br> <br> 
							<a href="#" id="remove"><input class="submitbutton" type="submit" name="Remove" id="savechanges" value="Remove"> </a>
						</td>
						<td colspan="2">
							<select name='pubgames[]' id='pubgames' class='pubgames'
								style='width: 250px; height: 300px;' multiple>																
							</select>
						</td>
					</tr>
					
					<tr>
						<td colspan="5" align="center"><input class="submitbutton" type="submit" name="asssubmit" id="asssubmit" value="Assign Games"></td>
					</tr>
					
					
				</table>
			</form>
  		</div>
	</div>
</header>
