<?php 
if(empty($adminid)){
	echo "<script>window.location='index.php?act=login'</script>";
}
include_once("usage_summary_report_bl.php");

?>
	 <script type="text/javascript" src="js/bootstrap-multiselect.js"></script>
 <?php $AverageScore=$skillQuotient_value; ?>

<div class="pageHomePager Dashboardhide mygameshide myreporthide myprofilehide">
   <div class="row">
  <div class="col-lg-12">
                    <h1 class="page-header">Usage Summary Report</h1>
                </div>
			</div>	
			<div class="row">
      			<div class="col-lg-12">
				<form name="frmStudentReport" id="frmStudentReport" method="post">
				<div class="panel panel-default">
            <div class="panel-body reportChartContainer">
			<div class="col-lg-4">
				<div class="row">
				<label class="col-lg-6">Grade</label>
				<select class="form-control col-lg-6" required="true" style="width:50%" name="ddlGradeType" id="ddlGradeType">
			<option value="">Select</option>
			<?php foreach($GradeList as $grades) {
				$gradinid='';
				$selected='';
				if($ddlGradeType==$grades['id']){$selected="selected='selected'";$gradinid=$grades['id'];}
				echo "<option id='".$grades['id']."' ".$selected." value='".$grades['id']."'>".trim(str_replace("Grade",'',$grades['classname']))."</option>";
			} ?>
			</select>
			</div>
			
				</div>
		
			<div class="col-lg-4">
			<div class="row">
			<label class="col-lg-6">Month</label>
			 <select id="chkveg1" multiple="multiple">
			 <?php $ini=0; foreach($academicMonths as $am){
				$ini++;
				
				echo '<option value="'.$am['monthNumber'].'">'.$am['monthName'].'</option>';
			} ?>
			
			
			            </select>
			         <input type="hidden" value="" name="hdnMonthID" id="hdnMonthID" />    
			            	<script type="text/javascript">
			            	    $(function() {
			            	        $('#chkveg1').multiselect({
			            	            includeSelectAllOption: true,numberDisplayed: 1,nonSelectedText: 'Select',buttonClass: 'form-control col-lg-6',
										onChange: function(element, checked) {
        var brands = $('#chkveg1 option:selected');
        var selected = [];
        $(brands).each(function(index, brand){
            selected.push([$(this).val()]);
			//alert(selected);
        });
			$("#hdnMonthID").val(selected);
			}
			            	        });
			            	        $('#btnget').click(function() {
			            	        alert($('#chkveg1').val());
			            	        })
			            	    });
			</script>
			<script type="text/javascript">
			    $(document).ready(function() {
			    $('#chkveg1').multiselect();
				<?php if(isset($_POST['btnSearch'])) { ?>
				var data1="<?php echo $hdnMonthID; ?>";
				var dataarray1=data1.split(",");
				$("#chkveg1").val(dataarray1);
				$("#chkveg1").multiselect("refresh");
				<?php } ?>
				
			    });
</script>
				
			</div>
			</div>
			
			<div class="col-lg-1"></div>
			<div class="col-lg-1">
			<div class="row">
			<input  type="submit" name="btnSearch" class="btn btn-success" id="btnSubmit" value="Submit" />
			</div>
				</div>
				
			<div class="col-lg-1">
			<div class="row">
			<input  type="button" onclick="javascript:window.location.href=window.location.href;" name="btnSubmit" class="btn btn-warning" id="btnSubmit" value="Reset" />
			</div>
				</div>
				
				
				</div>
				</div>
				</form>
				
				</div>
				</div>
	<div id="StudentReportResult"  >
             
		 <?php if(isset($_POST['btnSearch'])) { ?>  	
			
			<div class="row">
			<?php foreach($SectionList as $result){ ?> 
      			<div class="col-lg-12">
                	<label>Section - <?php echo $result['section']; ?></label>
                    <div class="panel panel-default">
                        <div class="panel-body reportChartContainer">
                            <div id="Chartsection<?php echo $result['section']; ?>" ></div>
                        </div>
                    </div>
                </div>
			<?php } ?>
				
				
 			</div>
			
			
			
		 <?php } ?>
			
</div>			
			
  
   
   
</div>

<script src="js/skillpiecharts.js" type="text/javascript"></script><script type="text/javascript" src="js/charts.widgets.js"></script>

 <script type="text/javascript" src="js/charts.js"></script>
<script type="text/javascript" src="js/charts-more.js"></script>
<script src="http://code.highcharts.com/modules/solid-gauge.src.js"></script>

 <script>
 <?php if(isset($_POST['btnSearch'])) { ?>


 
$(document).ready(function(){
<?php foreach($SectionList as $result){ ?> 
	$('#Chartsection<?php echo $result['section']; ?>').highcharts({
		chart: {
            type: 'column'
        },
		
        title: {
            text: '',
            x: -20 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
            categories: [<?php $ini=0; foreach($req_academicMonths as $am){
				$ini++;
				if($ini>1){echo ",";}
				echo "'".$am['monthName']."'";
			} ?>]
        },
        yAxis: {
            min: 0,tickInterval: 10,max:100,
            title: {
                text: ''
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#ff3300'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        credits: {
      enabled: false
  },
        series: [{showInLegend: true, dataLabels: {enabled: true} , 
		
		       color: '#984807',    
					
            name: 'Total Strength',
            data: [<?php $ini=0; foreach($req_academicMonths as $am){
				$ini++;
				if($ini>1){echo ",";}
				if(isset($month_array_chart[$result['section']][$am['monthNumber']]['totalUsers'])){
				echo "".($month_array_chart[$result['section']][$am['monthNumber']]['totalUsers'])."";
				}
				else{echo "0";}
			} ?>]
        },{showInLegend: true,   dataLabels: {enabled: true} , 
		color: '#f79646',
		           
					
            name: '# Students Attempted Training',
            data: [<?php $ini=0; foreach($req_academicMonths as $am){
				$ini++;
				if($ini>1){echo ",";}
				if(isset($month_array_chart[$result['section']][$am['monthNumber']]['attendedCount'])){
				echo "".($month_array_chart[$result['section']][$am['monthNumber']]['attendedCount'])."";
				}
				else{echo "0";}
			} ?>]
        },{showInLegend: true,   dataLabels: {enabled: true} , 
		color: '#fac090',
		           
					
            name: '# Students Completed Training',
            data: [<?php $ini=0; foreach($req_academicMonths as $am){
				$ini++;
				if($ini>1){echo ",";}
				if(isset($month_array_chart1[$result['section']][$am['monthNumber']]['UserCount'])){
				echo "".($month_array_chart1[$result['section']][$am['monthNumber']]['UserCount'])."";
				}
				else{echo "0";}
			} ?>]
        }]
    });
<?php } ?>
});
 <?php } ?>
 </script>


 