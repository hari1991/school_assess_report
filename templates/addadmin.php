<?php
if(empty($adminid)){
	echo "<script>window.location='index.php?act=login'</script>";
}
?>

<link rel="stylesheet" href="css/jquery-ui.css" />

<style type="text/css" title="currentStyle">
			@import "datatable/media/css/demo_page.css";
			@import "datatable/media/css/demo_table_jui.css";
			@import "datatable/examples/examples_support/themes/smoothness/jquery-ui-1.8.4.custom.css";
		</style>
				<script type="text/javascript" language="javascript" src="js/jquery-1.8.2.js"></script>
		<script type="text/javascript" language="javascript" src="js/jquery.validate.js"></script>
		<script type="text/javascript" language="javascript" src="datatable/media/js/jquery.dataTables.js"></script>
		<script type="text/javascript" src="js/jquery.fancybox.js"></script>
		<link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css" media="screen" />

		<style type="text/css">
#status
{
font-size:11px;
margin-left:10px;
}


#email { float: left; }



label.error { float: none; color: red; padding-left:20px; vertical-align: top; }
p { clear: both; }
.success { font-size: 15px; font-weight: bold; color: green;text-align: center;}
em { font-weight: bold; padding-right: 1em; vertical-align: top; }

.dte_header{
 	border-top-left-radius: 5px;
    border-top-right-radius: 5px;
    background-color: #F3F3F3;
    border-bottom: 1px solid #DDDDDD;
    height: 50px;
    left: 0;
   	top: 0;
    width: 100%;
}
</style>

<script>
$(document).ready(function(){
	
		
	var oTable = $('#adminlist').dataTable({
		
		"bJQueryUI": true,
		"sPaginationType": "full_numbers"
	});

	
	$("#addadmin").validate({
		 rules: {
			 fname: {
				 remote: {
					 url: "../ajax_data.php",
					 type: "post"
					
					}
				 }
		 },
		
		messages: {
			fname :{
					remote : 'Admin name is already taken'
				}
			}
	});
	
    
    
    
});
</script>

<?php

if(isset($_POST['admineditsubmit'])){

$id = $_POST['adminid'];
$fname =  $_POST['fname'];
$saddress = $_POST['saddress'];
$phone = $_POST['phone'];
$emailid = $_POST['emailid'];
$schoolid = $_POST['sid'];

mysql_query("update school_admin set fname = '$fname',address='$saddress',mobile='$phone',email='$emailid',schoolid='$schoolid' where id = $id");
$message = 'Admin Edited Successfully';
}

if(isset($_POST['adminsubmit'])){
	
	$fname =  $_POST['fname'];
	$lname = $_POST['lname'];
	$saddress = $_POST['saddress'];
	$phone = $_POST['phone'];
	$emailid = $_POST['emailid'];
	$schoolid = $_POST['sid'];
	
	$sql = "insert into school_admin (fname,lname,address,mobile,email,school_id) values ('$fname','$lname','$saddress','$phone','$emailid',$schoolid)";
	//echo $sql ."<br>";
	
	mysql_query($sql);
	$message = 'Admin added successfully';
}
if(isset($_POST['oktodel'])){

	$id = $_POST['adminid'];
	mysql_query("DELETE FROM school_admin WHERE id='$id'");
	$message = 'Admin deleted successfully';
}
?>
<?php 
$alladmins = mysql_query("select * from school_admin");
while($alladmin = mysql_fetch_array($alladmins)){

?>
<script>
$(document).ready(function(){
	$(".fancybox<?php echo $alladmin['id']; ?>").fancybox({
		 beforeShow : function() {
			 var adminname =$("#fname<?php echo $alladmin['id']; ?>").val();
			$("#editadmin<?php echo $alladmin['id']; ?>").validate({
				 rules: {
					 fname: {
						 remote: {
							 url: "../ajax_data.php",
							 type: "post",
							 data: {
								 adminname: function() {
								 return adminname;
								 }
							}
						 }
				 	 }
				},
				messages: {
					fname :{
							remote : 'Admin name is already taken'
						}
					}
			});
		},
	});
	 
});
</script>
					<!-- edit form -->
					
					<div id="editform<?php echo $alladmin['id']; ?>" class="editform" style="display: none;">
						<div class="edit-head">Edit Record</div>
						<div class="editlist">
							<form action="" class="cmxform" method="POST" id="editadmin<?php echo $alladmin['id']; ?>" accept-charset="utf-8">
								<input type="hidden" name="adminid" value="<?php echo $alladmin['id']; ?>">
								<div class="label">
									<div class="firstlabel"><label class="fields" for="fname">First Name </label><label class="mandatory">*</label></div>
									<div class="secondinput"><input type="text" name="fname" value="<?php echo $alladmin['fname']; ?>" title="Please enter first name" class="required" id="fname<?php echo $alladmin['id']; ?>">
										<div class="error"></div>
									</div>
								</div>												
								<div class="label">
									<div class="firstlabel"><label class="fields" for="saddress">Address </label><label class="mandatory">*</label></div>
									<div class="secondinput"><input type="text" name="saddress" value="<?php echo $alladmin['address']; ?>" title="Please enter address" class="required" id="saddress"></div>						
								</div>
								<div class="label">
									<div class="firstlabel"><label class="fields" for="phone">Phone Number </label><label class="mandatory">*</label></div>
									<div class="secondinput"><input type="text" name="phone" value="<?php echo $alladmin['phone']; ?>" title="Please enter phone number" class="required" id="phone"></div>
								</div>
								<div class="label">
									<div class="firstlabel"><label class="fields" for="emailid">Email id </label><label class="mandatory">*</label></div>
									<div class="secondinput"><input type="text" name="emailid" value="<?php echo $alladmin['email']; ?>" title="Please enter email id" class="required email" id="emailid"></div>
								</div>
								<div class="label">
									<div class="firstlabel"><label class="fields" for="sid">School ID </label><label class="mandatory">*</label></div>
									<div class="secondinput"><input type="text" name="sid" value="<?php echo $alladmin['schoolid']; ?>" title="Please enter school id" class="required" id="sid"></div>
								</div>
								<div class="submitlabel"><input type="submit" class="submitbutton" id="admineditsubmit" name="admineditsubmit" value="Update"></div>
							</form>
						</div>
					</div>
					
					<!-- delete form -->
					<div id="deleteform<?php echo $alladmin['id']; ?>" class="deleteform" style="display: none;">
						<form action"" method="post">
							<input type="hidden" name="adminid" value="<?php echo $alladmin['id']; ?>">
							<div class="edit-head">Are you sure to delete ? </div>
							<div class="editlist">
								<input class="submitbutton" type="submit" name="oktodel" id="oktodel" value="Delete">
								<input class="submitbutton" type="submit" name="canceltodel" id="canceltodel" value="Cancel">
							</div>
						</form>
					</div>
					<?php 
					}
					?>
					
<header>
	<div class="intro-head">Add Admin</div>
	<div class="intro cnt_min">
		<div class="support-note">
			<div id="main">				
				<div class="success"><?php echo $message; ?></div>
				<form action="" class="cmxform" method="POST" id="add" accept-charset="utf-8">				
						<div class="reqindicates"><label class="top_mandatory">*</label>&nbsp; Indicates required fields</div>
						<div class="label">
							<div class="firstlabel"><label class="fields" for="fname">First Name </label><label class="mandatory">*</label></div>
							<div class="secondinput"><input type="text" name="fname" title="Please enter first name" value="" class="required" id="fname"></div>
							<div id="status1"></div>
						</div>
                        <div class="label">
							<div class="firstlabel"><label class="fields" for="lname">Last Name </label></div>
							<div class="secondinput"><input type="text" name="lname" title="Please enter last name" value="" class="required" id="lname"></div>
							<div id="status1"></div>
						</div>												
						<div class="label">
							<div class="firstlabel"><label class="fields" for="saddress">Address </label><label class="mandatory">*</label></div>
							<div class="secondinput"><input type="text" name="saddress" title="Please enter address" value="" class="required" id="saddress"></div>						
						</div>
						<div class="label">
							<div class="firstlabel"><label class="fields" for="phone">Phone Number </label><label class="mandatory">*</label></div>
							<div class="secondinput"><input type="text" name="phone" title="Please enter phone number" value="" class="required" id="phone"></div>
						</div>
						<div class="label">
							<div class="firstlabel"><label class="fields" for="emailid">Email ID </label><label class="mandatory">*</label></div>
							<div class="secondinput"><input type="text" name="emailid" value="" title="Please enter email id" class="required email" id="emailid"></div>
						</div>
						<div class="label">
							<div class="firstlabel"><label class="fields" for="sid">School ID</label><label class="mandatory">*</label></div>
							<div class="secondinput"><input type="text" name="sid" value="" title="Please enterschool  id" class="required" id="sid"></div>
						</div>
						<div class="submitlabel"><input type="submit" class="submitbutton" id="adminsubmit" name="adminsubmit" value="Add New Admin"></div>
					
				</form>
  			</div>
		</div>
	
	<!-- View  List -->		

		<div class="support-note">
			<div id="main">
		<table cellpadding="0" cellspacing="0" border="0" class="display" id="list" >
			
			<thead>
				<tr>
					<th rowspan="2">Name</th>
					<th rowspan="2">Address</th>
					<th rowspan="2">Phone</th>
					<th rowspan="2">Email</th>
					<th rowspan="2">School ID</th>
					<th align="center" colspan="2">Action</th>
				</tr>
				<tr>
					<th align="center">Edit</th>
					<th align="center">Delete</th>
				</tr>
			</thead>
			<tbody>
			
			
				<?php 
				$alladmins = mysql_query("select * from school_admin");
				while($alladmin = mysql_fetch_array($alladmins)){
				?>
					
					<?php
					echo "<tr>"; 
					echo "<td>$alladmin[fname]</td>";
					echo "<td>$alladmin[address]</td>";
					echo "<td>$alladmin[phone]</td>";
					echo "<td>$alladmin[email]</td>";
					echo "<td>$alladmin[schoolid]</td>";
					echo "<td align='center'><a class='fancybox$alladmin[id]' href='#editform$alladmin[id]'><img src='images/edit.png' title='Edit Record' width='20' alt='Edit Record'></a></td>";
					echo "<td align='center'><a class='fancybox$alladmin[id]' href='#deleteform$alladmin[id]'><img src='images/delete.png' title='Delete Record' width='17' alt='Delete Record'></a></td>";
					echo "</tr>";
				}
					?>
			
			</tbody>
			</table>
  			</div>
		</div>

</header>