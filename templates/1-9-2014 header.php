<!-- Web Fonts  -->
	
<style type="text/css">
nav ul {
  margin: 0;
  padding: 0;
  list-style: none;
  position: relative;
  float: right;
  -moz-border-radius: 3px;
  -webkit-border-radius: 3px;
  border-radius: 3px;    
}

nav li {
  float: left;          
}

nav #login {
  border-right: 1px solid #ddd;
  -moz-box-shadow: 1px 0 0 #fff;
  -webkit-box-shadow: 1px 0 0 #fff;
  box-shadow: 1px 0 0 #fff;  
}

nav #login-trigger,
nav #signup a {
  display: inline-block;
  *display: inline;
  *zoom: 1;
  height: 25px;
  line-height: 25px;
  padding: 0 8px;
  text-decoration: none;
  color: #fff;

  
}
nav #login-trigger span { font-size: 15px; }
nav #signup a {
  -moz-border-radius: 0 3px 3px 0;
  -webkit-border-radius: 0 3px 3px 0;
  border-radius: 0 3px 3px 0;
}

nav #login-trigger {
  -moz-border-radius: 3px 0 0 3px;
  -webkit-border-radius: 3px 0 0 3px;
  border-radius: 3px 0 0 3px;
}



nav #login-content {
  display: none;
  position: absolute;
  top: 102px;
  right: 5px;
  z-index: 999;    
  background: #fff;

  padding:15px 12px 0px 12px;
  -moz-box-shadow: 0 2px 2px -1px rgba(0,0,0,.9);
  -webkit-box-shadow: 0 2px 2px -1px rgba(0,0,0,.9);
  box-shadow: 0 2px 2px -1px rgba(0,0,0,.9);
  -moz-border-radius: 3px 0 3px 3px;
  -webkit-border-radius: 3px 0 3px 3px;
  border-radius: 3px 0 3px 3px;
  
  margin: 0 2px 0 0;
}

nav li #login-content {
  right: 0;
  
}

/*--------------------*/

#inputs input {
  background: #f1f1f1;
  padding: 6px 5px;
  margin: 0 0 5px 0;
  width: 238px;
  border: 1px solid #ccc;
  -moz-border-radius: 3px;
  -webkit-border-radius: 3px;
  border-radius: 3px;
  -moz-box-shadow: 0 1px 1px #ccc inset;
  -webkit-box-shadow: 0 1px 1px #ccc inset;
  box-shadow: 0 1px 1px #ccc inset;
}

#inputs input:focus {
  background-color: #fff;
  border-color: #e8c291;
  outline: none;
  -moz-box-shadow: 0 0 0 1px #e8c291 inset;
  -webkit-box-shadow: 0 0 0 1px #e8c291 inset;
  box-shadow: 0 0 0 1px #e8c291 inset;
}

/*--------------------*/

#login #actions {
  margin: 10px 0 0 0;
}

#signup{
margin: 0 11px 0 0;
position: relative;

}
 
#login #submit::-moz-focus-inner {
  border: none;
}

#login label {
  float: right;
  line-height: 30px;
}

#login label input {
  position: relative;
  top: 2px;
  right: 2px;
}

.loginright { margin: -5px 0 0 0; }

.fb { margin: -5px 0 0; float: left; }
.fb a:hover { background: transparent; }
.fb a { padding: 0; }
.fb_text { float: left;text-align: right; color: white; text-transform: none; }
.separator { width: 10px; float: left; padding: 0 12px 0 6px; color: white;  }
#inputs{border:1px solid #898282;padding:8px 8px;}
</style>
<script type="text/javascript">
  var _kmq = _kmq || [];
  var _kmk = _kmk || '769d0e9319cc48213de236a183b4f5d6c02a4843';
  function _kms(u){
    setTimeout(function(){
      var d = document, f = d.getElementsByTagName('script')[0],
      s = d.createElement('script');
      s.type = 'text/javascript'; s.async = true; s.src = u;
      f.parentNode.insertBefore(s, f);
    }, 1);
  }
  _kms('//i.kissmetrics.com/i.js');
  _kms('//doug1izaerwt3.cloudfront.net/' + _kmk + '.1.js');
</script>




<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#login-trigger').click(function(){
		 
		$(this).next('#login-content').slideToggle();
		$(this).toggleClass('active');					
		
		if ($(this).hasClass('active')) $(this).find('span').html('&#x25B2;')
			else $(this).find('span').html('&#x25BC;')
		})
});
</script>

<?php

$current[0]="";
$current[1]="";
$current[2]="";
$current[3]="";
$current[4]="";
$current[5]="";
$current[6]="";
$current[7]="";
$current[8]="";
$current[9]="";
$current[10]="";
$current[11] = "";
$current[12] = "";
$current[13] = "";
$current[14] = "";
$current[15] = "";
$current[16] = "";
switch($act)
{	
	case "userslist":
		$current[1]="class='current'";
		break;	
	case "addsusers":		
		$current[2]="class='current'";
		break;
	case "addteacher":
		$current[3] = "class='current'";
		break;
	case "bulk_teacher":
		$current[4] = "class='current'";
		break;
	case "bulk_section":
		$current[5] = "class='current'";	
		break;
	case "assign_class":
		$current[6] = "class='current'";	
		break;	
	case "user_limit":
		$current[7] = "class='current'";	
		break;	
	case "sectioncreation":
		$current[8] = "class='current'";	
		break;	

	case "addstudent":
		$current[9] = "class='current'";	
		break;	
	case "editprofilestudent":
		$current[10] = "class='current'";	
		break;
			
}

?>
	<!-- Codrops top bar --> 

 <header>
				<div class="container" >
				<div class="span2">
					<h1 class="logo">
						<a href="index.html">
							<img alt="Skill Angels" src="images/logo.png" ></img>
						</a>
					</h1>
				</div>
				<div class="span9">
					
					<div align="center"><strong><span class="ed6">EdSix Brain Lab<sup>TM</sup> Pvt Ltd</span><br/>Incubated by IIT Madras' RTBI<br/>
					Supported by IIM Ahmedabad's CIIE and Village Capital, U.S.A</strong>

					</div>
					<?php if(!empty($adminid)) { ?>
					<nav>
						<ul class="nav nav-pills nav-main" id="mainMenu">
							 <li class="dropdown">
								<a class="dropdown-toggle" href="javascript:void(0);">
									<strong>Masters</strong>
									<i class="icon-angle-down"></i>
								</a>
								<ul class="dropdown-menu">
									<li><a href="index.php?act=addteacher">Add Teacher</a></li>
									<li><a href="index.php?act=assign_class">Assign Class</a></li>
									<li><a href="index.php?act=sectioncreation">Add Section</a></li>
									<li><a href="index.php?act=addstudent">Add Student</a></li>
									<li><a href="index.php?act=user_limit">User Limit</a></li>
								</ul>
							</li>  
							<li class="dropdown">
								<a class="dropdown-toggle" href="javascript:void(0);">
									<strong>Reports</strong>
									<i class="icon-angle-down"></i>
								</a>
								<ul class="dropdown-menu">
									<li><a href="index.php?act=student_performance_report">Student Performance</a></li>
									<li><a href="index.php?act=grade_report">Gradewise Report</a></li>
									<li><a href="index.php?act=skilltoppers">Skill Toppers</a></li>
									<li><a href="index.php?act=overalltoppers">Overall Toppers</a></li>
									<li><a href="index.php?act=datewise">Datewise Report</a></li>
									<li><a href="index.php?act=improvement">Improvement Decline</a></li>
								</ul>
							</li>
							<li><a href="index.php?act=logout">Logout</a></li>
						</ul>
					</nav>
					<?php } ?>
					</div>
				</div>
			</header>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="vendor/jquery.js"><\/script>')</script>
		<script src="vendor/jquery.easing.js"></script>
		<script src="vendor/jquery.cookie.js"></script>
		<!-- <script src="master/style-switcher/style.switcher.js"></script> -->
		<script src="vendor/bootstrap.js"></script>
		<script src="vendor/selectnav.js"></script>
		<script src="vendor/twitterjs/twitter.js"></script>
		<script src="vendor/flexslider/jquery.flexslider.js"></script>
		<script src="vendor/jflickrfeed/jflickrfeed.js"></script>
		<script src="vendor/fancybox/jquery.fancybox.js"></script>
		<script src="vendor/jquery.validate.js"></script>

		<script src="newjs/plugins.js"></script>

		<!-- Page Scripts -->

		<!-- Theme Initializer -->
		<script src="newjs/theme.js"></script>

		<!-- Custom JS -->
		<script src="newjs/custom.js"></script>
