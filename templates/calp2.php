<?php
session_start();
//This gets today's date

$date = time();

//This puts the day, month, and year in seperate variables

$day = date('d', $date) ;
$month = date('m', $date) ;
$year = date('Y', $date);


/*$day = '01';
$month = '04';
$year = '2014';
*/

//get Date between two date(s)
$dateMonthYearArr = array();
//$dateMonthYearArr = FdatesBetweenTwoDates(FtoSQLDate('01/05/2014'), FtoSQLDate('31/05/2014'));



$event_array[][] = "";
$user_id = $uid;

/*foreach($dateMonthYearArr as $k=>$val) {
$event_array[$val][$user_id] = "0";
}*/

$start_date = date('Y-m-01');
$last_date = date('Y-m-t');

$maxDays=date('t');

function range_date($first, $last, $user) {
  $arr = array();
  $now = strtotime($first);
  $last = strtotime($last);

  while($now < $last ) {
    $arr[date('d/m/Y', $now)][$user] = "1";
    $now = strtotime('+1 day', $now);
  }
 
  return $arr;
}

$event_array = range_date($start_date, date('d-m-Y'), $uid);

$sql = "SELECT id, date_format(creation_date, '%d/%m/%Y') as created_date FROM game_reports WHERE gu_id = '".$uid."' and creation_date between '".$start_date."' and '".$last_date."'";

$mysql_rs = mysql_query($sql);

while($rsData = mysql_fetch_array($mysql_rs)) {		
	$played_date = $rsData['created_date'];	
	$event_array[$played_date][$uid] = "2";
}

/*$event_array['26/05/2014'][$user_id] = "1";
$event_array['27/05/2014'][$user_id] = "2";*/

/*echo "<pre>";
print_r($event_array);*/



//Here we generate the first day of the month
$first_day = mktime(0,0,0,$month, 1, $year) ;

//This gets us the month name
$title = date('F', $first_day) ;

//Here we find out what day of the week the first day of the month falls on 
$day_of_week = date('D', $first_day) ; 


 //Once we know what day of the week it falls on, we know how many blank days occure before it. If the first day of the week is a Sunday then it would be zero

 switch($day_of_week){ 

 case "Sun": $blank = 0; break; 

 case "Mon": $blank = 1; break; 

 case "Tue": $blank = 2; break; 

 case "Wed": $blank = 3; break; 

 case "Thu": $blank = 4; break; 

 case "Fri": $blank = 5; break; 

 case "Sat": $blank = 6; break; 

 }



 //We then determine how many days are in the current month

 $days_in_month = cal_days_in_month(0, $month, $year) ; 



  //Here we start building the table heads 

 echo "<table border=1 width='225'>";

 echo "<tr><th colspan=7 align='center'> $title $year </th></tr>";

 echo "<tr><td width=42>S</td><td width=42>M</td><td 
		width=42>T</td><td width=42>W</td><td width=42>T</td><td 
		width=42>F</td><td width=42>S</td></tr>";



//This counts the days in the week, up to 7

 $day_count = 1;



 echo "<tr>";

 //first we take care of those blank days

 while ( $blank > 0 ) 
 { 
	 echo "<td></td>"; 
	 $blank = $blank-1; 
	 $day_count++;
 } 
 
  //sets the first day of the month to 1 

 $day_num = 1;

$curr_day = '';

 //count up the days, untill we've done all of them in the month

 while ( $day_num <= $days_in_month ) 
 { 
    $curr_day = '';
	$val_day = '';
	
    /*if($day == $day_num) { $curr_day = "style='background-color:#FFFF00';"; 
	} */
	
	if(strlen($day_num) <= 1)
	{
	  $day_num = "0".$day_num;
	}
	
	$val_day =  $day_num."/".$month."/".$year;
	
	/*echo $val_day;
	echo "-----";
	echo $day_num."/".$month."/".$year;
	echo "<br/>";*/
	
	if($day_num == $day)
	{
	   $curr_day = "style='background:url(images/line.png) no-repeat right; background-size: 16px 16px;'";
	}
	if($event_array[$val_day][$uid]== 1)
	{ 
	   $curr_day = "style='background:url(images/cross.png) no-repeat right; background-size: 16px 16px;'";
	}
	if($event_array[$val_day][$uid]== 2)
	{
	   $curr_day = "style='background:url(images/tick.png) no-repeat right; background-size: 16px 16px;'";
	}
	
	
	echo "<td ".$curr_day."> $day_num </td>"; 
	$day_num++; 
	$day_count++;
	
	//Make sure we start a new row every week
	if ($day_count > 7)
	{
		echo "</tr><tr>";
		$day_count = 1;
	}
  } 
  
  echo "</table>";
  
  echo "<table border=1 width='225'>
  <tr><td width=10%><img src='images/line.png' border=0 width=16 height=16 /></td><td width=90%>Current Day</td></tr>
  <tr><td width=10%><img src='images/cross.png' border=0 width=16 height=16 /></td><td width=90%>Not Attended</td></tr>
  <tr><td width=10%><img src='images/tick.png' border=0 width=16 height=16 /></td><td width=90%>Attended</td></tr></table>";
  
  
  
  function FdatesBetweenTwoDates($fromDate, $toDate) {
	$dateMonthYearArr = array();
	$fromDateTS = strtotime($fromDate);
	$toDateTS = strtotime($toDate);
	for($currentDateTS = $fromDateTS; $currentDateTS <= $toDateTS; $currentDateTS += (60 * 60 * 24)) {
		// use date() and $currentDateTS to format the dates in between
		$currentDateStr = date('d/m/Y',$currentDateTS);
		$dateMonthYearArr[] = $currentDateStr;
	}
	return $dateMonthYearArr;
}
  
  function FtoSQLDate($date) {
	if($date != '')	{
		$year = explode('/', $date, 3);
		if((strlen($year[0]) == '2' || strlen($year[0]) == '1')&&(strlen($year[1]) == '2')&&(strlen($year[2]) == '4'))	{
			$sdate = $year[2]."-".$year[1]."-".$year[0];
			$datetime = new DateTime($sdate);
			$Cdate = $datetime->format('Y-m-d');
		}	
		else $Cdate = '';
	}
	else $Cdate = '';
	return $Cdate;
}
  
  ?>
