<?php 
if(empty($adminid)){
	echo "<script>window.location='index.php?act=login'</script>";
}
include_once("skills_overalltopper_bl.php");

?>
	 <script type="text/javascript" src="js/bootstrap-multiselect.js"></script>
 <?php $AverageScore=$skillQuotient_value; ?>

<div class="pageHomePager Dashboardhide mygameshide myreporthide myprofilehide">
   <div class="row">
  <div class="col-lg-12">
                    <h1 class="page-header">Skills - Overall Toppers</h1>
                </div>
			</div>	
			
			
			<div class="row">
      			<div class="col-lg-12 landingContainer">
				<div class="row">
				
				 </div>
        			<table id="assementTable" class="table table-striped table-bordered table-hover table-condensed">
    <thead>
      <tr>
        <th>S.No.</th>
		<th>Skill</th>
        <th>Grade</th>
		
        
      </tr>
    </thead>
    <tbody>
	<?php 
	
	$ini=0; 
	foreach($reportdatas as $r1){
	$ini++;
	
	?>	
      <tr>
        <td><?php echo $ini; ?></td>
        <td><?php echo $r1['skillname']; ?></td>
        <td><?php echo $r1['gradename']." ".$r1['section']; ?></td>
      </tr>
	<?php } ?>
      
	  
    </tbody>
  </table>
                    
      			</div>
				
				
 			</div>
				

   
   
</div>


 

<script>

function ajaxsectionload(gradeID,selectID)
{
$.ajax({
		url: "templates/ajax_sectionbygrade.php", 
		data:{gradeid:gradeID,schoolid:'<?php echo $_SESSION['schoolid'];?>',defaultselect:selectID},
		success: function(result)
		{
			$("#ddlSection").html(result);
		}
	});
	}

$(document).ready(function() {
	
    $('#assementTable').DataTable( );
} );
</script>
 