<?php 
if(empty($adminid)){
	echo "<script>window.location='index.php?act=login'</script>";
}
?>
<header>
<div class="intro-head">Student Performance Report</div>
<div class="intro">
<div class="support-note"><!-- let's check browser support with modernizr -->
<!--span class="no-cssanimations">CSS animations are not supported in your browser</span-->
<!--  ><span class="no-csstransforms">CSS transforms are not supported in your browser</span>
<!--span class="no-csstransforms3d">CSS 3D transforms are not supported in your browser</span-->
<!--  <span class="no-csstransitions">CSS transitions are not supported in your browser</span>
<span class="note-ie">Sorry, only modern browsers.</span>-->
<div id="main">


<style type="text/css" title="currentStyle">
@import "datatable/media/css/demo_page.css";
@import "datatable/media/css/demo_table_jui.css";
@import "css/morris.css";
@import "datatable/examples/examples_support/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

	<link class="include" rel="stylesheet" type="text/css" href="../css/jquery.jqplot.min.css" />
    <link type="text/css" rel="stylesheet" href="../syntaxhighlighter/styles/shCoreDefault.min.css" />
    <link type="text/css" rel="stylesheet" href="../syntaxhighlighter/styles/shThemejqPlot.min.css" />
	<script class="include" type="text/javascript" src="../js/jquery-1.9.0.min.js"></script>
	
	    
<!-- Don't touch this! -->
    <script class="include" type="text/javascript" src="../js/jquery.jqplot.report.js"></script>
    <script type="text/javascript" src="../syntaxhighlighter/scripts/shCore.min.js"></script>
    <script type="text/javascript" src="../syntaxhighlighter/scripts/shBrushJScript.min.js"></script>
    <script type="text/javascript" src="../syntaxhighlighter/scripts/shBrushXml.min.js"></script>
<!-- Additional plugins go here -->
    
   
    <script type="text/javascript" src="../plugins/jqplot.dateAxisRenderer.min.js"></script>
<script type="text/javascript" src="../plugins/jqplot.canvasTextRenderer.min.js"></script>
<script type="text/javascript" src="../plugins/jqplot.canvasAxisTickRenderer.min.js"></script>

  <script class="include" type="text/javascript" src="../plugins/jqplot.barRenderer.min.js"></script>
  <script class="include" type="text/javascript" src="../plugins/jqplot.categoryAxisRenderer.min.js"></script>
  <script class="include" type="text/javascript" src="../plugins/jqplot.pointLabels.min.js"></script>
  <script class="include" type="text/javascript" src="../plugins/jqplot.highlighter.min.js"></script>
<!-- End additional plugins -->
        
<style type="text/css">       
   #chart1 .jqplot-point-label {
  /*border: 1.5px solid #aaaaaa;
  padding: 1px 3px;
  background-color: #eeccdd;*/
  color: #333333;
  font-size:12px;
  
</style>
	
<style type="text/css">
	table tbody td{
		
		
		padding: 5px;
		margin: 4px;
	}
	.monthscroll, .skillscroll {
		height: 150px;
		width: 250px;
		overflow-y: scroll;
	}
	.submitlabel {
		text-align: center;
		margin-top: 5px;
	}
</style>
<script type="text/javascript" language="javascript" src="datatable/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" language="javascript" src="js/raphael-min.js"></script>
<script type="text/javascript" language="javascript" src="js/morris.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		oTable = $('.example ').dataTable({
			"bJQueryUI": true,
			"sPaginationType": "full_numbers"
		});		
	});
</script>
<?php 

function listAllYearMonth($startDate,$endDate){
    $startDate = strtotime(str_replace("-", "/", $startDate));
    $endDate = strtotime(str_replace("-", "/", $endDate));
    $currentDate = $endDate;
    $result = array();
    while ($currentDate >= $startDate) {
        $result[] = date('Y/m',$currentDate);
        $currentDate = strtotime( date('Y/m/01/',$currentDate).' -1 month');
    }
    return $result;
}

function getAcademicYear(){
    $accYear = mysql_query("SELECT startdate,enddate FROM academic_year limit 0,1");
    while($acc = mysql_fetch_array($accYear)){
        $return = array("startDate" => $acc['startdate'],
                        "endDate" => $acc['enddate']);
    }
    return $return;
}

function getAcademicData($vMonth){
    $accDates = getAcademicYear();
    $acdMonths = listAllYearMonth($accDates['startDate'],$accDates['endDate']);
    foreach ($acdMonths as $acdMonths) {
        list($year,$month) = explode("/", $acdMonths);
        if($month == $vMonth){
            return $year;
        }
    }
}

	$school_id = $_SESSION['schoolid'];
	$skillsQuery = "select name,id from category_skills where category_id = 1";
	$objSkills = mysql_query($skillsQuery,$con);
	$months = array(
        '1' => 'Jan',
        '2' => 'Feb',
        '3' => 'Mar',
        '4' => 'Apr',
        '5' => 'May',
        '6' => 'Jun',
        '7' => 'Jul',
        '8' => 'Aug',
        '9' => 'Sep',
        '10' => 'Oct',
        '11' => 'Nov',
        '12' => 'Dec'
    );
    if(isset($_POST['studentSubmit'])) {
    	$success = true;
    	$params = $_POST;
    	if(!empty($params['username'])) {
    		$uid = toGetUserId($params['username']);
    		if(empty($uid)) {
    			$success = false;
    		}
    	}
    	else{
    		$success = false;
    	}
    	$skillType = !empty( $params['skill']) ? join(',', $params['skill']) : '';
    	$skillsTypeQuery = "select name,id from category_skills where id in(".$skillType.") order by id";
		$objSkillsList= mysql_query($skillsTypeQuery);
    }
    function toGetUserId($username) {
    	$uid = '';
		$school_id = $_SESSION['schoolid'];
    	$usersQuery = "select id from users where username='".trim($username)."' and sid = '".$school_id."'";
    	$objusersQuery= mysql_query($usersQuery);
    	while($objresult = mysql_fetch_assoc($objusersQuery)){	
    		$uid =  $objresult['id'];
		}
		return $uid;
    }

    function calculateScore($uid, $month, $year, $skills, $skillArray) {
		$school_id = $_SESSION['schoolid'];
    	$total = 0;
    	$attempt = 0;
    	$studentReport = array();
    	$skills = join(',', $skills);
    	$date = date('Y-m', strtotime($year.'-'.$month));
    	$Query = 'SELECT sum(gr.game_score) as score,count(gr.id) as count,gr.gs_id as skid,sk.name as skill FROM game_reports gr join category_skills sk join users u WHERE gr.gu_id = u.id and u.sid = '.$school_id.' and gr.gu_id='.$uid.' and sk.id = gr.gs_id and gr.gs_id in ('.$skills.') and DATE_FORMAT(lastupdate, "%Y-%m")=\''.$date.'\' group by gr.gs_id order by gr.gs_id';
		//echo "$Query";
    	$query = mysql_query($Query);
    	while($row = mysql_fetch_assoc($query)){
    		$studentReport[$row['skill']]= $row;
		}
		if(!empty($skillArray)) {
			foreach ($skillArray as $key => $value) {
				if(!array_key_exists($value, $studentReport)) {					
					$studentReport[$value]['skid'] = $key;
					$studentReport[$value]['skill'] = $value;
					$studentReport[$value]['score'] = 0;
				}
			}
		}
		//ksort($studentReport);
		foreach ($studentReport as $key => $value) {
			$total = $total + $value['score'];
			$attempt = $attempt + $value['count'];
		}

		$studentReport['total'] = $total;
		$studentReport['attempt'] = $attempt;
		return $studentReport;	  	
    }
?>
<form action="" class="cmxform" method="POST" id="editteacher34" accept-charset="utf-8">
	<table align="center" width="70%">
		<tr>
			<td>
				<div class="label">
					<div class="firstlabel"><label class="fields" for="username">Username </label></div>
					<div class="secondinput">
						<input type="text" value="<?php echo $_POST['username']?>" name="username" id="username" />
					</div>
				</div>
			</td>
			<td>&nbsp;</td>
			<td><div class="months"><label class="fields" for="lname">Months </label></div></td>
            <td>
                <div class="month_details">
					<?php 
						foreach ($months as $key => $month) { 
						$checked = (in_array($key, $_POST['month'])) ? "checked" : "";
					?>
							<div><input type="checkbox" name="month[]" <?php echo $checked;?> value="<?php echo $key;?>" class="month" /><?php echo $month;?></div>
				    <?php } ?>
                </div>
            </td>
            <td>&nbsp;</td>
            <td><div class="skills"><label class="fields" for="lname">Skills </label></div></td>
            <td>
                <div class="skills_details">
                    <?php while($objresult = mysql_fetch_object($objSkills)){ 
                    	$checked = (in_array($objresult->id, $_POST['skill'])) ? "checked" : "";
                    ?>	
							<div><input type="checkbox" name="skill[]" <?php echo $checked;?> value="<?php echo $objresult->id;?>" class="skill" /><?php echo $objresult->name;?></div>
					<?php } ?>
                </div>

            </td>

			
		</tr>
	</table>	
	<div class="submitlabel"><input type="submit" class="submitbutton" id="editsubmit" name="studentSubmit" value="Search"></div>
</form>
<?php if(isset($_POST['studentSubmit'])) {
$skillSetsLabel='';
		if($success) {
		 ?>
		
		<table class="display example" id="example">
			<thead>
				<tr>
					<th>Month</th>
					<?php 
						$skillArray = array();
						while($objresult = mysql_fetch_object($objSkillsList)){
							$skillArray[$objresult->id] =  $objresult->name;
						}
						$skill_arr=array();
						$skillColor_arr=array();
						if(!empty($skillArray)) {
							foreach ($skillArray as $key => $skill) {
								$skill_arr[]=$skill;
								switch ($skill) {
								  case "Memory":
									$skillColor_arr[]='#de2300';
									break;
								  case "Visual Processing":
									$skillColor_arr[]='#983818';
									break;
								  case "Problem Solving":
									$skillColor_arr[]='#178a1b';
									break;
								  case "Focus And Attention":
									$skillColor_arr[]='#ffac0f';
									break;
								  case "Linguistics":
									$skillColor_arr[]='#008c92';
									break;
								}
							}
						}	
						$skillSetsLabel='';
						for($inc=0,$cnt=count($skillColor_arr);$inc<$cnt;$inc++) {
						 $skillSetsLabel=$skillSetsLabel."{label:'".$skill_arr[$inc]."',color:'".$skillColor_arr[$inc]."'},";
						}
						if($skillSetsLabel!='')
						{
						 $skillSetsLabel=substr($skillSetsLabel,0,strlen($skillSetsLabel)-1);
						}
						
						if($skillSets!=''){ $skillSets='"'.$skillSets.'"'; }
						$skillColors = implode('","',$skillColor_arr);
						if($skillColors!=''){ $skillColors='"'.$skillColors.'"'; }
					?>
					<?php if(!empty($skillArray)) {
							foreach ($skillArray as $key => $skill) { ?>
								<th><?php echo $skill;?></th>
						<?php }						
					} ?>
					
					<th>Total</th>
					<th>BSPI</th>
				</tr>
			</thead>
			<tbody>
				<?php 
					$grapKey = '';
					$jsonArray = array();
					$tot_resVal=array();
					foreach ($skillArray as $key => $value) {
						$grapKey .= "'".$value."',";
					}
					$monthNames='';
					if(!empty($params['month'])) { 
						foreach ($params['month'] as $key => $month) {
                            $year = getAcademicData($month);
							$data = calculateScore($uid,$month,$year,$params['skill'],$skillArray);
							$total = $data['total'];
							$attempt = $data['attempt'];
							unset($data['total']);
							unset($data['attempt']);
							 ?>
							<tr>
								
								<?php 
									 if(!empty($data)) {
										$monthNames .= "'".$months[$month]."',"; ?>
										<td><?php echo $months[$month];?></td>
										<?php
										$i =1;
										$looptotal = 0;
										foreach ($data as $key => $value) {
												$jsonArray[$months[$month]][$key] = round($value['score']/$value['count']);
												$looptotal = $looptotal + round($value['score']/$value['count']);
											$tot_resVal[$i][]=round($value['score']/$value['count']);
											?>
											<td>
												<?php echo round($value['score']/$value['count']);?>					
											</td>	
										<?php $i++;} ?>
									<td>
										<?php echo $looptotal;?>
									</td>
									<td>
									<?php 
										$attempt = ($attempt == 0) ? 1 : $attempt;
										echo round($total/$attempt);?>
									</td>
								<?php } ?>
							</tr>

						<?php } ?>



				<?php 
				} ?>
		<script type="text/javascript">
$(document).ready(function(){  
	var tot_resVal = new Array();
	<?php	
	$i=0;
	foreach ($tot_resVal as $key => $value) {
	$resval=implode(",",$value);
	?>
	tot_resVal[<?php echo $i; ?>]=[<?php echo $resval;?>];
	<?php
	$i++;	
	}
	?>
	
    // Can specify a custom tick Array.
    // Ticks should match up one for each y value (category) in the series.
    var ticks = [<?php echo $monthNames;?>];
     
    var plot1 = $.jqplot('barchart', tot_resVal, {
        // The "seriesDefaults" option is an options object that will
        // be applied to all series in the chart.
        seriesDefaults:{
            renderer:$.jqplot.BarRenderer,
	pointLabels: { show: true }
            //rendererOptions: {fillToZero: true}
        },
        // Custom labels for the series are specified with the "label"
        // option on the series option.  Here a series option object
        // is specified for each series.
        series:[
            <?php echo $skillSetsLabel; ?>
        ],
        // Show the legend and put it outside the grid, but inside the
        // plot container, shrinking the grid to accomodate the legend.
        // A value of "outside" would not shrink the grid and allow
        // the legend to overflow the container.
        legend: {
            show: true,
            placement: 'outsideGrid'
        },
        axes: {
            // Use a category axis on the x axis and use our custom ticks.
            xaxis: {
                renderer: $.jqplot.CategoryAxisRenderer,
                ticks: ticks
            },
            // Pad the y axis just a little so bars can get close to, but
            // not touch, the grid boundaries.  1.2 is the default padding.
            yaxis: {
                numberTicks: 12,
                tickOptions: {formatString: '%d'}
            }
        }
    });
});
		</script>
		
			</tbody>
		</table>
<table><tr><td><div style="width:940px;float:left;margin-left:40px" id='barchart'></div></td></tr>		
		</table>
		
	<?php } else{ ?>
		<p><b>Student could not be found</b></p>
	<?php } } ?>	
	
</div>
</div>
</div>
</header>
