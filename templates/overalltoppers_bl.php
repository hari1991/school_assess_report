<?php 
if(empty($adminid)){
	echo "<script>window.location='index.php?act=login'</script>";
}
include_once("overalltoppers_bl.php");

?>

<script type="text/javascript" src="js/bootstrap-2.3.2.min.js"></script>
 <script type="text/javascript" src="js/bootstrap-multiselect.js"></script>

<div class="pageHomePager Dashboardhide mygameshide myreporthide myprofilehide">
   <div class="row">
  <div class="col-lg-12">
                    <h1 class="page-header">Overall Toppers</h1>
                </div>
			</div>	
			<br/>
			

   

   <div class="row">
      			<div class="col-lg-12">
				<form name="frmOverallToppers" id="frmOverallToppers" method="post">
				<div class="panel panel-default">
            <div class="panel-body reportChartContainer">
			
			<div class="col-lg-4">
			<div class="row">
				<label class="col-lg-6">Grade</label>
        <select name="chkGrade" id="chkveg" multiple="multiple">
			<?php foreach($GradeList as $grades) {
				$gradinid='';
				$selected='';
				if($ddlGradeType==$grades['id']){$selected="selected='selected'";$gradinid=$grades['id'];}
				echo "<option id='".$grades['id']."' ".$selected." value='".$grades['id']."'>".trim(str_replace("Grade",'',$grades['classname']))."</option>";
			} ?>
			            </select>
			            <input type="hidden" value="" name="hdnGradeID" id="hdnGradeID" />
			            	<script type="text/javascript">
			            	    $(function() {
			            	        $('#chkveg').multiselect({
			            	            includeSelectAllOption: true,numberDisplayed: 1,nonSelectedText: 'Select',buttonClass: 'form-control col-lg-6',
										onChange: function(element, checked) {
        var brands = $('#chkveg option:selected');
        var selected = [];
        $(brands).each(function(index, brand){
            selected.push([$(this).val()]);
        });
		$("#hdnGradeID").val(selected);
		
										}
			            	        });
									
			            	        $('#btnget').click(function() {
			            	       // alert($('#chkveg').val());
			            	        })
			            	    });
			</script>
			<script type="text/javascript">
			    $(document).ready(function() {
			    $('#chkveg').multiselect();
				<?php if(isset($_POST['btnSearch'])) { ?>
				var data="<?php echo $hdnGradeID; ?>";
				var dataarray=data.split(",");
				$("#chkveg").val(dataarray);
				$("#chkveg").multiselect("refresh");
				<?php } ?>
			    });
</script>
				
			</div>
			
				
				</div>
				
				<div class="col-lg-4">
			<div class="row">
			<label class="col-lg-6">Month</label>
			 <select id="chkveg1" multiple="multiple">
			 <?php $ini=0; foreach($academicMonths as $am){
				$ini++;
				
				echo '<option value="'.$am['monthNumber'].'">'.$am['monthName'].'</option>';
			} ?>
			
			
			            </select>
			         <input type="hidden" value="" name="hdnMonthID" id="hdnMonthID" />    
			            	<script type="text/javascript">
			            	    $(function() {
			            	        $('#chkveg1').multiselect({
			            	            includeSelectAllOption: true,numberDisplayed: 1,nonSelectedText: 'Select',buttonClass: 'form-control col-lg-6',
										onChange: function(element, checked) {
        var brands = $('#chkveg1 option:selected');
        var selected = [];
        $(brands).each(function(index, brand){
            selected.push([$(this).val()]);
			//alert(selected);
        });
			$("#hdnMonthID").val(selected);
			}
			            	        });
			            	        $('#btnget').click(function() {
			            	        alert($('#chkveg1').val());
			            	        })
			            	    });
			</script>
			<script type="text/javascript">
			    $(document).ready(function() {
			    $('#chkveg1').multiselect();
				<?php if(isset($_POST['btnSearch'])) { ?>
				var data1="<?php echo $hdnMonthID; ?>";
				var dataarray1=data1.split(",");
				$("#chkveg1").val(dataarray1);
				$("#chkveg1").multiselect("refresh");
				<?php } ?>
			    });
</script>
				
			</div>
				</div>
				<div class="col-lg-4">
				<div class="row">
				<span class="col-lg-4"></span>
				<input type="submit" value="Search" name="btnSearch" id="btnSearch" class="btn btn-success col-lg-4" />
				<span class="col-lg-2"></span>
			</div>
			<br/>
			<div class="row">
				<span class="col-lg-4"></span>
				<input type="button" value="Reset" onclick="javascript:window.location.href=window.location.href;" name="btnReset" id="btnReset" class="btn btn-warning col-lg-4" />
				<span class="col-lg-2"></span>
			</div>
				</div>
				</div>
				</div>
				</form>
				</div>
				</div>
				<br/>
				
              <div class="row" <?php if(isset($_POST['btnSearch'])) {  } else {?> style="display:none;" <?php } ?>>
      			<div class="col-lg-12 landingContainer">
				<div class="row">
				 <div class="alert alert-warning" role="alert"> <strong><?php echo $frontGradeName.' - '.$ddlSection; ?> </strong> </div>
				 </div>
        			<table id="assementTable" class="table table-striped table-bordered table-hover table-condensed">
    <thead>
      <tr>
        <th>S.No.</th>
        <th>Name</th>
        <th>Class</th>
        <?php if(in_array(59, $hdnSkillID_arr)) {?><th>M</th><?php } ?>
        <?php if(in_array(60, $hdnSkillID_arr)) {?><th>VP</th><?php } ?>
        <?php if(in_array(61, $hdnSkillID_arr)) {?><th>FA</th><?php } ?>
        <?php if(in_array(62, $hdnSkillID_arr)) {?><th>PS</th><?php } ?>
        <?php if(in_array(63, $hdnSkillID_arr)) {?><th>L</th><?php } ?>
        <?php if($hdnSkillID_count>=5){ ?><th>BSPI</th><?php } ?>
      </tr>
    </thead>
    <tbody>
	<?php 
	$exporttext='';
	$exporttext.='S.No.,Name,Class,Memory,Visual Processing,Focus and Attention,Problem Solving, Linguistics,BSPI'.""."PHP_EOL";
	$ini=0; 
	foreach($GradeWiseReport as $gradewise){
	$ini++;
	$exporttext.=$ini.','.$gradewise['fname']." ".$gradewise['lname'].','.$frontGradeName.'-'.$ddlSection.','.$gradewise['memory'].','.$gradewise['visualprocessing'].','.$gradewise['focusattention'].','.$gradewise['problemsolving'].','.$gradewise['linguistics'].','.$gradewise['bspi'].""."PHP_EOL";
	?>	
      <tr>
        <td><?php echo $ini; ?></td>
        <td><?php echo $gradewise['fname']." ".$gradewise['lname']; ?></td>
        <td><?php echo $frontGradeName.'-'.$ddlSection; ?></td>
		<?php if(in_array(59, $hdnSkillID_arr)) {?><td><?php echo $gradewise['memory']; ?></td><?php } ?>
        <?php if(in_array(60, $hdnSkillID_arr)) {?><td><?php echo $gradewise['visualprocessing']; ?></td><?php } ?>
        <?php if(in_array(61, $hdnSkillID_arr)) {?><td><?php echo $gradewise['focusattention']; ?></td><?php } ?>
        <?php if(in_array(62, $hdnSkillID_arr)) {?><td><?php echo $gradewise['problemsolving']; ?></td><?php } ?>
        <?php if(in_array(63, $hdnSkillID_arr)) {?><td><?php echo $gradewise['linguistics']; ?></td><?php } ?>
		 <?php if($hdnSkillID_count>=5){ ?><td><?php echo ($gradewise['bspi']); ?></td><?php } ?>
      </tr>
	<?php } ?>
      
	  
    </tbody>
  </table>
                    
      			</div>
				
				
 			</div>
			<?php $path='reports/'.$_SESSION['schoolid'].'/'.$frontGradeName.'-'.$ddlSection.'_GradeWiseReport_'.date('Ymdhis').'.xlsx'; ?>
<div class="row" <?php if(isset($_POST['btnSearch'])) {  } else {?> style="display:none;" <?php } ?>>
<input type="button" class="btn btn-success" id="btnExcelExport" name="btnExcelExport" value="Export to excel">
</div>
</div>


<script>
$("#btnExcelExport").click(function(){
		$.ajax({
		url: "templates/ajax_gradewise_excelexport.php", 
		data:{ddlAssessmentType:'<?php echo $ddlAssessmentType; ?>',ddlSection:'<?php echo $ddlSection; ?>',ddlGradeType:'<?php echo $ddlGradeType; ?>',hdnSkillID:'<?php echo $hdnSkillID; ?>',hdnMonthID:'<?php echo $hdnMonthID; ?>',content:'<?php echo $exporttext; ?>',path:'<?php echo $path; ?>',schoolid:'<?php echo $_SESSION['schoolid'];?>'},
		success: function(result)
		{
			if('<?php echo "templates/".$path;?>'==$.trim(result))
			window.location=result;
		}
	});
		});
function ajaxsectionload(gradeID,selectID)
{
$.ajax({
		url: "templates/ajax_sectionbygrade.php", 
		data:{gradeid:gradeID,schoolid:'<?php echo $_SESSION['schoolid'];?>',defaultselect:selectID},
		success: function(result)
		{
			$("#ddlSection").html(result);
		}
	});
	}
$("#ddlGradeType").change(function(){
ajaxsectionload($('option:selected', this).attr('id'),'')	
});
$(document).ready(function() {
	<?php if(isset($_POST['btnSearch'])) { ?>
		ajaxsectionload($('option:selected', $("#ddlGradeType")).attr('id'),'<?php echo $ddlSection; ?>')	;
		
	<?php } ?>
    $('#assementTable').DataTable( );
} );
</script>
 
 