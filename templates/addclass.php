<?php
if(empty($adminid)){
	echo "<script>window.location='index.php?act=login'</script>";
}
?>

<link rel="stylesheet" href="css/jquery-ui.css" />

<style type="text/css" title="currentStyle">
			@import "datatable/media/css/demo_page.css";
			@import "datatable/media/css/demo_table_jui.css";
			@import "datatable/examples/examples_support/themes/smoothness/jquery-ui-1.8.4.custom.css";
		</style>
				<script type="text/javascript" language="javascript" src="js/jquery-1.8.2.js"></script>
		<script type="text/javascript" language="javascript" src="js/jquery.validate.js"></script>
		<script type="text/javascript" language="javascript" src="datatable/media/js/jquery.dataTables.js"></script>
		<script type="text/javascript" src="js/jquery.fancybox.js"></script>
		<link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css" media="screen" />

		<style type="text/css">
#status
{
font-size:11px;
margin-left:10px;
}


#email { float: left; }



label.error { float: none; color: red; padding-left:20px; vertical-align: top; }
p { clear: both; }
.success { font-size: 15px; font-weight: bold; color: green;text-align: center;}
em { font-weight: bold; padding-right: 1em; vertical-align: top; }

.dte_header{
 	border-top-left-radius: 5px;
    border-top-right-radius: 5px;
    background-color: #F3F3F3;
    border-bottom: 1px solid #DDDDDD;
    height: 50px;
    left: 0;
   	top: 0;
    width: 100%;
}
</style>

<script>
$(document).ready(function(){
	
		
	var oTable = $('#classlist').dataTable({
		
		"bJQueryUI": true,
		"sPaginationType": "full_numbers"
	});

	
	$("#addclass").validate({
		 rules: {
			 fname: {
				 remote: {
					 url: "../ajax_data.php",
					 type: "post"
					
					}
				 }
		 },
		
		messages: {
			fname :{
					remote : 'Class name is already taken'
				}
			}
	});
	
    
    
    
});
</script>

<?php

if(isset($_POST['classeditsubmit'])){

$id = $_POST['classid'];
$cname =  $_POST['cname'];
$section = $_POST['section'];

mysql_query("update class set classname = '$cname',section='$section' where id = $classid");
$message = 'Class Edited Successfully';
}

if(isset($_POST['classsubmit'])){
	
	$cname =  $_POST['cname'];
	$section = $_POST['section'];	
	
	$sql = "insert into class (classname,section) values ('$cname','$section')";
	//echo $sql ."<br>";
	
	mysql_query($sql);
	$message = 'Class added successfully';
}
if(isset($_POST['oktodel'])){

	$id = $_POST['adminid'];
	mysql_query("DELETE FROM class WHERE id='$id'");
	$message = 'Class deleted successfully';
}
?>
<?php 
$allclasses = mysql_query("select * from class");
while($allclass = mysql_fetch_array($allclasses)){

?>
<script>
$(document).ready(function(){
	$(".fancybox<?php echo $allclass['id']; ?>").fancybox({
		 beforeShow : function() {
			 var classname =$("#fname<?php echo $allclass['id']; ?>").val();
			$("#editclass<?php echo $allclass['id']; ?>").validate({
				 rules: {
					 fname: {
						 remote: {
							 url: "../ajax_data.php",
							 type: "post",
							 data: {
								 classname: function() {
								 return classname;
								 }
							}
						 }
				 	 }
				},
				messages: {
					fname :{
							remote : 'Class name is already taken'
						}
					}
			});
		},
	});
	 
});
</script>
					<!-- edit form -->
					
					<div id="editform<?php echo $allclass['id']; ?>" class="editform" style="display: none;">
						<div class="edit-head">Edit Record</div>
						<div class="editlist">
							<form action="" class="cmxform" method="POST" id="editclass<?php echo $allclass['id']; ?>" accept-charset="utf-8">
								<input type="hidden" name="classid" value="<?php echo $allclass['id']; ?>">
								<div class="label">
									<div class="firstlabel"><label class="fields" for="cname">Class Name </label><label class="mandatory">*</label></div>
									<div class="secondinput"><input type="text" name="cname" value="<?php echo $allclass['classname']; ?>" title="Please enter class name" class="required" id="cname<?php echo $allclass['id']; ?>">
										<div class="error"></div>
									</div>
								</div>												
								<div class="label">
									<div class="firstlabel"><label class="fields" for="section">Section </label><label class="mandatory">*</label></div>
									<div class="secondinput"><input type="text" name="section" value="<?php echo $allclass['section']; ?>" title="Please enter section" class="required" id="saddress"></div>						
								</div>
								<div class="submitlabel"><input type="submit" class="submitbutton" id="classeditsubmit" name="classeditsubmit" value="Update"></div>
							</form>
						</div>
					</div>
					
					<!-- delete form -->
					<div id="deleteform<?php echo $allclass['id']; ?>" class="deleteform" style="display: none;">
						<form action"" method="post">
							<input type="hidden" name="classid" value="<?php echo $allclass['id']; ?>">
							<div class="edit-head">Are you sure to delete ? </div>
							<div class="editlist">
								<input class="submitbutton" type="submit" name="oktodel" id="oktodel" value="Delete">
								<input class="submitbutton" type="submit" name="canceltodel" id="canceltodel" value="Cancel">
							</div>
						</form>
					</div>
					<?php 
					}
					?>
					
<header>
	<div class="intro-head">Add Class</div>
	<div class="intro cnt_min">
		<div class="support-note">
			<div id="main">				
				<div class="success"><?php echo $message; ?></div>
				<form action="" class="cmxform" method="POST" id="add" accept-charset="utf-8">				
						<div class="reqindicates"><label class="top_mandatory">*</label>&nbsp; Indicates required fields</div>
						<div class="label">
							<div class="firstlabel"><label class="fields" for="cname">Class Name </label><label class="mandatory">*</label></div>
							<div class="secondinput"><input type="text" name="cname" title="Please enter class name" value="" class="required" id="cname"></div>
							<div id="status1"></div>
						</div>
                        <div class="label">
							<div class="firstlabel"><label class="fields" for="section">Section </label></div>
							<div class="secondinput"><input type="text" name="section" title="Please enter section" value="" class="required" id="section"></div>
							<div id="status1"></div>
						</div>
						<div class="submitlabel"><input type="submit" class="submitbutton" id="classsubmit" name="classsubmit" value="Add New Class"></div>
					
				</form>
  			</div>
		</div>
	
	<!-- View  List -->		

		<div class="support-note">
			<div id="main">
		<table cellpadding="0" cellspacing="0" border="0" class="display" id="list" >
			
			<thead>
				<tr>
					<th rowspan="2">Class Name-corrected</th>
					<th rowspan="2">Section</th>					
					<th align="center" colspan="2">Action</th>
				</tr>
				<tr>
					<th align="center">Edited</th>
					<th align="center">Delete</th>
				</tr>
			</thead>
			<tbody>
			
			
				<?php 
				$allclasses = mysql_query("select * from class");
				while($allclass = mysql_fetch_array($allclasses)){
				?>
					
					<?php
					echo "<tr>"; 
					echo "<td>$allclass[classname]</td>";
					echo "<td>$allclass[section]</td>";			
					echo "<td align='center'><a class='fancybox$allclass[id]' href='#editform$allclass[id]'><img src='images/edit.png' title='Edit Record' width='20' alt='Edit Record'></a></td>";
					echo "<td align='center'><a class='fancybox$allclass[id]' href='#deleteform$allclass[id]'><img src='images/delete.png' title='Delete Record' width='17' alt='Delete Record'></a></td>";
					echo "</tr>";
				}
					?>
			
			</tbody>
			</table>
  			</div>
		</div>

</header>