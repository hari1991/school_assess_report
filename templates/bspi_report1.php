<?php 
if(empty($adminid)){
	echo "<script>window.location='index.php?act=login'</script>";
}
include_once("bspi_report1_bl.php");

?>
	 <script type="text/javascript" src="js/bootstrap-multiselect.js"></script>
 <?php $AverageScore=$skillQuotient_value; ?>

<div class="pageHomePager Dashboardhide mygameshide myreporthide myprofilehide">
   <div class="row">
  <div class="col-lg-12">
                    <h1 class="page-header">BSPI Report I</h1>
                </div>
			</div>	
			<div class="row">
      			<div class="col-lg-12">
				<form name="frmStudentReport" id="frmStudentReport" method="post">
				<div class="panel panel-default">
            <div class="panel-body reportChartContainer">
			<div class="col-lg-4">
				<div class="row">
				<label class="col-lg-6">Grade</label>
				<select class="form-control col-lg-6" required="true" style="width:50%" name="ddlGradeType" id="ddlGradeType">
			<option value="">Select</option>
			<?php foreach($GradeList as $grades) {
				$gradinid='';
				$selected='';
				if($ddlGradeType==$grades['id']){$selected="selected='selected'";$gradinid=$grades['id'];}
				echo "<option id='".$grades['id']."' ".$selected." value='".$grades['id']."'>".trim(str_replace("Grade",'',$grades['classname']))."</option>";
			} ?>
			</select>
			</div>
			<br/>
			<div class="row">
			<label class="col-lg-6">Section</label>
				<select class="form-control col-lg-6" required="true" style="width:50%" name="ddlSection" id="ddlSection">
			<option value="">Select</option>
			</select>
			</div>
				</div>
		
			
			<div class="col-lg-4">
			<div class="row">
			<label class="col-lg-6">Month</label>
			 <select id="chkveg1" multiple="multiple">
			 <?php $ini=0; foreach($academicMonths as $am){
				$ini++;
				
				echo '<option value="'.$am['monthNumber'].'">'.$am['monthName'].'</option>';
			} ?>
			
			
			            </select>
			         <input type="hidden" value="" name="hdnMonthID" id="hdnMonthID" />    
			            	<script type="text/javascript">
			            	    $(function() {
			            	        $('#chkveg1').multiselect({
			            	            includeSelectAllOption: true,numberDisplayed: 1,nonSelectedText: 'Select',buttonClass: 'form-control col-lg-6',
										onChange: function(element, checked) {
        var brands = $('#chkveg1 option:selected');
        var selected = [];
        $(brands).each(function(index, brand){
            selected.push([$(this).val()]);
			//alert(selected);
        });
			$("#hdnMonthID").val(selected);
			}
			            	        });
			            	        $('#btnget').click(function() {
			            	        alert($('#chkveg1').val());
			            	        })
			            	    });
			</script>
			<script type="text/javascript">
			    $(document).ready(function() {
			    $('#chkveg1').multiselect();
				<?php if(isset($_POST['btnSearch'])) { ?>
				var data1="<?php echo $hdnMonthID; ?>";
				var dataarray1=data1.split(",");
				$("#chkveg1").val(dataarray1);
				$("#chkveg1").multiselect("refresh");
				<?php } ?>
				
			    });
</script>
				
			</div>
			</div>
			<div class="col-lg-1"></div>
			<div class="col-lg-1">
			<div class="row">
			<input  type="submit" name="btnSearch" class="btn btn-success" id="btnSubmit" value="Submit" />
			</div>
				</div>
				
			<div class="col-lg-1">
			<div class="row">
			<input  type="button" onclick="javascript:window.location.href=window.location.href;" name="btnSubmit" class="btn btn-warning" id="btnSubmit" value="Reset" />
			</div>
				</div>
				
				
				</div>
				</div>
				</form>
				
				</div>
				</div>
				
			
  
   
   <div id="StudentReportResult"  >
       <?php if(isset($_POST['btnSearch'])) { ?>      
			
			<table id="assementTable" class="table table-striped table-bordered table-hover table-condensed">
    <thead>
      <tr>
        <th>S.No.</th>
        <th>Name</th>
        <th>Username</th>
       <?php $ini=0; foreach($req_academicMonths as $am){
				$ini++;
				 
				echo "<th>".$am['monthName']."</th>";
			} ?>
		<th>Average BSPI</th>

      </tr>
    </thead>
    <tbody>
	<?php 
	
	$ini=0; 
	foreach($month_array_chart as $result){
	$ini++;
	
	?>	
      <tr>
        <td><?php echo $ini; ?></td>
        <td><?php echo $result['Name']; ?></td>
        <td><?php echo $result['Username']; ?></td>
		<?php  $inij=0; $inib=0;foreach($req_academicMonths as $am){
			
				if($result[$am['monthNumber']]>0)
				{
					$inij++;
					$inib+=$result[$am['monthNumber']];
				}
				 
				echo "<td>".round($result[$am['monthNumber']],2)."</td>";
			} ?>
			 <td><?php echo round(($inib)/count($req_academicMonths),2); ?></td>
      </tr>
	<?php } ?>
     
	  
    </tbody>
  </table>
			
	   <?php } ?>		
			
</div>
</div>

<script src="js/skillpiecharts.js" type="text/javascript"></script><script type="text/javascript" src="js/charts.widgets.js"></script>

 <script type="text/javascript" src="js/charts.js"></script>
<script type="text/javascript" src="js/charts-more.js"></script>
<script src="http://code.highcharts.com/modules/solid-gauge.src.js"></script>

 

<script>

function ajaxsectionload(gradeID,selectID)
{
$.ajax({
		url: "templates/ajax_sectionbygrade.php", 
		data:{gradeid:gradeID,schoolid:'<?php echo $_SESSION['schoolid'];?>',defaultselect:selectID},
		success: function(result)
		{
			$("#ddlSection").html(result);
		}
	});
	}
$("#ddlGradeType").change(function(){
ajaxsectionload($('option:selected', this).attr('id'),'')	
});
$(document).ready(function() {
	<?php if(isset($_POST['btnSearch'])) { ?>
		ajaxsectionload($('option:selected', $("#ddlGradeType")).attr('id'),'<?php echo $ddlSection; ?>')	;
		
	<?php } ?>
    $('#assementTable').DataTable( );
} );
</script>
 