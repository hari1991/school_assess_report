<?php 
if(empty($adminid)){
	echo "<script>window.location='index.php?act=login'</script>";
}

require_once 'excelreader/reader.php';
date_default_timezone_set('Asia/Calcutta');
require_once 'phpexcel/Classes/PHPExcel.php';
require_once 'phpexcel/Classes/PHPExcel/IOFactory.php';
require_once 'phpexcel/Classes/PHPExcel/Writer/Excel5.php';
//error_reporting(E_ALL);
?>

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.validate.js"></script>
<style type="text/css">
#status
{
font-size:11px;
margin-left:10px;
}
p { clear: both; }
.success { font-size: 15px; font-weight: bold; color: green;text-align: center;}
em { font-weight: bold; padding-right: 1em; vertical-align: top; }
.label { padding: 0 0 15px }
</style>

<?php 

if(isset($_POST['sectionsubmit']))
{
	$data = new Spreadsheet_Excel_Reader();
	$data->setOutputEncoding('CP1251');
	$path = '';
	$path	= $_FILES['uploadesection']['tmp_name'];
	$data->read($path);
	for ($i=2; $i<=$data->sheets[0]['numRows']; $i++)
	{
		$id = $data->sheets[0]['cells'][$i][1];
		$section = $data->sheets[0]['cells'][$i][2];
	
		if($section != ''){
			$sectionssql = "UPDATE sections SET section='$section' WHERE id=$id";
			mysql_query($sectionssql);
			$message = 'Sections Successfully edited';
		}
	}
}
?>
<header>
	<div class="intro-head">Bulk Upload section</div>
	<div class="intro cnt_min">
		<div class="support-note">
			<div id="main">
				<div class="success"><?php if(isset($_GET['msg']) == 1){ echo "Successfully uploaded"; }?></div>
				<form enctype="multipart/form-data" action="templates/phpexcel/Tests/excelsection.php" class="cmxform" method="POST" id="addclasses" accept-charset="utf-8">
                <div class="reqindicates"><label class="top_mandatory">*</label>&nbsp; Indicates required fields</div>
					<table width="520px" class="bulk_classes" border="0" cellspacing="4" cellpadding="6">                    
                        <tr>
							<td width="181px" valign="top"><label class="fields" for="class">Class </label><label class="mandatory">*</label></td>
							<td width="250px" valign="top">
								<select name="class" id="class" class="required" >
									<option value="">--Select Class--</option>
									<?php 
									$class = mysql_query("select id,classname from class");
									while ($classinarr =mysql_fetch_array($class)) {
										
									?>
										<option value="<?php echo $classinarr['id']; ?>"><?php echo $classinarr['classname'];?></option>
									<?php }?>
								</select>
							</td>							
						</tr>				
						<tr class="uploadclasses">
							<td valign="top" >Upload Section</td>
							<td colspan="3" valign="top">
								<div style="width: 150px;"><input type="file" name="uploadsection" size="10" id="uploadsection" value="" class="required"></div>
							</td>
						</tr>						
						<tr>
							<td><input type="hidden" name="schoolid" id="schoolid" value="<?= $_SESSION['schoolid']; ?>" /></td>
							<td colspan="3" align="left"><input class="submitbutton" type="submit" name="sectionsubmit" id="sectionsubmit" value="Bulk Upload Section"></td>
						</tr>
					</table>
					<div class="temp_down"><a href="school_section.xls">Click here to download template</a></div>
				</form>
			</div>
  		</div>
	</div>
</header>
<header>
	<div class="intro-head">Bulk Edit Section</div>
	<div class="intro cnt_min">
		<div class="support-note">
			<div id="main">
				<div class="success"><?php echo $message; ?></div>			
				<form enctype="multipart/form-data" action="" class="cmxform" method="POST" id="editschoolclasses" accept-charset="utf-8">
					<div class="reqindicates"><label class="top_mandatory">*</label>&nbsp; Indicates required fields</div>
					<table class="bulk_classes" border="0" cellspacing="4" cellpadding="4">                    
						<tr>
							<td valign="top">Upload Section</td>
							<td>
								<div style="width: 150px;"><input type="file" size="10" name="uploadesection" id="uploadesection" value="" class="required"></div>
							</td>
						</tr>
						<tr class="uploadclasses">
							<td></td>
							<td align="center"><input  class="submitbutton" type="submit" name="schoolesubmit" id="schoolesubmit" value="Bulk Edit Section"></td>
						</tr>
					</table>
					<div class="temp_down"><a href="edit_section.xls">Click here to download template</a></div>
				</form>
				
  			</div>
  		</div>
	</div>
</header>