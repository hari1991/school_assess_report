 <script type="text/javascript" src="js/charts.js"></script>
  <script type="text/javascript">  $(document).ready(function(){
	  $('#container_averagescore').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        
        xAxis: {
            categories: [
                'I',
                'II',
                'III',
                'IV',
                'V',
                'VI',
                'VII',
                'VIII',
                'IX',
                'X',
                'XI',
                'XII'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,tickInterval: 10,max:100,
            title: {
                text: 'Average Score'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Average Score',
            data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1]

        }]
    });
	 });
	</script>
<div id="container_averagescore">
</div>